/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file integrator_parallel.h
//! This file implements Mc, a subclass of Deterministic that allows "online" accumulation of mean/var for a level.

#include <vector>

#include "integrator_skeleton.h"

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/quantity.h"

#include "../solver/projection_solver.h"
#include "../solver/vortex_solver.h"

#include "../models/model_base.h"

//! Specializes an IntegratorSkeleton in order to run more levels
//! in parallel.
//! In particular the role of this class is to add two new quantities
//! (mean and m2) for the in-place computation of
//! mean and variance: used for large number of samples which could
//! result in huge I/O overhead.
//! TODO: this copy/paste in start has to be condensed.
//! @brief This class improves the simpler Deterministic super by
//! including some "online" mean and variance tools.
class IntegratorParallel : public IntegratorSkeleton {
public:
    //!
    IntegratorParallel(Options * p_opt, ModelBase *p_mod,
                       Attila * p_cm)
        : IntegratorSkeleton(p_opt, p_mod, p_cm) {
        FUNCTIONBEGIN(p_cm_);
    }
    //!
    virtual ~IntegratorParallel() {}
    //! Start the simulations
    //     PetscErrorCode start(void) { ierr = solve(); CHKERRQ(ierr); };

    //! Allocate a mean/var container for the level
    PetscErrorCode level_pre(void);
    //! Normalize/Save/Delete mean/var
    PetscErrorCode level_post(void);
    //! Add solution to mean/var
    PetscErrorCode sim_post(void);
    //! Routine launched before a simulation is completed
    PetscErrorCode solve_pre(void);
    //! Routine launched before a simulation is completed
    PetscErrorCode solve_post(void);

    template <bool scalar>
    PetscErrorCode assemble_quantity(Mat mat_int, Vec smaller,
                                     Vec bigger, Vec assembled,
                                     Vec temp);

    PetscErrorCode send_quantity_to_next(Quantity * assembled_mean,
                                         Quantity * assembled_var,
                                         int frame);

    template <bool scalar>
    PetscErrorCode recv_and_interp(Mat int_mat, Vec recv,
                                   Vec tmp1, Vec tmp2, Vec res,
                                   TAG_COMP tag_comp,
                                   TAG_STAT tag_stat, int frame);

    PetscErrorCode quantity_to_root(Quantity *mean,
                                    Quantity *m2, int frame);

};

//!
//!
template <bool scalar>
PetscErrorCode IntegratorParallel::assemble_quantity(Mat mat_int, Vec smaller,
                                                     Vec bigger, Vec assembled,
                                                     Vec temp) {

    ierr = MatInterpolate(mat_int, smaller, temp); CHKERRQ(ierr);
    ierr = p_dom_->avg<scalar, -1>(temp, assembled); CHKERRQ(ierr);
    //   ierr = p_dom_->refine_to_domain<true>(mean.at(0)->omega,
    //                                         assembled_mean->omega); CHKERRQ(ierr);
    ierr = VecAXPBY(assembled, 1, -1, bigger); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//!
//!
template <bool scalar>
PetscErrorCode IntegratorParallel::recv_and_interp(Mat int_mat, Vec recv,
                                                   Vec tmp1, Vec tmp2, Vec res,
                                                   TAG_COMP tag_comp,
                                                   TAG_STAT tag_stat, int frame) {

    ierr = p_cm_->recv_from_prev<scalar>(recv,
                                         get_inter_level_tag(tag_comp,
                                                             tag_stat, frame)
                                         );
    CHKERRQ(ierr);
    ierr = MatInterpolate(int_mat, recv, tmp1); CHKERRQ(ierr);
    ierr = p_dom_->avg<scalar, -1>(tmp1, tmp2); CHKERRQ(ierr);
    ierr = VecAXPY(res, 1., tmp2); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
