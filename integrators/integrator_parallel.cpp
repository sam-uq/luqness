/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "integrator_parallel.h"

//! Runned before a set simulations is started in the level
//! of refined (runs once per level of refinement).
//! @return Error code
PetscErrorCode IntegratorParallel::level_pre(void) {
    PetscFunctionReturn(ierr);
}

//! Runned when all simulation have finished on the level.
//! If NOT root, then nothing is done.
//! If it is a root (i.e. receives data for the level), then creates
//! an "assembled" container for the received data. Also sends data
//! to the next root in line.
//! @return Error code
PetscErrorCode IntegratorParallel::level_post(void) {
    // FIXME: working only for omega nowadays
    // FIXME: periodic cell-centered interpolation assembled 2 sucks
    FUNCTIONBEGIN(p_cm_);

    p_profiler->tic("level_post_begin");

    if( !p_cm_->is_root ) {
        p_profiler->tic("level_post_end");
        PetscFunctionReturn(ierr);
    }

    Quantity * assembled_mean = NULL;
    Quantity * assembled_var = NULL;

    OUTPUT(p_cm_, INTRADM,
           "Ready for inter-level communication, standing by...\n", "");

    p_cm_->generate_inter_level_comm_data(p_dom_->da, p_dom_->da_sc);

    if( !p_cm_->is_smallest_root ) {
        assembled_mean = new Quantity(p_opt_, p_dom_, p_cm_, p_mod_, "", true);
        assembled_var = new Quantity(p_opt_, p_dom_, p_cm_, p_mod_, "", true);
    }

    // Foreach time frame
    for(uint frame = 0; frame < means_.at(0).size(); ++frame) {
        if( p_cm_->is_smallest_root ) {
            assembled_mean = means_.at(0).at(frame);
            assembled_var = M2s_.at(0).at(frame);
            OUTPUT(p_cm_, INTRADM,
                   "Ready for receiving from inter-level, standing by...\n", "");
        } else {

            Vec tmp, tmp_sc;

            ierr = DMGetGlobalVector(p_cm_->da_, &tmp); CHKERRQ(ierr);
            ierr = DMGetGlobalVector(p_cm_->da_sc_, &tmp_sc); CHKERRQ(ierr);

            if(assembled_mean->components_ & CompType::U) {
                ierr = assemble_quantity<false>(p_dom_->interp_mat,
                                                means_.at(0).at(frame)->U,
                                                means_.at(1).at(frame)->U,
                                                assembled_mean->U, tmp);
                CHKERRQ(ierr);
                ierr = assemble_quantity<false>(p_dom_->interp_mat,
                                                M2s_.at(0).at(frame)->U,
                                                M2s_.at(1).at(frame)->U,
                                                assembled_var->U, tmp);
                CHKERRQ(ierr);
            }
            if(assembled_mean->components_ & CompType::omega) {
                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               means_.at(0).at(frame)->omega,
                                               means_.at(1).at(frame)->omega,
                                               assembled_mean->omega, tmp_sc);
                CHKERRQ(ierr);
                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               M2s_.at(0).at(frame)->omega,
                                               M2s_.at(1).at(frame)->omega,
                                               assembled_var->omega, tmp_sc);
                CHKERRQ(ierr);
            }
            if(assembled_mean->components_ & CompType::Gp) {
                ierr = assemble_quantity<false>(p_dom_->interp_mat,
                                                means_.at(0).at(frame)->Gp,
                                                means_.at(1).at(frame)->Gp,
                                                assembled_mean->Gp, tmp);
                CHKERRQ(ierr);
                ierr = assemble_quantity<false>(p_dom_->interp_mat,
                                                M2s_.at(0).at(frame)->Gp,
                                                M2s_.at(1).at(frame)->Gp,
                                                assembled_var->Gp, tmp);
                CHKERRQ(ierr);
            }
            if(assembled_mean->components_ & CompType::ps) {
                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               means_.at(0).at(frame)->ps,
                                               means_.at(1).at(frame)->ps,
                                               assembled_mean->ps, tmp_sc);
                CHKERRQ(ierr);
                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               M2s_.at(0).at(frame)->ps,
                                               M2s_.at(1).at(frame)->ps,
                                               assembled_var->ps, tmp_sc);
                CHKERRQ(ierr);
            }
            if(assembled_mean->components_ & CompType::gamma2) {
                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               means_.at(0).at(frame)->gamma2,
                                               means_.at(1).at(frame)->gamma2,
                                               assembled_mean->gamma2, tmp_sc);
                CHKERRQ(ierr);

                ierr = assemble_quantity<true>(p_dom_->interp_sc_mat,
                                               M2s_.at(0).at(frame)->gamma2,
                                               M2s_.at(1).at(frame)->gamma2,
                                               assembled_var->gamma2, tmp_sc);
                CHKERRQ(ierr);
            }

            ierr = DMRestoreGlobalVector(p_cm_->da_, &tmp); CHKERRQ(ierr);
            ierr = DMRestoreGlobalVector(p_cm_->da_sc_, &tmp_sc); CHKERRQ(ierr);
        }

        if( !p_cm_->is_smallest_root && p_opt_->save_diff ) {
            std::stringstream ss1;
            ss1 << "mean-diff-col";
            ss1 << p_cm_->intra_domain_color;
            ss1 << "_f" << frame;
            ierr = assembled_mean->set_name(ss1.str()); CHKERRQ(ierr);
            ierr = assembled_mean->save_all(); CHKERRQ(ierr);
            std::stringstream ss2;
            ss2 << "var-diff-col";
            ss2 << p_cm_->intra_domain_color;
            ss2 << "_f" << frame;
            ierr = assembled_var->set_name(ss2.str()); CHKERRQ(ierr);
            ierr = assembled_var->save_all(); CHKERRQ(ierr);
        }


        ierr = send_quantity_to_next(assembled_mean,
                                     assembled_var, frame); CHKERRQ(ierr);

        if( p_cm_->is_super_root && p_opt_->save_mean ) {
            std::stringstream ss3;
            ss3 << "mean_f" << frame;
            assembled_mean->set_name(ss3.str());
            assembled_mean->save_all();
        }
        if( p_cm_->is_super_root && p_opt_->save_m2 ) {
            std::stringstream ss4;
            ss4 << "var_f" << frame;
            assembled_var->set_name(ss4.str());
            assembled_var->save_all();
        }
    }

    if( !p_cm_->is_smallest_root ) {
        delete assembled_mean;
        delete assembled_var;
    }

    // TODO delete elements
    for(uint solve_nr = 0; solve_nr < means_.size(); ++solve_nr) {
        for(uint frame = 0; frame < means_.at(solve_nr).size(); ++frame) {
            delete means_.at(solve_nr).at(frame);
            delete M2s_.at(solve_nr).at(frame);
        }
        means_.at(solve_nr).clear();
        M2s_.at(solve_nr).clear();
    }
    means_.clear();
    M2s_.clear();

    p_profiler->tic("level_post_end");

    PetscFunctionReturn(ierr);
}

//!
//!
//! @return Error code
PetscErrorCode IntegratorParallel::solve_pre(void) {
    PetscFunctionReturn(ierr);
}

//! Performed once all solves of the level are completed.
//! Save level snapshot (if needed) and start sending data to root
//! (this should be frame based?)
//! @return Error code
PetscErrorCode IntegratorParallel::solve_post(void) {
    FUNCTIONBEGIN(p_cm_);

    p_profiler->tic("solve_post_begin");

    for(uint frame = 0; frame < means_.at(solve_nr_).size(); ++frame) {

        if( p_opt_->save_mean && p_opt_->save_levels ) {
            ierr = means_.at(solve_nr_).at(frame)->save_all(); CHKERRQ(ierr);
        }
        if( p_opt_->save_m2 && p_opt_->save_levels ) {
            ierr = M2s_.at(solve_nr_).at(frame)->save_all(); CHKERRQ(ierr);
        }

        DEBUG(p_cm_, SELF, "Sending frame %d of %d...\n",
              frame,  means_.at(solve_nr_).size() );
        ierr = quantity_to_root(means_.at(solve_nr_).at(frame),
                                M2s_.at(solve_nr_).at(frame), frame);
        CHKERRQ(ierr);

        if( p_opt_->quad_type == QuadratureType::Stochastic ) {
            ierr = M2s_.at(solve_nr_).at(frame)->normalize_as_var();
            CHKERRQ(ierr);
        }
    }
    p_profiler->tic("solve_post_end");

    PetscFunctionReturn(ierr);
}

//! Performed after a simulation, request the quantity from the solver and
//! add it to mean/var. The solver is still
//! owner of the quantity, an this is just copied by value onto mean/var,
//! @return Error code
PetscErrorCode IntegratorParallel::sim_post(void) {
    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode IntegratorParallel::send_quantity_to_next(Quantity * assembled_mean,
                                                         Quantity * assembled_var,
                                                         int frame) {
    FUNCTIONBEGIN(p_cm_);
    if( !p_cm_->is_smallest_root ) {
        Vec tmp1, tmp2, recv, tmp1_sc, tmp2_sc, recv_sc;

        ierr = DMGetGlobalVector(p_cm_->da_, &tmp1); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_cm_->da_, &tmp2); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_cm_->da_recv_, &recv); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_cm_->da_sc_, &tmp1_sc); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_cm_->da_sc_, &tmp2_sc); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_cm_->da_recv_sc_, &recv_sc); CHKERRQ(ierr);

        if(assembled_mean->components_ & CompType::U) {
            ierr = recv_and_interp<false>(p_dom_->interp_mat, recv,
                                          tmp1, tmp2, assembled_mean->U,
                                          TAG_U, TAG_MEAN, frame); CHKERRQ(ierr);
            ierr = recv_and_interp<false>(p_dom_->interp_mat, recv,
                                          tmp1, tmp2, assembled_var->U,
                                          TAG_U, TAG_VAR, frame); CHKERRQ(ierr);
        }
        if(assembled_mean->components_ & CompType::omega) {
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_mean->omega,
                                         TAG_w, TAG_MEAN, frame); CHKERRQ(ierr);
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_var->omega,
                                         TAG_w, TAG_VAR, frame); CHKERRQ(ierr);
        }
        if(assembled_mean->components_ & CompType::Gp) {
            ierr = recv_and_interp<false>(p_dom_->interp_mat, recv,
                                          tmp1, tmp2, assembled_mean->Gp,
                                          TAG_Gp, TAG_MEAN, frame); CHKERRQ(ierr);
            ierr = recv_and_interp<false>(p_dom_->interp_mat, recv,
                                          tmp1, tmp2, assembled_var->Gp,
                                          TAG_Gp, TAG_VAR, frame); CHKERRQ(ierr);
        }
        if(assembled_mean->components_ & CompType::ps) {
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_mean->ps,
                                         TAG_ps, TAG_MEAN, frame); CHKERRQ(ierr);
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_var->ps,
                                         TAG_ps, TAG_VAR, frame); CHKERRQ(ierr);
        }
        if(assembled_mean->components_ & CompType::gamma2) {
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_mean->gamma2,
                                         TAG_gamma2, TAG_MEAN, frame); CHKERRQ(ierr);
            ierr = recv_and_interp<true>(p_dom_->interp_sc_mat, recv_sc,
                                         tmp1_sc, tmp2_sc, assembled_var->gamma2,
                                         TAG_gamma2, TAG_VAR, frame); CHKERRQ(ierr);
        }

        ierr = DMRestoreGlobalVector(p_cm_->da_, &tmp1); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_cm_->da_, &tmp2); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_cm_->da_recv_, &recv); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_cm_->da_sc_, &tmp1_sc); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_cm_->da_sc_, &tmp2_sc); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_cm_->da_recv_sc_, &recv_sc); CHKERRQ(ierr);

    }

    if( !p_cm_->is_super_root ) {
        OUTPUT(p_cm_, INTRADM,
               "Ready for sending to inter-level, standing by...\n", "");
    }
    p_profiler->push_stage("mpi_inter_level_comm");
    if(assembled_mean->components_ & CompType::U) {
        p_cm_->send_to_next<false>(assembled_mean->U,
                                   get_inter_level_tag(TAG_U, TAG_MEAN, frame));
        p_cm_->send_to_next<false>(assembled_var->U,
                                   get_inter_level_tag(TAG_U, TAG_VAR, frame));
    }
    if(assembled_mean->components_ & CompType::omega) {
        p_cm_->send_to_next<true>(assembled_mean->omega,
                                  get_inter_level_tag(TAG_w, TAG_MEAN, frame));
        p_cm_->send_to_next<true>(assembled_var->omega,
                                  get_inter_level_tag(TAG_w, TAG_VAR, frame));
    }
    if(assembled_mean->components_ & CompType::Gp) {
        p_cm_->send_to_next<false>(assembled_mean->Gp,
                                   get_inter_level_tag(TAG_Gp, TAG_MEAN, frame));
        p_cm_->send_to_next<false>(assembled_var->Gp,
                                   get_inter_level_tag(TAG_Gp, TAG_VAR, frame));
    }
    if(assembled_mean->components_ & CompType::ps) {
        p_cm_->send_to_next<true>(assembled_mean->ps,
                                  get_inter_level_tag(TAG_ps, TAG_MEAN, frame));
        p_cm_->send_to_next<true>(assembled_var->ps,
                                  get_inter_level_tag(TAG_ps, TAG_VAR, frame));
    }
    if(assembled_mean->components_ & CompType::gamma2) {
        p_cm_->send_to_next<true>(assembled_mean->gamma2,
                                  get_inter_level_tag(TAG_gamma2, TAG_MEAN, frame));
        p_cm_->send_to_next<true>(assembled_var->gamma2,
                                  get_inter_level_tag(TAG_gamma2, TAG_VAR, frame));
    }
    p_profiler->pop_stage();

    PetscFunctionReturn(ierr);
}

//! Send a quantity consisting of a mean and a variance and corresponding
//! to a frame to the root of that level
//! TODO: currently not sending quadrature
PetscErrorCode IntegratorParallel::quantity_to_root(Quantity *mean,
                                                    Quantity *m2, int frame) {
    FUNCTIONBEGIN(p_cm_);
    // Will contain the number of values stored in the thing
    int new_adds = 0;
    // Wil contain the sum of the weights
    PetscReal new_sum_weights = 0;

    std::deque<TransmissionBuffer*> tb;
    p_profiler->push_stage("mpi_intra_level_comm");
    if(mean->components_ & CompType::U) {
        DEBUG(p_cm_, INTRADM, "Sending U to root...\n", p_cm_->intra_domain_color,
              p_cm_->intra_domain_size);
        if( mean->U == NULL ) {
            ERROR(p_cm_, INTRADM, "Found no U in mean, something went wrong?\n",
                  p_cm_->intra_domain_color, p_cm_->intra_domain_size);
        }
        ierr = p_cm_->level_to_root_begin<false>(mean->U, m2->U,
                                                 tb, TAG_U, frame);
        CHKERRQ(ierr);
        ierr = p_cm_->level_to_root_end<false>(mean->U, m2->U, tb,
                                               mean->num_adds, &new_adds,
                                               mean->sum_weights, &new_sum_weights,
                                               p_smp_->weights_per_deepness);
        CHKERRQ(ierr);
    }
    if(mean->components_ & CompType::omega) {
        DEBUG(p_cm_, INTRADM, "Sending omega to root...\n",
              p_cm_->intra_domain_color,
              p_cm_->intra_domain_size);
        if( mean->omega == NULL ) {
            ERROR(p_cm_, INTRADM,
                  "Found no omega in mean, something went wrong?\n", "");
        }
        ierr = p_cm_->level_to_root_begin<true>(mean->omega, m2->omega,
                                                tb, TAG_w, frame);
        CHKERRQ(ierr);
        ierr = p_cm_->level_to_root_end<true>(mean->omega, m2->omega,
                                              tb, mean->num_adds, &new_adds,
                                              mean->sum_weights, &new_sum_weights,
                                              p_smp_->weights_per_deepness);
        CHKERRQ(ierr);
    }
    if(mean->components_ & CompType::Gp) {
        DEBUG(p_cm_, INTRADM, "Sending Gp to root...\n", "");
        ierr = p_cm_->level_to_root_begin<false>(mean->Gp, m2->Gp,
                                                 tb, TAG_Gp, frame);
        CHKERRQ(ierr);
        ierr = p_cm_->level_to_root_end<false>(mean->Gp, m2->Gp,
                                               tb, mean->num_adds, &new_adds,
                                               mean->sum_weights, &new_sum_weights,
                                               p_smp_->weights_per_deepness);
        CHKERRQ(ierr);
    }
    if(mean->components_ & CompType::ps) {
        DEBUG(p_cm_, INTRADM, "Sending ps to root...\n", "");
        ierr = p_cm_->level_to_root_begin<true>(mean->ps, m2->ps,
                                                tb, TAG_ps, frame);
        CHKERRQ(ierr);
        ierr = p_cm_->level_to_root_end<true>(mean->ps, m2->ps,
                                              tb, mean->num_adds, &new_adds,
                                              mean->sum_weights, &new_sum_weights,
                                              p_smp_->weights_per_deepness);
        CHKERRQ(ierr);
    }
    if(mean->components_ & CompType::gamma2) {
        DEBUG(p_cm_, INTRADM, "Sending gamma2 to root...\n", "");
        ierr = p_cm_->level_to_root_begin<true>(mean->gamma2, m2->gamma2,
                                                tb, TAG_gamma2, frame);
        CHKERRQ(ierr);
        ierr = p_cm_->level_to_root_end<true>(mean->gamma2, m2->gamma2,
                                              tb, mean->num_adds, &new_adds,
                                              mean->sum_weights, &new_sum_weights,
                                              p_smp_->weights_per_deepness);
        CHKERRQ(ierr);
    }
    p_profiler->pop_stage();

    if( p_cm_->is_root ) {
        mean->num_adds = new_adds;
        m2->num_adds = new_adds;
        mean->sum_weights = new_sum_weights;
        m2->sum_weights = new_sum_weights;
    }

    PetscFunctionReturn(ierr);
}
