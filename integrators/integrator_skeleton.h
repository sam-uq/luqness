/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file integrator_skeleton.h
//! This file contains the base implementation of the
//! pure virtual class IntegratorSkeleton.

#include <sstream>
#include <vector>

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/quantity.h"
#include "../tools/utilities.h"

#include "../solver/projection_solver.h"
#include "../solver/vortex_solver.h"

#include "../samplers/well512a.h"
#include "../samplers/boostrng.h"
#include "../samplers/loadrng.h"
#include "../samplers/gauss.h"

#include "../tools/histo_data.h"

#include "../models/model_base.h"

//! @brief This class provides a way to run parallel and
//! sequential instances  of the Solver class.
//! This class takes care of allocation of its own pointer,
//! in particular  reading the option database (PETSc) and
//! generating an initial data model.
class IntegratorSkeleton {
public:
    //! The constructor creates Sampler and calls factory()
    IntegratorSkeleton(Options *opt_,
                       ModelBase *p_mod, Attila *p_cm);
    //! Deletes Sampler
    virtual ~IntegratorSkeleton();

    //! Start the simulation, calling for each refinement
    //! level solve() 1 or 2 times
    PetscErrorCode start(void);
protected:
    //! Instantiate Model/Solver and solve a couples of time
    PetscErrorCode solve(void);
    //! Routine launched before a level starts
    virtual PetscErrorCode level_pre(void) { return ierr; }
    //! Routine launched after a level is completed
    virtual PetscErrorCode level_post(void) { return ierr; }
    //! Routine launched before a simulation is completed
    virtual PetscErrorCode sim_pre(void) { return ierr; }
    //! Routine launched after a simulation is completed
    virtual PetscErrorCode sim_post(void) { return ierr; }
    //! Routine launched before a simulation is completed
    virtual PetscErrorCode solve_pre(void) { return ierr; }
    //! Routine launched before a simulation is completed
    virtual PetscErrorCode solve_post(void) { return ierr; }
    //! Options container
    Options * const       p_opt_;
    //! The ModelBase container (initial data,
    //! autoallocated and reinit)
    ModelBase *           p_mod_;
    //! Communicator Attila
    Attila * const        p_cm_;
    //! Domain container (autoallocated at each level
    //! and refined)
    Domain *              p_dom_;
    //! Solver instance (autoallocated at each level)
    Solver *              p_solver_;

    //! Histogram data
    HistoData *           p_hist_;

    //! Number of solves done so far
    int solve_nr_;

    //! Contains list of sim IDs done by the rank
    std::vector<int> sim_ids_;

    //! Quantities for pointers to mean/variance
    //! (first component is solve number,
    //! second is frame number)
    std::vector< std::vector <Quantity *> > means_, M2s_;

    //! Sampler instance, autoallocated and destructed at begin/end
    Sampler             * p_smp_;
private:
};

