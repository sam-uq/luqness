/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file integrator_serial.h
//!
//!
#include <vector>

#include "integrator_skeleton.h"

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/quantity.h"

#include "../solver/projection_solver.h"
#include "../solver/vortex_solver.h"

#include "../models/model_base.h"

//!
//! \brief The IntegratorSerial class is an abstract base providing basic
//! functionality for parallel/multiple integration.
//!
class IntegratorSerial : public IntegratorSkeleton {
public:
    //!
    IntegratorSerial(Options *opt_, ModelBase *p_mod, Attila *p_cm)
    : IntegratorSkeleton(opt_, p_mod, p_cm) {}
    //! Clear mean/m2 vectors, delete dom
    virtual ~IntegratorSerial();

    //! Start the simulations
    PetscErrorCode start(void);
    //! Start the simulations
    PetscErrorCode solve(void);

    //! Allocate a mean/var container for the level
    PetscErrorCode level_pre(void);
    //! Normalize/Save/Delete mean/var
    PetscErrorCode level_post(void);
    //! Add solution to mean/var
    PetscErrorCode sim_post(void);
private:
    // Quantities for mean and variance
    std::vector<Quantity *> mean, m2;

};

