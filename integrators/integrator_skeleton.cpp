/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "integrator_skeleton.h"

//! Allocates  Sampler and fill in the factory
//! available.
IntegratorSkeleton::IntegratorSkeleton(Options *opt_,
                                       ModelBase *p_mod, Attila *p_cm)
    : p_opt_(opt_), p_mod_(p_mod), p_cm_(p_cm) {
    FUNCTIONBEGIN(p_cm_);

    solve_nr_ = -1;

    // Initialize random number generator
    switch (p_opt_->sampler_type) {
    default:
#ifdef USE_RANDOM
    case SamplerType::Well:
        p_smp_ = new Well512a(p_opt_, p_cm_);
        break;
#if BOOST_ENABLE
    case SamplerType::Boost:
        p_smp_ = new BoostRng<boost::mt19937, boost::random::uniform_real_distribution < > >(p_opt_, p_cm_);
        break;
#endif // BOOST_ENABLE
#endif // USE_RANDOM
    case SamplerType::Gauss:
        p_smp_ = new Gauss(p_opt_, p_cm_);
        break;
    case SamplerType::Load:
        p_smp_ = new LoadRng(p_opt_, p_cm_);
        break;
    }

    p_smp_->set_model(p_mod_);

}

//! Frees previously allocated Options and Sampler
//!
IntegratorSkeleton::~IntegratorSkeleton()
{
    FUNCTIONBEGIN(p_cm_);

    // p_dom_ is deleted after start
    delete p_smp_;

}

//!
//!
PetscErrorCode IntegratorSkeleton::start(void) {
    FUNCTIONBEGIN(p_cm_);
    // Just break if you only requested help
    if (p_opt_->help) return ierr;

    p_profiler->tic("integrate_begin");

    // Iterate through levels and subdivide the work accordingly
    for (int level = 0; level < p_opt_->num_refinements; level++) {
        // Cycle through each refinement
        OUTPUT(p_cm_, INTRADM,
               "[RLVL %d] Will do %d simulation/s on %d processor/s.\n",
               level, p_cm_->sim_num, p_cm_->intra_domain_size);

        if (level > 0) {
            if (!p_cm_->is_smallest_root) {
                p_dom_->keep_old = false;
                ierr = p_dom_->refine(); CHKERRQ(ierr);
                p_dom_->keep_old = true;
            }
        } else {
            // Generate the domain
            p_dom_ = new Domain(p_opt_, p_cm_);
            p_dom_->setup_from_options();
            p_dom_->keep_old = true;
        }

        // Performs pre-level things
        ierr = level_pre(); CHKERRQ(ierr);

        p_cm_->generate_intra_level_comm_data(p_dom_->da,
                                              p_dom_->da_sc);

        ierr = solve_pre();
        /*
            if( p_cm_->level == 1) {
                sleep(1000);
            }*/

        p_profiler->tic("solve_1_begin");
        ierr = solve();
        p_profiler->tic("solve_2_end");

        ierr = solve_post();


        if (p_cm_->level != p_cm_->p_alloc_->levels_ - 1) {

            ierr = p_dom_->refine(); CHKERRQ(ierr);

            p_cm_->generate_intra_level_comm_data(p_dom_->da,
                                                  p_dom_->da_sc);

            p_cm_->reinit_sim();

            ierr = solve_pre();

            p_profiler->tic("solve_2_begin");
            ierr = solve();
            p_profiler->tic("solve_2_end");

            ierr = solve_post();
        }

        // Performs post actions after level completed
        ierr = level_post();
        CHKERRQ(ierr);

    }

    p_profiler->tic("integrate_end");

    delete p_dom_;

    OUTPUT(p_cm_, INTRADM,
           "Done! Completed %d simulations with IDs: %s!\n",
           sim_ids_.size(), to_str(sim_ids_).c_str());

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode IntegratorSkeleton::solve(void) {
    FUNCTIONBEGIN(p_cm_);

    p_mod_->set_data(p_dom_, p_cm_);
    ierr = p_smp_->setup(); CHKERRQ(ierr);

    // Create a solver
    switch (p_opt_->solver) {
    case SolverType::Vort:
        p_solver_ = new VortexSolver(p_opt_, p_dom_,
                                     p_mod_, p_cm_);
        break;
    case SolverType::Proj:
        p_solver_ = new ProjectionSolver(p_opt_, p_dom_,
                                         p_mod_, p_cm_);
        break;
    }

    means_.push_back(std::vector<Quantity *>());
    M2s_.push_back(std::vector<Quantity *>());
    p_solver_->set_quantities_vectors(&means_.back(), &M2s_.back());

    if( p_opt_->save_hist ) {
        OUTPUT(p_cm_, INTRADM, "As you requested we'll allocate space and save histograms, this feature is "
                               "work in progress...\n", "");
        p_hist_ = new HistoData(p_dom_, p_opt_->hist_data);
        p_solver_->set_histo_data(p_hist_);
    }


    // Iterate trough number of iterations of each process
    // Cycle through each simulation for the process
    for (int k = 0; k < p_cm_->sim_num_of_rank[p_cm_->world_rank]; k++) {

        // Assign unique id to simulations, they must be numbers from 0 to simulations[level]
        sim_ids_.push_back(p_cm_->sim_id);

        // Performs post simulation things
        ierr = sim_pre(); CHKERRQ(ierr);

        // Initialize the model (setup seeds etc, not the vectors)
        p_mod_->init(p_cm_->sim_id);

        // Finally actually solve the system
        ierr = p_solver_->solve(); CHKERRQ(ierr);

        // Performs post simulation things
        ierr = sim_post(); CHKERRQ(ierr);

        p_cm_->next_sim();
    }

    if( p_opt_->save_hist ) {
        //         TODO: fixme find way to save coordinates
        p_hist_->merge();
        p_hist_->save();
        delete p_hist_;
    }

    ++solve_nr_;

    delete p_solver_;

    PetscFunctionReturn(ierr);
}
