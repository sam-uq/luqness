/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "loadrng.h"

#ifdef USE_RANDOM

//! Constructor
//!
LoadRng::LoadRng(Options * opt_, Attila *p_cm) : Sampler(opt_, p_cm) {
    char filename[] = "data.bin";

    std::ifstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);
    if ( file.is_open() ) {
        size = file.tellg();
        memblock = new char [size];
        file.seekg (0, std::ios::beg);
        file.read (memblock, size);
        file.close();
        // Find length of array
        size = size / sizeof(double);
        VERBOSE(p_cm_, WORLD, "The entire file content is in memory, it has size %d.\n", size);


        //     delete[] memblock;
    } else {
        WARNING(p_cm_, WORLD, "Unable to open file %s.\n", filename);
    }

    values = (double*) memblock;

}

//! Destructor
//!
LoadRng::~LoadRng() {
    delete[] memblock;
}

//!
//!
PetscErrorCode LoadRng::setup(void) {
    print();

    PetscFunctionReturn(ierr);
}

//! Inizialize rng
//!

PetscErrorCode LoadRng::init(int) {
    //   VecCreate(&v);
    PetscFunctionReturn(ierr);
}

//! Call next number
//!
double LoadRng::next(uint dim, uint stream) {
    UNUSED(dim); UNUSED(stream);
    if( last_draw < size) {
        //
    } else {
        WARNING("", WORLD, "Ran out of data, restarting from the begin..\n", "");
        last_draw = 0;
    }
    PetscReal nn = values[last_draw++];
    VERBOSE(p_cm_, INTRADM, "Grabbin' next as Load (%d of %d), dim = %d, stream = %d and next = %f\n",
            last_draw, size, dim, stream, nn);

    return nn;
}

#endif // USE_RANDOM
