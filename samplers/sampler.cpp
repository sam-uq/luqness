/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "sampler.h"

// Needed for getpid
// #define _POSIX_SOURCE

#ifdef USE_RANDOM

//! Constructor, inizialize seeds and distribute
//!
Sampler::Sampler(Options * opt_, Attila *p_cm)
    : opt(opt_), p_cm_(p_cm)
    , paramc(opt->samp_paramc)
    , paramv(opt->samp_paramv)
    , paramn(0)
    , a(0)
    , b(1)
    , is_normal_buffer_empty(true)
{

    //!  Type of distribution for *random* generators
    par.push_back("distrib");
    if( paramc > ++paramn ) {
        if( !strcmp("unif", paramv[paramn]) ) {
            dist = Uniform;
        } else {
            dist = Normal;
        }
    }

    //! Left boundary of nodes
    par.push_back("a");
    if( paramc > ++paramn ) {
        a = atof(paramv[paramn]);
    }
    //! Right boundary of nodes
    par.push_back("b");
    if( paramc > ++paramn ) {
        b = atof(paramv[paramn]);
    }
    assert(a < b);

    // Linear Shift to map (-1,1) to (a,b)
    A = 0.5*(b-a);
    B = 0.5*(b+a);

    // Master generates ONE seed for everybody and broadcast to everybody
    // We use the POSIX getpid in order to avoid any conflict on contermporary jobs
    // TODO allow master seed obtained by user input
    //   if( opt->master_seed >= 0 ) {
    //   master_seed = opt->master_seed;
    //   } else {
    // Mangeling pid= time(NULL);
    root = 0;
    int *seeds = NULL;
    MPI_Request * req = NULL;

    if( p_cm_->world_rank == root ) {
        int master_seed = opt_->master_seed == -1 ? time(NULL) * getpid() : opt_->master_seed;
        req = new MPI_Request[p_cm_->world_size - 1];
        DEBUG(p_cm_, SELF, "Mangling universal entropy, got master seed %d, sending to peers...\n",
              master_seed);

        // Setup the generators for the seeds
#if BOOST_ENABLE
        boost::random::mt19937 seed_generator(master_seed);
        boost::random::uniform_int_distribution<int> seed_distribution;

        // Init the array and generate size numbers (integers)
        seeds = new int[p_cm_->parallel_runs];
        for(int i = 0; i < p_cm_->parallel_runs; ++i) {
            seeds[i] = seed_distribution(seed_generator);
        }
#else // HAVE_BOOST == 0
        srand (master_seed);

        // Init the array and generate size numbers (integers)
        seeds = new int[p_cm_->parallel_runs];
        for(int i = 0; i < p_cm_->parallel_runs; ++i) {
            seeds[i] = rand();
        }
#endif // BOOST_ENABLE

        // Broadcast the master seed
        seed = seeds[0];
        for(int i = 1; i < p_cm_->world_size; ++i) {
            DEBUG(p_cm_, SELF, "Sending seed %d (color=%d) to %d from %d...\n",
                  seeds[p_cm_->intra_domain_color_table[i]], p_cm_->intra_domain_color_table[i],
                    i, root);
            MPI_Isend(&seeds[p_cm_->intra_domain_color_table[i]], 1, MPI_INT, i, TAG_SEEDS, PETSC_COMM_WORLD, &req[i-1]);
        }
        DEBUG(p_cm_, INTRADM, "My seed is %d...\n", seed);
    } else {
        MPI_Recv(&seed, 1, MPI_INT, root, TAG_SEEDS, PETSC_COMM_WORLD, MPI_STATUS_IGNORE);
        DEBUG(p_cm_, INTRADM, "Yay, got the magnificent seed %d...\n", seed);
    }

    sim_seeds = new int[p_cm_->sim_num];

#if BOOST_ENABLE
    boost::random::mt19937 seed_generator(seed);
    boost::random::uniform_int_distribution<int> seed_distribution;

    // Init the array and generate size numbers (integers)
    for(int i = 0; i < p_cm_->sim_num; ++i) {
        sim_seeds[i] = seed_distribution(seed_generator);
    }
#else // HAVE_BOOST == 0
    srand (seed);

    // Init the array and generate size numbers (integers)
    for(int i = 0; i < p_cm_->sim_num; ++i) {
        sim_seeds[i] = rand();
    }
#endif // BOOST_ENABLE

    if( p_cm_->world_rank == root ) {
        MPI_Waitall(p_cm_->world_size - 1, req, MPI_STATUSES_IGNORE);
        delete[] seeds;
        delete[] req;
    }

    size_.sim = 0u;
    size_.dim = 0u;
    size_.stream = 0u;
}

//!
//!
void Sampler::set_model(ModelBase * mod_) {
    mod = mod_;
    mod_->p_smp_ = this;
    size_.sim += mod_->smp_size_.sim;
    size_.dim += mod_->smp_size_.dim;
    size_.stream += mod_->smp_size_.stream;
}

//!
//!
PetscErrorCode Sampler::print(void)
{
    int i = 0;
    DEBUG(p_cm_, WORLD, "Sampler %d (name = %s) parameters:\n", paramc, paramv[i], paramc - 1);
    for(std::vector<std::string>::iterator it = par.begin() ; it != par.end(); ++it) {
        ++i;
        DEBUG(p_cm_, WORLD, "%10s%20s\n", it->c_str(), paramv[i]);
    }

    DEBUG(p_cm_, WORLD, "Sampler will use dim = %d, stream = %d.\n",
          size_.dim, size_.stream);

    PetscFunctionReturn(ierr);
}

//! Get a seed from the sim_id number
//!
void Sampler::restart(uint)
{
    VERBOSE(p_cm_, INTRADM, "Restarting Sampler at sim_id = %d, with seed %d.\n",
            p_cm_->sim_id, sim_seeds[p_cm_->sim_id - p_cm_->sim_id_of_rank[p_cm_->world_rank]]);

    // Get a seed

    // Inizialize the RNGs with seeds
    init(sim_seeds[p_cm_->sim_id - p_cm_->sim_id_of_rank[p_cm_->world_rank]]);
}

//! Generates standard normal random variable using rejection method and Box-Muller transformation
//! (c) ALSVID-UQ
//! TODO: multime dims and streams
double Sampler::next_normal(uint dim, uint stream )
{
    VERBOSE(p_cm_, WORLD, "Drawing next normal from dim = %d, stream = %d:\n", dim, stream);
    PetscReal U1, U2;

    // TODO: buffer located in carefully chosen array for various dimensiions

    // The rejection method needs two uniform numbers and generates *two* normal
    // distributed number so we want to keep (buffer) the other one
    if (!is_normal_buffer_empty) {
        is_normal_buffer_empty = true;
        return normal_buffer;
    } else {
        // Rejection method to obtain (U1,U2) - uniform random variable on unit circle
        PetscReal r = 2.0;
        while (r > 1.0) {
            // Draw two uniform random variables on [-1, 1] ( a = 0, b = 1)
            U1 = 2 * next(dim, stream) - 1;
            U2 = 2 * next(dim, stream) - 1;
            // compute radius
            r = sqrt(U1) + sqrt(U2);
        }

        // Box-Muller transformation
        PetscReal w  = sqrt( -2 * log(r) / r );
        U1 *= U1 * w;
        U2 *= U2 * w;

        normal_buffer = U2;
        is_normal_buffer_empty = false;

        return U1;
    }
}

#endif // USE_RANDOM
