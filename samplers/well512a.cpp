/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "well512a.h"

#ifdef USE_RANDOM

/*** Well512a source gode from http://www.iro.umontreal.ca/~panneton/well/WELL512a.c ***/
/* ***************************************************************************** */
/* Copyright:      Francois Panneton and Pierre L'Ecuyer, University of Montreal */
/*                 Makoto Matsumoto, Hiroshima University                        */
/* Notice:         This code can be used freely for personal, academic,          */
/*                 or non-commercial purposes. For commercial purposes,          */
/*                 please contact P. L'Ecuyer at: lecuyer@iro.UMontreal.ca       */
/* ***************************************************************************** */

#define W 32
#define R 16
#define P 0
#define M1 13
#define M2 9
#define M3 5

#define MAT0POS(t,v) (v^(v>>t))
#define MAT0NEG(t,v) (v^(v<<(-(t))))
#define MAT3NEG(t,v) (v<<(-(t)))
#define MAT4NEG(t,b,v) (v ^ ((v<<(-(t))) & b))

#define V0            STATE[state_i                   ]
#define VM1           STATE[(state_i+M1) & 0x0000000fU]
#define VM2           STATE[(state_i+M2) & 0x0000000fU]
#define VM3           STATE[(state_i+M3) & 0x0000000fU]
#define VRm1          STATE[(state_i+15) & 0x0000000fU]
#define VRm2          STATE[(state_i+14) & 0x0000000fU]
#define newV0         STATE[(state_i+15) & 0x0000000fU]
#define newV1         STATE[state_i                 ]
#define newVRm1       STATE[(state_i+14) & 0x0000000fU]

#define FACT 2.32830643653869628906e-10

static unsigned int state_i = 0;
static unsigned int STATE[R];
static unsigned int z0, z1, z2;

void InitWELLRNG512a (unsigned int *init){
    int j;
    state_i = 0;
    for (j = 0; j < R; j++)
        STATE[j] = init[j];
}

double WELLRNG512a (void){
    z0    = VRm1;
    z1    = MAT0NEG (-16,V0)    ^ MAT0NEG (-15, VM1);
    z2    = MAT0POS (11, VM2)  ;
    newV1 = z1                  ^ z2;
    newV0 = MAT0NEG (-2,z0)     ^ MAT0NEG(-18,z1)    ^ MAT3NEG(-28,z2) ^ MAT4NEG(-5,0xda442d24U,newV1) ;
    state_i = (state_i + 15) & 0x0000000fU;
    return ((double) STATE[state_i]) * FACT;
}

/*** End of wrapper ***/

//! Constructor
//!
Well512a::Well512a(Options * opt_, Attila *p_cm) : Sampler(opt_, p_cm) {

}

//! Destructor
//!
Well512a::~Well512a()
{

}

//!
//!
PetscErrorCode Well512a::setup(void) {
    print();

    PetscFunctionReturn(ierr);
}

//! Inizialize rng
//!
PetscErrorCode Well512a::init(int seed) {
    unsigned int buffer[R];

    // init buffer using Linear Congruential Generator (from ALSVID-UQ)
    const long a = 1103515245;
    const long c = 12345;

    unsigned int x = seed;

    for (int i=0; i<R; i++) {
        x = a*x + c;
        buffer[i] = x;
    }

    InitWELLRNG512a (buffer);

    PetscFunctionReturn(ierr);
}

//! Call next number
//!
double Well512a::next(uint dim, uint stream) {
    UNUSED(dim);
    UNUSED(stream);
    PetscReal nn = WELLRNG512a() * (b - a) + a;
    VERBOSE(p_cm_, INTRADM,
            "Grabbin' next as Well512a, dim = %d, stream = %d and next = %f\n",
            dim, stream, nn);
    return nn;
}

#endif // USE_RANDOM
