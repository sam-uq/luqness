/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file boostrng.h
//! An implementation of the Well512a PRNG (wrapper)

#include "sampler.h"

#if defined(USE_RANDOM) && BOOST_ENABLE

//!
//!
template<class gene, class distr>
class BoostRng : public Sampler {
public:
    //! Inizialize the RNGs
    BoostRng(Options * opt_, Attila *p_cm);
    //! Delete the RNGs
    ~BoostRng();

    //! Setup all the necessary data after model has been defined
    PetscErrorCode setup(void);

    //! Draw the next number from the sequence
    double next(uint dim, uint stream);

protected:
    //! Virtual member for initialization of seed, can be anything
    PetscErrorCode init(int seed);

private:
    gene seed_generator;
    distr seed_distribution;
};

//! Constructor
//!
template<class gene, class distr>
BoostRng<gene, distr>::BoostRng(Options * opt_, Attila *p_cm)
    : Sampler(opt_, p_cm) {

}

//! Destructor
//!
template<class gene, class distr>
BoostRng<gene, distr>::~BoostRng()
{

}

//! Inizialize rng
//! TODO
template<class gene, class distr>
PetscErrorCode BoostRng<gene, distr>::init(int seed)
{

    // Setup the generators for the seeds
    seed_generator = gene(seed);

    PetscFunctionReturn(ierr);
}

//!
//!
template<class gene, class distr>
PetscErrorCode BoostRng<gene, distr>::setup(void)
{
    print();

    PetscFunctionReturn(ierr);
}

//! Call next number
//! TODO
template<class gene, class distr>
double BoostRng<gene, distr>::next(uint dim, uint stream)
{
    return seed_distribution(seed_generator) * (b - a) + a;
}

#endif // Flag check have boost and use random
