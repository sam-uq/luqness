/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file gauss.h
//! Implements a "sampler" that provides nodes/weights for a Gaussian quadrature
//! in several dimensions (wraps around a
//! node/weight generator).

#include <sstream>

#include "./sampler.h"
#include "./gauss_legendre.h"

//!
//! \brief The Gauss class provides a wrapper around a Gauss/Legendre quadrature points generator and allows the generation of
//! tensor product quadratures.
//!
class Gauss : public Sampler {
public:
    //! Constructor, initialize the generator
    Gauss(Options * opt_, Attila *p_cm);
    //! Virtual destructor
    virtual ~Gauss();

    //! Setup all the necessary data after model has been defined
    PetscErrorCode setup(void);
    //! Converts the simulation number to Multi D coordinate as  tensor product
    void compute_coord(uint n, uint * c);

    //! Draw next number
    double next(uint dim, uint stream);
    //! Draw next number
    double next_weight(void);

    void generate_weights_per_deepness(void);
protected:
    //! Virtual member for initialization of seed, can be anything
    PetscErrorCode init(int seed);

private:
    //! Pointer to weights
    PetscReal   *w_;
    //! Pointer to nodes
    PetscReal   *x_;

    //! Number of gauss nodes to use
    int nodes;
    //! Starting node index
    int from_node;
    //! Ending node index
    int to_node;

    //! array containing coordinate of dimensions
    uint * coord;

};
