/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef LOADRNG_H
#define LOADRNG_H

//! @file loadrng.h
//! Load a file containing random numbers and

#include "sampler.h"

#include <iostream>
#include <fstream>

#ifdef USE_RANDOM

//! Load a previously generated file containing a sufficiently large number of of random numbers PER PROCESSOR
class LoadRng : public Sampler
{
public:
    //! Inizialize the RNGs
    LoadRng(Options * opt_, Attila *p_cm);
    //! Delete the RNGs
    ~LoadRng();

    //! Setup all the necessary data after model has been defined
    PetscErrorCode setup(void);
    //! Draw the next number from the sequence
    double next(uint dim, uint stream);

protected:
    //! Virtual member for initialization of seed, can be anything
    PetscErrorCode init(int seed);

private:
    int last_draw;

    char* memblock;

    double* values;

    int size;
};

#endif // USE_RANDOM

#endif // WELL512A_H
