/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gauss.h"

Gauss::Gauss(Options * opt_, Attila *p_cm)
    : Sampler(opt_, p_cm)
    , nodes(20)
    //   , dim(1)
    , from_node(0)
    , to_node(nodes-1) {
    par.push_back("nodes");
    if( paramc > ++paramn ) nodes = atoi(paramv[paramn]);
    //   par.push_back("dim");
    //   if( paramc > ++paramn ) dim = atoi(paramv[paramn]);
    par.push_back("fromnode");
    if( paramc > ++paramn ) from_node = atoi(paramv[paramn]);
    par.push_back("tonode");
    if( paramc > ++paramn ) to_node = atoi(paramv[paramn]);

    //   VERBOSE(PETSC_COMM_SELF, "%s[Rank %d / ID %d] Gauss: %d %d %s %s %s %s...\n",
    //  comm_rank ,sim_id, paramn, paramc, paramv[0], paramv[1], paramv[3], paramv[4]);
    //! TODO: variable number of nodes (anysotropic)
    x_ = new double[nodes];
    w_ = new double[nodes];

    // TODO: INSERT SANITY CHECK HERE, AUTO NODES ETC...

    gauss_data(nodes, x_, w_);
}

Gauss::~Gauss() {
    if(p_cm_->is_root) {
        delete[] weights_per_deepness;
    }

    delete x_;
    delete w_;

    delete coord;
}

PetscErrorCode Gauss::init(int) {

    if((uint) p_cm_->p_alloc_->sim_per_level_[p_cm_->level] > pow(nodes, size_.dim)) {
        WARNING(p_cm_, WORLD,
                "Seems like you want to use mode simulations than the nodes you generated (sim = %d = "
                "%d^%d = nodes^dim).\n",
                p_cm_->p_alloc_->sim_per_level_[p_cm_->level], nodes, size_.dim);
    } else {
        DEBUG(p_cm_, WORLD,
              "Gauss sampler init data is: sim = %d = %d^%d = nodes^dim.\n",
              p_cm_->p_alloc_->sim_per_level_[p_cm_->level], nodes, size_.dim);
    }
//     p_cm_->sim_num_of_rank;

    compute_coord(p_cm_->sim_id+from_node, coord);
    generate_weights_per_deepness();

    PetscFunctionReturn(ierr);
}

PetscErrorCode Gauss::setup(void) {
    coord = new uint[size_.dim];
    print();

    VERBOSE(p_cm_, SELF, "Setting up Gauss quadrature with %d dim\n", size_.dim);

//     if(nodes < p_cm_) {
//         WARNING(p_cm_, WORLD, "May it be that you ain't using enough nodes for the simulations u need?\n", "");
//     }

    PetscFunctionReturn(ierr);
}

void Gauss::compute_coord(uint n, uint *c) {
    std::stringstream str;
    str << "[";
#if OUTPUT_LEVEL > 4
    uint tn = n;
#endif // OUTPUT_LEVEL > 4
    for(uint i = 0; i < size_.dim; ++i) {
        c[i] = n % nodes;
        n /= nodes;
        str << c[i] << ", ";
    }
    str << "]";
    VERBOSE(p_cm_, WORLD,
            "Mapping simulation number %d to coordinates = %s\n",
            tn, str.str().c_str());
}

//! Call next number
//!
double Gauss::next(uint dim, uint)
{
    VERBOSE(p_cm_, SELFSID, "Grabbing next value from dim = %d, (with shift: %d) with coord = %d,"
                            "value is = %f...\n", p_cm_->sim_id, from_node, dim, coord[dim], x_[coord[dim]] * A + B);

    return x_[coord[dim]] * A + B;
}

//! Call next weight. The weight is computed starting from the current coordinares, in particular, init has to be
//! computed for this to be meaningful.
//!
double Gauss::next_weight(void)
{
    compute_coord(p_cm_->sim_id+from_node, coord);
    PetscReal W = 1.;
    for( uint i = 0; i < size_.dim; ++i) {
        W *= w_[coord[i]] * A;
    }
    VERBOSE(p_cm_, SELFSID, "Grabbing next weight (with shift: %d) value = %f...\n", from_node, W);

    return W;
}

//!
//!
void Gauss::generate_weights_per_deepness(void) {

    uint * c = new uint[size_.dim];
    if(p_cm_->is_root) {
        weights_per_deepness = new double[p_cm_->intra_level_size];
        for(int i = 0; i < p_cm_->intra_level_size; ++i) {
            weights_per_deepness[i] = 0;
            // UNCLEAN FIXME
            int sim_start = 0;
            for(int r = 0; r < p_cm_->world_size; ++r) {
                if(p_cm_->intra_level_color_table[r] == p_cm_->inter_level_color &&
                   p_cm_->intra_level_deepness_table[r] == i) {
                    sim_start = p_cm_->sim_id_of_rank[r];
                    break;
                }
            }
            VERBOSE(p_cm_, SELF, "Deepness %d from this root starts from sim %d deepness %d...\n",
                    i, sim_start, p_cm_->intra_level_size);
            for(int j = 0; j < p_cm_->sim_num_table[i]; ++j) {
                compute_coord(sim_start+j+from_node, c); // FIXME
                PetscReal W = 1.;
                for( uint k = 0; k < size_.dim; ++k) {
                    W *= w_[c[k]] * A;
//                     VERBOSE(p_cm_, SELF, "adsddddadasd %d dDDDD  %f DDDDDDDDDDDD %d asd %f...\n", i, A, sim_start, W);
                }
//                 VERBOSE(p_cm_, SELF, "adsadasd %d dDDDD  %f DDDDDDDDDDDD %d asd %f...\n", i, A, sim_start, W);
                weights_per_deepness[i] += W;
            }
//             VERBOSE(p_cm_, SELF, "adsadasd %d dDDDD  %f DDDDDDDDDDDD %d asd %f...\n", i, A,
//                     sim_start, weights_per_deepness[i]);
        }
    }
    delete c;
}
