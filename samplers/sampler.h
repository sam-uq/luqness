/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file sampler.h
//! An interface for (pseudo) random number generators. Can be subclassed with any boost or std generator.
#include <vector>

// Internal includes
#include "../tools/tools.h"
#include "../tools/structures.h"
#include "../models/model_base.h"

class ModelBase;

#ifdef USE_RANDOM

#if BOOST_ENABLE == 1
#include <boost/random/mersenne_twister.hpp>
//#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#endif // BOOST_ENABLE

//! This class provides a wrapper for many different Prngs
//! Need to be subclassed
//! This class implements a wrapper for boost, c++ std random, well512 prngs and initialize the seed of each process with a random seed
//! from master (generated with ctime)
class Sampler {
public:
    //! Constructor, initialize the generator
    Sampler(Options * opt_, Attila *p_cm);
    //! Virtual destructor
    virtual ~Sampler() {
        delete[] sim_seeds;
    }
    //! Give the sampler access to the modelBase
    void set_model(ModelBase * mod_);

    //! Restart the generator according to the current sim_id
    void restart(uint sim_id);

    //! Setup the sampler after model has been initialized
    virtual PetscErrorCode setup(void) = 0;
    //! Print info
    PetscErrorCode print(void);

    //! Draw next number
    virtual double next(uint dim, uint stream) = 0;
    //! TODO: Draw next number applying normal transformation
    virtual double next_normal(uint dim, uint stream);
    //! Draw next number weight (used for quadratures)
    virtual double next_weight(void) { return 1; }

    //! The seed for the current domain
    int   seed;
    //! The seed for the current domain
    int * sim_seeds;

    //! Sampler size (number of simulations, number of streams and number of dimensions
    SamplerData size_;

    //! Sum of weights for each domain
    double * weights_per_deepness;
protected:
    //! Virtual member for initialization of seed, can be anything
    virtual PetscErrorCode init(int seed) = 0;

    //! Option context
    Options               * const opt;
    Attila                * const p_cm_;
    ModelBase             * mod;

    //! Number of parameters
    PetscInt              paramc;
    //! All parameters of the sampler settings (the first entry is the name)
    char **               paramv;
    //! Number of parameters
    int                   paramn;

    //! Parameter name array
    std::vector<std::string>    par;

    //! Interval data
    PetscReal a, b, A, B;

    //! Type of distribution selected (if possible to choose)
    enum Distribution { Uniform, Normal };

    Distribution  dist;
private:
    //! The root that BCasts the seed to ALL other processes
    int root;
    //! Temporary store a normal distr variable
    PetscReal   normal_buffer;
    //! Is the variable stored
    bool        is_normal_buffer_empty;

};

#endif // USE_RANDOM

