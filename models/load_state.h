/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

#include "../tools/utilities.h"

//! TODO: timer counter should be increased accordingly

//! @brief Load an initial state from a file
//! This class is used to load a previously broken ".bin" file. You should assure yourself that the data is the same
//! TODO: implement
class LoadState : public ModelBase  {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    LoadState(Options * p_opt)
    : ModelBase(p_opt) {

        par.push_back("prefix");
        if( p_opt_->paramc > ++paramn ) strcpy(prefix, p_opt_->paramv[paramn]);
        else strcpy(prefix, p_opt_->simulation_name);

        par.push_back("postfix");
        if( p_opt_->paramc > ++paramn ) strcpy(postfix, p_opt_->paramv[paramn]);
        else strcpy(postfix, "init");

        //     par.push_back("usesim");
        //     if( opt->paramc > ++paramn ) strcpy(postfix, opt->paramv[++paramn]);
        //     else strcpy(postfix, "init);
    }

    //! Compose the string an load the data
    PetscErrorCode load_with_component(std::string comp, Vec & V);

    //! Setup the vector for U_0, defaults to function-style
    PetscErrorCode set_U_0(Vec & V);
    //! Setup the vector for Gp_0, defaults to function-style
    PetscErrorCode set_Gp_0(Vec & V);
    //! Setup the vector for w_0, defaults to function-style
    PetscErrorCode set_w_0(Vec & V);
    //! Setup the vector for scalar, defaults to function-style
    PetscErrorCode set_scalar_0(Vec & V);
    //! Setup the vector for forcing, defaults to function-style
    PetscErrorCode set_F(Vec & V, PetscReal t);
private:
    // Used to contain the petsc fiever
    PetscViewer           viewer;

    //! Name data
    char                  prefix[PETSC_MAX_PATH_LEN], postfix[PETSC_MAX_PATH_LEN];
};

//! Setup the vector for U_0, defaults to function-style
PetscErrorCode LoadState::set_U_0(Vec & V) {

    ierr = load_with_component("U", V); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Setup the vector for Gp_0, defaults to function-style
PetscErrorCode LoadState::set_Gp_0(Vec & V) {

    ierr = load_with_component("Gp", V); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Setup the vector for w_0, defaults to function-style
PetscErrorCode LoadState::set_w_0(Vec & V) {

    ierr = load_with_component("w", V); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Setup the vector for scalar, defaults to function-style
PetscErrorCode LoadState::set_scalar_0(Vec & V) {

    ierr = load_with_component("ps", V); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Setup the vector for forcing, defaults to function-style
PetscErrorCode LoadState::set_F(Vec &, PetscReal) {

    ERROR(p_cm_, WORLD, "%sNo forcing load, yet!\n", "");

    PetscFunctionReturn(ierr);
}

//! Setup the vector for forcing, defaults to function-style
PetscErrorCode LoadState::load_with_component(std::string comp, Vec & V) {

    char bname[PETSC_MAX_PATH_LEN], fname[PETSC_MAX_PATH_LEN];
    sprintf(bname, "%s_%s_%s", prefix, p_dom_->mesh_str.c_str(), comp.c_str());

    DEBUG(p_cm_, WORLD, "Looking for variants of: \"%s\"!\n", bname);

    // TODO fixme
#if defined(BIN_OUT)
    sprintf(fname, "%s_sim%d_%s.bin", bname, p_cm_->sim_id, postfix);
    if( !exists(fname) ) sprintf(fname, "%s_%s.bin", bname, postfix);
    if( !exists(fname) ) {
        WARNING(p_cm_, WORLD, "%sFile \"%s\" not found!\n", fname);
        PetscFunctionReturn(ierr);
    }
    ierr = PetscViewerBinaryOpen(p_cm_->intra_domain_comm, fname, FILE_MODE_READ, &viewer); CHKERRQ(ierr);
#elif defined(VTS_OUT)
    ERROR(p_cm_, WORLD, "%sVTS loading not currently supported, switch to BIN!\n", "");
    sprintf(fname, "%s_sim%d_%s.vts", bname, p_cm_->sim_id, postfix);
    if( !exists(fname) ) sprintf(fname, "%s_%s.vts", bname, postfix);
    if( !exists(fname) ) {
        WARNING(p_cm_, WORLD, "%sFile \"%s\" not found!\n", fname);
        PetscFunctionReturn(ierr);
    }
    //   ierr = PetscViewerVTKOpen(p_cm_->intra_domain_comm, fname, FILE_MODE_READ, &viewer); CHKERRQ(ierr);
#elif defined(HDF_OUT)
    sprintf(fname, "%s_sim%d_%s.hdf", bname, p_cm_->sim_id, postfix);
    if( !exists(fname) ) sprintf(fname, "%s_%s.hdf", bname, postfix);
    if( !exists(fname) ) {
        WARNING(p_cm_, WORLD, "%sFile \"%s\" not found!\n", fname);
        PetscFunctionReturn(ierr);
    }
    //   ierr =  PetscViewerHDF5Open(p_cm_->intra_domain_comm, FILE_MODE_READ, &viewer); CHKERRQ(ierr);
#endif
    ierr = VecLoad(V, viewer); CHKERRQ(ierr);
    OUTPUT(p_cm_, WORLD, "%sLoaded: \"%s\"!\n", fname);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

