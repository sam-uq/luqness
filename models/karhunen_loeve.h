/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

#if  USE_RANDOM

// TODO: insert scaling, insert coiche of force to be zero-averaged

PetscReal bump(PetscReal x, PetscReal y) {
    return exp(- x * x - y * y);
}
PetscReal quadratic_zeroint(PetscReal x, PetscReal y) {
    return x * (1 - x) * y * (1 - y) - 1. / 36.;
    //   return 0;
    //   return sin(PI*x);
}
PetscReal quadratic(PetscReal x, PetscReal y) {
    return x * (1 - x) * y * (1 - y);
    //   return 0;
    //   return sin(PI*x);
}

//! KarhunenLoeve expansion in 2D for Vortex \n
//!  - Requirements: 2D Vortex with/without viscosity and periodic boundaries;
//!  - Provides: vorticity (2D)
//!  - Parameters: size and first eigenvalue
//!  - Domain: for doubly periodic 2D in [0,1]^2
//!  - Type: random
class KarhunenLoeve
        : public ModelBase {
    //! Default values
    //! Number of draws (size of KL)
    const long ORDER = 1L;
    //! First eigenvalue
    const double LAMBDA_1 = 1.;
    //! First eigenvalue
    const double ALPHA = 2.;
    //! Scaling of perturbation
    const double SCALING = PI;
    //! Scaling of perturbation
    const double X0 = 0.;
    //! Scaling of perturbation
    const double Y0 = 0.;
    //! Scaling of perturbation
    const bool FORCE_ZEROINT = false;
    //! Scaling of perturbation
    const double AMPLIFY = 1.;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    KarhunenLoeve(Options * p_opt)
    : ModelBase(p_opt) {
        push_param("N", N, ORDER);
        push_param("lambda_1", lambda_1, LAMBDA_1);
        push_param("alpha", alpha, ALPHA);
        push_param("scaling", scaling, SCALING);
        push_param("x_0", x_0, X0);
        push_param("y_0", y_0, Y0);
        push_param("force_zeroint", force_zeroint_, FORCE_ZEROINT);
        push_param("amplify", amplify_, AMPLIFY);

        par.push_back("meanfield");
        if( p_opt_->paramc > ++paramn && !strcmp(p_opt_->paramv[++paramn], "bump") ) {
            f = bump;
        } else {
            if( force_zeroint_ ) {
                f = quadratic_zeroint;
            } else {
                f = quadratic;
            }
        }

        provide_comp(CompType::omega);
        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Stochastic);

        // This setup the sampler to the correct dimension required by the model.
        smp_size_.dim += N;
        smp_size_.stream += 1;
    }

    //! Allocate/draw random array
    void sub_init()  {
        Y = new PetscReal[N];
        for(int s = 0; s < N; ++s) {
            // WARNING FIXME: we want the generated interval to be (-1,1), since rngs generate on (0,1) we scale. Gauss is
            // usually defined on (-1,1) but we rescale this on (0,1) to be consistend with rngs. That being said *every*
            // sampler here samples by default on (0,1) but this particular example is rescaled to (-1,1). You have been
            // warned!
            Y[s] = p_smp_->next(s, 0) * 2. - 1.;
        }
    }

    //! Vorticity
    PetscReal omega(PetscReal x, PetscReal y, PetscReal) {
        // Base field
        PetscReal r = f(x,y);
        for(int s = 0; s < N; ++s) {
            PetscReal w = sin( 2 * scaling * (s + 1) * (x - x_0)) * sin( 2 * scaling * (s + 1) * (y - y_0));
//             if(force_zeroint_ && s == 0) {
//                 w -= 4. / PI / PI;
//             }
            r += w * Y[s] / pow(lambda_1 * (s + 1), alpha);
        }
        return amplify_ * r;
    }

private:
    //! Representation size
    PetscInt           N;
    //! First eigenvalue
    PetscReal          lambda_1;
    //! Decay of sequence
    PetscReal          alpha;
    //! Scaling of perturbation
    PetscReal          scaling;
    //! Center of perturbation
    PetscReal          x_0, y_0;

    //! Contains the generated random number
    PetscReal          *Y;

    //! Keep initial data mean-free
    bool               force_zeroint_;

    //! Amplify solution by this factor
    PetscReal          amplify_;

    //! The mean function
    PetscReal (*f)(PetscReal, PetscReal);

};

#endif

