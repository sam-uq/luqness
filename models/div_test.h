/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Divergence test (implements divergence free vector field) \n
//!  - Requirements: 2D Projection with/without viscosity and periodic boundaries;
//!  - Provides: velocity (2D)
//!  - Parameters: none
//!  - Domain: for doubly periodic 2D in [0,1]^2
//!  - Type: non random
//! @brief Small divergence free vector field as initial data.
class DivTest : public ModelBase  {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    DivTest(Options * p_opt)
    : ModelBase(p_opt) { }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal) {
        return - 1 * cos(2 * PI * (x + y)) + 1 * cos(2 * PI * x);
    }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal) {
        return 1 * cos(2 * PI * (x + y)) + 1 * cos(2 * PI * y);
    }

};

