/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Test for Projection (perfturbated divergence free vector field) \n
//!  - Requirements: 2D Projection with/without viscosity and periodic boundaries;
//!  - Provides: velocity (2D)
//!  - Parameters: none
//!  - Domain: for doubly periodic 2D in [0,1]^2
//!  - Type: non random
class ProjTest : public ModelBase {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    ProjTest(Options * p_opt)
    : ModelBase(p_opt) { }

    //! Velocity x
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal) { return sin( PI * x) + sin( PI * x) * sin( PI * y); }

    //! Velocity y
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal) { return sin( PI * y) + sin( PI * x) * sin( PI * y); }
};
