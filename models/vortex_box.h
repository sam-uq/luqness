/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! VORTEX in a Box problem \n
//!  - Requirements: 2D Projection or Vortex, periodic boundaries;
//!  - Provides: velocity (2D), and vorticity (2D)
//!  - Parameters: none
//!  - Domain: periodic [0, 1]^2
//!  - Type: non random
class VortexBox : public ModelBase {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    VortexBox(Options * p_opt)
    : ModelBase(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);
    }

    //! Velocity x
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal) {
        return -2 * sin(PI * x) * sin(PI * x) * sin(PI * y) * cos(PI * y);
    }

    //! Velocity y
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal) {
        return 2 * sin(PI * x) * cos(PI * x) * sin(PI * y) * sin(PI * y);
    }

    //! Vorticity
    PetscReal omega(PetscReal x, PetscReal y, PetscReal) {
        return - PI * ( cos(2.0*PI*y) - cos(2.0*PI*(x+y)) - cos(2.0*PI*(y-x)) + cos(2.0*PI*x) );
    }

private:

};

