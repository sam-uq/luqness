/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "rnd_interface.h"

#ifdef USE_RANDOM

// PetscReal x_0 = 0.5;
// PetscReal y_0 = 0.5;
// PetscReal z_0 = 0;
// PetscReal R = 0.25;

//! Number of vortex cores
#define NCORES 2

//! List of vortex cores: contains coordinates (x,y) and radii
PetscReal core[NCORES][4] = {
    { 0.333333, 0.5, 0, 0.166666 },
    { 0.666666, 0.5, 0, 0.166666 }
    //   , { 0.5, 0.75, 0, 0.05 }
};


const static bool reflect[] = {false, true};

//! Model the doubly periodic and continuous shear layer for a rapidly changing velocity in the x direction. \n
//!  - Requirements:
//!  - Provides:
//!  - Parameters:
//!  - Domain:
//!  - BCs:
//!  - Type:
//! @tparam disc Choose a discontinuous layer or not
template <bool disc>
class VortexCore : public RandomInterface {
    //! Default values
    //! Dirac thickness, higher means thicker
    const double  RHO = 1.0 / 30.;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    VortexCore(Options * p_opt)
            : ModelBase(p_opt), RandomInterface(p_opt)
        , rho(RHO) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        par.push_back("rho");
        if( p_opt_->paramc > ++paramn ) rho = atof(p_opt_->paramv[paramn]);

        provide_comp(CompType::omega);
        provide_comp(CompType::U);
        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Stochastic);
    }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) {
        //     PetscReal f = 0, r = cart2radius(x - x_0, y - y_0, z - z_0) / R;
        //
        //     if( r >= 0.25 && r <= 0.5 )
        //       f = (r - 0.25) * 2 * PI;
        //     else if( r >= 0.5 && r <= 1)
        //       f = PI / 2;

        PetscReal f = 0, t = 0;
        for(int i = 0; i < NCORES; ++i) {
            double coordx = (reflect[i] == true ? -1 : 1) * (x - core[i][0]);
            PetscReal r = cart2radius(coordx, y - core[i][1], z - core[i][2]) / core[i][3];
            PetscReal theta = cart2angle<0>(coordx, y - core[i][1], z - core[i][2]);
            if(reflect[i] == true) {
                r -= atx(theta);
            } else {
                r += atx(theta);
            }

            if( r >= 0.25 && r <= 0.5 ) {
                f = (r - 0.25) * 2 * PI;
                t += - (y - core[i][1]) * f;
            } else if ( disc && r >= 0.5 && r <= 1) {
                f = PI / 2;
                t +=  - (y - core[i][1]) * f;
            } else if ( !disc && r >= 0.5) {
                f = PI / 2 * (tanh((-r + 1) / rho )  + 1) * 0.5;
                t +=  - (y - core[i][1]) * f;
            }

        }
        return t;
    }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal z)
    {
        //     PetscReal f = 0, r = cart2radius(x - x_0, y - y_0, z - z_0) / R;
        //
        //     if( r >= 0.25 && r <= 0.5 )
        //       f = (r - 0.25) * 2 * PI;
        //     else if( r >= 0.5 && r <= 1)
        //       f = PI / 2;
        PetscReal f = 0, t = 0;
        for(int i = 0; i < NCORES; ++i) {
            double coordx = (reflect[i] == true ? -1 : 1) * (x - core[i][0]);
            PetscReal r = cart2radius(coordx, y - core[i][1], z - core[i][2]) / core[i][3];
            PetscReal theta = cart2angle<0>(coordx, y - core[i][1], z - core[i][2]);
            if(reflect[i] == true) {
                r -= atx(theta);
            } else {
                r += atx(theta);
            }

            if( r >= 0.25 && r <= 0.5 ) {
                f = (r - 0.25) * 2 * PI;
                t += (x - core[i][0]) * f;
            } else if ( disc && r >= 0.5 && r <= 1) {
                f = PI / 2;
                t += (x - core[i][0]) * f;
            } else if ( !disc && r >= 0.5) {
                f = PI / 2 * (tanh((-r + 1) / rho ) + 1) * 0.5;
                t += (x - core[i][0]) * f;
            }

        }
        return t;
    }

    //! Velocity z component
    PetscReal w_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }

    //! Vorticity 2D z-component
    PetscReal omega(PetscScalar, PetscScalar, PetscScalar) { return 0; }

    //! Passive tracer
    PetscReal coloring(PetscScalar, PetscScalar, PetscScalar) { return 0; }

protected:
    //! y-component perturbation
    PetscReal  rho;

};

#endif // USE_RANDOM
