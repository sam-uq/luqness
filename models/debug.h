/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Default values
//! None

//! Benchmarks useful for debugging, constants, rank-based-contant, cross, etc...
//! Unfomment the requested function
class Debug : public virtual ModelBase {
public:
    //! No parameters for this debug class, funcitonalities are compile time only.
    //!
    Debug(Options * p_opt)
    : ModelBase(p_opt) {

        // Read parameters
        //        push_param("delta", delta, DELTA);
    }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal, PetscReal) {
//         return p_cm_->sim_id;
        return x;
//         return 3;
    };

    //! Velocity y component
    PetscReal v_0(PetscReal, PetscReal y, PetscReal) {
//         return - p_cm_->sim_id;
//         return 4;
        return y;
    };

    //! Velocity z component
    PetscReal w_0(PetscReal, PetscReal, PetscReal) {
//         return - 3 * p_cm_->sim_id;
        return p_cm_->sim_id;
    };

    //! Vorticity 2D z-component
    PetscReal omega(PetscReal x, PetscReal y, PetscReal) {
        //    return 99;
        //     return p_cm_->world_rank;
        return ( x > 0.2 && x < 0.8 && y > 0.4 && y < 0.6 ) || ( y > 0.2 && y < 0.8 && x > 0.4 && x < 0.6 ) ? 1 : 0;
    }

    //! Passive tracer
    PetscReal coloring(PetscReal, PetscReal, PetscReal) {
        return 0;
    }

protected:
    static const int provides_comp = 1;
};

