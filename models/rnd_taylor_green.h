/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "shear_layer.h"
#include "rnd_interface.h"

#ifdef USE_RANDOM

//!
class RndTaylorGreen : public TaylorGreen, public RandomInterface {
    //! Scaling factor for y axis (scaling equal to one means periodicity of 1)
    const double SCALING = 1.0;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RndTaylorGreen(Options *p_opt)
            : ModelBase(p_opt),
              TaylorGreen(p_opt),
              RandomInterface(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        TaylorGreen::provide_mode(QuadratureType::Stochastic);

        push_param("scaling", scaling, SCALING);
        factor = 2 * PI / scaling;
    }

    ~RndTaylorGreen() {}

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) {
        return TaylorGreen::u_0(x, y, z);
    }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal z) {
        return TaylorGreen::u_0(x, y, z);
    }

    //! Velocity z component
    PetscReal w_0(PetscReal x, PetscReal, PetscReal) {
        return atx(x * this->factor);
    }

    //! Scaling factor for the x-axis
    PetscReal scaling;
    //! Precomputed factor (aka 2 * PI / scaling) (just to e more efficient?)
    PetscReal factor;
};

#endif
