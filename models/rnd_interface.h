/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

#ifdef USE_RANDOM

//!
//!
class RandomInterface : public virtual ModelBase {
    //! Default values
    //! Number of expansions for the interface
    const PetscInt ORDER = 10L;
    //! Number of expansions for the interface
    const double PERTURBATION = 0.01;
    //! Shift the interface
    const double SHIFT = 0.01;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RandomInterface(Options *p_opt)
    : ModelBase(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);
        shift = 0;

        // Read parameters
        push_param("N", N, ORDER);
        push_param("Pert.", perturbation, PERTURBATION);
        push_param("Shift", shift, SHIFT);

        assert( N >= 1 );
        assert( perturbation >= 0 );

        // TODO TODO
        smp_size_.dim += N;
        smp_size_.stream += 2;

        a_k.resize(N);
        b_k.resize(N);
    }

    virtual ~RandomInterface() {}

    //! Initialize arrays
    void sub_init()  {
        PetscReal sum = 0;
        for(PetscInt s = 0; s < N; ++s) {
            a_k[s] = p_smp_->next(s, 0);
            sum += a_k[s] * a_k[s];
            b_k[s] = 2 * PI * p_smp_->next(s, 1) - shift;
        }
        for(PetscInt s = 0; s < N; ++s) {
            // WARNING: since you get this always wrong: perturbation OUTSIDE sqrt gives a L^2-type scaling, in fact if
            // \sum a_k^2 = sum ==> \sum a_k^2 / ((\sum a_k^2)^{1/2} / pert)^2 = pert^2
            a_k[s] *= std::sqrt(perturbation / sum);
        }
    }

protected:
    //! Interface modifier: perturbation as sum of sin with varying amplitude and phase.
    //! The periodicity of this function is \f$2 \pi\f$ due to \f$s = 1\f$.
    inline PetscReal atx(PetscReal x) {
        PetscReal interface = 0;
        for(int s = 0; s < N; s++) {
            interface += a_k[s] * std::sin( (s+1) * x - b_k[s]);
        }
        return interface;
    }

    //! Perturbation depth
    PetscInt   N;
    //! Perturbation size
    PetscReal  perturbation;
    //! Left shift of perturbation
    PetscReal shift;
    //! Random array for amplitude
    std::vector<PetscReal>    a_k;
    //! Random array for shift
    std::vector<PetscReal>    b_k;
};

#endif
