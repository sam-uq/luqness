/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef RND_PERT_H
#define RND_PERT_H

// #include <iostream>

#include "model_base.h"
#include "rnd_interface.h"

#ifdef USE_RANDOM

//! Default values
//! Number of expansions for the interface
#undef                  PERTYPE
#define                 PERTYPE         "radial"

//! Center of radial perturbation
PetscReal C[] = {0.5,0.5,0.5};

//! Define a random perturbatio of the data
//!
class RndPert : public RandomInterface {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RndPert(Options * p_opt)
            : ModelBase(p_opt), RandomInterface(p_opt) {

        // Read parameters
        push_param("Pert. type", pertype, std::string(PERTYPE));

        assert( N >= 1 );

    }

    ~RndPert() {
    }

protected:
    //! Interface modifier
    inline void pert(PetscReal x, PetscReal y, PetscReal, PetscReal & x_, PetscReal & y_, PetscReal & z_) {

        PetscReal r = cart2radius(x - C[0], y - C[1], 0);
        PetscReal theta = cart2angle<0>(x - C[0], y - C[1], 0);
//         std::cout << "AAA r = " << r << " theta = " << theta << " x = " << x << " y = " << y << std::endl;
        r += atx(theta);
//         std::cout << "AAA r = " << r << " theta = " << r << std::endl;
        pol2cart(r, theta, x_, y_);
        z_ = 0;
        x_ += C[0];
        y_ += C[1];
        return;
    }

    //! Perturbation type
    std::string       pertype;
};

#endif

#endif // RND_PERT_H
