/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "shear_layer.h"

#ifdef  USE_RANDOM

//! Random generated shear layer with perturbed velocities \n
//!  - Requirements: 2D Projection with/without viscosity and passive tracer, periodic boundaries;
//!  - Provides: velocity (2D), passive tracer (2D)
//!  - Parameters: perturbation size, thickness, position of interfaces and scaling (from base), number of draws;
//!  - Domain: for doubly periodic 2D from 0 to POS * 2 in positive direction and from 0 to SCALING in y direction,
//!    periodic BC; for single layer from -X to X (arbitrary) and outflow/slip;
//!  - Type: random
template <bool disc, bool single>
class RndPertShearLayer : public ShearLayer<disc, single> {
    //! Default values
    //! Number of expansions for the interface
    const int ORDER = 10;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RndPertShearLayer(Options * p_opt)
        : ModelBase(p_opt)
        , ShearLayer<disc, single>(p_opt)
        , N(ORDER) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        N = atoi(this->p_opt_->paramv[++this->paramn]);

        this->smp_size_.dim += N;
        this->smp_size_.stream += 4;

        ShearLayer<disc, single>::provide_dim(DimType::d2);
        ShearLayer<disc, single>::provide_mode(QuadratureType::Stochastic);
    }

    //! Generate perturbation arrays
    // TODO Remove
    void sub_init()  {

        PetscReal sum_u = 0, sum_v = 0;
        for(int s = 0; s < N; s++) {
            a_u_k[s] = this->p_smp_->next(0, 0);
            a_v_k[s] = this->p_smp_->next(0, 1);
            sum_u += a_u_k[s];
            sum_v += a_v_k[s];
            b_u_k[s] = PI * this->p_smp_->next(0, 2);
            b_v_k[s] = PI * this->p_smp_->next(0, 3);
        }
        for(int s = 0; s < N; s++) {
            a_u_k[s] /= sum_u;
            a_v_k[s] /= sum_v;
        }
    }

    //! Velocity x-comp
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal prt_u = 0;
        for(int s = 0; s < N; s++) {
            prt_u += this->delta * a_u_k[s] * sin( b_u_k[s] + PI * s * y);
        }
        return ShearLayer<disc, single>::u_0(x,y,z) + prt_u;
    }

    //! Velocity y-comp
    PetscReal v_0(PetscReal x, PetscReal, PetscReal) {
        PetscReal prt_v = 0;
        for(int s = 0; s < N; s++) {
            prt_v += this->delta * a_v_k[s] * sin( b_v_k[s] + PI * s * x);
        }
        return prt_v;
    }

    //! Velocity x-comp
    PetscReal omega(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal prt_u = 0;
        for(int s = 0; s < N; s++) {
            prt_u += this->delta * a_u_k[s] * sin( b_u_k[s] + PI * s * y);
        }
        return ShearLayer<disc, single>::omega(x,y,z) + prt_u;
    }

private:
    int                N;

    PetscReal          *a_u_k;
    PetscReal          *b_u_k;
    PetscReal          *a_v_k;
    PetscReal          *b_v_k;
};

#endif
