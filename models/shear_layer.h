/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Model the doubly periodic and continuous shear layer for a rapidly changing velocity in the x direction. \n
//!  - Requirements: 2D Vortex or Projection with/without viscosity and passive tracer, periodic boundaries;
//!  - Provides: velocity (3D), vorticity(2D), passive tracer (2D)
//!  - Parameters: perturbation size, thickness, position of interfaces and scaling
//!  - Domain: for doubly èeropdoc2D from 0 to POS * 2 in positive direction and from 0 to SCALING in y direction,
//!    periodic BC; for single layer from -X to X (arbitrary) and outflow/slip
//!  - Type: non random
template <bool disc, bool single>
    class ShearLayer : public virtual ModelBase {
    //! Default values
    //! Perturbation size (max of y-component)
    const double DELTA = 0.05;
    //! Layer thickness, higher means thicker (BCG uses 1/30)
    const double RHO =  1.0 / 30.0;
    //! Layer center position
    const double POS = 0.5;
    //! Scaling factor for y axis (scaling equal to one means periodicity of 1)
    const double SCALING = 1.0;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    ShearLayer(Options * p_opt)
    : ModelBase(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        // Read parameters
        push_param("delta", delta, DELTA);
        push_param("rho", rho, RHO);
        push_param("pos", pos, POS);
        push_param("scaling", scaling, SCALING);

        disc_r = false;
        if(!disc && rho < M_EPSILON) {
            ERROR(p_cm_, WORLD,
                  "Cannot use smooth layer with no rho == 0.\n", "");
            disc_r = true;
        }

        provide_comp(CompType::U | CompType::omega | CompType::ps);
        provide_dim(DimType::d2 | DimType::d3);
        provide_mode(QuadratureType::Deterministic);

        factor = 2 * PI / scaling;
    }

    //! Velocity x component
    PetscReal u_0(PetscReal, PetscReal y, PetscReal) {
        if( disc || disc_r ) {
            if( single ) {
                return y >= 0 ? 1 : -1;
            } else {
                return ( y <= pos * 0.5 || y >= pos * 1.5 ) ? 1 : -1;
            }
        } else {
            if( single ) {
                return tanh(y  / rho);
            } else {
                return (y <= pos ? tanh((y -  pos * 0.5) / rho) : tanh(( pos * 1.5 - y) / rho));
            }
        }
    }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal, PetscReal) { return delta * sin(factor * x); }

    //! Velocity z component
    PetscReal w_0(PetscReal, PetscReal, PetscReal) { return 0; }

    //! Vorticity 2D z-component
    PetscReal omega(PetscReal x, PetscReal y, PetscReal) {
        if( single ) {
            PetscReal sec = sech(y  / rho);
            return sec * sec / rho;
        } else {
            if( y <= pos ) {
                PetscReal sec = sech((y -  pos * 0.5) / rho);
                return sec * sec / rho + delta * factor * cos(factor * x);
            } else {
                PetscReal sec = sech(( pos * 1.5 - y) / rho);
                return - sec * sec / rho + delta * factor * cos(factor * x);
            }
        }
    }

    //! Passive tracer
    PetscReal coloring(PetscReal, PetscReal y, PetscReal) {
        if( single ) {
            return y <= 0. ? -1. : 1.;
        } else {
            return (y <= pos * 0.5 || y >= pos * 1.5 ? -1 : 1);
        }
    }

protected:
    //! Layer amplitude
    PetscReal  delta;
    //! y-component perturbation
    PetscReal  rho;
    //! Position of the middle shear
    PetscReal  pos;
    //! Scaling factor for the x-axis
    PetscReal  scaling;
    //! Precomputed factor (aka 2 * PI / scaling) (just to e more efficient?)
    PetscReal  factor;
    //   //! Discontinuous layer
    //   bool       disc;
    //   //! TODO: Use a single layer
    //   bool       single;
    bool disc_r;

};
