/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"


PetscReal cross(PetscReal x, PetscReal y, PetscReal) {
    return ( x > 0.2 && x < 0.8 && y > 0.4 && y < 0.6 ) || ( y > 0.2 && y < 0.8 && x > 0.4 && x < 0.6 ) ? 1 : 0;
}

template <bool remove_average>
PetscReal circle(PetscReal x, PetscReal y, PetscReal) {
    float R = 0.4;
    float x_0 = .5, y_0 = .5;
    if(remove_average) {
        double area = R * R * PI;
        area /= (1 - area);
        return (x-x_0)*(x-x_0)+(y-y_0)*(y-y_0) < R*R ? 1 : -area;
    }
    else return  (x-x_0)*(x-x_0)+(y-y_0)*(y-y_0) < R*R;
}

int sam_patch_size_x = 13;
int sam_patch_size_y = 7;
PetscReal sam_patch[7*13] = {
    0, 0,0,0, 0, 0,0,0, 0, 0,0,0, 0,

    0, 1,1,1, 0, 1,1,1, 0, 1,0,1, 0,
    0, 1,0,0, 0, 1,0,1, 0, 1,1,1, 0,
    0, 1,1,1, 0, 1,1,1, 0, 1,0,1, 0,
    0, 0,0,1, 0, 1,0,1, 0, 1,0,1, 0,
    0, 1,1,1, 0, 1,0,1, 0, 1,0,1, 0,

    0, 0,0,0, 0, 0,0,0, 0, 0,0,0, 0
};

// int donut_patch_size_x = 5;
// int donut_patch_size_y = 5;
// PetscReal donut_patch[5*5] = {
//  0, 0,0,0, 0,
//
//  0, 1,1,1, 0,
//  0, 1,0,1, 0,
//  0, 1,1,1, 0,
//
//  0, 0,0,0, 0,
// };

int donut_patch_size_x = 7;
int donut_patch_size_y = 7;
PetscReal donut_patch[7*7] = {
    0, 0,0,0,0,0, 0,

    0, 1,1,1,1,1, 0,
    0, 1,0,0,0,1, 0,
    0, 1,0,0,0,1, 0,
    0, 1,0,0,0,1, 0,
    0, 1,1,1,1,1, 0,

    0, 0,0,0,0,0, 0,
};

//!
//! \brief The bit_patch class allows to generate indicator
//! inidial data from an array of data.
//!
class bit_patch {
public:
    bit_patch() {};
    bit_patch(PetscReal size_x, PetscReal size_y)
        : size_x_(size_x), size_y_(size_y) {

    }

    void set_patch(int patch_size_x, int patch_size_y, PetscReal const *  bit_mask) {
        patch_size_x_ = patch_size_x;
        patch_size_y_ = patch_size_y;
        bit_mask_ = bit_mask;
    }


    int get_coord(PetscReal x, PetscReal y, PetscReal) {
        return floor(x / size_x_ * patch_size_x_) + patch_size_x_ * (patch_size_y_ - ceil(y / size_y_ * patch_size_y_));
    }

    PetscReal patch(PetscReal x, PetscReal y, PetscReal z) {
        return bit_mask_[get_coord(x,y,z)];
    }

    PetscReal size_x_, size_y_;

    int patch_size_x_, patch_size_y_;
    PetscReal const * bit_mask_;
};

//! Default values
//! None

//! Benchmarks useful for debugging, constants, rank-based-contant, cross, etc...
//! Unfomment the requested function
class VortexPatch : public virtual ModelBase {
    //! Default values
    //! Functional of Bitmasked data
    const bool FUNCTIONAL = true;
    //! Patch to be used
    const std::string NAME = "circle";
public:
    //! No parameters for this debug class, funcitonalities are compile time only.
    //!
    VortexPatch(Options * p_opt)
    : ModelBase(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        push_param("Functional", functional_, FUNCTIONAL);
        push_param("Name", name_, NAME);

        provide_comp(CompType::omega | CompType::ps);
        provide_dim(DimType::d2);
        //         require_bc(BCType::Periodic || BCType::Zero);
        //         require_stat(StatType::Det);

        patch = circle<false>;
        if(functional_) {
            if(name_ == "circle") {
                patch = circle<false>;
            } else if (name_ == "cross") {
                patch = cross;
            }
        } else {
            p_ = bit_patch(p_opt_->domain_array[1] - p_opt_->domain_array[0],
                           p_opt_->domain_array[3] - p_opt_->domain_array[2]);
            if(name_ == "sam") {
                p_.set_patch(sam_patch_size_x, sam_patch_size_y, sam_patch);
            } else if(name_ == "donut") {
                p_.set_patch(donut_patch_size_x, donut_patch_size_y, donut_patch);
            }
        }

    }

    //! Velocity x component
    PetscReal u_0(PetscScalar, PetscScalar, PetscScalar) {
        return 0;
    };

    //! Velocity y component
    PetscReal v_0(PetscScalar, PetscScalar, PetscScalar) {
        return 0;
    };

    //! Velocity z component
    PetscReal w_0(PetscScalar, PetscScalar, PetscScalar) {
        return 0;
    };

    //! Vorticity 2D z-component
    PetscReal omega(PetscReal x, PetscReal y, PetscReal z) {
        // FIXME horrible
        if(functional_) {
            return patch(x,y,z);
        } else {
            return p_.patch(x,y,z);
        }
    }

    //! Passive tracer
    PetscReal coloring(PetscReal x, PetscReal y, PetscReal z) {
        return patch(x,y,z);
    }

protected:
    bool functional_;

    std::string name_;

    bit_patch p_;

    static const int provides_comp = 1;

    PetscReal (*patch)(PetscReal, PetscReal, PetscReal);
};

