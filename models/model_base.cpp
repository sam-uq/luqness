/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "model_base.h"

// All types of initial data need to be included here: this is because we need to factory them all
// Standard Benchmarks
#include "taylor_green.h"
#include "shear_layer.h"
#include "vortex_box.h"
#include "vortex_core.h"
#include "vortex_patch.h"
// Inflow/Outflow
#include "driven_cavity.h"
#include "vortex_stream.h"
// Testing Benchmarks/References
#include "reference.h"
#include "steady_state.h"
#include "proj_test.h"
#include "div_test.h"
#include "debug.h"
// #include "custom.h"
// Random data / UQ
#include "rnd_int_shear_layer.h"
#include "rnd_pert_shear_layer.h"
#include "karhunen_loeve.h"
#include "rnd_patch.h"
#include "rnd_taylor_green.h"
// Others
#include "load_state.h"

//! Given a string c, check if it can be casted to (using keywords), something that is default
//!
bool is_default(char * c) {
    if( !strcmp(c, "null") || !strcmp(c, "default") ) {
        return true;
    }
    return false;
}

#if CXX11_ENABLE
#include <string>
inline void param_to_val_c11(char * c, int & i) { i = std::stoi(c); }
inline void param_to_val_c11(char * c, double & i) { i = std::stof(c); }
inline void param_to_val_c11(char * c, bool & i) {
    if( !str_to_bool(c, i)  ) {
        i = (bool) std::stoi(c);
    }
}
inline void param_to_val_c11(char * c, std::string & i) {
    i = param_to_val<std::string>(c);
}
#endif // CXX11_ENABLE

//! Restart the model for a new simulation (setup sim id and regenerate random numbers)
//!
void ModelBase::init(uint sim_id) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    p_smp_->restart(sim_id);

    sub_init();
}

//!
//!
void ModelBase::check(void) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    if( !check_provides_dim(DimType::d3) && p_opt_->enable_3d ) {
        WARNING("", WORLD, "Requesting 3D simulation on model NOT providing no 3D data.\n", "");
    } else if ( !check_provides_dim(DimType::d2) && !p_opt_->enable_3d ) {
        WARNING("", WORLD, "Requesting 2D simulation on model NOT providing no 2D data.\n", "");
    } else {
        VERBOSE("", WORLD, "Model is compatible with requested dimension.\n", "");
    }


    // CHeck provide mode
    if(!p_opt_->dom_provided & this->provides_dom) {
        memcpy(p_opt_->domain_array, this->provided_dom, 6 * sizeof(PetscReal));
        WARNING("", WORLD, "Boundaries are set to default ones for this model.\n", "");
    }
    if(!p_opt_->boundaries_provided & this->provides_bdry) {

    }
}

//!
//!
PetscErrorCode ModelBase::print(void) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    int i = 0;
    DEBUG(p_cm_, WORLD, "Model %d (name = %s) parameters:\n",
          p_opt_->paramc, p_opt_->paramv[i]);
    for(std::vector<std::string>::iterator it = par.begin() ; it != par.end(); ++it) {
        ++i;
        DEBUG(p_cm_, WORLD, "%10s%20s\n", it->c_str(), p_opt_->paramv[i]);
    }

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode ModelBase::set_U_0(Vec & V) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal     x, y, z = 0;
    Vec           coords;
    DM            cda;

    if( !check_provides_comp(CompType::U) ) {
        WARNING(p_cm_, WORLD, "Seems you model isn't providing the component "
                              "you requested, mismatched model?\n", "");
    }

    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) { // IF YOU WANT THE 3D
        DMDACoor3d         ***coords_array;
        Field3D            ***V_array;
        Volume3D           ***d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    x = coords_array[k][j][i].x + d[k][j][i].x / 2.;
                    y = coords_array[k][j][i].y + d[k][j][i].y / 2.;
                    z = coords_array[k][j][i].z + d[k][j][i].z / 2.;

                    V_array[k][j][i][0] = u_0(x,y,z);
                    V_array[k][j][i][1] = v_0(x,y,z);
                    V_array[k][j][i][2] = w_0(x,y,z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    } else { /// IF YOU WANT TO RESTRICT TO 2D
        DMDACoor2d   **coords_array;
        Field2D      **V_array;
        Volume2D     **d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                x = coords_array[j][i].x + d[j][i].x / 2.;
                y = coords_array[j][i].y + d[j][i].y / 2.;

                //               OUTPUT(PETSC_COMM_WORLD, "%s(%f, %f)\n", x, y);
                V_array[j][i][0] = u_0(x, y, z);
                V_array[j][i][1] = v_0(x, y, z);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode ModelBase::set_w_0(Vec & V) {
    FUNCTIONBEGIN(p_cm_);

    if( !check_provides_comp(CompType::omega) ) {
        WARNING(p_cm_, WORLD, "Seems you model isn't providing the component "
        "omega (vorticity) you requested, mismatched model?\n", "");
    }

    PetscReal     x, y, z = 0;
    Vec           coords;
    DM            cda;

    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) { // IF YOU WANT THE 3D
        DMDACoor3d         ***coords_array;
        PetscReal          ***V_array;
        Volume3D           ***d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    x = coords_array[k][j][i].x + d[k][j][i].x / 2.;
                    y = coords_array[k][j][i].y + d[k][j][i].y / 2.;
                    z = coords_array[k][j][i].z + d[k][j][i].z / 2.;

                    V_array[k][j][i] = omega(x, y, z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    } else { /// IF YOU WANT TO RESTRICT TO 2D
        DMDACoor2d   **coords_array;
        PetscReal    **V_array;
        Volume2D     **d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                x = coords_array[j][i].x + d[j][i].x / 2.;
                y = coords_array[j][i].y + d[j][i].y / 2.;

                //               OUTPUT(PETSC_COMM_WORLD, "%s(%f, %f)\n", x, y);
                V_array[j][i] = omega(x, y, z);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode ModelBase::set_Gp_0(Vec & V) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal     x = 0, y = 0, z = 0;
    Vec           coords;
    DM            cda;

    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);

    // TODO: In future we may want a function that takes care of this, since it's used everywhere.
    if( p_opt_->enable_3d ) { // IF YOU WANT THE 3D
        DMDACoor3d    ***coords_array;
        Field3D       ***V_array;
        Volume3D      ***d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; k++) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                    x = coords_array[k][j][i].x + d[k][j][i].x / 2.;
                    y = coords_array[k][j][i].y + d[k][j][i].y / 2.;
                    z = coords_array[k][j][i].z + d[k][j][i].z / 2.;

                    V_array[k][j][i][0] = g_p_x_0(x,y,z);
                    V_array[k][j][i][1] = g_p_y_0(x,y,z);
                    V_array[k][j][i][2] = g_p_z_0(x,y,z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    } else { /// IF YOU WANT TO RESTRICT TO 2D
        DMDACoor2d    **coords_array;
        Field2D       **V_array;
        Volume2D      **d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                x = coords_array[j][i].x + d[j][i].x / 2.;
                y = coords_array[j][i].y + d[j][i].y / 2.;

                V_array[j][i][0] = g_p_x_0(x,y,z);
                V_array[j][i][1] = g_p_y_0(x,y,z);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);

}

//!
//!
PetscErrorCode ModelBase::set_scalar_0(Vec & V) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal     x, y, z = 0;
    Vec           coords;
    DM            cda;

    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) { // IF YOU WANT THE 3D
        DMDACoor3d         ***coords_array;
        PetscReal          ***V_array;
        Volume3D                ***d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    x = coords_array[k][j][i].x + d[k][j][i].x / 2.;
                    y = coords_array[k][j][i].y + d[k][j][i].y / 2.;
                    z = coords_array[k][j][i].z + d[k][j][i].z / 2.;

                    V_array[k][j][i] = coloring(x, y, z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    } else { /// IF YOU WANT TO RESTRICT TO 2D
        DMDACoor2d   **coords_array;
        PetscReal    **V_array;
        Volume2D                **d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                x = coords_array[j][i].x + d[j][i].x / 2.;
                y = coords_array[j][i].y + d[j][i].y / 2.;

                V_array[j][i] = coloring(x, y, z);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);

}

//!
//!
PetscErrorCode ModelBase::set_F(Vec & V, PetscReal t) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal     x = 0, y = 0, z = 0;
    Vec           coords;
    DM            cda;

    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);

    // TODO: In future we may want a function that takes care of this, since it's used everywhere.
    if( p_opt_->enable_3d ) { // IF YOU WANT THE 3D
        DMDACoor3d    ***coords_array;
        Field3D       ***V_array;
        Volume3D      ***d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; k++) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                    x = coords_array[k][j][i].x + d[k][j][i].x / 2.;
                    y = coords_array[k][j][i].y + d[k][j][i].y / 2.;
                    z = coords_array[k][j][i].z + d[k][j][i].z / 2.;

                    V_array[k][j][i][0] = F_x(x, y, z, t);
                    V_array[k][j][i][1] = F_y(x, y, z, t);
                    V_array[k][j][i][2] = F_z(x, y, z, t);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    } else { /// IF YOU WANT TO RESTRICT TO 2D
        DMDACoor2d    **coords_array;
        Field2D       **V_array;
        Volume2D      **d;
        ierr = DMDAVecGetArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                x = coords_array[j][i].x + d[j][i].x / 2.;
                y = coords_array[j][i].y + d[j][i].y / 2.;

                V_array[j][i][0] = F_x(x, y, z, t);
                V_array[j][i][1] = F_y(x, y, z, t);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, V, &V_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);

}

// ghost_update_2d(Field2D **array, int k, int j, int i);

template <class Field>
class lambdaForcing {
public:
    //!
    //! \brief lambdaForcing
    //! \param ptr
    //!
    lambdaForcing(ModelBase * ptr) : ptr(ptr) {}

    void operator()(Field & outer, Field & inner,
            const Field & inner2, const Volume3D & coord) {
        ptr->ForcingM(outer, inner, inner2, coord);
    }
private:
    ModelBase * const ptr;
};

//! Update a VecField quantity with the wanted boundary conditions
//! TODO: batch the function and iterate
PetscErrorCode ModelBase::update_bd_field(Vec & U, const BD & bd,
                                          PetscReal) {
    FUNCTIONBEGIN(p_cm_);

    Field2D                **U_array;
    ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
    //// ONLY FOR INFLOW
    Vec           coords;
    DMDACoor2d    **coords_array;
    DM            cda;
    Volume2D      **volume;
    ierr = DMGetCoordinatesLocal(p_dom_->da, &coords); CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(p_dom_->da, &cda); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(cda, coords, &coords_array);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &volume); CHKERRQ(ierr);

    lambdaForcing<Field2D> Forcing(this);

    if(isPhysical(Left)) {
        switch(bd.left()) {
            case BCType::NoSlip:
                applyBoundaryLoop(NoSlipLR, Left,
                                  U_array, coords_array, volume);
                break;
            case BCType::Slip:
                applyBoundaryLoop(SlipLR, Left,
                                  U_array, coords_array, volume);
                break;
            case BCType::Outflow:
                applyBoundaryLoop(Outflow, Left,
                                  U_array, coords_array, volume);
                break;
            case BCType::Inflow:
                applyBoundaryLoop(Forcing, Left,
                                  U_array, coords_array, volume);
                break;
            default:
                break;
        }
    }

    if(isPhysical(Right)) {
        switch(bd.right()) {
            case BCType::NoSlip:
                applyBoundaryLoop(NoSlipLR, Right,
                                  U_array, coords_array, volume);
                break;
            case BCType::Slip:
                applyBoundaryLoop(SlipLR, Right,
                                  U_array, coords_array, volume);
                break;
            case BCType::Outflow:
                applyBoundaryLoop(Outflow, Right,
                                  U_array, coords_array, volume);
            case BCType::Inflow:
                applyBoundaryLoop(Forcing, Right,
                                  U_array, coords_array, volume);
                break;
            default:
                break;
        }
    }

    if(isPhysical(Bottom)) {
        switch(bd.bottom()) {
            case BCType::NoSlip:
                applyBoundaryLoop(NoSlipBT, Bottom,
                                  U_array, coords_array, volume);
                break;
            case BCType::Slip:
                applyBoundaryLoop(SlipBT, Bottom,
                                  U_array, coords_array, volume);
                break;
            case BCType::Outflow:
                applyBoundaryLoop(Outflow, Bottom,
                                  U_array, coords_array, volume);
                break;
            case BCType::Inflow:
                applyBoundaryLoop(Forcing, Bottom,
                                  U_array, coords_array, volume);
                break;
            default:
                break;
        }
    }

    if(isPhysical(Top)) {
        switch(bd.top()) {
            case BCType::NoSlip:
                applyBoundaryLoop(NoSlipBT, Top,
                                  U_array, coords_array, volume);
                break;
            case BCType::Slip:
                applyBoundaryLoop(SlipBT, Top,
                                  U_array, coords_array, volume);
                break;
            case BCType::Outflow:
                applyBoundaryLoop(Outflow, Top,
                                  U_array, coords_array, volume);
                break;
            case BCType::Inflow:
                applyBoundaryLoop(Forcing, Top,
                                  U_array, coords_array, volume);
                break;
            default:
                break;
        }
    }

    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &volume); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Update boundaries for a scalar.
//! FIXME: very ugly
PetscErrorCode ModelBase::update_bd_scalar(Vec & U, const BD & bd, PetscReal) {
    FUNCTIONBEGIN(p_cm_);

    // TODO 3D
    PetscErrorCode        ierr;
    PetscReal             **U_array;
    int nx = p_dom_->mesh_size[0], ny = p_dom_->mesh_size[1]; //, nz = p_dom_->mesh_size[2];

    ierr = DMDAVecGetArray(p_dom_->da_sc, U, &U_array); CHKERRQ(ierr);
    if( bd.left() == BCType::Dirichlet ) {
        if( p_dom_->xs == 0 ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][-1] = 0; //U_array[j][1];
            }
        }
    }
    if( bd.right() == BCType::Dirichlet ) {
        if( p_dom_->xs+p_dom_->xm == nx ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][nx] = 0; //U_array[j][nx-2];
            }
        }
    }
    if( bd.bottom() == BCType::Dirichlet ) {
        if( p_dom_->ys == 0 ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[-1][i] = 0; //U_array[1][i];
            }
        }
    }
    if( bd.top() == BCType::Dirichlet ) {
        if( p_dom_->ys+p_dom_->ym == ny ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[ny][i] = 0; //U_array[ny-2][i];
            }
        }
    }

    if( bd.left() == BCType::Neumann ) {
        if( p_dom_->xs == 0 ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][-1] = U_array[j][1];
            }
        }
    }
    if( bd.right() == BCType::Neumann ) {
        if( p_dom_->xs+p_dom_->xm == nx ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][nx] = U_array[j][nx-2];
            }
        }
    }
    if( bd.bottom() == BCType::Neumann ) {
        if( p_dom_->ys == 0 ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[-1][i] = U_array[1][i];
            }
        }
    }
    if( bd.top() == BCType::Neumann ) {
        if( p_dom_->ys+p_dom_->ym == ny ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[ny][i] = U_array[ny-2][i];
            }
        }
    }

    if( bd.left() == BCType::Outflow ) {
        if( p_dom_->xs == 0 ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][-1] = 2 * U_array[j][0] - U_array[j][1];
            }
        }
    }
    if( bd.right() == BCType::Outflow ) {
        if( p_dom_->xs+p_dom_->xm == nx ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][nx] = 2. * U_array[j][nx-1] - U_array[j][nx-2];
            }
        }
    }
    if( bd.bottom() == BCType::Outflow ) {
        if( p_dom_->ys == 0 ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[-1][i] = 2. * U_array[0][i] - U_array[1][i];
            }
        }
    }
    if( bd.top() == BCType::Outflow ) {
        if( p_dom_->ys+p_dom_->ym == ny ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[ny][i] = 2. * U_array[ny-1][i] - U_array[ny-2][i];
            }
        }
    }

    if( bd.left() == BCType::Interpolate ) {
        if( p_dom_->xs == 0 ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][-1] = U_array[j][2] - 3. * ( U_array[j][1] - U_array[j][0] );
            }
        }
    }
    if( bd.right() == BCType::Interpolate ) {
        if( p_dom_->xs+p_dom_->xm == nx ) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                U_array[j][nx] = U_array[j][nx-3] - 3. * ( U_array[j][nx-2] - U_array[j][nx-1]
                        );
            }
        }
    }
    if( bd.bottom() == BCType::Interpolate ) {
        if( p_dom_->ys == 0 ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[-1][i] = U_array[2][i] - 3. * ( U_array[1][i] - U_array[0][i] );
            }
        }
    }
    if( bd.top() == BCType::Interpolate ) {
        if( p_dom_->ys+p_dom_->ym == ny ) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                U_array[ny][i] = U_array[ny-3][i] - 3. * ( U_array[ny-2][i] - U_array[ny-1][i]
                        );
            }
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, U, &U_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

model_table tb;

//! Based on a string (initial_data_name), instantiate the proper class
//!
PetscErrorCode factory()
{
    FUNCTIONBEGIN_MACRO("", WORLD);
    // This type map contains a reference to the templated function registerInstance<T>
    tb["dbg"] = &registerInstance<Debug>;

    // Simple benchmarks/debug
    tb["div-test"] = &registerInstance<DivTest>;
    tb["proj-test"] = &registerInstance<ProjTest>;
    tb["reference"] = &registerInstance<Reference>;
    tb["steady"] = &registerInstance<SteadyState>;
    tb["patch"] = &registerInstance<VortexPatch>;

    // Standard benchmarks
    tb["vortex-box"] = &registerInstance< VortexBox >;
    tb["sl"] = &registerInstance< ShearLayer< false, false> >;
    tb["disc-sl"] = &registerInstance< ShearLayer<true, false> >;
    tb["sl-single"] = &registerInstance< ShearLayer<false, true> >;
    tb["disc-sl-single"] = &registerInstance< ShearLayer<true, true> >;

    // 3D benchmarks
    tb["tg"] = &registerInstance<TaylorGreen>;

    // Inflows
    tb["vortex-stream"] = &registerInstance<VortexStream>;
    tb["driven-cavity"] = &registerInstance<DrivenCavity>;

#ifdef USE_RANDOM
    // Random generated initial data
    tb["rnd-pert-sl"] = &registerInstance< RndPertShearLayer<false, false> >;
    tb["rnd-int-sl"] = &registerInstance< RndIntShearLayer<false, false> >;


    tb["rnd-int-disc-sl"] = &registerInstance< RndIntShearLayer<true, false> >;
//     tb["rnd-int-disc-sl"] = &registerInstance< RndIntShearLayer<true, false> >;

    tb["rnd-int-sl-single"] = &registerInstance< RndIntShearLayer<false, true> >;
//     tb["rnd-int-sl-single"] = &registerInstance< RndIntShearLayer<false, true> >;

    tb["rnd-int-disc-sl-single"] = &registerInstance< RndIntShearLayer<true, true> >;
//     tb["rnd-int-disc-sl-single"] = &registerInstance< RndIntShearLayer<true, true> >;

    // 3D benchmarks
    tb["rnd-tg"] = &registerInstance<RndTaylorGreen>;


    tb["kl"] = &registerInstance<KarhunenLoeve>;

    tb["rnd-patch"] = &registerInstance< RndPatch >;

    tb["core"] = &registerInstance< VortexCore<false> >;
    tb["disc-core"] = &registerInstance< VortexCore<true> >;
#endif

    // Others
    tb["load"] = &registerInstance<LoadState>;

    PetscFunctionReturn(ierr);
}

//! Based on a string (initial_data_name), instantiate the proper class
//!
PetscErrorCode InstantiateModel(Options * opt, ModelBase *& mod) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    // Fill in initial data table required afterwards
    // TODO: if has factory
    factory();

    // Catch "wrong model name" exeption if model do not exists
    try {
        mod = tb.at(opt->model_name())(opt);
    }
    catch (const std::exception& oor) {
        WARNING("", WORLD, "Seems no initial data has been set or a "
                "wrong type has been chosen (%s), "
                "available are:\n", opt->model_name());
        // Show content:
        for (model_table::iterator it=tb.begin(); it != tb.end(); ++it) {
            WARNING("", WORLD, "%s => %s\n", it->first.c_str());
        }
        ERROR("", WORLD, "No initial data specified! You MUST "
                "SPECIFY SOME INITIAL DATA! I quit...\n", "");
    }

    mod->check();


    PetscFunctionReturn(ierr);
}
