/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

typedef PetscReal func2d(PetscReal, PetscReal);
typedef PetscReal func3d(PetscReal, PetscReal, PetscReal);
typedef PetscReal func4d(PetscReal, PetscReal, PetscReal, PetscReal);

//!
//! \brief The Custom class allows to use a custom function handler.
//! TODO: Feature Not Implemented!
//!
template <class vorticity>
class Custom : public ModelBase {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    Custom(Options * p_opt)
        : ModelBase(p_opt)
    { }

    //! Vorticity (2D)
    PetscReal omega(PetscReal x, PetscReal y, PetscReal z) {
        return w(x,y,z);
    }

private:
    vorticity    w;
};

