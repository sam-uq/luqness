/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

#include <iostream>

//! Vortex stream \n
//!  - Requirements: 2D Projection, inflow/noslip/outflow boundaries;
//!  - Provides: velocity (2D), passive tracer (2D) and inflow BC
//!  - Parameters: layer perturbation and layer thickness
//!  - Domain: periodic [0,1]^2
//!  - Type: non random
class VortexStream : public ModelBase {
    //! Default values
    //! Perturbation size (max of y-component)
    const double DELTA = 0.05;
    //! Layer thickness, higher means thicker (BCG uses 1/30)
    const double RHO = 1.0 / 30.0;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    VortexStream(Options * p_opt)
        : ModelBase(p_opt)
        , delta(DELTA)
        , rho(RHO) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        push_param("delta", delta, DELTA);
        push_param("rho", rho, RHO);

        provide_comp(CompType::U);
        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Deterministic);

        PetscReal dom[6] = {0,4,0,1,0,0};
        char bdr[6] = {'I', 'O', 'N', 'N', 'P', 'P'};
        provide_domain(dom, bdr);

    }

    //! Velocity x
    PetscReal u_0(PetscReal, PetscReal y, PetscReal) { return 1. - 0.5 * tanh((y - 0.5) / rho); }
//     PetscReal v_0(PetscReal x, PetscReal y, PetscReal z) { return delta * sin(2*PI*x); } // TEST ONLY

    //! Tracer
    PetscReal coloring(PetscReal, PetscReal y, PetscReal) { return 1. - 0.5 * tanh((y - 0.5) / rho); }

    //! Inflow velocity
    PetscReal u_in(PetscReal, PetscReal y, PetscReal, PetscReal) {
        return 1. - 0.5 * tanh((y - 0.5) / rho);
//         - 0.1 * sin(t*10);
    }

    //! Perturbed v_in velocity (time dependent)
    PetscReal v_in(PetscReal, PetscReal, PetscReal, PetscReal) {
//         const PetscInt K = 20;
//         PetscReal v_inflow = 0.;
//         for(PetscInt k = 0; k <= K; k++) {
//             v_inflow += delta / pow(2., k) * sin(4. * pow(2., k) * PI * t);
//         }
//         return v_inflow;
        return 0;
    }

private:
    //! Layer perturbation
    PetscReal  delta;
    //! Layer thickness
    PetscReal  rho;
};
