/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Steady state, non-forced \n
//!  - Requirements: 2D Vortex or Projection with/without viscosity and passive tracer, periodic boundaries;
//!  - Provides: velocity (2D), vorticity(2D), passive tracer (2D)
//!  - Parameters: none
//!  - Domain: periodic [0,1]^2
//!  - Type: non random
class SteadyState : public ModelBase {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    SteadyState(Options * p_opt)
    : ModelBase(p_opt) { }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal) { return -cos(2 * PI * x) * sin(2 * PI * y); }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal) { return sin(2 * PI * x) * cos(2 * PI * y); }

    //! Pressure x component
    PetscReal g_p_x_0(PetscReal x, PetscReal, PetscReal) { return PI * sin(4 * PI * x); }

    //! Pressure y component
    PetscReal g_p_y_0(PetscReal, PetscReal y, PetscReal) { return PI * sin(4 * PI * y); }

    //! Vorticity
    PetscReal omega(PetscReal x, PetscReal y, PetscReal) {
        return 4 * PI * cos(2 * PI * x) * cos(2 * PI * y);
    }

    //! Tracer
    PetscReal coloring(PetscReal, PetscReal y, PetscReal) { return y >= .5 ? -1. : 1.; }

};

