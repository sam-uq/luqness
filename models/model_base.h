/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <map>
#include <string>
#include <stdexcept>
#include <vector>
#include <iostream>

// Internal includes
#include "../attila/attila.h"
#include "../tools/options.h"
#include "../tools/domain.h"

#include "../samplers/sampler.h"

#if BOOST_ENABLE
#include <boost/lexical_cast.hpp>
#endif // BOOST_ENABLE

class Sampler;
class SamplerData;

//! Cast c checking if represent some default keyword
bool is_default(char * c);

//! Given a string c, check if it can be casted to bool (using keywords), return true if successful, i contains the
//! converted boolean
inline bool str_to_bool(char * c, bool & i) {
    if( !strcmp(c, "1") || !strcmp(c, "true") || !strcmp(c, "yes") ) {
        i = true;
        return true;
    }
    if( !strcmp(c, "0") || !strcmp(c, "false") || !strcmp(c, "no") ) {
        i = false;
        return true;
    }
    return false;
}

#if CXX11_ENABLE
inline void param_to_val_c11(char * c, int & i);
inline void param_to_val_c11(char * c, double & i);
inline void param_to_val_c11(char * c, bool & i);
inline void param_to_val_c11(char * c, std::string & i);
#endif // CXX11_ENABLE

template <typename T>
inline T param_to_val(char * c) {
    return atoi(c);
}

template <>
inline int param_to_val<int>(char * c) {
    return atoi(c);
}
template <>
inline double param_to_val<double>(char * c) {
    return atof(c);
}
template <>
inline bool param_to_val<bool>(char * c) {
    bool b;
    if( !str_to_bool(c, b)  ) {
        return (bool) atoi(c);
    } else return b;
}
template <>
inline std::string param_to_val<std::string>(char * c) {
    std::string i;
    i.assign(c, strlen(c));
    return i;
}


//! Base class for initial data info (VIRTUAL: has to be subclassed)
//!
class ModelBase {
public:
    //! Initialize virtually the class, initialize and start the RNg
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    ModelBase(Options * p_opt)
        : dt(0), dt_old(0), t(0), dt_changed(true)
        , viscosity_violated(PETSC_FALSE)
        , porosity_violated(PETSC_FALSE)
        , provided_comp(CompType::None)
        , provided_mode(QuadratureType::None)
        , provided_dim(DimType::None)
        , provides_dom(false), provides_bdry(false)
        , p_opt_(p_opt)
        , paramn(0)
        , load_as_vec(false) {
        FUNCTIONBEGIN_MACRO("", WORLD);
        // Sanity check
        assert(p_opt_->paramc >= 1);

        smp_size_.sim = 0;
        smp_size_.dim = 0;
        smp_size_.stream = 0;
    }
    //! Virtually delete the rng
    virtual ~ModelBase() { }

    //!
    //! \brief The Boundary enum is used to pick which side
    //! of the domain is used. Left/Right relative to x-coord,
    //! Bottom-Top relative to y-coord, Below/Above relative to z-coord.
    //!
    enum Boundary { Left, Right, Bottom, Top, Below, Above};

    //! Model needs data to do stuff

    void set_data(Domain * p_dom, Attila * p_cm) {
        p_dom_ = p_dom;
        p_cm_ = p_cm;
    }

    //! Reinitilize the Rng
    void init(uint sim_id);
    //! Overridden by subclasses
    virtual void sub_init(void) {}

    //! Performs sanity check on options and model data
    void check(void);

    //! Print model base info
    PetscErrorCode print(void);

    //! Speed x axis
    virtual PetscScalar u_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Speed y axis
    virtual PetscScalar v_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Speed z axis
    virtual PetscScalar w_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Vorticity (TODO: make it work in 3D)
    virtual PetscScalar omega(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Pressure x axis
    virtual PetscScalar g_p_x_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Pressure y axis
    virtual PetscScalar g_p_y_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Pressure z axis
    virtual PetscScalar g_p_z_0(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Passive scalar
    virtual PetscScalar coloring(PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Forcing x axis
    virtual PetscScalar F_x(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Forcing y axis
    virtual PetscScalar F_y(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Forcing z axis
    virtual PetscScalar F_z(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Inflow celocity x axis
    virtual PetscScalar u_in(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Inflow celocity y axis
    virtual PetscScalar v_in(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }
    //! Inflow celocity z axis
    virtual PetscScalar w_in(PetscScalar, PetscScalar, PetscScalar, PetscScalar) { return 0; }

    //! Setup the vector for U_0, defaults to function-style
    virtual PetscErrorCode set_U_0(Vec & V);
    //! Setup the vector for Gp_0, defaults to function-style
    virtual PetscErrorCode set_Gp_0(Vec & V);
    //! Setup the vector for w_0, defaults to function-style
    virtual PetscErrorCode set_w_0(Vec & V);
    //! Setup the vector for scalar, defaults to function-style
    virtual PetscErrorCode set_scalar_0(Vec & V);
    //! Setup the vector for forcing, defaults to function-style
    virtual PetscErrorCode set_F(Vec & V, PetscReal t);

    // TODO virtualize
    //!
    PetscErrorCode update_bd_field(Vec & V, const BD & bd, PetscScalar t);
    //!
    PetscErrorCode update_bd_scalar(Vec & V, const BD & bd, PetscScalar t);

    // Temporal data
    //! Current dt for solution (also dt old for efficiency reasons)
    PetscReal             dt, dt_old;
    //! Current time for solution
    PetscReal             t;
    //! has the dt changed from the last use? (Reuse diffusion matrices)
    bool                  dt_changed;
    //! The current supremum of the solution
    PetscReal             sup;

    // Other info
    //! Becomes ture once transport "CFL" conditions are violated
    PetscBool             viscosity_violated, porosity_violated;

    //! Reads next paramn option (increments a counter) and set the value val either to default or a converted version
    //! of this option, name is given to push an option database with names
    template <typename T, typename U>
    PetscErrorCode push_param(std::string name,
                              T & val, const U & default_val) {
        par.push_back(name);
        if( p_opt_->paramc > ++this->paramn ) {
            if ( is_default(p_opt_->paramv[this->paramn])) {
                val = default_val;
                PetscFunctionReturn(ierr);
            }
#if CXX11_ENABLE
            try
            {
                param_to_val_c11(p_opt_->paramv[this->paramn], val);
            }
            catch(const std::invalid_argument &)
            {
                val = default_val;
            }
        }
#elif BOOST_ENABLE
            try
            {
                val = boost::lexical_cast<T>(p_opt_->paramv[this->paramn]);
            }
            catch(const boost::bad_lexical_cast &)
            {
                val = default_val;
            }
        }
#else // PLAIN C++ < 11
            // Kinda unsafe.
            val = param_to_val<T>(p_opt_->paramv[this->paramn]);
        }
#endif // CXX11_ENABLE
        else {
            val = default_val;
        }
        PetscFunctionReturn(ierr);
    }

    Sampler                     * p_smp_;

    //! Returns true if model provides the data you want (all of them)
    bool check_provides_comp(int wanted_comp) {
        return (provided_comp | ~wanted_comp) == ~0;
    }

    //! Add a provider for the model
    void provide_comp(int provide_comp) {
        provided_comp |= provide_comp;
    }

    //! Returns true if model provides the data mode you want (all of them)
    bool check_provides_mode(int wanted_mode) {
        return (provided_mode | ~wanted_mode) == ~0;
    }
    //! Add a mode (Det or Random) for the model
    void provide_mode(int provide_mode) {
        provided_mode |= provide_mode;
    }
    //! Returns true if model provides the data mode you want (all of them)
    bool check_provides_dim(int wanted_dim) {
        return (provided_dim | ~wanted_dim) == ~0;
    }

    //! Set which dimension can be handled by the domain
    void provide_dim(int provide_dim) {
        provided_dim |= provide_dim;
    }

    void provide_domain(PetscReal * provide_dom, char * provide_bdry = nullptr) {
        memcpy(provided_dom, provide_dom, 6 * sizeof(PetscReal));
        provides_dom = true;
        if(provide_bdry != NULL) {
            memcpy(provided_bdry, provide_bdry, 6 * sizeof(char));
            provides_bdry = true;
            if(!p_opt_->boundaries_provided) {
                *p_opt_->boundaries_array[0] = provide_bdry[0];
                *p_opt_->boundaries_array[1] = provide_bdry[1];
                *p_opt_->boundaries_array[2] = provide_bdry[2];
                *p_opt_->boundaries_array[3] = provide_bdry[3];
                *p_opt_->boundaries_array[4] = provide_bdry[4];
                *p_opt_->boundaries_array[5] = provide_bdry[5];
            }
        }
    }
    //     void require_bc TODO

    //! Return true if phisical boundary is contained in this processor
    bool isPhysical(Boundary position) {
        int nx = p_dom_->mesh_size[0],
            ny = p_dom_->mesh_size[1];

        switch(position) {
            case Left:
                return p_dom_->xs == 0;
            case Right:
                return p_dom_->xs+p_dom_->xm == nx;
            case Bottom:
                return p_dom_->ys == 0;
            case Top:
                return p_dom_->ys+p_dom_->ym == ny;
            default:
                return 0;
        }
    }

    static void Outflow(Field2D & outer,
                        const Field2D & inner, const Field2D &, Volume3D) {
        outer[0] = inner[0];
        outer[1] = inner[1];
    }

    static void NoSlipLR(Field2D & outer,
                         const Field2D & inner, const Field2D &, Volume3D) {
        outer[0] = -inner[0];
        outer[1] = inner[1];
    }

    static void NoSlipBT(Field2D & outer,
                         const Field2D & inner, const Field2D &, Volume3D) {
        outer[0] = inner[0];
        outer[1] = -inner[1];
    }

    static void SlipLR(Field2D & outer,
                       const Field2D & inner, const Field2D & inner2, Volume3D) {
        outer[0] = -inner[0];
        outer[1] = 2* inner[1] - inner2[1];
    }

    static void SlipBT(Field2D & outer,
                       const Field2D & inner, const Field2D & inner2, Volume3D) {
        outer[0] = 2*inner[0] - inner2[0];
        outer[1] = -inner[1];
    }

    void ForcingM(Field2D & outer, Field2D &inner,
                         const Field2D &, Volume3D coord) {
        outer[0] = this->u_in(coord.x, coord.y, coord.z, this->t);
        outer[1] = this->v_in(coord.x, coord.y, coord.z, this->t);
        inner[0] = this->u_in(coord.x, coord.y, coord.z, this->t);
        inner[1] = this->v_in(coord.x, coord.y, coord.z, this->t);
    }

    template <class BoundaryFunction>
    void applyBoundaryLoop(BoundaryFunction boundaryFunction,
                           Boundary position,
                           Field2D ** U_array,
                           DMDACoor2d ** coords_array,
                           Volume2D   ** d) {
        int nx = p_dom_->mesh_size[0],
            ny = p_dom_->mesh_size[1];

        PetscInt jstart, jend;
        switch(position) {
            case Left:
            case Right:
                jstart = p_dom_->ys;
                jend = p_dom_->ys+p_dom_->ym;
                break;
            case Top:
            case Bottom:
                jstart = p_dom_->xs;
                jend = p_dom_->xs+p_dom_->xm;
                break;
            default:
                return;
        }
        PetscInt fixedInnerIndex, fixedInner2Index, fixedOuterIndex;
        switch(position) {
            case Left:
                fixedOuterIndex = -1;
                fixedInnerIndex = 0;
                fixedInner2Index = 1;
                break;
            case Right:
                fixedInner2Index = nx-2;
                fixedInnerIndex = nx-1;
                fixedOuterIndex = nx;
                break;
            case Top:
                fixedOuterIndex = -1;
                fixedInnerIndex = 0;
                fixedInner2Index = 1;
                break;
            case Bottom:
                fixedInner2Index = ny-2;
                fixedInnerIndex = ny-1;
                fixedOuterIndex = ny;
                break;
            default:
                return;
        }

        PetscReal x, y, z = 0;
        switch(position) {
            case Left:
            case Right:
                for (PetscInt j = jstart; j < jend; ++j) {
                    x = coords_array[j][fixedInnerIndex].x + d[j][fixedInnerIndex].x / 2;
                    y = coords_array[j][fixedInnerIndex].x + d[j][fixedInnerIndex].y / 2;
                    boundaryFunction(U_array[j][fixedOuterIndex],
                                     U_array[j][fixedInnerIndex],
                                     U_array[j][fixedInner2Index],
                                     Volume3D(x,y,z,1.));
                }
                break;
            case Top:
            case Bottom:
                for (PetscInt j = jstart; j < jend; ++j) {
                    x = coords_array[fixedInnerIndex][j].x + d[fixedInnerIndex][j].x / 2;
                    y = coords_array[fixedInnerIndex][j].x + d[fixedInnerIndex][j].y / 2;

                    boundaryFunction(U_array[fixedOuterIndex][j],
                                     U_array[fixedInnerIndex][j],
                                     U_array[fixedInner2Index][j],
                                     Volume3D(x,y,z,1.));
                }
                break;
            default:
                return;
        }
    }

    int         provided_comp;
    int         provided_mode;
    int         provided_dim;
    PetscReal   provided_dom[6];
    char        provided_bdry[6];

    bool        provides_dom;
    bool        provides_bdry;
    //     int         require_bc;

    //! Sampler size (number of simulations, number of streams and number of dimensions
    SamplerData smp_size_;
protected:
    Options                     * p_opt_;
    Domain                      * p_dom_;
    Attila                      * p_cm_;

    //! Number of parameters read so far
    int                         paramn;

    //! Parameter name array
    std::vector<std::string>    par;

    //! Load the vector as a Vec TODO
    const bool                  load_as_vec;
private:
};

//! This map maps from a string to a function handle for instantiating a new initial data thingy
typedef std::map<std::string, ModelBase*(*)(Options *)> model_table;

//! @brief Provides a template constructor for derived classes of Initialdata.
//!
template<typename T> ModelBase * registerInstance(Options * opt)
{
    return new T(opt);
}

//! Based on a string (initial_data_name), instantiate the proper class
//!
PetscErrorCode factory();

//! Based on a string (initial_data_name), instantiate the proper class
//!
PetscErrorCode InstantiateModel(Options * opt, ModelBase *& mod);

