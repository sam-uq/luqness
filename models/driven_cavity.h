/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Driven cavity problem (for inflow boundary conditions) \n
//!  - Requirements: 2D Projection with/without viscosity and inflow/noslip boundaries;
//!  - Provides: velocity (2D);
//!  - Parameters: top layer thickness
//!  - Domain: velocity in [0,1]^2
//!  - Type: non random
class DrivenCavity : public ModelBase  {
    //! Default values
    //! Layer thickness
    const double RHO = 1.0 / 90.0;
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    DrivenCavity(Options * p_opt)
    : ModelBase(p_opt)
        , rho(RHO) {

        if( p_opt_->paramc > ++paramn ) rho = atof(p_opt_->paramv[paramn]);


        PetscReal dom[4] = {0,1,0,1};
        char bdry[4] = {'N', 'N', 'N', 'I'};
        provide_domain(dom, bdry);
        provide_dim(DimType::d2);
        provide_comp(CompType::U);
    }

    // Initial velocity (top layer)
//    PetscReal v_0(PetscReal x, PetscReal, PetscReal) { return 0.5 + 0.5 * tanh((x - 0.9) / rho); }

    // Tracer
    PetscReal coloring(PetscReal x, PetscReal, PetscReal) { return x <= 0.5 ? 1. : 0.; }

    // Inflow from the top
    PetscReal u_in(PetscReal, PetscReal, PetscReal, PetscReal) { return 1; }

    // Inflow from the top
//    PetscReal v_in(PetscReal x, PetscReal, PetscReal, PetscReal) { return 0.5 + 0.5 * tanh((x - 0.9) / rho); }

private:
    PetscReal  rho;

};
