/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef RND_PATCH_H
#define RND_PATCH_H

#include "vortex_patch.h"
#include "rnd_pert.h"

#ifdef USE_RANDOM

//! Random generated vortex patch
//!
class RndPatch : public VortexPatch, public RndPert {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RndPatch(Options * p_opt)
        : ModelBase(p_opt)
        , VortexPatch(p_opt)
        , RndPert(p_opt) {

        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Stochastic);
    }

    ~RndPatch() { }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal x_, y_, z_;
        pert(x, y, z, x_, y_, z_);
        return VortexPatch::u_0(x_, y_, z_);
    }

    //! Velocity y component
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal x_, y_, z_;
        pert(x, y, z, x_, y_, z_);
        return VortexPatch::v_0(x_, y_, z_);
    }

    //! Velocity z component
    PetscReal w_0(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal x_, y_, z_;
        pert(x, y, z, x_, y_, z_);
        return VortexPatch::w_0(x_, y_, z_);
    }

    //! Vorticity 2D z-component
    PetscReal omega(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal x_, y_, z_;
        pert(x, y, z, x_, y_, z_);
        return VortexPatch::omega(x_, y_, z_);
    }

    //! Passive tracer
    PetscReal coloring(PetscReal x, PetscReal y, PetscReal z) {
        PetscReal x_, y_, z_;
        pert(x, y, z, x_, y_, z_);
        return VortexPatch::coloring(x_, y_, z_);
    }

};

#endif

#endif // RND_PATCH_H
