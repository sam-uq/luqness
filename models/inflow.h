/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"


//!
//! \brief The Inflow class provide a model of a flow from the left of a recangular domain.
//!
class Inflow : public ModelBase {
    //! Default values
    //! Perturbation size (max of y-component)
    const double DELTA = 0.05;
public:
    //!
    Inflow(Options * p_opt)
    : ModelBase(p_opt) {
        FUNCTIONBEGIN_MACRO("", WORLD);

        // Read parameters
        push_param("delta", delta, DELTA);

        provide_comp(CompType::U);
        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Deterministic);

        PetscReal dom[6] = {0,1,-1,1,0,0};
        char bdr[6] = {'I', 'O', 'N', 'N', 'P', 'P'};
        provide_domain(dom, bdr);
    }

    //! Initial velocity (top layer)
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal z) { return 0; }

    //! Inflow from the left
    PetscReal u_in(PetscReal x, PetscReal y, PetscReal z, PetscReal t) { return 0.5 + 0.5 * tanh((x - 0.9) / rho); }

private:
    PetscReal delta;
};
