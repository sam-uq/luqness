/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "shear_layer.h"
#include "rnd_interface.h"

#ifdef USE_RANDOM

//! Random generated shear layer (interface) for a rapidly changing velocity in the x direction. \n
//!  - Requirements: 2D Vortex or Projection with/without viscosity and passive tracer, periodic boundaries;
//!  - Provides: velocity (3D), vorticity(2D), passive tracer (2D)
//!  - Parameters: perturbation size, thickness, position of interfaces and scaling (from base), number of draws
//!  - Domain: for doubly periodic 2D from 0 to POS * 2 in positive direction and from 0 to SCALING in y direction,
//!    periodic BC; for single layer from -X to X (arbitrary) and outflow/slip
//!  - Type: random
//! TODO: random interface only supports "2pi" interfaces, this is because we do not support scaling in that class
//! FIX: find clean way to deal with that
template <bool disc, bool single>
class RndIntShearLayer : public ShearLayer<disc, single>, public RandomInterface {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    RndIntShearLayer(Options * p_opt)
        : ModelBase(p_opt), ShearLayer<disc, single>(p_opt), RandomInterface(p_opt) {
            FUNCTIONBEGIN_MACRO("", WORLD);

        provide_dim(DimType::d2);
        provide_mode(QuadratureType::Stochastic);
        if( single ) {
            PetscReal dom[6] = {0,1,-1,1,0,0};
            char bdr[6] = {'P', 'P', 'O', 'O', 'P', 'P'};
            provide_domain(dom, bdr);
        }
    }

    ~RndIntShearLayer() { }

    //! Velocity x component
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) {
        y -= atx(x * this->factor);
        return ShearLayer<disc, single>::u_0(x,y,z);
    }

    //! Velocity y component
    PetscReal v_0(PetscReal, PetscReal, PetscReal) {
        return 0;
    }

    //! Velocity z component
    PetscReal w_0(PetscReal, PetscReal, PetscReal) {
        return 0;
    }

    //! Vorticity 2D z-component
    PetscReal omega(PetscReal x, PetscReal y, PetscReal z) {
        y -= atx(x * this->factor);
        return ShearLayer<disc, single>::omega(x,y,z);
    }

    //! Passive tracer
    PetscReal coloring(PetscReal x, PetscReal y, PetscReal z) {
        y -= atx(x * this->factor);
        return ShearLayer<disc, single>::coloring(x,y,z);
    }

};

#endif
