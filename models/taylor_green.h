/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "model_base.h"

//! Model the periodic and continuous shear layer in 3D with cylindrical symmetry: rapidly changing velocity in the
//! x direction. \n
//!  - Requirements: 3D Projection, periodic boundaries;
//!  - Provides: velocity (2D), vorticity(2D), passive tracer (2D)
//!  - Parameters: none
//!  - Domain: periodic [0,2 * PI]^3
//!  - Type: non random
class TaylorGreen : public virtual ModelBase {
public:
    //! Construct the super class and initialize parameters
    //! @param[in] opt_ Options context from which parameters are grabbed
    //! @param[in] dom_ Domain context, used by super class to initialize vectors
    //! @param[in] smp_ Sampler context, used for random generation of initial data
    TaylorGreen(Options * p_opt)
        : ModelBase(p_opt) {

            PetscReal dom[6] = {0,6.283185307179586,0,6.283185307179586,0,6.283185307179586};
            char bdry[6] = {'P', 'P', 'P', 'P', 'P', 'P'};
            provide_domain(dom, bdry);
            provide_dim(DimType::d3);
            provide_comp(CompType::U);
//             provide_mode();
        }

    //! Velocity x
    PetscReal u_0(PetscReal x, PetscReal y, PetscReal z) { return sin( x) * cos(y) * cos( z); }

    //! Velocity y
    PetscReal v_0(PetscReal x, PetscReal y, PetscReal z) { return - cos( x) * sin( y) * cos( z); }

    //! Velocity z
    PetscReal w_0(PetscReal, PetscReal, PetscReal) { return 0.; }
private:

};

