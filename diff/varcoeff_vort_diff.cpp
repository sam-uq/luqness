/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "varcoeff_vort_diff.h"

VarcoeffVortDiff::VarcoeffVortDiff(Options *p_opt, Domain *p_dom,
                                   ModelBase *p_mod, Attila *p_cm,
                                   Projection * p_proj,
                                   PetscReal theta, PetscReal factor)
    : VortDiff(p_opt, p_dom, p_mod, p_cm, p_proj, theta, factor)
    , coeff_vec(NULL)
    , num_diff_mat_alloc_(false) {
    FUNCTIONBEGIN(p_cm_);

    compute_mat = ComputeMatrixVarDiffCoeff;
}

VarcoeffVortDiff::~VarcoeffVortDiff() {
    FUNCTIONBEGIN(p_cm_);

    if( num_diff_mat_alloc_ ) {
        ierr =  MatDestroy(&num_diff_mat_); CHKERRV(ierr);
    }
}

PetscErrorCode VarcoeffVortDiff::set_diff_mat(Mat diff_mat) {
    has_diff_mat_ = true;
    if( diff_mat ) diff_mat_ = diff_mat;
    if( !num_diff_mat_alloc_ ) {
        ierr =  CreateMatrix(p_dom_->da_sc, &num_diff_mat_); CHKERRQ(ierr);
        num_diff_mat_alloc_ = true;
    }
    if( coeff_vec != NULL ) {
        laplacian_matrix_coeff<1>(p_dom_->da_sc, num_diff_mat_,
                                  p_dom_->bd_fam.U, p_dom_->da, coeff_vec);
    } else {
        //     WARNING(p_cm)
    }
    diff_mat_has_changed_ = true;
    diff_mat_same_as_num_mat_ = false;
    PetscFunctionReturn(ierr);
}

PetscErrorCode VarcoeffVortDiff::set_coeff_vec(Vec V) {
    coeff_vec = V;
    diff_mat_has_changed_ = true;
    PetscFunctionReturn(ierr);
}

PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat mat, Mat jac, MatStructure *, void *ctx) {
    return ComputeMatrixVarDiffCoeff(ksp_, mat, jac, ctx);
}

PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat, Mat jac, void *ctx) {
    VarcoeffVortDiff *p_diff = (VarcoeffVortDiff*) ctx;
    DEBUG(p_diff->p_cm_, INTRALVL,
          "[MAT] Setting up matrix for variable coefficient diffusion "
          "with dt = %f and factor = %d...\n",
          p_diff->p_mod_->dt, p_diff->factor);

    DM da;
    ierr  = KSPGetDM(ksp_, &da); CHKERRQ(ierr);

    ierr = p_diff->laplacian_matrix_coeff<1>(da, jac,
                                             p_diff->p_dom_->bd_fam.U,
                                             p_diff->p_dom_->da,
                                             p_diff->coeff_vec); CHKERRQ(ierr);

    PetscReal scaling = -p_diff->theta*p_diff->get_complete_diff_coefficient();
    ierr = MatScale(jac, scaling); CHKERRQ(ierr);
    ierr = MatShift(jac, p_diff->matrix_scaling(da)); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
