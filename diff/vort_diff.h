/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file vort_diff.h
//!
#include "diff.h"

//!
//! \brief ComputeRHSVortDiff
//! \param ksp_
//! \param b
//! \param ctx
//! \return
//!
PetscErrorCode ComputeRHSVortDiff(KSP ksp_, Vec b, void *ctx);

//!
//! \brief The VortDiff class implements diffusion for the voritcity formulation.
//!
class VortDiff : public Diff {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    VortDiff(Options *p_opt, Domain *p_dom,
             ModelBase *p_mod, Attila *p_cm,
             Projection * p_proj,
             PetscReal theta, PetscReal factor = 1);
    //! Free the memory from buffers and other stuff, destroy unneded matrices
    virtual ~VortDiff();

    PetscReal matrix_scaling(DM da) {
        return p_proj_->matrix_scaling(da);
    }

    //! Happy friends, those are function that need
    //! access to protected members but cannot be included
    //! in class due to
    //! PETSc function pointer
    friend PetscErrorCode ComputeRHSVortDiff(KSP ksp_, Vec b, void *ctx);

};
