/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file vort_diff.h
//!

#include "diff.h"

//!
PetscErrorCode ComputeRHSProjDiff(KSP ksp_, Vec b, void *ctx);

//!
//! \brief The ProjDiff class implements the diffusion for the projection method.
//!
//! Implement diffusion for Velocity-Projection: diffusion comes from
//! numerical and physical considerations. Let \f$\theta \in [0,1]\f$ (in the code
//! \f$\theta = 0,1/2,2\f$).
//! Assuming the evolution in time is
//!     \f$U^{n+1} = U^{n} + dt * F(U^{n}) + dt \nu' \Delta (\theta U^{n+1} + (1-\theta) U^{n})\f$
//! with \f$\nu' = \epsilon + \nu_{art}\f$, we obtain an equivalent system:
//!     \f$(1 - dt \nu' \theta \Delta) U^{n+1} = rhs(U^{n})\f$.
//! If Navier-Stokes diffusion is used (\f$\epsilon > 0\f$),
//! with explicit diffusion (\f$\theta < 1/2\f$),
//! the coefficient \dt must be small, i.e. smaller than
//!     \f$ \epsilon dt \leq 0.5 \Delta x^2\f$
//! The coefficient for the numerical diffusion  is \f$\nu_{art} \sim O(\Delta x)\f$.
//!
class ProjDiff : public Diff {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    ProjDiff(Options *p_opt, Domain *p_dom,
             ModelBase *p_mod, Attila *p_cm,
             Projection * p_proj, PetscReal theta, PetscReal factor = 1);
    //! Free the memory from buffers and other stuff, destroy unneded matrices
    virtual ~ProjDiff();

    // Happy friends, those are function that need access to protected members but cannot be included in class due to
    // PETSc function pointer
    friend PetscErrorCode ComputeRHSProjDiff(KSP ksp_, Vec b, void *ctx);
};

