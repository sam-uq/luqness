/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file diff.h
//!

#include "../tools/options.h"
#include "../tools/domain.h"
#include "../models/model_base.h"
#include "../attila/attila.h"
#include "../projection/projection.h"

//!
//! \brief ComputeMatrixProjDiff Set up matrix for diffusion.
//!
//! The matrix includes the implicit part of the Laplacian and
//! the time stepping part of the time-derivative. The scaling of the
//! matrix is assumed to be the one of the original equation, multiplied by
//! the "normalization" muiltiplier for the Laplacian matrix.
//! \param ksp_
//! \param jac
//! \param str
//! \param ctx
//! \return
//!
PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat J,
                                     Mat jac, MatStructure * str, void *ctx);

PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat J,
                                     Mat jac, void *ctx);

//!
//! \brief The Diff class is an abstract base class for the handling of the
//! diffusion term.
//!
class Diff {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    Diff(Options *p_opt, Domain *p_dom,
         ModelBase *p_mod, Attila *p_cm,
         Projection * p_proj, PetscReal theta, PetscReal factor);
    //! Free the memory from buffers and other stuff, destroy unneded matrices
    virtual ~Diff();

    PetscReal get_navier_stokes_coefficient() {
        return factor * p_opt_->nu * p_mod_->dt;
    }

    PetscReal get_numerical_viscosity_coefficient() {
        return factor * epsilon_ * p_mod_->dt * p_mod_->dt;
    }

    PetscReal get_complete_diff_coefficient() {
        return factor * (p_opt_->nu + epsilon_ * p_mod_->dt) * p_mod_->dt;
    }

    bool zero_diffusion() {
        return p_opt_->nu + epsilon_ * p_mod_->dt == 0;
    }

    //!
    //! \brief set_diff_mat Set the laplacian matrix that will be used for the diffusion.
    //!
    //! Does not take ownership of the matrix, only grabs a reference.
    //! \param diff_mat Matrix that will be used to compute diffusion.
    //! \return Error Code
    //!
    PetscErrorCode set_diff_mat(Mat diff_mat);

    //! Set the multiplicative scaling for the matrix
    //!
    PetscErrorCode set_num_visc_scaling(PetscReal epsilon);

    //! Add diffusion of U to V, V get an addition (inline)
    PetscErrorCode vec_add_diff(Vec U, Vec V);

    //! Add diffusion to matrix M (for implicity schemes), does not take care
    //!  of M, just modifies
    //! Add diffusion to the matrix M. Diffusion is considered as numerical
    //! diffusion AND vicosity for NS eqs.
    //! Here we assume that diff_mat_ is the diffusion matrix, scaled as
    //! \f$\Delta V \cdot \Delta x \Delta y\f$
    //! The returned diffusion is something of the form
    //! \f$(\epsilon \Delta t + \cdot \nu) \Delta t \Delta V\f$
    //! @param s Scaling, an optional scaling factor, usually 0.5 is used
    //! for midpoint rule, 1 for backwards Euler
    PetscErrorCode add_diffusion_to_matrix(Mat input_matrix);

    PetscErrorCode update_shifted_matrix();

    //!
    //! \brief diff_mat_has_changed Check if diffusion matrix has changed.
    //!
    //! If \f$dt\f$ is different from the last used (stored in \old_dt_) or if we explicitly
    //! setted diff_mat_has_changed_ to true, then returns true, meaning that we need to change
    //! and recompute the shifted matrix. Otherwise we can recycle the old matrix.
    //!
    //! After this call, diff_mat_has_changed_ is set to false
    //! \return
    //!
    bool diff_mat_has_changed(void);

    // Happy friends, those are function that need access to protected members but cannot be included in class due to
    // PETSc function pointer
    friend PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat J,
                                                Mat jac,
                                                MatStructure * str, void *ctx);
    friend PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat J,
                                                Mat jac, void *ctx);

    //! Matrix containing shifted
    //! Laplace matrix (I + D), update it with mat_upd_diff
    Mat                           shift_diff_mat_;
    //! Matrix containing Laplace matrix (just an handle, do not modify)
    Mat                           diff_mat_;
    //! Matrix containing Laplace matrix for numerical diffusion
    Mat                           num_diff_mat_;
    //! Vector containing RHS to be passed to Compute Operators
    Vec                           rhs_vector;
    //! The backwardness of the method
    //! (1 mean fully backwards, 0 means no backwards)
    PetscReal theta;
    //! What fraction of dt is the diffusion supposed to handle?
    PetscReal factor;
protected:
    //! Function that consturcts the matrix for diffusion
    #ifdef PETSC_OLDAPI_KSP // Old API fix (v. < 3.5)
    PetscErrorCode (*compute_mat)(KSP, Mat, Mat, MatStructure *, void *);
    #else
    PetscErrorCode (*compute_mat)(KSP , Mat , Mat , void *);
    #endif
    //! Function that constructs the right hand side for the diffusion
    PetscErrorCode (*compute_rhs)(KSP , Vec , void *);

    //! Composited pointers
    Options               * const p_opt_;
    Domain                * const p_dom_;
    ModelBase             * const p_mod_;
    Attila                * const p_cm_;
    Projection            * const p_proj_;

    //! Flag to check if a diffusion matrix is present.
    bool                          has_diff_mat_;
    //!
    bool                          has_shift_diff_mat_;
    //! Store the last dt that has been used before a call to
    //!  diff_mat_has_changed(void)
    PetscReal                     old_dt_;
    //! Each time the matrix is modified this turns true, and
    //! is resetted only by diff_mat_has_changed(void)
    bool                          diff_mat_has_changed_;
public:
    //! Numerical diffusion coefficient
    PetscReal                     epsilon_;
    //! Is the diffusion matrix the same as the numerical diff matrix?
    bool                          diff_mat_same_as_num_mat_;
};
