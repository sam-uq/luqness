/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "diff.h"

Diff::Diff(Options *p_opt, Domain *p_dom,
           ModelBase *p_mod, Attila *p_cm,
           Projection * p_proj, PetscReal theta, PetscReal factor)
    : theta(theta), factor(factor)
    , p_opt_ (p_opt), p_dom_(p_dom), p_mod_(p_mod)
    , p_cm_(p_cm), p_proj_(p_proj)
    , has_diff_mat_(false), has_shift_diff_mat_(false) {
    FUNCTIONBEGIN(p_cm_);
}

Diff::~Diff() {
    FUNCTIONBEGIN(p_cm_);

    if( has_shift_diff_mat_ ) {
        ierr = MatDestroy(&shift_diff_mat_);  CHKERRV(ierr);
        has_shift_diff_mat_ = false;
    }
}

PetscErrorCode Diff::set_diff_mat(Mat diff_mat) {
    has_diff_mat_ = true;
    diff_mat_ = diff_mat;
    num_diff_mat_ = diff_mat;
    diff_mat_has_changed_ = true;
    diff_mat_same_as_num_mat_ = true;
    PetscFunctionReturn(ierr);
}

PetscErrorCode Diff::set_num_visc_scaling(PetscReal epsilon) {
    epsilon_ = epsilon;
    diff_mat_has_changed_ = true;
    PetscFunctionReturn(ierr);
}

bool Diff::diff_mat_has_changed() {
    bool ret = false;
    if( p_mod_->dt != old_dt_ || diff_mat_has_changed_ ) {
        old_dt_ = p_mod_->dt;
        ret = true;
    }
    diff_mat_has_changed_ = false;
    return ret;
}

PetscErrorCode Diff::vec_add_diff(Vec U, Vec U_out) {
    FUNCTIONBEGIN(p_cm_);

    // Find out if we really need to add viscosity to U (in V),
    // and how to add it.
    Vec U_diff;
    ierr = DMGetGlobalVector(p_dom_->da_sc, &U_diff); CHKERRQ(ierr);
    if( zero_diffusion() ) {
        // This means no diffusion is added, skip this step
        PetscFunctionReturn(ierr);
    }
    // Make sure vector is scaled correctly
    ierr = VecScale(U_out,
                    p_proj_->matrix_scaling(p_dom_->da_sc)); CHKERRQ(ierr);

    if( diff_mat_same_as_num_mat_ ) {
        // V = scaling * (nu + eps*dt) * dt * \Delta * U
        ierr = MatMult(diff_mat_, U, U_diff); CHKERRQ(ierr);
        ierr = VecAXPY(U_out, (1-theta) * get_complete_diff_coefficient(),
                       U_diff); CHKERRQ(ierr);
    } else {
        // V = scaling*nu*dt*\Delta*U + scaling*eps*dt^2*\Delta_num*U
        ierr = MatMult(diff_mat_, U, U_diff); CHKERRQ(ierr);
        ierr = VecAXPY(U_out, (1-theta) * get_navier_stokes_coefficient(),
                       U_diff); CHKERRQ(ierr);
        ierr = MatMult(num_diff_mat_, U, U_diff); CHKERRQ(ierr);
        ierr = VecAXPY(U_out,
                       (1-theta) * get_numerical_viscosity_coefficient(),
                       U_diff); CHKERRQ(ierr);
    }
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &U_diff); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

PetscErrorCode Diff::add_diffusion_to_matrix(Mat input_matrix) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->nu + epsilon_ * p_mod_->dt == 0 ) {
        // This means no diffusion is added, skip this step
        PetscFunctionReturn(ierr);
    } else if( diff_mat_same_as_num_mat_ ) {
        // Combined sclaing including time stepping
        PetscReal scaling_factor = -theta*get_complete_diff_coefficient();
        ierr = MatAXPY(input_matrix, scaling_factor,
                       diff_mat_, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    } else {
        ierr = MatAXPY(input_matrix,
                       -theta*get_navier_stokes_coefficient(),
                       diff_mat_, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
        ierr = MatAXPY(input_matrix,
                       -theta*get_numerical_viscosity_coefficient(),
                       num_diff_mat_, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    }
    PetscFunctionReturn(ierr);
}

PetscErrorCode Diff::update_shifted_matrix() {
    FUNCTIONBEGIN(p_cm_);
    if( !has_shift_diff_mat_ ) {
        ierr = CreateMatrix(p_dom_->da_sc,
                            &shift_diff_mat_); CHKERRQ(ierr);
        has_shift_diff_mat_ = true;
    }
    assert(diff_mat_same_as_num_mat_ && "Not yet implemented!");
    // TODO: can be smarter here
    ierr = MatCopy(diff_mat_,
                   shift_diff_mat_, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    ierr = MatScale(shift_diff_mat_,
                    -theta*get_complete_diff_coefficient() ); CHKERRQ(ierr);
    ierr = MatShift(shift_diff_mat_,
                    p_proj_->matrix_scaling(p_dom_->da_sc)); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat mat, Mat jac,
                                     MatStructure *, void *ctx) {
    return ComputeMatrixDiff(ksp_, mat, jac, ctx);
}

PetscErrorCode ComputeMatrixDiff(KSP ksp_, Mat, Mat jac, void *ctx) {
    Diff * p_diff = (Diff*) ctx;
    DEBUG(p_diff->p_cm_, INTRALVL,
          "[MAT] Setting up matrix for diffusion with dt = %f and factor = %d...\n",
          p_diff->p_mod_->dt, p_diff->factor);

    DM da;
    ierr  = KSPGetDM(ksp_, &da); CHKERRQ(ierr);
    ierr = p_diff->p_proj_->laplacian_matrix<1>(da, jac,
                                              p_diff->p_dom_->bd_fam.U);
                                              CHKERRQ(ierr);

    // Scale matrix to account for dt, Navier-Stokes and diffusion
    ierr = MatScale(jac, -p_diff->theta*p_diff->get_complete_diff_coefficient() );
    CHKERRQ(ierr);
    ierr = MatShift(jac, p_diff->p_proj_->matrix_scaling(da)); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
