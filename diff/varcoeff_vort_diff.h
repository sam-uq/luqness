/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file varcoeff_vort_diff.h
//!
#include "vort_diff.h"

//!
//! \brief ComputeMatrixDiffCoeff
//! \param ksp_
//! \param J
//! \param jac
//! \param str
//! \param ctx
//! \return
//!
PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat J,Mat jac,
                                      MatStructure * str, void *ctx);
PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat J,Mat jac, void *ctx);

//!
//! \brief apply_lpl_stencil3d_diff
//! \param factor
//! \param ncols
//! \param col
//! \param v_c
//! \param v
//! \param bdtp
//! \param row
//! \param end_idx
//! \param V_arr
//!
//!
//! Shorthand for the laplacian index generator, templated as much as
//! possible for each coordinate left, right, bottom,
//! top, down, up and for extension ext
//! TODO comment all params
template <int dim, bool begin, int ext>
inline void apply_lpl_stencil3d_diff(PetscReal factor, int * ncols, MatStencil * col,
                                     PetscReal * v_c, PetscReal * v,
                                     BCType::BCType bdtp, MatStencil row,
                                     PetscInt end_idx, Field3D *** V_arr);

//!
//! \brief apply_lpl_stencil2d_diff
//! \param factor
//! \param ncols
//! \param col
//! \param v_c
//! \param v
//! \param bdtp
//! \param row
//! \param end_idx
//! \param V_arr
//!
//! Shorthand for the laplacian index generator, templated as much as
//! possible for each coordinate left, right, bottom,
//! top, down, up and for extension ext, in 2D dimensions
//! @tparam dim dimension index, 0 for i, 1 for j and 2 for k, undefined for the rest
//! @tparam begin flag to decide wether we are in the negative stancil
//! or in the positive one
//! @tparam ext, used for extended laplacians: extension of the stencil
//! TODO comment all params
template <int dim, bool begin, int ext>
inline void apply_lpl_stencil2d_diff(PetscReal factor, int * ncols, MatStencil * col,
                                     PetscReal * v_c, PetscReal * v,
                                     BCType::BCType bdtp, MatStencil row,
                                     PetscInt end_idx, Field2D ** V_arr);

//!
//! \brief The VarcoeffVortDiff class
//!
class VarcoeffVortDiff : public VortDiff {
public:
    //! Constructor, allocate space for temporary vectors,
    //! solution vectors and for laplacian matrix
    VarcoeffVortDiff(Options *p_opt, Domain *p_dom,
                     ModelBase *p_mod, Attila *p_cm,
                     Projection * p_proj, PetscReal theta, PetscReal factor = 1);
    //! Free the memory from buffers and other stuff, destroy unneded matrices
    virtual ~VarcoeffVortDiff();

    //! Set the laplacian matrix to be used as diffusion (does not
    //! take care of the matrix, just get a pointer)
    PetscErrorCode set_diff_mat(Mat diff_mat);

    //! Set the coefficient vector to what you want, triggers diff has changed
    //! and DOES NOT GET OWNERSHIP OF THE VECTOR
    PetscErrorCode set_coeff_vec(Vec V);

    template <uint ext>
    PetscErrorCode laplacian_matrix_coeff(DM da, Mat jac, const BD & bd,
                                          DM da_coeff, Vec coeff);

    //! Happy friends, those are function that need access to protected
    //! members but cannot be included in class due to
    //! PETSc function pointer
    friend PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat J,
                                                 Mat jac, MatStructure * str, void *ctx);
    friend PetscErrorCode ComputeMatrixVarDiffCoeff(KSP ksp_, Mat J,
                                                 Mat jac, void *ctx);
protected:
    //! Coefficient vector for diffusion. Quantities are stored edge-wise,
    //! and we use the following convention:
    //! edges are indexed with integer indices + 1 half, integer index,
    //! e.g k,j+1/2,i. As integer PETSc/C++ indices we
    //! use the mapping k,j+1/2,i ---> k,j+1,i (so that an edge is stored
    //! at the NEXT (in terms of left to right) cell
    //! center)
    Vec coeff_vec;

    //! Was the num diff matrix allocated, perhaps?
    bool num_diff_mat_alloc_;
};

//! Allocate and build the matrix A for the projection method
//! @tparam ext Extension of the stencil: the standard
//! Laplacian uses ext=1, the decoupled extended Laplacian uses ext=2
template <uint ext>
PetscErrorCode VarcoeffVortDiff::laplacian_matrix_coeff(DM da, Mat jac,
                                                        const BD & bd,
                                                        DM da_coeff, Vec coeff) {
    PetscInt       mx, my, mz, xm, ym, zm, xs, ys, zs;
    PetscScalar    Hx, Hy, Hz;

    ierr  = DMDAGetInfo(da, 0, &mx, &my, &mz, 0, 0, 0, 0, 0, 0, 0, 0, 0); CHKERRQ(ierr);

    // Current domain size
    Hx = p_dom_->l_x / (PetscReal) (mx * ext);
    Hy = p_dom_->l_y / (PetscReal) (my * ext);
    Hz = p_dom_->l_z / (PetscReal) (mz * ext);

    PetscScalar         v_c;
    PetscInt            ncols;

    Vec loc_coeff;
    ierr = DMGetLocalVector(da_coeff, &loc_coeff); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(da_coeff, coeff, INSERT_VALUES, loc_coeff); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(da_coeff, coeff, INSERT_VALUES, loc_coeff); CHKERRQ(ierr);

    // TODO: only periodic
    ierr = MatSetOption(jac, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_TRUE); CHKERRQ(ierr);
    ierr = MatSetOption(jac, MAT_SYMMETRIC, PETSC_TRUE); CHKERRQ(ierr);
    //   ierr = MatSetOption(jac, MAT_SPD, PETSC_TRUE); CHKERRQ(ierr); // absolutely WRONG
    ierr = MatSetFromOptions(jac); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) {
        PetscScalar       v[7],HyHzdHx,HxHzdHy,HxHydHz;
        MatStencil        row; // This is the 7 dimensional stencil for the laplacian matrix

        HyHzdHx = Hy*Hz/Hx;
        HxHzdHy = Hx*Hz/Hy;
        HxHydHz = Hx*Hy/Hz;
        ierr  = DMDAGetCorners(da, &xs, &ys, &zs, &xm, &ym, &zm); CHKERRQ(ierr);

        Field3D *** V_arr;

        ierr = DMDAVecGetArray(da_coeff, loc_coeff, &V_arr); CHKERRQ(ierr);

        DEBUG(p_cm_, INTRADM, "Assembly %dx%dx%d variable coefficients Laplacian matrix (ext = %d)...\n",
              mx*my*mz, mx*my*mz, mx*my*mz, ext);
        for (int k=zs; k<zs+zm; k++) {
            for (int j=ys; j<ys+ym; j++) {
                for (int i=xs; i<xs+xm; i++) {
                    ncols = 0; // Number of elements really in the stencil
                    MatStencil    col[7] = {{0,0,0,0}}; // Allocate a zero stencil
                    v_c = 0;
                    row.k = k;             row.j = j;          row.i = i;          row.c = 0;

                    // HANDLES LEFT (x axis) boundaries
                    apply_lpl_stencil3d_diff<0, true, ext>(HyHzdHx, &ncols, col, &v_c, v, bd.left(), row, 0, V_arr);
                    apply_lpl_stencil3d_diff<0, false, ext>(HyHzdHx, &ncols, col, &v_c, v, bd.right(), row, mx, V_arr);
                    apply_lpl_stencil3d_diff<1, true, ext>(HxHzdHy, &ncols, col, &v_c, v, bd.bottom(), row, 0, V_arr);
                    apply_lpl_stencil3d_diff<1, false, ext>(HxHzdHy, &ncols, col, &v_c, v, bd.top(), row, my, V_arr);
                    apply_lpl_stencil3d_diff<2, true, ext>(HxHydHz, &ncols, col, &v_c, v, bd.down(), row, 0, V_arr);
                    apply_lpl_stencil3d_diff<2, false, ext>(HxHydHz, &ncols, col, &v_c, v, bd.up(), row, mz, V_arr);

                    col[ncols].k = k;
                    col[ncols].j = j;
                    col[ncols].i = i;
                    v[ncols++] = v_c;
                    ierr = MatSetValuesStencil(jac, 1, &row, ncols, col, v, INSERT_VALUES); CHKERRQ(ierr);
                }
            }
        }
        ierr = DMDAVecRestoreArray(da_coeff, loc_coeff, &V_arr); CHKERRQ(ierr);

    } else { // 2D Case
        MatStencil          row;
        PetscScalar         HydHx, HxdHy;
        HxdHy = Hx/Hy;
        HydHx = Hy/Hx;
        ierr  = DMDAGetCorners(da, &xs, &ys, &zs, &xm, &ym, &zm); CHKERRQ(ierr);

        Field2D ** V_arr;
        ierr = DMDAVecGetArray(da_coeff, loc_coeff, &V_arr); CHKERRQ(ierr);

        DEBUG(p_cm_, INTRADM, "Assembly 2D %dx%d variable coefficients Laplacian matrix (ext = %d)...\n",
              mx*my, my*my, ext);
        PetscScalar    v[5];
        int            k = 0;
        for (int j=ys; j<ys+ym; ++j) {
            for (int i=xs; i<xs+xm; ++i) {
                ncols = 0;
                MatStencil    col[5] = {{0,0,0,0}};
                v_c = 0;
                row.k = k;             row.j = j;          row.i = i;          row.c = 0;

                apply_lpl_stencil2d_diff<0, true, ext>(HydHx, &ncols, col, &v_c, v, bd.left(), row, 0, V_arr);
                apply_lpl_stencil2d_diff<0, false, ext>(HydHx, &ncols, col, &v_c, v, bd.right(), row, mx, V_arr);
                apply_lpl_stencil2d_diff<1, true, ext>(HxdHy, &ncols, col, &v_c, v, bd.bottom(), row, 0, V_arr);
                apply_lpl_stencil2d_diff<1, false, ext>(HxdHy, &ncols, col, &v_c, v, bd.top(), row, my, V_arr);

                col[ncols].k = k;
                col[ncols].j = j;
                col[ncols].i = i;
                v[ncols++] = v_c;
                ierr = MatSetValuesStencil(jac, 1, &row, ncols, col, v, INSERT_VALUES); CHKERRQ(ierr);
            }
        }

        ierr = DMDAVecRestoreArray(da_coeff, loc_coeff, &V_arr); CHKERRQ(ierr);
    }

    ierr = MatAssemblyBegin(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(da_coeff, &loc_coeff); CHKERRQ(ierr);

    if ( bd.has_nullspace() ) {
        WARNING(p_cm_, INTRADM, "Detected nullspace...\n", "");
        MatNullSpace nullspace;

        ierr = MatNullSpaceCreate(p_cm_->intra_domain_comm, PETSC_TRUE, 0, 0, &nullspace);CHKERRQ(ierr);
        ierr = MatSetNullSpace(jac,nullspace); CHKERRQ(ierr);
        ierr = MatNullSpaceDestroy(&nullspace); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

template <int dim, bool begin, int ext>
inline void apply_lpl_stencil3d_diff(PetscReal factor, PetscInt * ncols, MatStencil * col,
                                     PetscReal * v_c, PetscReal * v, BCType::BCType bdtp,
                                     MatStencil row, PetscInt end_idx, Field3D *** V_arr)
{
    PetscInt k_ = row.k;
    PetscInt j_ = row.j;
    PetscInt i_ = row.i;
    PetscInt ref_idx = 0;
    if( dim == 0 ) {
        ref_idx = row.i;
        begin ? i_ -= ext: i_ += ext;
    } else if (dim == 1) {
        ref_idx = row.j;
        begin ? j_ -= ext: j_ += ext;
    } else if( dim == 2 ) {
        ref_idx = row.k;
        begin ? k_ -= ext: k_ += ext;
    }

    if( begin ) {
        factor *= std::abs(V_arr[row.k][row.j][row.i][dim]);
    } else {
        factor *= std::abs(V_arr[k_][j_][i_][dim]);
    }

    if( ( begin && ref_idx > 0 ) || ( !begin && ref_idx < end_idx - 1 ) ) {
        col[*ncols].k = k_;
        col[*ncols].j = j_;
        col[*ncols].i = i_;
        v[(*ncols)++] = factor;
        *v_c -= factor;
    } else {
        switch( bdtp ) {
        case BCType::Neumann:
            break;
        case BCType::Dirichlet:
            *v_c -= 2.* factor;
            break;
        case BCType::Periodic:
        default:
            col[*ncols].k = k_;
            col[*ncols].j = j_;
            col[*ncols].i = i_;
            v[(*ncols)++] = factor;
            *v_c -= factor;
            break;
        }
    }
}

template <int dim, bool begin, int ext>
inline void apply_lpl_stencil2d_diff(PetscReal factor, PetscInt * ncols, MatStencil * col,
                                     PetscReal * v_c, PetscReal * v, BCType::BCType bdtp,
                                     MatStencil row, PetscInt end_idx, Field2D ** V_arr) {
    PetscInt j_ = row.j;
    PetscInt i_ = row.i;
    PetscInt ref_idx = 0;
    if( dim == 0 ) {
        ref_idx = row.i;
        begin ? i_ -= ext: i_ += ext;
    } else if (dim == 1) {
        ref_idx = row.j;
        begin ? j_ -= ext: j_ += ext;
    }

    if( begin ) {
        //   factor *= abs(V_arr[row.j][row.i][dim]);
        factor *= std::abs(V_arr[row.j][row.i][dim] + V_arr[row.j-1][row.i][dim]
                + V_arr[row.j][row.i-1][dim] + V_arr[row.j-1][row.i-1][dim]) / 4.;
        //   factor *= max(abs(V_arr[row.j][row.i][0] + V_arr[row.j-1][row.i][0]
        //   + V_arr[row.j][row.i-1][0] + V_arr[row.j-1][row.i-1][0]) / 4.,
        //      abs(V_arr[row.j][row.i][1] + V_arr[row.j-1][row.i][1]
        //   + V_arr[row.j][row.i-1][1] + V_arr[row.j-1][row.i-1][1]) / 4.);
    } else {
        //   factor *= abs(V_arr[j_][i_][dim]);
        factor *= std::abs(V_arr[j_][i_][dim] + V_arr[j_][i_-1][dim]
                + V_arr[j_-1][i_][dim] + V_arr[j_-1][i_-1][dim]) / 4.;
        //   factor *= max(abs(V_arr[j_][i_][0] + V_arr[j_][i_-1][0]
        //   + V_arr[j_-1][i_][0] + V_arr[j_-1][i_-1][0]) / 4.,
        //     abs(V_arr[j_][i_][1] + V_arr[j_][i_-1][1]
        //   + V_arr[j_-1][i_][1] + V_arr[j_-1][i_-1][1]) / 4.);
    }

    if( ( begin && ref_idx > 0 ) || ( !begin && ref_idx < end_idx - 1 ) ) {
        col[*ncols].j = j_;
        col[*ncols].i = i_;
        v[(*ncols)++] = factor;
        *v_c -= factor;
    } else {
        switch( bdtp ) {
        case BCType::Neumann:
            break;
        case BCType::Dirichlet:
            *v_c -= 2.* factor;
            break;
        case BCType::Periodic:
        default:
            col[*ncols].j = j_;
            col[*ncols].i = i_;
            v[(*ncols)++] = factor;
            *v_c -= factor;
            break;
        }
    }
}
