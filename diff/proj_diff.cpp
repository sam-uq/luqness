/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "proj_diff.h"

ProjDiff::ProjDiff(Options *p_opt, Domain *p_dom,
                   ModelBase *p_mod, Attila *p_cm,
                   Projection * p_proj, PetscReal theta, PetscReal factor)
    : Diff(p_opt, p_dom, p_mod, p_cm, p_proj, theta, factor) {
    FUNCTIONBEGIN(p_cm_);

    compute_mat = ComputeMatrixDiff;
    compute_rhs = ComputeRHSProjDiff;
}

ProjDiff::~ProjDiff() {
    FUNCTIONBEGIN(p_cm_);
}

PetscErrorCode ComputeRHSProjDiff(KSP ksp_, Vec b, void *ctx) {
    ProjDiff      *p_diff = (ProjDiff*) ctx;
    FUNCTIONBEGIN(p_diff->p_cm_);

    DM da;
    ierr = KSPGetDM(ksp_, &da); CHKERRQ(ierr);
    ierr = VecCopy(p_diff->rhs_vector, b); CHKERRQ(ierr);

    // This maybe needs interpolations
    PetscReal scaling = (1-p_diff->theta)*p_diff->p_proj_->matrix_scaling(da);
    ierr = VecScale(b, scaling); CHKERRQ(ierr);

    // TODO: check if it is needed
    if ( p_diff->p_dom_->bd_fam.U.has_nullspace() ) {
        MatNullSpace nullspace;
        ierr = MatNullSpaceCreate(p_diff->p_cm_->intra_domain_comm,
                                  PETSC_TRUE, 0, 0, &nullspace);
        CHKERRQ(ierr);

        ierr = NullSpaceRemove(nullspace, b); CHKERRQ(ierr);
        ierr = MatNullSpaceDestroy(&nullspace); CHKERRQ(ierr);

    }
    ierr = VecAssemblyBegin(b);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(b);CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
