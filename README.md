[TOC]

# 1. What is this? {#whatis_H1}

lUQness (lUQness: uncertainty quantification for Navier-(Euler)-Stokes simulations) provides tools
for the solution of incompressible Navier-Stokes and Euler equations. Currently the code consists
on two types of solver: a fractional step method in primitive variables (velocity and pressure),
also known as projection method or pressure Poisson method, and a scalar vorticity formulation
for incompressible 2D flows.

Alongside the incompressible Solver, the code provides an infrastructure for the integration of
parametric solutions, currently with Monte Carlo methods, Multilevel Monte Carlo methods and
deterministic Gaussian quadratures.

The code aims, in one hand, to provide suitable ways to compute admissible measure valued solutions
of incompressible flows and, in the other, to allow computation of statistical quantities arising
in the context of uncertainty quantification.

The project originated at ETH Zurich and the code is freely available under Apache 2.0 license
(except otherwise specified). The code is heavily based,and hence dependent, on the PETSc framework.
It uses mainly C++, with Python (with Numpy/Matplotlib) and Bash a pre/post processing tools. Additional,
useful software may include `CMake`, `Doxygen`, `GDB`/`VampirTrace`/`Vite`, `Paraview`/`MayaVI`/`VisIT`.
You can also use `Python` with extra bindings (`vtk`, `matplotlib`/`numpy`) for testing and post processing.

The software has been tested on 10'000+ CPU clusters with solutions up to 10e9 spatial degrees of freedom.

If you spot a bug, missing feature or error, feel free to notify the author with suggestions, patches and requests.


# 2. Prerequisites

Needed packages:
- `MPI` implementation is suggested (tested with `OpenMPI`/`MPICH2`/`Intel MPI`)
- `PETSc` (tested with 3.3.0 to 3.7.0, API changes are needed for each version)
- A `C++` compiler (tested on `icc`, `gcc`, cray C++ compiler, PGI)

Optional packages:
- C++11 (or c++0x if you have boost) or Boost for UQ-related stuff
- Python (with Numpy/Matplotlib) for post/pre-processing
- VisIt or paraview (and qt) (or MayaVI for visualization) for 3D visualization
- CMake for easy compiling
- Doxygen for documentation generation
- VampirTrace (+ Vampir or ViTE for visualization)
- Valgrind, Perftools (Cray) and gdb for debugging/profiling
- VtK for visualizaion (with python bindings, and python2-pyodbc)

## 2.1. PETSc

The code is heavily based on the [PETSc framework](http://www.mcs.anl.gov/petsc/).
PETSc can be freely downloaded and compiled from sources under BSD license.

Needed from PETSc package:
- CXX support

Optional packages:
- SCALApack/MUMPS for parallel LU
- Parmetis/Metis for parallel LU
- Hypre (BoomerAMG) for algebraic multigrid
- Complex numbers and FFTW for spectral decomposition (**not yet implemented**)
- ML for an algebraic multigrid

Following are some host-specific information.

1. Local

    You can use the following command to configure PETSc (for Debug release):

        ./configure --with-clanguage=cxx --download-scalapack --download-mumps --download-parmetis
        --download-metis --with-debugging=0 CXXOPTFLAGS='-O3' --download-fftw

    or, for a Release build (faster):

        ./configure --with-clanguage=cxx --download-scalapack --download-mumps --download-parmetis
        --download-metis --download-fftw CXXOPTFLAGS='-O3 -ffast-math -funroll-loops -mtune=native'

    Remember to choose `PETSC_ARCH` by setting `export PETSC_ARCH=<YOUR_ARCH>`

    You can compile:

        make PETSC_DIR=<PETSC_DIRECTORY> PETSC_ARCH=<YOUR_ARCH> all

    And test the build:

        make PETSC_DIR=<PETSC_DIRECTORY> PETSC_ARCH=PETSCARCH test

    where you replace the directories and arch to the relevant ones.

    You can choose the optimization flag and the debugging type at compile time.
    We suggest to export the variables `PETSC_DIR` and `PETSC_ARCH` on the environment
    (for Linux you may edit `~/.bashrc`).

    You may need to help PETSc find your MPI directories, use e.g. `--with-mpiexec=/usr/bin/mpiexec`. You can also
    `--download-mpich`, but then remember to link/execute with the correct MPI library.

2. Brutus (ETHZ) (Deprecated)

    Install PETSc to the directory you want, usually somewhere in `$HOME`. The procedure is similar
    to the local procedure. For Python configuration you may need to load the
    Python module or run with `python2`.

3. Euler (ETHZ)

    Install PETSc to the directory you want, usually somewhere in `$HOME`. The procedure is
    similar to the local procedure. For Python configuration you may need to load the
    `python` module or run with `python2`.

    Load the appropriate modules (beforehand) using the `euler_modules.sh` scripts (must source the script using `.`).

    As of 19/06/17 the best `gcc`compiler on Euler is probably `gcc/5.2.0`.
    
    ## Option 1, custom MPI:
    
    Use the script `petsc-config/petsc-reconfigure-euler-downloadall-***.py`, *without* loading the PETSc module. 
    This is the safes way to compile PETSc, but might be sub-optimal.
    
    ## Option 2, PETSc module
    
    Use the `PETSc` module (loaded with `./euler_modules.sh`) and call `CMake` with the options:
    
        -DUSE_ALIAS_COMPILERS=ON -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpiCC
    
    
    ## Option 3, Intel compiler
    
    TBA

4. Rosa (CSCS) (Deprecated)

    PETSc is already installed on CSCS's Rosa and Daint under (`cray-petsc`).
    (WARNING: they actually use an older, unsupported by PETSc-dev version and
    newer versions do not always work as expected for every compile/MPI implementation.
    We actually supports old APIs of PETSc (3.3.x, 3.4.x and 3.5.x). All linking/flag
    is taken care by compiler wrappers `cc` and `CC`.

5. Dora (CSCS) (Deprecated)

    Cray's PETSc version 3.5.x is already installed on Dora. Currently there is a
    small issue with PETSc failing to compile CXX files without some preprocessor workaround.

6. Daint (CSCS) \n
    Daint works similarly to Dora.

In `$PETSC_DIR/config/examples/` examples of configurations for PETSc can be found.
Our package comes with some additional examples (located in `/utilities`), which
provides some configuration tuned towards local machines, brutus and euler.
We offer an example of CUDA/ViennaCL compilation and examples of useful external
packages (in particular factorization packages and multigrid preconditoners).

## 2.2. Vampir(Trace)

You may already have OpenMPI wrappers `mpic**-vt` for the MPI compiler, otherwise
download and install the VampirTrace
source code. You may need to export the environment variable `VT_ROOT` to allow proper
CMake finding. CMake may do this for you,
try to enable the flag `VAMPIR_ENABLE`.

# 3. Compile lUQness

After exporting the variables (you can export to `~/.bashrc`):

    PETSC_DIR=<YOUR_PETSC_DIR>

directory of PETSc (found in PETSc dir) and

    PETSC_ARCH=<YOUR_PETSC_ARCH>

the architecture used (i.e. the compiled version of PETSc (e.g. debug or release),
a folder in `<YOUR_PETSC_DIR>`).

You can compile with `CMake`. You can do:

    mkdir build

    cd build

    cmake $SOURCE_DIR

    make [-j$NUMBER_OF_PROCESSES]

Flags used as options for `cmake`:

| Option              | Type           | Default | Description                                                               |
|:--------------------|:--------------:|:------- | ---------------------------------------------------------------- |
| -DVAMPIR_ENABLE     | bool           | false   | Enable VampirTrace support.                                           |
| -DDOXYGEN_ENABLE    | bool           | false   | Enable Doxygen support.                        |
| -DDBOOST_ENABLE     | bool           | false   | Enable Boost support.                                       |
| -DC11_ENABLE        | bool           | false   | Enable C++11 support.                                                   |
| -DOUTPUT_LEVEL      | int            | 0       | Output level (from 0, silent, to 5, verbose).                                           |
| -DFORMAT            | str            | "BIN"   | File format for export (BIN, VTS, DRAW).                                       |
| -DUSE_COLORS        | bool           | 1       | Enable colorized output.                                           |
| -DMACHINE           | str            | "local" | Type of machine you are compiling from, currently: local, rosa, dora, daint, brutus, euler.                                |

Documentation is generated, if enabled, with:

    make doc

## 3.1. Host specific: local machine




## 3.2. Host specific: Brutus (ETHZ) (Deprecated)

Remember to load the modules on Brutus:

    module load cmake
    module load boost
    module load open_mpi

Optional for profiling:

    module load valgrind

For postprocessing it may be needed:

    module load python
    module load matplotlib


## 3.3. Host specific: Euler (ETHZ)

Firewall blocks PETSc download of external packages, that is, they should be imported manually.

Remember to load the modules (can do on .bashrc):

    module unload gcc
    module load gcc/5.2.0
    module load cmake
    module load boost
    module load open_mpi

Valgrind is currently unavailable:

    module load valgrind

For postprocessing it may be needed:

    module load python
    module load matplotlib

Scratch space is different than Brutus.

InfiniBand is much faster than Brutus, but racks contains less processors (i.e. more MPI internode comm.).

WARNING: make sure `gcc/5.2.0` is loaded when the job is launched,
otherwise the executable won't find the correct `LIBCXX`.

Can source `euler_modules.sh` to load a viable module configuration.


## 3.4 Host specific: Rosa (CSCS) (Deprecated)

Monte Rosa requires the loading of mosules for compilers/libraries, in particular one may need:

    module load PrgEnv-gnu (or -cray, etc...)
    module load boost (optional)
    module load cray-petsc
    module load cmake
    module load python  (for postprocessing)

Rmk 1: VTS output slow on 3.4.0, one may want to set BIN_OUT to toggle the binary output instead.

Can visualize ".vts" on Julier (discontinued), "paraview" needs qt (on Julier). The ssh may be forwarded to X11 for thus:

    ssh -Y [srv]

MPI run is done with `aprun -n N`, no need to load it, but works only via batch system (no "debug" on login nodes).

PETSc APIs are slightly different or ROSA since it uses v. 3.3.x,
the program should recover and choose the right version's APIs.

Currently (08.01.2014) gamg is not working as multigrid
procedure on Rosa (apparently), same for some external package.

Rosa is being discontinued the 07.02.2015.


## 3.5 Host specific: Daint (CSCS)

PETSc works on Daint with a small workaround.


## 3.6 Host specific: Dora (CSCS) (Deprecated)

PETSc works on Dora with a small workaround (as of 02.02.2014).

When launching jobs on Dora remember that each node is made of 24 CPUs.

# 4. Running lUQness

lUQness make use of PETSc options. All the options can be printed with the `-help` flag.
lUQness automatically load a file `conf.cfg`, if available. Otherwise a custom filename
can be used with the `-i filename` option. A basic skelteon is provided, in the build
directory, with comments on some useful option. Some option is listed is this page (cf.
the section option list). Some benchmark configuration is also provided (cf. the benchmark
section for further details).

In order to launch the program on a computer cluster we provide launchers for Brutus, Euler,
Rosa, Dora and Daint as well as a batch launcher (`batch_launcher.sh`) for embarassingly
parallel MC simulations on different jobs.

The script `batch_launcher.sh` should be used if you wish to launch multiple parallel jobs,
it calls `${machine}_launcher.sh`, a script that automatically calls the appropriate batch
system and gets the following options (parsed in `opt_parser.sh`:

| Flag | Description                                                                              |
| ---- | :--------------------------------------------------------------------------------------- |
| n    | Number of processors (should automatically retrieve the number of nodes for batch jobs). |
| w    | Wall-time in the form hh:mm (used only for batch jobs.                                   |
| j    | Job name (for output, batch job name, and std io redirection.                            |
| m    | Mesh size (square mesh) in the form int.                                                 |
| l    | MG levels, if needed.                                                                    |
| s    | Number of simulations.                                                                   |
| i    | Integration type (e.g. quad, mc, mlmc, ...).                                             |
| e    | Viscosity for Navier-Stokes equation (if needed).                                        |
| f    | Index number for first simulation.                                                       |
| t    | Index number for last simulation.                                                        |
| c    | Take timings of simulation with "time".                                                  |
| d    | Send email upons start/end of job (batch only).                                          |
| q    | Don't ask before launching commands (batch jobs).                                        |


# 5. Post processing

Currently PETSc outputs in VtK (`.vts`) or binary format in `pwd` (current working directory).

Postprocessing is currently done via `poseidon` Python routines (plot and other light computations)
on `.bin`and `.vts` files and visualization and basic postprocessing
is done by paraview/VisIT on Vtk `.vts` files.


## 5.1. Testing

You can run `ctest` within the `tests` directory to perform some sanity test (a.k.a unit test).

# 6. FAQ

## lUQness is sloooow

Unfortunately not much can be done. Using multigrid is usually the best choice,
but the convergence speed/scaling higly depends on a lot of parameters. If meshes are
small it is possible to use lu decomposition. For many thing we tried to reuse matrices/factorizations
for suitable options (e.g. non adaptive/fixed time stepping), but this is highly dependent from the problem
type. Anyway the bulk of work is currently the linear algebra so if optimization is done,
the first target should be that.

Make sure both PETSc and luqness are compiled with `-O3` and `mtune`.

## lUQness is a mess

Alas, I wrote this code with a given structure (did not now much of C++) and changed style periodically. Also wrote
this code a looooong time ago: now refactoring all the code would require too much effort. Also lUQness uses PETSc,
which follow the "C-style", whilst I prefer to write in "C++(Qt)-style".

## How to interpret the massive output of lUQness?

For debug purposes lUQness may output a HUGE amount of data, we came up with the following categorization of the output,
which may be chosen by cmake.

First of all we output at 5 different levels:
0. Errors, should always be on to be on the safe side
1. Warnings, outputs when find some inconsistency or problem
2. Normal information, small amount of output but may grow with large number of processors
3. Debug information, large amount of data is being printed (from here we start to output also from SELF communicator)
4. Verbose information, very large amount of data, just to find the one bug you cannot find with gdb

Second, we output from different communicators:
0. WORLD: used for very global outputs, errors and warnings are as such, no prefix
1. INTRA_DOMAIN: output from the single domain, angle brakets prefix <%d/%d>, first numer is color of domain, then size
2. INTRA_LEVEL: output from level
3. INTER_LEVEL: output from roots
4. SELF: output from every provess, squared brakets prefix [%d/%d] with rank and size, may be flagged for type:
    0. '-' super root
    1. '^' root
    2. '|' smallest root
    3. 'number' deepness

Other conventions:
' ---> ' This is outputted in Verbose from every big function.

## How to debug?

You may use print, PETSc helper function (compoling with dbg) and also laucnh in gdb:

    mpirun -np <number_of_processors> xterm -e gdb --args ./luqness <arguments>

then hit run on each terminal.

# 7. Compatibility

lUQness has been tested only on Linux. Following particular implementations have been tested.

PETSc: tested v. 3.3.x, 3.4.x, 3.5.x, 3.6.x, 3.7.x

MPI: tested with OpenMPI (Local, Buruts, Euler and Rosa) on Linux (Ubuntu/CentOS) abd tested with MPICH2 (Rosa)

Compilers:
- gnu (local, Buruts, Euler and Rosa)
- cray (Rosa): flags are different!, no stdcxx11, no Boost
- pgi (Rosa): flags are different!, no stdcxx11, no Boost, no min/max/abs, no map.at
- pathscale: not working
- ic (Intel. Rosa)

# 8. Miscellaneous

See TODO and CHANGELOG for todos and changelogs.

## 8.1 Known issues

Currently there is some missing features, some solver works only on some specific case and

CMake may not set correctly all scripts to executables and `make install` does not work.

It is possible to run the DMDAs on GPUs (`cusp` and `viennacl`)
but there is not an observable performance gain, in
particular because the fast solver multigrid is unavailable.

There is currently no load balanging and intra-level communication
for MLMC simulations, this is done in preprocessing for now. Plan
is to insterd dynamic load balancing and MPI communication infra level.

Some options may be incompatible or nonsensical and lUQness won't
complain and just give crazy results: always check
your options and never trust the code (e.g. using less gauss nodes than simulations).

# 9. Linear solvers

To spy the options and convergence of KSP, those flags may be useful:

    -ksp_view -ksp_monitor_true_residual -ksp_monitor

The option `-log_view`will display a useful profiling information.

Here's a list of linear systems that may appear in the code and
how differently the may have to be taken in consideration.

1. Laplacian (coupled)

    Matrix is a 5-point star-stencil  Laplacian, very standard.
    Does not change at all at each time step, i.e. LU decomposition may be
    worthwhile (with enough memory). Another idea is to use MG to reduce
    memory consumption (and fast computatons).
    However:
    - parallel LU need external packages (`MUMPS` OR vsuperlu-dist`) and it is VERY memory consuming:

          -ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps

    - With MG you may want to use galerking matrices for inner levels and use a parallel LU for coarse solve (from MUMPS or superlu-dist):
      - Outer loop:

            -ksp_type gmres -pc_type mg -pc_mg_galerkin -pc_mg_levels 7

      - Inner loop:

            -mg_levels_ksp_type gmres

      - Coarse solver:

            -mg_coarse_pc_factor_mat_solver_package mumps -mg_coarse_pc_type lu -mg_coarse_ksp_type preonly

2. Exact Laplacian (decoupled/extended)

    Used for the exactly conservative scheme (not all boundary conditions!).

3. Navier-Stokes Diffusion

    The matrix change at each iteration, so LU is not an option, tough one might reuse the factorization
    in some clever way.

4. Implicit transport for conservative scheme

    The matrix change at each iteration, so LU is not an option, moreover the matrix may be poorly conditioned.

# 10. Project structure

The project structure is the following:
 - `/` (root): main program files, documentation files (TODO, README, COPYNG, etc...), Doxygen configuration file, CMake config file and other config files
 - `/cmake-modules`: extra modules used by cmake (e.g. FindPETSc)
 - `/modelbase`: subclasses of ModelBase that provide initial data to the solver
 - `/utilities`: launcher scripts (bsub, slurm, local)
 - `/post`: Python post-processing utilities and examples (DEPRECATED
 - `/projection`: subclasses of Projection that implements a projection operator
 - `/samplers`: subclasses of Sampler that provides different sampler generators (also wrappers)
 - `/solver`: subclasses of Solver that provides various interfaces for numerical methods
 - `/tools`: header files and utlity tools (option reades, math, IO, buffers, buondary tools, etc...)
 - `/transport`: implementation of transport/convection steps
 - `/integrators`: implementation of uncertainity quantification tools (as Mc, MlMc and Quadrature)

# 11. Reference timings/storage

Storage of one snapshot of a solution in binary format takes:

    \f$(8 bytes) \cdot N_x \cdot N_y \cdot N_z \cdot dof\f$

For instance a \f$512^2\f$ vorticity snapshot, takes, in binary
format 2 MB (in addition to an ".info" file with the same basename),
whilst a 3D simulation in \f$512^3\f$, in binary format 1 GB.

Unfortunately the VTS file format also saves the grid, this,
in addition to some metadata, bring the storage for each file to:

    \f$`(8 bytes) \cdot N_x \cdot N_y \cdot N_z \cdot (dof + dim) `$\f$

The timing of a simulation depends largely on the machine structure,
compilers/flags and simulation options. Simulations usually scale as
\f$h^{(dim + 1)}\f$. We provide some benchmark simulation with the results on our machines:

Tests:
a) deterministic projection method for Euler, vortex-sheet on 256^2 mesh
b) deterministic projection method for Euler, 3d taylor-green on 256^3
c) Monte Carlo projection method for Euler, vortex-core on 1024^2
d) Monte Carlo vorticity NavierStokes, vortex-bump on 256^2
e) MLMC vorticity for Navier-Stokes, vortex-bump on 256^2

| # | Test    | CPU time [s]  | Machine                                                                     |
| - | :-----: | ------------: | :-------------------------------------------------------------------------- |
| 1 | a       | 3m3.708s      | Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz                                    |
| 2 | a       | 0m48.714s     | Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz                                    |
| 3 | b       | s             | Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz                                    |
| 4 | c       | s             | Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz                                    |
| 5 | d       | 1.83E+006 s   | Cray XE6                                                                    |
| 6 | e       | 4.64E+002 s   | Cray XE6                                                                    |

# 12. List of options

Here is a comprehensive table of the options for `lUQness` (those API are
at version 0.2 and not compatible with 0.1 config files), in addition
to `PETSc`'s own options. You can also ru n `-help` to print the latest
updated version for you:

## Simulation options

| Option               | Default value     | Description                                                                |
|:-------------------- |:-----------------:|:-------------------------------------------------------------------------- |
| -help                | not set           | Print help and quit.                                                       |
| -name [str]          | unnamed-[ctime]   | Name to give to the simulation.                                            |
| -pretend             | not set           | Pretend to run the simulation but actually don't.                          |
| -enable_3d           | not set           | Enable 3D simulation [experimental].                                       |
| -adaptive            | not set           | Use adaptive CFL number.                                                   |
| -no_force_snapshots  | not set           | Step size is *NOT* forced to enforce selected snapshots.                   |
| -forcing             | not set           | Enable computation with forcing [experimental].                            |

## Options for statistical integration

| Option                               | Default value     | Description                                                                             |
|:------------------------------------ |:-----------------:|:--------------------------------------------------------------------------------------- |
| -smp [str],[str],[int],[int],...     | well              | Choose the type of the sampler (one of: well, load, gauss, boost, c11) and its options. |
| -master_seed                         | -1                | Select a master seed for reproducibility of results.                                    |
| -ref [int]                           | 1                 | Number of refinements (for error convergence study).                                    |
| -sim [int]                           | 1                 | Number of simulations (for MLMC this is the number of simulations on the finest level). |
| -int [str]                           | mc                | Choose the type of integrator (one of: det, mc, mlmc, quad).                            |

A master seed of -1 is converted to a random master seed srand(time(NULL))*PID. Otherwise the same master seed will generate the same simulation. 

## Generic parameters

| Option                                            | Default value     | Description                                                                      |
|:------------------------------------------------- |:-----------------:|:-------------------------------------------------------------------------------- |
| -solver [str]                                     | projection        | Solver type (one of: projection, vortex).                                        |
| -lambda [double]                                  | 0.5               | Set the CFL number.                                                              |
| -T [double]                                       | 1                 | Final time for the simulations.                                                  |
| -itr [int]                                        | -1                | Maximal number of time steps (-1 for None).                                      |
| -dom [int],[int],[int],[int](,[int],[int])        | [0,1]^2(or 3)     | Select domain for the simulation (domain is \f$[a,b]\times[c,d]\times[e,f]\f$).  |
| -mesh [int],[int](,[int])                         | 128,128(,128)     | Specify dimension of (coarsest) mesh.                                            |
| -model [str],...                                  | None              | Select initial data (see below for a list)                                       |
| -bd [char],[char],[char],[char](,[char],[char])   | P,P,P,P(,P,P)     | Select boundary conditions.                                                      |
| -coords                                           |                   | Type of coordinates.                                                             |
| -use_stab [str]                                   | None              | Enable stable scheme (str = backward to use backward 1st order).                 |

Selection of boundary conditions is done with a corresponding character:
- I inflow (must have an outflow condition)
- O outflow 
- P periodic
- N no-slip
- S slip

The selection of the model is done via the model name and extra options. See the Model section or see example configuration files.

## Snapshots options

| Option                                       | Default value       | Description                                                                 |
| :------------------------------------------- | :-----------------: | :-------------------------------------------------------------------------- |
| -save_times [double],...                     | None                | Take snapshots of the simulation at given times.                            |
| -save_fps [double]                           | 0.1                 | Frames per second for snapshots (overrides other snapshot options).         |
| -save_initial                                | None                | Snapshot initial data.                                                      |
| -save_final                                  | None                | Snapshot final data.                                                        |
| -save_velocity                               | None                | Save quantity velocity (in case solver == vortex).                          |
| -save_vorticity                              | None                | Save quantity pressure (in case solver == projection).                      |
| -save_pressure                               | None                | Save quantity pressure (in case solver == projection).                      |
| -save_ps                                     | None                | Save quantity passive scalar (if enabled).                                  |
| -save_gamma2                                 | None                | Save quantity gamma2 for 3D simulations (a bit expensive if the case).      |
| -save_mean                                   | None                | Save statistics of the solution (Mean) (in case int == mc or mlmc).         |
| -save_m2                                     | None                | Save statistics of the solution (Var (M2)) (in case int = mc or mlmc).      |
| -save_sim                                    | None                | Save histograms.                                                            |
| -save_levels                                 | None                |                                                                             |
| -save_diff                                   | None                | Save statistics of the solution (level difference) (in case int == mlmc).   |
| -save_hist                                   | None                | Save histograms.                                                            |
| -save_int                                    | None                | Save statistics of the solution (integral) (in case int == quad).           |
| -save_energy                                 | None                | Save energy (L_2 norm) (maybea bit expensive if the case).                  |
| -save_nothing                                | None                | Snapshot is removed, save I/O, speed up. Has the precedence on everything.  |
| -save_push-to-mav                            | None                | Interval of snapshots that will be used for ensemble computations.          |

## Naviers-Stokes (viscosity) options

| Option                                                              | Default value       | Description                                                                 |
| :-------------------------------------------                        | :-----------------: | :-------------------------------------------------------------------------- |
| -ns_re [double]                                                     | 0                   | Set the value of viscosity (1/Re), has precedence on -ns_re.                |
| -ns_eps [double]                                                    | 0                   | Set the Reynold number value (-ns_eps has the precedence on this).          |
| -ns_explicit [double]                                               | None                | Diffusion is computed explicitly.                                           |

## Projection options

| Option                                       | Default value       | Description                                                                           |
| :------------------------------------------- | :-----------------: | :------------------------------------------------------------------------------------ |
| -proj_use_fft                                | None                | Enable FFT for solution of poisson equation (not working).                            |
| -proj_oldksp                                 | None                | Disable Multigrid (or other set compute operator) as solver for poisson equation.     |
| -proj_exact                                  | None                | Use an exact, decoupled Laplacian projection.                                         |
| -proj_initial_cleansing                      | None                | Do a data divergence cleansing for the initial data.                                  |
| -proj_initial_iterates                       | None                | Compute a couple of iterates for the pressure in the projection method.               |

(WARNING: as of 3.5.0, with MG and divergence free ID, the option `-proj_initial_cleansing` should not be used.)

## Projection solver options

| Option                                      | Default value     | Description                                                                |
|:------------------------------------------- |:-----------------:|:-------------------------------------------------------------------------- |
| -vp_use_almgren                             | None              | Enable FFT for solution of poisson equation.                               |
| -vp_direct_pressure                         | None              | Use an direct pressure as update for the Projection methods.               |
| -vp_proj_velocity                           | None              | Project velocity instead of acceleration.                                  |
| -vp_use_diff                                | None              | Enable numerical diffusion.                                                |

The option `-vp_direct_pressure` might be implemented only for certain methods.

## Vortex solver options

| Option                                      | Default value     | Description                                                                |
|:------------------------------------------- |:-----------------:|:-------------------------------------------------------------------------- |
| -vort_ftcs                                  | None              | Enable (1st order) FTCS for vortex solver.                                 |

## Transport options

| Option                                      | Default value     | Description                                                                |
|:------------------------------------------- |:-----------------:|:-------------------------------------------------------------------------- |
| -tracer                                     | None              | Do not advect the passive scalar (turn off for efficiency).                |
| -tracer_implicit                            | None              | Use implicit tracer advection (for avoid strong CFL). Not yet impemented.  |
| -viscosity [double]                         | 0.0005            | Viscosity for transport of passive scalar.                                 |
| -porosity [double]                          | 1.                | Porosity for transport of passive scalar.                                  |

## General options

| Option                                      | Default value     | Description                                                                |
|:------------------------------------------- |:-----------------:|:-------------------------------------------------------------------------- |
| -help                                       | None              | Print help and quit.                                                       |


## ModelBase


The option is selected as follows:

    -model <initial_data>,<op1>,<op2>,...
    
List of initial data:

| Option                 | Argument types     | Argument default     | Description                                                                |
|:---------------------- |:------------------:|:---------------------|:---------------------------------------------------------------------------|
| core                   |                    |                      | Vortex core (smooth): two kissing vortices (Dirac measures on a ball).     |
| dbg                    |                    |                      | Debugging initial data (checkerboard).                                     |
| disc-core              |                    |                      | Non-smooth `core` variant.                                                 |
| disc-sl                |                    |                      | Discontinuous Vortex-Sheet (double layer, suitable for periodic data).     |
| disc-sl-single         |                    |                      | Discontinuous Vortex-Sheet (single layer).                                 |
| div-test               |                    |                      | Debugging test for Divergence cleaning (analytic function).                |
| driven-cavity          |                    |                      | Lid driven cavity initial data.                                            |
| kl                     |                    |                      | Vortex-Blob with additive Karhunen-Loeve expansion.                        |
| load                   |                    |                      | Load data from file.                                                       |
| patch                  |                    |                      | Vortex patch (indicator function for vorticity).                           |
| proj-test              |                    |                      | Debugging test for Projection.                                             |
| reference              |                    |                      | Manifactured solution.                                                     |
| rnd-int-disc-sl        |                    |                      | Random discontinuous Vortex-Sheet (double layer).                          |
| rnd-int-disc-sl-single |                    |                      | Random discontinuous Vortex-Sheet (single layer).                          |
| rnd-int-sl             |                    |                      | Random smooth Vortex-Sheet (double layer).                                 |
| rnd-int-sl-single      |                    |                      | Random smooth Vortex-Sheet (single layer).                                 |
| rnd-patch              |                    |                      | Perturbed Vortex-Patch.                                                    |
| rnd-pert-sl            |                    |                      | Perturbed Vortex-Sheet.                                                    |
| sl                     |                    |                      | Smooth Vortex-Sheet (double layer).                                        |
| sl-single              |                    |                      | Smooth Vortex-Sheet (single layer).                                        |
| steady                 |                    |                      | Steady state of Euler.                                                     |
| tg                     |                    |                      | Taylor-Green vortex.                                                       |
| vortex-box             |                    |                      | ???                                                                        |
| vortex-stream          |                    |                      | ???                                                                        |

# 13. License/Copyright/Warranty/Liability

The software is provided as is, no guarantee is made. Many functionalities are still unavailable/experimental.

See COPYING for additional information.

Licensed under Apache v. 2.0. Developed at ETH Zurich. (c) 2014-17 Filippo Leonardi.

Last updated 06.01.2017.
