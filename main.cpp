#include <petscsys.h>

#include "./attila/attila.h"

#include "./integrators/integrator_serial.h"
#include "./integrators/integrator_parallel.h"

// External definition
PetscErrorCode        ierr;
Profiler              * p_profiler;

int main(int argc,char **argv) {
//    static_assert(std::is_same<int, PetscInt>::value == false, "int != PetscInt");

    // Retrieve mpi data
    ierr = PetscInitialize(&argc, &argv, NULL, NULL); CHKERRQ(ierr);

    IntegratorSkeleton * p_int;

    // Parse options from command line and performs checks
    Options * p_opt = new Options();
    p_opt->read_options();

    // Allocate the model, this calls "new" on the with model, don't forget to delete
    // Can move this before attila and after options, just read options and sets stuff, then here we use set_domain etc
    // This way we can use predefined model options
    ModelBase * p_mod;
    InstantiateModel(p_opt, p_mod);
    ierr = p_mod->print(); CHKERRQ(ierr);

    Attila * p_cm = new Attila(p_opt);

    if(p_opt->alloc_only) {
        ierr = PetscFinalize(); CHKERRQ(ierr);
        PetscFunctionReturn(ierr);
    }

    p_profiler = new Profiler(p_cm);

    //
    switch( p_opt->integrator_type ) {
    case IntegratorType::Parallel:
        p_int = new IntegratorParallel(p_opt, p_mod, p_cm);
        break;
    case IntegratorType::Serial:
    default:
        p_int = new IntegratorSerial(p_opt, p_mod, p_cm);
        break;
    }
    p_int->start();

    //
    delete p_int;
    p_profiler->set_name(p_opt->simulation_name);
    delete p_profiler;
    delete p_cm;
    delete p_mod;
    delete p_opt;

    //
    ierr = PetscFinalize(); CHKERRQ(ierr);
    PetscFunctionReturn(ierr);
}
