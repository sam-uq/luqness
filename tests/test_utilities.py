#! /usr/bin/env python2
# -*- coding: utf-8 -*-

petsc_errors = {
    "PETSC_ERR_MEM" : "55", #unable to allocate requested memory */
    "PETSC_ERR_SUP" : "56", #no support for requested operation */
    "PETSC_ERR_SUP_SYS" : "57", #no support for requested operation on this computer system */
    "PETSC_ERR_ORDER" : "58", #operation done in wrong order */
    "PETSC_ERR_SIG" : "59", #signal received */
    "PETSC_ERR_FP" : "72", #floating point exception */
    "PETSC_ERR_COR" : "74", #corrupted PETSc object */
    "PETSC_ERR_LIB" : "76", #error in library called by PETSc */
    "PETSC_ERR_PLIB" : "77", #PETSc library generated inconsistent data */
    "PETSC_ERR_MEMC" : "78", #memory corruption */
    "PETSC_ERR_CONV_FAILED" : "82", #iterative method (KSP or SNES) failed */
    "PETSC_ERR_USER" : "83", #user has not provided needed function */
    "PETSC_ERR_SYS" : "88", #error in system call */
    "PETSC_ERR_POINTER" : "70", #pointer does not point to valid address */
    "PETSC_ERR_ARG_SIZ" : "60", #nonconforming object sizes used in operation */
    "PETSC_ERR_ARG_IDN" : "61", #two arguments not allowed to be the same */
    "PETSC_ERR_ARG_WRONG" : "62", #wrong argument (but object probably ok) */
    "PETSC_ERR_ARG_CORRUPT" : "64", #null or corrupted PETSc object as argument */
    "PETSC_ERR_ARG_OUTOFRANGE" : "63", #input argument, out of range */
    "PETSC_ERR_ARG_BADPTR" : "68", #invalid pointer argument */
    "PETSC_ERR_ARG_NOTSAMETYPE" : "69", #two args must be same object type */
    "PETSC_ERR_ARG_NOTSAMECOMM" : "80", #two args must be same communicators */
    "PETSC_ERR_ARG_WRONGSTATE" : "73", #object in argument is in wrong state, e.g. unassembled mat */
    "PETSC_ERR_ARG_TYPENOTSET" : "89", #the type of the object has not yet been set */
    "PETSC_ERR_ARG_INCOMP" : "75", #two arguments are incompatible */
    "PETSC_ERR_ARG_NULL" : "85", #argument is null that should not be */
    "PETSC_ERR_ARG_UNKNOWN_TYPE" : "86", #type name doesn't match any registered type */
    "PETSC_ERR_FILE_OPEN" : "65", #unable to open file */
    "PETSC_ERR_FILE_READ" : "66", #unable to read from file */
    "PETSC_ERR_FILE_WRITE" : "67", #unable to write to file */
    "PETSC_ERR_FILE_UNEXPECTED" : "79", #unexpected data in file */
    "PETSC_ERR_MAT_LU_ZRPVT" : "71", #detected a zero pivot during LU factorization */
    "PETSC_ERR_MAT_CH_ZRPVT" : "81", #detected a zero pivot during Cholesky factorization */
    "PETSC_ERR_INT_OVERFLOW" : "84",
    "PETSC_ERR_FLOP_COUNT" : "90",
    "PETSC_ERR_NOT_CONVERGED" : "91", #solver did not converge */
    "PETSC_ERR_MISSING_FACTOR" : "92", #MatGetFactor() failed */
    "PETSC_ERR_OPT_OVERWRITE" : "93", #attempted to over wrote options which should not be changed */
    "PETSC_ERR_MAX_VALUE" : "94", #this is always the one more than the largest error code */
}
petsc_errors = {v: k for k, v in petsc_errors.iteritems()}

import subprocess

import numpy as np
import sys
# TODO: automatic path
sys.path.append("../poseidon/")
import base.tools
import base.data

EXEC_DIR = "../"
EXEC = "luqness"

CONFIG_DIR = None

tolerance = 10e-6

TOLERANCE = 101
err_desc = {
    TOLERANCE : "Equality/Inequality unsatisfied within tolerance.",
}

stage_ = -1

def pretty_petsc_errc(err):
    if str(err) in petsc_errors:
        return petsc_errors[str(err)]
    return "Unknown ERROR"

def failure(num, msg = None):
    msg_ = ""
    if msg:
        msg_ = " [" + msg + "]"
    base.tools.error("[STAGE {0}] Test failure, error code: {1} ('{2}')".format(stage_, num, err_desc[num]) + msg_)
    return num

def run(name, config, check, stages = 1,
        config_dir = CONFIG_DIR,
        pre = None, post = None, opt=None):
    stage_ = -1
    if config_dir == None:
        config_dir = "./" + name + "/"

    # Test Pre-Trigger
    if pre:
        pre()

    # Run test
    outf = open(name+".out", "w")
    errf = open(name+".err", "w")
    COMMAND = [EXEC_DIR+EXEC, 
                    "-name", name]
    if config:
        COMMAND += ["-i", config_dir+config]
    if opt:
        COMMAND += opt
    base.tools.output("Running: "+ " ".join(COMMAND))
    errc = subprocess.call(COMMAND,
                    stdout=outf, stderr=errf
                )
    # Test Post-Trigger
    if post:
        post()
    
    # CHECK for internal failure (error code)
    if errc != 0:
        base.tools.error("[STAGE {0}] Non zero error code ({1}) from C++ run: {2}".format(stage_, errc, pretty_petsc_errc(errc)) )
        # PRETTIFY_ERROR_CODE
    
    for stage in range(0,stages):
        stage_ = stage
        errcc = check(stage)
        if errcc != 0:
            base.tools.error("[STAGE {0}] FAILURE: Non zero error code ({1}) from test run.".format(stage, errcc))
            # PRETTIFY_ERROR_CODE

    if errc == 0 and errcc == 0:
        base.tools.output("Run succesful!")

    return errc or errcc


