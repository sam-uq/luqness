#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import test_utilities
import numpy as np

stages = 1
name = "test_exec"
description = "Run plain executable, no test."

def pre():
    pass

def post():
    pass

def check(stage = 0):
    return 0

def test():
    return test_utilities.run(name, None, check, 1, opt = ["-model", "dbg"])

if __name__ == "__main__":
    err = test()
    exit(err)

