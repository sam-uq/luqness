file(GLOB PROGRAMS *.cpp *.h)
set(PY_FILES test_utilities.py)

foreach(program ${PROGRAMS})
  list(APPEND luqness_SOURCES ${program})
endforeach(program)

set(luqness_SOURCES ${luqness_SOURCES} PARENT_SCOPE)

macro(create_test test_name)
    message(STATUS "New test: " ${test_name})
    if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${test_name}")
#        message(STATUS "Copy!")
        file(COPY ${test_name} DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
          FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
          GROUP_READ GROUP_EXECUTE
          WORLD_READ WORLD_EXECUTE)
      set(CFG_FILES ${CFG_FILES} ${test_name}/conf.cfg)
    endif()
    add_test(NAME ${test_name} COMMAND ${test_name}.py)
    set(PY_FILES ${PY_FILES} ${test_name}.py)
endmacro(create_test)

enable_testing()
create_test("test_proj_ns")
create_test("test_exec")

add_custom_target(unit_test SOURCES ${PY_FILES} ${CFG_FILES})

file(COPY ${PY_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
    GROUP_READ GROUP_EXECUTE
    WORLD_READ WORLD_EXECUTE)

