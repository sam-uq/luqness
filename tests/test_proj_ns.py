#! /usr/bin/env python2
# -*- coding: utf-8 -*-
import test_utilities
import numpy as np

stages = 1
name = "test_proj_ns"
description = "Run a simple Navier-Stokes simularion on a discontinuous vortex-sheet."

def pre():
    pass

def post():
    pass
        
def check(stage = 0):
    results_folder = "./"
    reference_folder = "./"+name+"/"
    
    fname = name + r"_128,128,1_U_sim0_f{0}"
    
    return_f = lambda folder, frame: folder + fname.format(frame)
    
    frames = [0,1,2]
    
    for frame in frames:
        res = test_utilities.base.data.Data(return_f(results_folder, frame))
        print(res.dsize)
        ref = test_utilities.base.data.Data(return_f(reference_folder, frame))
        print(ref.dsize)
        err = (res - ref).norm()
        print(err)
        if np.any(err > test_utilities.tolerance):
            return test_utilities.failure(test_utilities.TOLERANCE)
    
    return 0


def test():
    return test_utilities.run(name, "conf.cfg", check, 1)

if __name__ == "__main__":
    err = test()
    exit(err)
