/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "dynamic_allocator.h"

#define TOL_MAX_IDLE_TIME 0.1
#define TOL_AVG_IDLE_TIME 0.1
#define TOL_TOTAL_RANKS 0.1

//! TODO: function that creates everything above (given lvlv, smallest mesh):
//! levels and coarse_mesh given and size and nodes
//! alloc all
//! mesh_per_level[i] = coarse_mesh * 2^{levels - i - 1}
//! sim_per_level[i] = sim_num_per_level(level)
//! work_per_evel[i] = sim_per_level[i]*(level (2^((3d_or_2d)+1*() levels - i - 1) * *base_work
//! runtime_per_level = work * parallel_efficiency(i, ranks)
DynamicAllocator::DynamicAllocator(Options *p_opt, int world_size)
    : Allocator(p_opt, world_size)
    , base_work(p_opt->base_work)
    , base_sim(p_opt->simulations_array[0]) // TODO
    , order(1) // TODO
    , max_dof_per_rank(p_opt->max_dof_per_rank)
    , min_dof_per_rank(1024)
    , det_parallel_portion(p_opt->det_parallel_portion)
{

    // Work behaviour (exponent)
    PetscReal alpha = 1.;

    sim_per_level_ = new PetscInt[levels_];
    sim_work_per_level_ = new int[levels_];
    perc_work_per_level_ = new double[levels_];
    work_per_level_ = new int[levels_];
    runtime_per_level_ = new double[levels_];

    tot_ranks_per_level_ = new int[levels_];
    ranks_per_parallel_level_ = new PetscInt[levels_];
    parallel_levels_ = new PetscInt[levels_];

    tot_work_ = 0;

    for(int i = levels_ - 1; i >= 0; --i) {
        // FIXME
        sim_per_level_[i] = sim_num_per_level(order, i) * base_sim;
        //      sim_per_level_[i] = base_sim;
    }

    sim_work_per_level_[levels_ - 1] = base_work;
    work_per_level_[levels_ - 1] = sim_work_per_level_[levels_ - 1] * sim_per_level_[levels_ - 1];
    tot_work_ = work_per_level_[levels_ - 1];

    // Compute work per deterministic run and total work/work per level
    for(int i = levels_ - 2; i >= 0; --i) {
        sim_work_per_level_[i] = sim_work_per_level_[i+1]
                * pow(factor_x * factor_y * factor_z * max(factor_x, factor_y, factor_z), alpha);
        work_per_level_[i] = (sim_work_per_level_[i] + sim_work_per_level_[i+1])* sim_per_level_[i];
        tot_work_ += work_per_level_[i];
    }

    // TODO suggest number of processors based on work
    // TODO warn for massive unbalancing (runtime, cores etc...)
    //
    int wall_time = 8 * 60 * 60;
    double time_of_coarsest_sim = 3;
    int suggested_ranks = std::max(std::ceil((double) tot_work_ * time_of_coarsest_sim  / (int) wall_time), (double) levels_);
    if( (double) world_size  < (1. - TOL_TOTAL_RANKS) * suggested_ranks ) {
        WARNING("", WORLD, "You provided %d ranks and a wall-time of %d [s], our lamp genius says you may "
                           "want to use about %d ranks, we are wanring you because this is not within tolerance %f, "
                           " this may result in the job being killed.\n",
                world_size, wall_time, suggested_ranks, TOL_TOTAL_RANKS );
    }
    if( (double) world_size  > (1. + TOL_TOTAL_RANKS) * suggested_ranks ) {
        WARNING("", WORLD, "You provided %d ranks and a wall-time of %d [s], our killer mice says you may "
                           "want to use about %d ranks, we are warning you because this is not within tolerance %f, "
                           " this may result in weird ranks allocations (or even failures).\n",
                world_size, wall_time, suggested_ranks, TOL_TOTAL_RANKS );
    }

    avg_work_ = tot_work_ / world_size;
    tot_ranks_ = 0;
    double tot_runtime = 0;
    for(int i = levels_ - 1; i >= 0; --i) {
        // Compute the smallest amount of ranks needed
        PetscInt smallest_rank_allowed =
                std::ceil(dof(mesh_per_level_big_[i], dim_) / (double) max_dof_per_rank);
        OUTPUT("", WORLD, "Mesh %s (biggest of level %d) needs at least %d rank(s)...\n",
               mesh_per_level_big_[i].str().c_str(), i, smallest_rank_allowed);

        PetscInt total_work = std::floor(perc_work_per_level_[i] * world_size);
        perc_work_per_level_[i] = (double) work_per_level_[i] / tot_work_;
        tot_ranks_per_level_[i] =
                std::max(
                    std::min(
                        std::max(total_work, (PetscInt) 1),
                        sim_per_level_[i]
                        ),
                    smallest_rank_allowed
                    );

        if( tot_ranks_ +  tot_ranks_per_level_[i] > world_size ) {
            if(i == 0) tot_ranks_per_level_[i] = world_size - tot_ranks_;
            else {
                ERROR("", WORLD, "Seems all ranks had been allocated before finishing splitting "
                                 "the work...\n", "");
            }
        }
        if( i == 0 && tot_ranks_ + tot_ranks_per_level_[i] < world_size) {
            WARNING("", WORLD, "Allocation left over some rank, increasing size of biggest...\n", "");
            tot_ranks_per_level_[0] = world_size - tot_ranks_;
        }
        tot_ranks_ += tot_ranks_per_level_[i];

        if( tot_ranks_per_level_[i] < smallest_rank_allowed) {
            WARNING("", WORLD, "Seems that this level %d has not enough ranks %d (need at least %d), using "
                               "all of them but might fail...\n", i, tot_ranks_per_level_[i], smallest_rank_allowed);
            ranks_per_parallel_level_[i] = tot_ranks_per_level_[i];
            parallel_levels_[i] = 1;
        } else {
            bool flag = false;
            for(int j = smallest_rank_allowed; j <= tot_ranks_per_level_[i]; ++j) {
                if( tot_ranks_per_level_[i] % j == 0 ) {
                    if( sim_per_level_[i] < tot_ranks_per_level_[i] / j ) {
                        WARNING("", WORLD, "Not enough simulations (%d) to maximize the number of parallel "
                                           "levels (%d) as I'd like, continuing search...)\n",
                                sim_per_level_[i], tot_ranks_per_level_[i] / j);
                        continue;
                    }
                    ranks_per_parallel_level_[i] = j;
                    parallel_levels_[i] = tot_ranks_per_level_[i] / j;
                    flag = true;
                    break;
                }
            }
            if( !flag ) {

                WARNING("", WORLD, "Seems that at level %d we have to use every rank available (%d) "
                                   "(ask for unbalanced parallel levels (WIP)...)\n", i, tot_ranks_per_level_[i]);
                ranks_per_parallel_level_[i] = tot_ranks_per_level_[i];
                parallel_levels_[i] = 1;
            }
        }
    }

    for(int i = levels_ - 1; i >= 0; --i) {
        runtime_per_level_[i] = work_per_level_[i]
                / parallel_efficiency(ranks_per_parallel_level_[i], det_parallel_portion) / parallel_levels_[i];
        tot_runtime += runtime_per_level_[i] * tot_ranks_per_level_[i];
    }

    int max_runtime = 0, min_runtime = runtime_per_level_[0];
    double avg_runtime = tot_runtime / (double) world_size_;
    for(int i = 0; i < levels_; ++i) {
        if ( runtime_per_level_[i] > max_runtime ) max_runtime = runtime_per_level_[i];
        if ( runtime_per_level_[i] < min_runtime ) min_runtime = runtime_per_level_[i];
    }

    double avg_idle_time = 0;
    for(int i = levels_ - 1; i >= 0; --i) {
        avg_idle_time += ( max_runtime - runtime_per_level_[i] ) * parallel_levels_[i];
        if( max_runtime - runtime_per_level_[i] > TOL_MAX_IDLE_TIME * max_runtime ) {
            WARNING("", WORLD, "Seems that there is an unbalance in runtime (>%f), idle time of level %d "
                               "(with %d ranks) was %d and max runtime is %d"
                               "(maybe increase the number of simulations/ranks?).\n",
                    TOL_MAX_IDLE_TIME, i, tot_ranks_per_level_[i], (int)(max_runtime - runtime_per_level_[i]),
                    max_runtime );
        }
    }
    avg_idle_time /= (double) world_size_;

    if( avg_idle_time > TOL_AVG_IDLE_TIME * avg_runtime ) {
        WARNING("", WORLD, "Seems that there is an unbalance in runtime (>%f), average idle time was %f and "
                           "avg. runtime is %f.\n", TOL_AVG_IDLE_TIME, avg_idle_time, avg_runtime);
    }

#if OUTPUT_LEVEL > 1
    std::stringstream ss;
    const char HEADER2[] =
        "/////////////////////////////////////////////////////\n"
        "////////////////////  GENERAL DATA  /////////////////\n"
        "/////////////////////////////////////////////////////\n";
    ss << HEADER2 << std::endl;
    ss << "Starting with " << levels_ << " levels and a world size of " << world_size << " in "
       << dim_ << " dimensions." << std::endl;
    ss << " --- Input" << std::endl;
    ss << "Levels:" << levels_ << std::endl;
    ss << "Ranks:" << world_size << std::endl;
    ss << "Coarsest mesh:" << coarse_mesh_size_.str() << std::endl;
    ss << "Dimension:" << dim_ << std::endl;
    ss << " --- Data" << std::endl;
    ss << "Refinement factor:          " << factor_x << "," << factor_y << "," << factor_z << std::endl;
    ss << "Base work:                  " << base_work << std::endl;
    ss << "Base numer of sim.:         " << base_sim << std::endl;
    ss << "Order:                      " << order << std::endl;
    ss << "Max dof per rank:           " << max_dof_per_rank << std::endl;
    ss << "Parallel portion:           " << det_parallel_portion << std::endl;
    ss << " --- Allocation" << std::endl;
    ss << "Tot rank:                   " << tot_ranks_ << std::endl;
    ss << " --- Work" << std::endl;
    ss << "Total work:                 " << tot_work_ << std::endl;
    ss << "Averaged work:              " << avg_work_ << std::endl;
    ss << "Total runtime:              " << tot_runtime << std::endl;
    ss << "Avg. runtime (per rank):    " << avg_runtime << std::endl;
    ss << "Avg. idle time (per rank):  " << avg_idle_time << std::endl;

    const char HEADER[] =
            "/////////////////////////////////////////////////////\n"
            "////////////////////  LEVEL DATA  ///////////////////\n"
            "/////////////////////////////////////////////////////\n";
    int width[] = {4,18,14,12,12,12,8,8,8,8,8,8,12, 12};

    int j = 0;
    ss.precision(2);
    ss.setf(std::ios::fixed, std::ios::floatfield);
    ss.setf(std::ios::showpoint);
    ss << HEADER << std::endl;
    ss << std::setw(width[j++]) << "l";
    ss << std::setw(width[j++]) << "N_l";
    ss << std::setw(width[j++]) << "M_l";
    ss << std::setw(width[j++]) << "M_p";
    ss << std::setw(width[j++]) << "res_p";
    ss << std::setw(width[j++]) << "w_l";
    ss << std::setw(width[j++]) << "W_l";
    ss << std::setw(width[j++]) << "W_l [%]";
    ss << std::setw(width[j++]) << "P_l";
    ss << std::setw(width[j++]) << "P_l [%]";
    ss << std::setw(width[j++]) << "Q_l";
    ss << std::setw(width[j++]) << "L_l";
    ss << std::setw(width[j++]) << "R_l";
    ss << std::setw(width[j++]) << "I_l";
    ss << std::endl;
    for(int  i = 0; i < levels_; ++i) {
        j = 0;
        ss << std::setw(width[j++]) << i;
        ss << std::setw(width[j++]) << mesh_per_level_[i].str();
        ss << std::setw(width[j++]) << sim_per_level_[i];
        ss << std::setw(width[j++]) << sim_per_level_[i] / parallel_levels_[i];
        ss << std::setw(width[j++]) << sim_per_level_[i] % parallel_levels_[i];
        ss << std::setw(width[j++]) << sim_work_per_level_[i];
        ss << std::setw(width[j++]) << work_per_level_[i];
        ss << std::setw(width[j++]) << perc_work_per_level_[i] * 100.;
        ss << std::setw(width[j++]) << tot_ranks_per_level_[i];
        ss << std::setw(width[j++]) << (double) tot_ranks_per_level_[i] / world_size * 100.;
        ss << std::setw(width[j++]) << ranks_per_parallel_level_[i];
        ss << std::setw(width[j++]) << parallel_levels_[i];
        ss << std::setw(width[j++]) << runtime_per_level_[i];
        ss << std::setw(width[j++]) << (double) max_runtime - runtime_per_level_[i];
        ss << std::endl;
    }

    ss << "Legend:" << std::endl;
    ss << "l            number of levels" << std::endl;
    ss << "N_l          coarsest mesh size on the level" << std::endl;
    ss << "M_l          MC simulations per level" << std::endl;
    ss << "M_p          MC simulations per partition of a level" << std::endl;
    ss << "res_p        MC simulations left ofer in the level" << std::endl;
    ss << "w_l          estimated cost of a single simulations" << std::endl;
    ss << "W_l          total estimated cost for the level + percentage of total" << std::endl;
    ss << "P_l          number of ranks used for the level" << std::endl;
    ss << "Q_l          ranks used for parallel simulations" << std::endl;
    ss << "L_l          number of parallel simulations for the same level" << std::endl;
    ss << "R_l          estimated runtime per level (considered parallel efficiency)" << std::endl;
    ss << "I_l          estimated idle time per level" << std::endl;

    char fname[PETSC_MAX_PATH_LEN];
    sprintf(fname, "%s_allocation.out", p_opt_->simulation_name);

#if !BLOCK_IO
    FILE * f = fopen(fname, "a");
    PetscFPrintf(PETSC_COMM_WORLD, f, ss.str().c_str());
    fclose(f);
#endif //BLOCK_IO

#endif // OUTPUT_LEVEL > 1
}

//!
//!
DynamicAllocator::~DynamicAllocator()
{

    delete[] sim_per_level_;
    delete[] sim_work_per_level_;
    delete[] perc_work_per_level_;
    delete[] work_per_level_;
    delete[] runtime_per_level_;

    delete[] tot_ranks_per_level_;
    delete[] ranks_per_parallel_level_;
    delete[] parallel_levels_;
}
