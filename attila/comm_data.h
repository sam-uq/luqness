/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef COMMDATA_H
#define COMMDATA_H

#include "../tools/tools.h"

//! Commdata serves as map between coordinates in this process and coordinates in the receiving process: here we store
//! to which rank we send which data and of which size. No check of bound of existence is ever performed, is up to the
//! user. Provides an intersect function that allow to check if two coordinate regions intersect and perform an actual
//! intersection.
class CommData
{
public:
    //! Initialize all variables to -1 (for error checking), rank == -1 means that there is no communication going on for
    //! that portion.
    CommData() {
        rank = -1;
        for(int i = 0; i < 9; ++i ) {
            csm[i] = -1;
        }
    }

    //! Rank to/from which the communication is performed (intended as rank in the communicator for which the CommData
    //! was created)
    PetscInt rank;
    //! Data portion that is transmitted /from/to the data (w.r.t. this rank data format)
    //! First three are corner indices, middle tree are spans, last three are sizes
    PetscInt csm[9];
    //! Size of the data being transmitted
    inline PetscInt  size() const { return csm[3]*csm[4]*csm[5]; }

    //! FIXME: shortcut for retrifal, pretty bad, fix this
    inline PetscInt  & xc() { return csm[0]; }
    inline PetscInt  & yc() { return csm[1]; }
    inline PetscInt  & zc() { return csm[2]; }
    inline PetscInt  & xs() { return csm[3]; }
    inline PetscInt  & ys() { return csm[4]; }
    inline PetscInt  & zs() { return csm[5]; }
    inline PetscInt  & xn() { return csm[6]; }
    inline PetscInt  & yn() { return csm[7]; }
    inline PetscInt  & zn() { return csm[8]; }

    //! Perform an intersection between two CommData classes, writes the result into *this, return true if intersect
    bool intersect(CommData & other);
};

#endif // COMMDATA_H
