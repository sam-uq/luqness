/*
* Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/

#include <iostream>

#include "../tools/utilities.h"

#include "attila.h"

static std::string prettify_byte(long int bytes) {
    if(1000 <= bytes && bytes < 1000000L) {
        std::stringstream ss;
        ss << bytes / 1000 << "kb";
        return ss.str();
    } else if(100000 <= bytes && bytes < 1000000000L) {
        std::stringstream ss;
        ss << bytes / 1000000 << "Mb";
        return ss.str();
    } else if(100000000L <= bytes && bytes < 1000000000000L) {
        std::stringstream ss;
        ss << bytes / 1000000000L << "Mb";
        return ss.str();
    } else {
        std::stringstream ss;
        ss << bytes << "b";
        return ss.str();
    }
}

///
///
/// Constructors & Destructors
///
///
Attila::Attila(Options *p_opt)
    : p_opt_(p_opt) {

    assert(p_opt_ != nullptr);

    // Get global rank and global size
    MPI_Comm_rank(PETSC_COMM_WORLD, &world_rank);
    MPI_Comm_size(PETSC_COMM_WORLD, &world_size);

    if(p_opt->alloc_only) {
        world_size = p_opt->alloc_size;
    }

    switch( p_opt_->alloc_type ) {
    case AllocatorType::Dynamic:
        p_alloc_ = new DynamicAllocator(p_opt, world_size);
        break;
    case AllocatorType::Static:
        p_alloc_ = new StaticAllocator(p_opt, world_size);
        break;
    }

    // Get color table and color of current rank relative to sub-domain communicators
    generate_intra_domain_color_table();
    if( world_rank < used_ranks ) {
        intra_domain_color = intra_domain_color_table[world_rank];
        inter_level_color = inter_level_color_table[world_rank];
        level = level_of_rank[world_rank];
    } else {
        intra_domain_color = MPI_UNDEFINED;
        inter_level_color = MPI_UNDEFINED;
        level = MPI_UNDEFINED;
    }

    // FIXME For now
    assert( world_size >= p_opt->mlmc_levels );

    mesh = p_alloc_->mesh_per_level_[level];

    /// THINGS ARE READY
    if(p_opt_->push_snapshot_to_mav > 0) {
        int nsaves = (p_opt_->T/ p_opt_->snapshot_fps
                      + p_opt_->save_initial
                      + p_opt_->save_final) / p_opt_->push_snapshot_to_mav;

        int ranks = p_alloc_->ranks_per_parallel_level_[level];
        int dofs = 0;
        if( p_opt_->solver == SolverType::Proj
                || p_opt_->save_velocity ) {
            dofs += 1;
        }
        if( p_opt_->solver == SolverType::Proj ) {
            dofs += 2;
        }
        if( p_opt_->solver == SolverType::Vort
                || p_opt_->save_vorticity ) {
            ++dofs;
        }
        if( p_opt_->do_transport ) {
            ++dofs;
        }
        int level = this->level;
        int spacedofs = p_alloc_->dof(
                            this->p_alloc_->mesh_per_level_big_[level],
                            p_opt_->enable_3d ? 3 : 2);
        int memory = dofs * spacedofs * nsaves / ranks;
        int bytes = memory * sizeof(double);


        std::stringstream ss;
        ss << "Data is kept in memory for MAV computation.\n";
        ss << "This might mean you might run out of memory.\n";
        ss << "We save " << nsaves << " times with " << dofs << " components "
           << "and with " << spacedofs << " spatial dofs.\n"
           << "We predict a memory usage of "
           << prettify_byte(bytes) << " for level " << level  << ".\n" << std::flush;
        const std::string& tmp = ss.str();
        const char * c = tmp.c_str();
        std::cout << c;
//        OUTPUT(this, INTRADM, "%s" , c);
    }

    // Split off the intra domain communicator,
    // this will be used by PETSCc for DMDA communication
    MPI_Comm_split(PETSC_COMM_WORLD, intra_domain_color, 0, &intra_domain_comm);

    MPI_Comm_rank(intra_domain_comm, &intra_domain_rank);
    MPI_Comm_size(intra_domain_comm, &intra_domain_size);

    // Split off the inter level communicator, this
    // will be used by PETSCc for DMDA communication
    // WARNING: since roots of p_alloc_->levels_ may
    // be swapped we forcefully select the rank to assign to roots
    // to be in order of p_alloc_->levels_
    MPI_Comm_split(PETSC_COMM_WORLD, inter_level_color,
                   intra_domain_rank + cumu_ranks_per_level[level], &inter_level_comm);

    MPI_Comm_rank(inter_level_comm, &inter_level_rank);
    MPI_Comm_size(inter_level_comm, &inter_level_size);

    VERBOSE(this, SELF, "x [LVL %d] Giving the root rank %d in color %d of size %d\n",
            world_rank, world_size, level,
            inter_level_rank, inter_level_color, inter_level_size);

    // Get color table and color of current rank relative to intra level comm communicators
    generate_intra_level_color_table();
    if( world_rank < used_ranks ) {
        intra_level_color = intra_level_color_table[world_rank];
    } else {
        intra_level_color = MPI_UNDEFINED;
    }

    // Split off the intra level communicator, this will be used by PETSCc for DMDA communication
    MPI_Comm_split(PETSC_COMM_WORLD, intra_level_color, 0, &intra_level_comm);

    MPI_Comm_rank(intra_level_comm, &intra_level_rank);
    MPI_Comm_size(intra_level_comm, &intra_level_size);

    // Sim id data
    generate_sim_id_table();

    reinit_sim();

    if( world_rank < used_ranks ) {
        sim_num = sim_num_of_rank[world_rank];
    } else {
        sim_num = MPI_UNDEFINED;
    }

    if(sim_num <= 0) {
        ERROR(this, INTRADM, "Seems like this domain is not doing any simulation, something must be "
                             "gone wrong with the allocation unit, check that pleez...\n", "");
    }

    // Generate other data
    generate_more_details();

    if( is_root ) {
        assert( intra_level_size > 0 );
        from_levels_data_stream = new CommData[std::max(intra_level_size-1,1)];
    } else {
        to_root_data_stream = new CommData();
    }

    // Generate comm data
    //     generate_comm_data();

    // Get an idea
    print();
}

Attila::~Attila() {
    VERBOSE(this, SELF, "Shutting down ATTILA.\n", "");
    delete[] intra_domain_color_table;
    delete[] intra_level_color_table;
    delete[] inter_level_color_table;
    delete[] intra_level_deepness_table;
    delete[] level_of_rank;
    delete[] level_of_domain;
    delete[] deepness_of_domain;
    delete[] cumu_ranks_per_level;
    delete[] sim_id_table;
    delete[] sim_id_of_rank;
    delete[] sim_num_of_rank;

    delete[] sim_num_table;

    if( is_root && !is_smallest_root ) {
        delete[] from_prev_root_data_stream;
        delete[] intersects_prev_root_data_stream;
    }
    if( is_root && !is_super_root ) {
        delete[] to_next_root_data_stream;
        delete[] intersects_next_root_data_stream;
    }

    if( is_root ) {
        delete[] from_levels_data_stream;
    } else {
        delete[] to_root_data_stream;
    }

    delete p_alloc_;
}

///
///
/// I/O, Debug and Info methods
///
///
PetscErrorCode Attila::print(void) {

#if OUTPUT_LEVEL > 1
    const char HEADER[] = "/////////////////////////////////////////////////////\n"
                          "///////////  DISTRIBUTION OF SIMULATIONS  ///////////\n"
                          "/////////////////////////////////////////////////////\n";
    const char RANK_BARRIER[] = "###############################################";

    int width[] = {5,5,18,18,18,18,6,6,9,9,9,9,9,9};
    std::stringstream ss;
    int j = 0;
    ss << HEADER << std::endl;
    ss  << std::setw(width[j++]) << "rk.";
    ss << std::setw(width[j++]) << "lvl.";
    ss << std::setw(width[j++]) << "intra dom. col";
    ss << std::setw(width[j++]) << "intra lvl. col";
    ss << std::setw(width[j++]) << "inter lvl. col";
    ss << std::setw(width[j++]) << "sim. id start";
    ss << std::setw(width[j++]) << "#";
    ss << std::setw(width[j++]) << "root";
    ss << std::endl;
    for(int i = 0; i < used_ranks; ++i ) {
        if( i % NODE_SIZE == 0 ) {
            ss << RANK_BARRIER << " [ NODE " << i / NODE_SIZE << " ] " << RANK_BARRIER << std::endl;
        }
        j = 0;
        ss << std::setw(width[j++]) << i ;
        ss << std::setw(width[j++]) << level_of_rank[i];
        ss << std::setw(width[j++]) << intra_domain_color_table[i];
        ss << std::setw(width[j++]) << intra_level_color_table[i];
        ss << std::setw(width[j++]) << inter_level_color_table[i];
        ss << std::setw(width[j++]) << sim_id_of_rank[i];
        ss << std::setw(width[j++]) << sim_num_of_rank[i];

        std::string flag;
        if( intra_domain_color_table[i] == 0 ) {
            flag = "-";
        } else if( inter_level_color_table[i] == 0 && level_of_rank[i] == p_alloc_->levels_ - 1 ) {
            flag = "|";
        } else if( inter_level_color_table[i] == 0 ) {
            flag = "^";
        } else {
            std::ostringstream temp;  //temp as in temporary
            temp << intra_level_deepness_table[i];
            flag = temp.str();
        }
        ss << std::setw(width[j++]) << flag;
        ss << std::endl;
    }

    char fname[PETSC_MAX_PATH_LEN];
    sprintf(fname, "%s_allocation.out", p_opt_->simulation_name);

    FILE * f = fopen(fname, "a");
    PetscFPrintf(PETSC_COMM_WORLD, f, ss.str().c_str());
    fclose(f);

#if OUTPUT_LEVEL > 3
    char flag;
    if( is_super_root ) {
        flag = '-';
    } else if( is_smallest_root ) {
        flag = '|';
    } else if( is_root ) {
        flag = '^';
    } else {
        flag = ' ';
    }
#endif // OUTPUT_LEVEL > 3
    DEBUG(this, SELF, "%c [LVL %d] Intra domain: %d of %d [%d], Intra level %d of %d [%d],"
                      "Inter level %d of %d [%d].\n",
          flag, level,
          intra_domain_rank, intra_domain_size, intra_domain_color,
          intra_level_rank, intra_level_size, intra_level_color,
          inter_level_rank, inter_level_size, inter_level_color);
#endif // OUTPUT_LEVEL > 1

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::reinit_sim(void)
{
    if( world_rank < used_ranks ) {
        sim_id = sim_id_of_rank[world_rank];
    } else {
        sim_id = MPI_UNDEFINED;
    }

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::next_sim(void)
{
    ++sim_id;
    if( intra_domain_color != parallel_runs-1 && sim_id > sim_id_table[intra_domain_color+1]) {
        WARNING(this, INTRADM, "Conflicting sim IDs (%d), not in range (%d, %d) "
                               "generated because you ran too many simulations!\n",
                sim_id, sim_id_of_rank[world_rank], sim_id_table[intra_domain_color+1]);
    }

    PetscFunctionReturn(ierr);
}

///
///
///  Grab/Compute info methods
///
///
PetscErrorCode Attila::generate_intra_domain_color_table() {

    parallel_runs = 0;
    used_ranks = 0;
    assert(p_alloc_->levels_ > 0);
    cumu_ranks_per_level = new int[p_alloc_->levels_];
    cumu_ranks_per_level[0] = 0;
    for(int i = 1; i < p_alloc_->levels_; ++i ) {
        cumu_ranks_per_level[i] = cumu_ranks_per_level[i-1] + p_alloc_->ranks_per_parallel_level_[i-1];
        //             PetscPrintf(PETSC_COMM_WORLD, " ---> %d \n", cumu_ranks_per_level[i]);
    }

    for(int i = 0; i < p_alloc_->levels_; ++i ) {
        parallel_runs += p_alloc_->parallel_levels_[i];
        used_ranks += p_alloc_->ranks_per_parallel_level_[i] * p_alloc_->parallel_levels_[i];
    }

    if( used_ranks > world_size ) {
        ERROR(this, SELF,
              "Seems that you are using more ranks (%d) than what you provided (%d)!\n",
              used_ranks, world_size);
    } else if ( used_ranks < world_size ) {
        ERROR(this, SELF,
              "Seems that you are using less ranks (%d) than what you provided (%d)!\n",
              used_ranks, world_size);
    }

    assert(used_ranks > 0);
    level_of_rank = new int[used_ranks];
    intra_domain_color_table = new int[used_ranks];
    intra_level_color_table = new int[used_ranks];
    inter_level_color_table = new int[used_ranks];
    intra_level_deepness_table = new int[used_ranks];

    assert(parallel_runs > 0);
    level_of_domain = new int[parallel_runs];
    deepness_of_domain = new int[parallel_runs];

    int * unalloc_parallel_levels = new int[p_alloc_->levels_];
    memcpy(unalloc_parallel_levels, p_alloc_->parallel_levels_,
           p_alloc_->levels_ * sizeof(int));
    int alloc_ranks = 0;

    for(int i = 0; i < parallel_runs; ++i ) {
        // Pick a level that may fitin the remaining of the node or the biggest one
        int j = 0, first_j = 0;
        bool found_j = false, found_first_j = false;
        for(; j < p_alloc_->levels_; ++j) {
            if( unalloc_parallel_levels[j] > 0 ) {
                if( !found_first_j ) {
                    first_j = j;
                    found_first_j = true;
                }
                if ( NODE_SIZE - (alloc_ranks % NODE_SIZE)
                     >= p_alloc_->ranks_per_parallel_level_[j] ) {
                    found_j = true;
                    break;
                }
            }
        }
        if( i == 0 ) {
            j = 0;
        }
        if( ! found_j ) {
            WARNING("", WORLD,
                    "Seems like the parallel run will span multiple nodes!\n", i);
            j = first_j;
        }
        if( ! found_first_j ) {
            ERROR("", WORLD,
                  "Already allocated all data!\n", "");
        }

        // Index j is now containing the level which has been chosen
        for(int k = 0; k < p_alloc_->ranks_per_parallel_level_[j]; ++k) {
            level_of_rank[alloc_ranks+k] = j;
            intra_domain_color_table[alloc_ranks+k] = i;
        }
        level_of_domain[i] = j;

        int root_color, deepness = p_alloc_->parallel_levels_[j] - unalloc_parallel_levels[j];
        deepness_of_domain[i] = deepness;
        if( unalloc_parallel_levels[j] == p_alloc_->parallel_levels_[j] ) {
            root_color = 0;
        } else {
            root_color = MPI_UNDEFINED;
        }
        for( int k = 0; k < p_alloc_->ranks_per_parallel_level_[j]; ++k ) {
            inter_level_color_table[alloc_ranks+k] = root_color;
            intra_level_deepness_table[alloc_ranks+k] = deepness;
        }
        alloc_ranks += p_alloc_->ranks_per_parallel_level_[j];
        --unalloc_parallel_levels[j];
    }

    delete[] unalloc_parallel_levels;

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::generate_intra_level_color_table() {
    int * used_ranks_per_domain = new int[parallel_runs];

    for(int i = 0; i < parallel_runs; ++i ) {
        used_ranks_per_domain[i] = 0;
    }


    for(int i = 0; i < used_ranks; ++i ) {
        intra_level_color_table[i] = used_ranks_per_domain[intra_domain_color_table[i]]++
                + cumu_ranks_per_level[level_of_rank[i]];
    }

    delete[] used_ranks_per_domain;

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::generate_inter_level_color_table()
{
    //   done in generate_inter_domain_color_table();

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::generate_sim_id_table() {
    sim_id_table = new int[parallel_runs];
    sim_id_of_rank = new int[used_ranks];
    sim_num_of_rank = new int[used_ranks];

    OUTPUT("", WORLD, "[ATT] Doing a total of %d parallel runs using a total of %d ranks.\n",
           parallel_runs, used_ranks);

    sim_id_table[0] = 0;
    for(int i = 1; i < parallel_runs; ++i ) {
        sim_id_table[i] = sim_id_table[i-1]
                +  p_alloc_->sim_per_level_[level_of_domain[i-1]] / p_alloc_->parallel_levels_[level_of_domain[i-1]]
                + 1 * (deepness_of_domain[i-1] <
                p_alloc_->sim_per_level_[level_of_domain[i-1]] % p_alloc_->parallel_levels_[level_of_domain[i-1]]);
        if( deepness_of_domain[i-1] <
                p_alloc_->sim_per_level_[level_of_domain[i-1]] % p_alloc_->parallel_levels_[level_of_domain[i-1]] ) {
            OUTPUT("", WORLD, "[ATT] Parallel run %d is on lvl. %d (needs %d sim.) with %d parallel run"
                              " --> gets index %d (got the offset).\n",
                   i, level_of_domain[i],
                   p_alloc_->sim_per_level_[level_of_domain[i]], p_alloc_->parallel_levels_[level_of_domain[i]],
                    sim_id_table[i]);
        } else {
            OUTPUT("", WORLD, "[ATT] Parallel run %d is on lvl. %d (needs %d sim.) with %d parallel run"
                              " --> gets index %d.\n",
                   i, level_of_domain[i],
                   p_alloc_->sim_per_level_[level_of_domain[i]],
                    p_alloc_->parallel_levels_[level_of_domain[i]],
                    sim_id_table[i]);
        }
    }
    for(int i = 0; i < used_ranks; ++i ) {
        int d = intra_domain_color_table[i]; // Domain color of rank i
        sim_num_of_rank[i] =  p_alloc_->sim_per_level_[level_of_domain[d]] /
                p_alloc_->parallel_levels_[level_of_domain[d]]
                + 1 * (deepness_of_domain[d] <
                       p_alloc_->sim_per_level_[level_of_domain[d]] % p_alloc_->parallel_levels_[level_of_domain[d]]);
        sim_id_of_rank[i] = sim_id_table[d];
        OUTPUT("", WORLD, "[ATT] Rank %d is with sim id %d and has intra dom col. %d (id=%d, #=%d)\n",
               i, sim_id_of_rank[i],
               d, sim_id_table[d], sim_num_of_rank[i]);
    }

    assert(p_alloc_->parallel_levels_[level] > 0);
    sim_num_table = new int[p_alloc_->parallel_levels_[level]];
    for(int i = 0; i < p_alloc_->parallel_levels_[level]; ++i ) {
        sim_num_table[i] = p_alloc_->sim_per_level_[level] /
                p_alloc_->parallel_levels_[level]
                + 1 * (i <  p_alloc_->sim_per_level_[level] % p_alloc_->parallel_levels_[level]);
    }

    PetscFunctionReturn(ierr);
}

PetscErrorCode Attila::generate_more_details()
{
    if( inter_level_color_table[world_rank] == 0 ) {
        is_root = true;
    } else {
        is_root = false;
    }
    if( intra_domain_color_table[world_rank] == 0 ) {
        is_super_root = true;
    } else {
        is_super_root = false;
    }
    if( is_root && level == p_alloc_->levels_ - 1 ) {
        is_smallest_root = true;
    } else {
        is_smallest_root = false;
    }

    super_root_size = p_alloc_->ranks_per_parallel_level_[0];
    not_in_super_root_size = inter_level_size - super_root_size;

    VERBOSE(this, SELF, "x Inter level size is %d, "
                        "Super-Root size is %d, roots not in super root size %d.\n",
            inter_level_size, super_root_size, not_in_super_root_size);

    PetscFunctionReturn(ierr);
}

//!
//! Generate communication data (does not communicate)
//! @param da DMDA for the fields of the current simulations
//! @param da_sc DMDA for the scalars of the current simulations
//!
PetscErrorCode Attila::generate_intra_level_comm_data(DM da, DM da_sc) {
    da_ = da;
    da_sc_ = da_sc;
    comm_data.rank = 0;
    comm_data_recv.rank = 0;

    DMDAGetCorners(da_, &comm_data.xc(), &comm_data.yc(), &comm_data.zc(),
                   &comm_data.xs(), &comm_data.ys(), &comm_data.zs());
    DMDAGetInfo(da_, PETSC_NULL, &comm_data.xn(), &comm_data.yn(), &comm_data.zn(),
                PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL,
                PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL);

    if( is_root ) {
        for(int i = 0; i < intra_level_size-1; ++i ) {
            from_levels_data_stream[i] = this->comm_data;
            // FIXME use real rank /CHECK if fixed
            from_levels_data_stream[i].rank = i+1;
        }
    } else {
        *to_root_data_stream = this->comm_data;
        to_root_data_stream->rank = 0;
    }

    PetscFunctionReturn(ierr);
}

//!
//! @brief Generate communication data for inter level communication
//! @param da DMDA for the fields of the current simulations
//! @param da_sc DMDA for the scalars of the current simulations
//! Send data to next root and the receive data from previous one, then send data back
//!
PetscErrorCode Attila::generate_inter_level_comm_data(DM da, DM da_sc) {
    // TODO FIXME FIXME
    // Generate this comm_data (and all info associated)
    // Need two DMDA: one for data you
    // send away (current one) and one for data you receive (if you are a root >
    // smallest one), thus you also need another comm_data,
    // receiving dmda may be created from data received?
    generate_intra_level_comm_data(da, da_sc);

    int offset_of_prev, offset_of_next;

    // Generate info (size of next/prev and offset thereof
    if( is_root && !is_smallest_root ) {
        prev_root_size = p_alloc_->ranks_per_parallel_level_[level + 1];
        offset_of_prev = cumu_ranks_per_level[level + 1];
    } else {
        prev_root_size = 0;
        offset_of_prev = 0;
    }
    if( is_root && !is_super_root ) {
        next_root_size = p_alloc_->ranks_per_parallel_level_[level - 1];
        offset_of_next = cumu_ranks_per_level[level - 1];
    } else {
        next_root_size = 0;
        offset_of_next = 0;
    }
    real_prev_root_size = 0;
    real_next_root_size = 0;


    //! Allocate what you'll need for CommData details
    //! Will contain data received from smaller mesh
    //     if( is_root && !is_smallest_root ) {
    //      // TODO may be allocating to much
    //      assert(prev_root_size > 0);
    //      intersects_prev_root_data_stream = new bool[prev_root_size];
    //      from_prev_root_data_stream = new CommData[prev_root_size];
    //     }
    // Data exchange between roots
    // Generate send data to smaller level (i.e. finer mesh), i.e. receive from smaller mesh (bigger level)
    //     if( is_root && !is_super_root ) {
    //      // TODO may be allocating to much
    //      assert(next_root_size > 0);
    //      intersects_next_root_data_stream = new bool[next_root_size];
    //      to_next_root_data_stream = new CommData[next_root_size];
    //     }

    VERBOSE(this, SELF, "* Has %d pot. prev ranks (off=%d) and has %d next pot. "
                        "ranks (off=%d).\n",
            prev_root_size, offset_of_prev, next_root_size, offset_of_next);
    MPI_Request * requests = NULL;

    //! Will contain data received from smaller mesh
    if( is_root && !is_smallest_root ) {
        // TODO may be allocating to much
        assert(prev_root_size > 0);
        intersects_prev_root_data_stream = new bool[prev_root_size];
        from_prev_root_data_stream = new CommData[prev_root_size];
        requests = new MPI_Request[prev_root_size];
    }

    //! Send to each next if any
    for(int i = offset_of_next; i < offset_of_next+next_root_size; ++i ) {
        VERBOSE(this, SELF, "* Root %d is sending its mesh data to next "
                            "(i.e. finer mesh) rank %d.\n", inter_level_rank, i);
        send_mesh_data(i, comm_data, TAG_COMMDATA);
    }
    //! Receive from each prev if any
    for(int i = offset_of_prev; i < offset_of_prev+prev_root_size; ++i ) {
        VERBOSE(this, SELF, "* Root %d is receiving mesh data from prev "
                            "(i.e. coarser mesh) rank %d.\n", inter_level_rank, i);
        requests[i-offset_of_prev] = recv_mesh_data(i, from_prev_root_data_stream[i-offset_of_prev], TAG_COMMDATA);
    }

    //         MPI_Barrier(PETSC_COMM_WORLD);

    if( is_root && !is_smallest_root ) {
        // TODO not waitall but just wait the current needed
        // TODO check statuses
        VERBOSE(this, SELF, "* Waiting %d requests to complete.\n", prev_root_size);
        MPI_Status * sts = new MPI_Status[prev_root_size];
        ierr = MPI_Waitall(prev_root_size, requests, sts); CHKERRQ(ierr);

        delete[] requests;
        delete[] sts;

        comm_data_recv = from_prev_root_data_stream[0];

        VERBOSE(this, INTRADM, "* Creating a receiving DMDA (%d,%d,%d), "
                               "std. DMDA is (%d,%d,%d).\n",
                comm_data_recv.xn(), comm_data_recv.yn(), comm_data_recv.zn(),
                comm_data.xn(), comm_data.yn(), comm_data.zn());
        if( p_opt_->enable_3d ) {
            ierr = DMDACreate3d(intra_domain_comm, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,
                                DMDA_STENCIL_STAR,
                                comm_data_recv.xn(), comm_data_recv.yn(), comm_data_recv.zn(),
                                PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, 3, 1,
                                NULL, NULL, NULL, &da_recv_); CHKERRQ(ierr);
        } else {
            ierr = DMDACreate2d(intra_domain_comm, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC, DMDA_STENCIL_STAR,
                                comm_data_recv.xn(), comm_data_recv.yn(), PETSC_DECIDE, PETSC_DECIDE, 2, 1,
                                NULL, NULL, &da_recv_); CHKERRQ(ierr);
        }
        ierr = DMDAGetCorners(da_recv_, &comm_data_recv.xc(), &comm_data_recv.yc(), &comm_data_recv.zc(),
                              &comm_data_recv.xs(), &comm_data_recv.ys(), &comm_data_recv.zs()); CHKERRQ(ierr);


        //         DMDAGetInfo(da_recv, PETSC_NULL, &comm_data_recv.xn(), &comm_data_recv.yn(), &comm_data_recv.zn(),
        //                     PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL,
        //                     PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL);

        ierr = DMDAGetReducedDMDA(da_recv_, 1, &da_recv_sc_); CHKERRQ(ierr);

        for(int i = 0; i < prev_root_size; ++i ) {
            if(( intersects_prev_root_data_stream[i] = from_prev_root_data_stream[i].intersect(comm_data_recv) )) {
                ++real_prev_root_size;
            }
        }

        OUTPUT(this, INTRADM, "* Created a receiving DMDA (%d,%d,%d - %d,%d,%d - %d,%d,%d), "
                              "std. DMDA is (%d,%d,%d - %d,%d,%d - %d,%d,%d). \n",
               comm_data_recv.xc(), comm_data_recv.yc(), comm_data_recv.zc(),
               comm_data_recv.xs(), comm_data_recv.ys(), comm_data_recv.zs(),
               comm_data_recv.xn(), comm_data_recv.yn(), comm_data_recv.zn(),
               comm_data.xc(), comm_data.yc(), comm_data.zc(),
               comm_data.xs(), comm_data.ys(), comm_data.zs(),
               comm_data.xn(), comm_data.yn(), comm_data.zn());

    }

    // Data exchange between roots
    // Generate send data to smaller level (i.e. finer mesh), i.e. receive from smaller mesh (bigger level)
    if( is_root && !is_super_root ) {
        // TODO may be allocating to much
        assert(next_root_size > 0);
        intersects_next_root_data_stream = new bool[next_root_size];
        to_next_root_data_stream = new CommData[next_root_size];
        requests = new MPI_Request[next_root_size];
    }

    for(int i = offset_of_prev; i < offset_of_prev+prev_root_size; ++i ) {
        DEBUG(this, SELF, "* Root %d is sending its recv mesh data to prev "
                          "(i.e. coarser mesh) rank %d.\n", inter_level_rank, i);
        send_mesh_data(i, comm_data_recv, TAG_COMMDATA);
    }
    for(int i = offset_of_next; i < offset_of_next+next_root_size; ++i ) {
        DEBUG(this, SELF, "* Root %d is receiving mesh recv data from next "
                          "(i.e. finer mesh) rank %d.\n",
              world_rank, world_size, inter_level_rank, i);
        requests[i-offset_of_next] = recv_mesh_data(i, to_next_root_data_stream[i-offset_of_next], TAG_COMMDATA);
    }

    if( is_root && !is_super_root ) {
        // TODO not waitall but just wait the current needed
        // TODO check statuses
        MPI_Waitall(next_root_size, requests, MPI_STATUSES_IGNORE);
        delete[] requests;

        for(int i = 0; i < next_root_size; ++i ) {
            if(( intersects_next_root_data_stream[i] = to_next_root_data_stream[i].intersect(comm_data) )) {
                ++real_next_root_size;
            }
            if( intersects_next_root_data_stream[i] ) {
                DEBUG(this, SELF, "* Root %d is intersecting with super root %d\n", inter_level_rank, i);
            }
        }
    }

    std::stringstream ss;

    if( is_root && level != p_alloc_->levels_ - 1) {
        ss.str("");
        ss.clear();
        ss << "Root: " << world_rank << " of " << world_size << " intersects with prev: ";
        for(int i = 0; i < prev_root_size; ++i ) {
            if( intersects_prev_root_data_stream[i] ) {
                ss << i +  cumu_ranks_per_level[level + 1] << ",";
            }
        }
        ss << " [tot: " << prev_root_size << "/" << real_prev_root_size << "]";
        ss << " local is: ";
        ss << comm_data.xc() << "," << comm_data.yc() << "," << comm_data.zc() << "--";
        ss << comm_data.xs() << "," << comm_data.ys() << "," << comm_data.zs() << "--";
        ss << comm_data.xn() << "," << comm_data.yn() << "," << comm_data.zn() << "--";
        ss << "." << std::endl;

        DEBUG(this, SELF, "%s", ss.str().c_str());
    }
    if( is_root && level != 0) {
        ss.str("");
        ss.clear();
        ss << "Root: " << world_rank << " of " << world_size << " intersects with next: ";
        for(int i = 0; i < next_root_size; ++i ) {
            if( intersects_next_root_data_stream[i] ) {
                ss << i + cumu_ranks_per_level[level - 1] << ",";
            }
        }
        ss << " [tot: " << next_root_size << "/" << real_next_root_size << "]";
        ss << " local is: ";
        ss << comm_data.xc() << "," << comm_data.yc() << "," << comm_data.zc() << "--";
        ss << comm_data.xs() << "," << comm_data.ys() << "," << comm_data.zs() << "--";
        ss << comm_data.xn() << "," << comm_data.yn() << "," << comm_data.zn() << "--";
        ss << "." << std::endl;

        VERBOSE(this, SELF, "%s", ss.str().c_str());
    }

    PetscFunctionReturn(ierr);
}

///
///
///  Communication routines for preparation
///
///

//!
//! @param rank number of rank, relative the inter level communicator
//! @param dt contaner for the data of the mesh
//! @param tag tag for MPI communication to avoid comm conflict
//!
MPI_Request Attila::send_mesh_data(int rank, CommData & dt, int tag) {
    MPI_Request req;
    MPI_Isend(dt.csm, 9, MPI_INT, rank, tag, inter_level_comm, &req);
    return req;
}

//!
//! Receive data regarding the mesh from rank
//! @param rank number of rank, relative the inter level communicator
//! @param dt contaner for the data of the mesh
//! @param tag tag for MPI communication to avoid comm conflict
//!
MPI_Request Attila::recv_mesh_data(int rank, CommData & dt, int tag) {
    MPI_Request req;
    MPI_Irecv(dt.csm, 9, MPI_INT, rank, tag, inter_level_comm, &req);
    dt.rank = rank;
    return req;
}
