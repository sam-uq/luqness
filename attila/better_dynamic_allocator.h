/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

#include <iomanip>
#include <sstream>

#include "allocator.h"

#include "../tools/maths.h"

//!
//!
//! TODO check validations warnings and errors
//! FIXME: if you use to few simulations and too many processes may assign 0 sim to a process
//!
class BetterDynamicAllocator : public Allocator {
public:
    BetterDynamicAllocator(Options * p_opt, int world_size);
    ~BetterDynamicAllocator();

private:
    //! cost of as signle, deterministic run on coarsest mesh (can be normalized to 1)
    const int base_work;
    //! Number of simulations that you want to perform on the coarsest mesh
    const int base_sim;
    //! Order of the numerical scheme
    const int order;
    //! Each rank can, at most, handle this number of dof (around 1000000)
    const int max_dof_per_rank;
    //! Each rank can, at least, handle this number of dof (around 1000)
    const int min_dof_per_rank;
    //! Amdahl's law parallelizated portion factor, used to compute efficiency
    const double det_parallel_portion;

    //! Generated array of work per simulation per level
    PetscInt             *sim_work_per_level_;
    //! Percentage of work for each level
    double               *perc_work_per_level_;
    //! Total work for each level
    PetscInt             *work_per_level_;
    //! Expected runtime per level (including efficiency)
    double               *runtime_per_level_;
    //! Total number of ranks working on each level
    PetscInt             *tot_ranks_per_level_;

    //! Total work and avg for per level
    int tot_work_, avg_work_;
    //! Ranks available
    int tot_ranks_;
};
