/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>

#include "static_allocator.h"

//! Array containing the dofs of each Data communication
//! FIXME fixed only for now, each level has his own mesh
//!  static con
StaticAllocator::StaticAllocator(Options *p_opt, int world_size)
    : Allocator(p_opt, world_size) {

    sim_per_level_ = p_opt->sim_per_level;
    ranks_per_parallel_level_ = p_opt->ranks_per_level;
    parallel_levels_ = p_opt->parallel_sims;

    int tot_sim = 0;
    for(int i = 0; i < p_opt->mlmc_levels; ++i) {
        tot_sim += p_opt->ranks_per_level[i] * p_opt->parallel_sims[i];
    }

    assert(tot_sim == world_size);
}

//!
//!
StaticAllocator::~StaticAllocator() {

}
