/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "transmission_buffer.h"

//! Handles the memory for sending/receiving the data (buffer)
//! automatically "chunks" the data according to where it should go, ready for MPI data structures.
//! @param comm specify which communicator you want to use, and therefore, which ranks you are referring to
//! @param da the domain where the vec, which you want to be transmitted, lies
//! @param dof the degrees of freedom of that vector
//! @param use3d if you are using 3d routines
//! @param scalar is the vec a scalar quantity or a field?
//! @param tag the MPI tag you want to use for communications
//! @param comm_data_array array containing each commdata to/from which you receive
//! @param bool_data_array array saying if you really need to buffer the data (if there is a communcation going on)
//! @param size size of the array above
//! @param real_size number of nonzero in array above
TransmissionBuffer::TransmissionBuffer(MPI_Comm comm, DM da, int dof, bool use3d, bool scalar, int tag,
                                       CommData * const  comm_data_array, bool * const bool_data_array,
                                       int size, int real_size)
    : comm_(comm)
    , da_(da)
    , dof_(dof)
    , comm_data_array_(comm_data_array)
    , bool_data_array_(bool_data_array)
    , size_(size)
    , real_size_(real_size)
    , tag_(tag)
    , use3d_(use3d)
    , scalar_(scalar) {
    // Something is horribly wrong if real_size is zero
    if( real_size_ == 0 ) {
        ERROR(comm_, WORLD, "Real size of SendBuffer is zero: something must be horribly wrong!\n", "");
    }

    chunks_ = new PetscScalar*[real_size_];
    int j = 0;
    for(int i = 0; i < size_; ++i) {
        if( bool_data_array_ == NULL || bool_data_array[i] ) {
            chunks_[j] = new PetscScalar[comm_data_array_[i].size()*dof_];
            ++j;
        }
    }

    req_ = new MPI_Request[real_size_];
}

//! Deletes all the chunks and reuqests and other (may want to call wait or done before)
//!
TransmissionBuffer::~TransmissionBuffer() {
    for(int i = 0; i < real_size_; ++i) {
        delete[] chunks_[i];
    }
    delete[] chunks_;

    delete[] req_;
}

//! Takes a vector (right! dimension) and fills buffers with data of that vector, when returns, vector can be used
//!
PetscErrorCode TransmissionBuffer::fill(Vec v) {
    // TODO, check for contiguous data

    int I_ = 0;
    for(int I = 0; I < size_; ++I) {
        CommData &dt = comm_data_array_[I];
        if( bool_data_array_ == NULL || bool_data_array_[I] ) {
            // TODO
            // TODO 3D !!!!!
            // TODO fix routines
            int J = 0;
            if( scalar_ ) {
                if( use3d_ ) {
                    PetscScalar *** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                chunks_[I_][J] = v_array[k][j][i];
                                ++J;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                } else { // 2D
                    PetscScalar ** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    PetscInt a,b,c,d, e,f;
                    ierr = DMDAGetCorners(da_, &a, &b, &c, &d, &e, &f); CHKERRQ(ierr);
                    //                     DEBUG(comm_, SELF, "Filling %d dofs into (%d, %d, %d / %d, %d, %d) of DMDA "
                    //                     "(%d, %d, %d / %d, %d, %d)\n", dof_, dt.xc(), dt.yc(), dt.zc(), dt.xs(), dt.ys(), dt.zs(),
                    //                         a, b, c, d, e, f);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            chunks_[I_][J] = v_array[j][i];
                            ++J;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                }
            } else { // Field
                if( use3d_ ) {
                    Field3D *** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                for(int d = 0; d < dof_; ++d) {
                                    chunks_[I_][J+d] = v_array[k][j][i][d];
                                }
                                J += dof_;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                } else { // 2D
                    Field2D ** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    PetscInt a,b,c,d, e,f;
                    ierr = DMDAGetCorners(da_, &a, &b, &c, &d, &e, &f); CHKERRQ(ierr);
                    //                     DEBUG(comm_, SELF, "Filling %d dofs into (%d, %d, %d / %d, %d, %d) of DMDA "
                    //                     "(%d, %d, %d / %d, %d, %d)\n", dof_, dt.xc(), dt.yc(), dt.zc(), dt.xs(), dt.ys(), dt.zs(),
                    //                         a, b, c, d, e, f);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            for(int d = 0; d < dof_; ++d) {
                                chunks_[I_][J+d] = v_array[j][i][d];
                            }
                            J += dof_;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                }
            }
            ++I_;
        }
    }

    PetscFunctionReturn(ierr);
}

//! Takes a vector (of the right! dimension) and fills vector with data from buffers, when returns,
//! vector can be reused
PetscErrorCode TransmissionBuffer::empty(Vec v)
{
    // TODO, check for contigous data
    int I_ = 0;
    for(int I = 0; I < size_; ++I) {
        CommData &dt = comm_data_array_[I];
        if( bool_data_array_ == NULL || bool_data_array_[I] ) {
            // TODO

            int J = 0;
            if( scalar_ ) {
                if( use3d_ ) {
                    PetscScalar *** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                v_array[k][j][i] = chunks_[I_][J];
                                ++J;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                } else {
                    PetscScalar ** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            v_array[j][i] = chunks_[I_][J];
                            ++J;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                }
            } else { // Field
                if( use3d_ ) {
                    Field3D *** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                for(int d = 0; d < dof_; ++d) {
                                    v_array[k][j][i][d] = chunks_[I_][J+d];
                                }
                                J += dof_;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                } else {
                    Field2D ** v_array;
                    ierr = DMDAVecGetArray(da_, v, &v_array); CHKERRQ(ierr);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            for(int d = 0; d < dof_; ++d) {
                                v_array[j][i][d] = chunks_[I_][J+d];
                            }
                            J += dof_;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, v, &v_array); CHKERRQ(ierr);
                }
            }


            ++I_;
        }
    }

    PetscFunctionReturn(ierr);
}

//! Asyngronously (non-blocking) send data to everyone who may be interested, when returns buffer may still be in
//! use
PetscErrorCode  TransmissionBuffer::send(void)
{
    int j = 0;
    for(int i = 0; i < size_; ++i) {
        if( bool_data_array_ == NULL || bool_data_array_[i] ) {
            //             DEBUG(comm_, SELF, "Sending to rank %d.\n", comm_data_array_[i].rank);
            ierr = MPI_Isend(chunks_[j], comm_data_array_[i].size()*dof_, MPI_DOUBLE,
                             comm_data_array_[i].rank, tag_, comm_, &req_[j]); CHKERRQ(ierr);
            ++j;
        }
    }

    PetscFunctionReturn(ierr);
}

//! Asyngronously (non-blocking) receive data to everyone who may be interested, when returns buffer may still be in
//! use
PetscErrorCode TransmissionBuffer::recv(void) {
    int j = 0;
    for(int i = 0; i < size_; ++i) {
        if( bool_data_array_ == NULL || bool_data_array_[i] ) {
            //             DEBUG(comm_, SELF, "Receiving from rank %d.\n", comm_data_array_[i].rank);
            ierr = MPI_Irecv(chunks_[j], comm_data_array_[i].size()*dof_, MPI_DOUBLE,
                             comm_data_array_[i].rank, tag_, comm_, &req_[j]); CHKERRQ(ierr);
            ++j;
        }
    }

    PetscFunctionReturn(ierr);
}

//! Wait for *ALL* requests of the buffer to be completed, returns when done (status malus, vana salus)
//!
PetscErrorCode TransmissionBuffer::wait(void) {
    MPI_Status * st = new MPI_Status[real_size_];
    ierr = MPI_Waitall(real_size_, req_, st); CHKERRQ(ierr);
    //TODO check statuses
    delete[] st;

    PetscFunctionReturn(ierr);
}

//! Just check if transmission was done
//!
bool TransmissionBuffer::done(void) {
    //     int flag;
    //     MPI_Status * st = new MPI_Status[real_size_];
    //     ierr = MPI_Testall(real_size_, req_, &flag, st); CHKERRV(ierr);
    //     //TODO check statuses
    //     delete[] st;

    //     return flag;
    return true;
}
