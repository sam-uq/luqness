/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "comm_data.h"

//! Perform an intersection between two domain decompositions.
//! @param other other comm data whith which this is intersected, result is overwritten in other, this is kept const
//! @return true if intersection in nonempty, false othw.
//! FIXME: Other is kept const, I promise, however we are using xv() which discard the qualifiers so is nonconst.
bool CommData::intersect(CommData & other) {
    int factor_x, factor_y, factor_z;

    //         PetscPrintf(PETSC_COMM_SELF, " ---> Intersecting (%d,%d,%d,%d,%d,%d,%d,%d,%d) with "
    //                     "(%d,%d,%d,%d,%d,%d,%d,%d,%d).\n",
    //                     csm[0], csm[1], csm[2], csm[3], csm[4], csm[5], csm[6], csm[7], csm[8],
    //                     other.csm[0], other.csm[1], other.csm[2], other.csm[3], other.csm[4], other.csm[5],
    //                     other.csm[6], other.csm[7], other.csm[8]);

    if(  other.xn() == this->xn() ) {
        factor_x = 1;
    } else if (  other.xn() >= this->xn() ) {
        if( other.xn() % this->xn() == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (x-dir), "
            //             "instead is %d / %d\n", other.xn(), this->xn());
        }
        factor_x = other.xn() / this->xn();
    } else {
        if( this->xn() % other.xn()  == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (x-dir), "
            //             "instead is %d / %d\n", this->xn(), other.xn());
        }
        factor_x = - this->xn() / other.xn();
    }

    if(  other.yn() == this->yn() ) {
        factor_y = 1;
    } else if (  other.yn() >= this->yn() ) {
        if( other.yn() % this->yn() == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (y-dir), "
            //             "instead is %d / %d\n", this->yn(), other.yn());
        }
        factor_y = other.yn() / this->yn();
    } else {
        if( this->yn() % other.yn() == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (y-dir), "
            //             "instead is %d / %d\n", this->yn(), other.yn());
        }
        factor_y = - this->yn() / other.yn();
    }

    if(  other.zn() == this->zn() ) {
        factor_z = 1;
    } else if (  other.zn() >= this->zn() ) {
        if( other.zn() % this->zn() == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (z-dir), "
            //             "instead is %d / %d\n", this->zn(), other.zn());
        }
        factor_z = other.zn() / this->zn();
    } else {
        if( this->zn() % other.zn() == 0 ) {
            //             ERROR(SELF, "%sIntersection failed, we allow only integer size rations (z-dir), "
            //             "instead is %d / %d\n", this->zn(), other.zn());
        }
        factor_z = - this->zn() / other.zn();
    }
    // TODO: more elegant
    // TODO: maybe? also send halo
    // FIXME TODO NOW: check intersection if factor = 1 (+ halo), wanna use just that one

    if( factor_x == 1 ) {
        int temp_corner = std::max(this->xc(), other.xc());
        this->xs() = std::min(this->xc()+this->xs(), other.xc()+other.xs()) - temp_corner;
        this->xc() = temp_corner;
    } else if(factor_x >= 2) {
        int temp_corner = std::max(factor_x*this->xc(), other.xc());
        this->xs() = std::min(factor_x*(this->xc()+this->xs()), other.xc()+other.xs()) - temp_corner;
        this->xc() = temp_corner;
    } else if( factor_x <= -2 ) {
        int temp_corner = std::max(this->xc(), -factor_x*other.xc());
        this->xs() = std::min(this->xc()+this->xs(), -factor_x*(other.xc()+other.xs())) - temp_corner;
        this->xc() = temp_corner;
    }
    if( this->xs() <= 0 ) {
        //                     PetscPrintf(PETSC_COMM_SELF, " ---> Result is false (factor=%d,%d,%d), now this is "
        //                     "(%d,%d,%d,%d,%d,%d,%d,%d,%d)\n",
        //                                 factor_x, factor_y, factor_z,
        //                                 csm[0], csm[1], csm[2], csm[3], csm[4], csm[5], csm[6], csm[7], csm[8]);
        return false;
    }

    if( factor_y == 1 ) {
        int temp_corner = std::max(this->yc(), other.yc());
        this->ys() = std::min(this->yc()+this->ys(), other.yc()+other.ys()) - temp_corner;
        this->yc() = temp_corner;
    } else if(factor_y >= 2) {
        int temp_corner = std::max(factor_y*this->yc(), other.yc());
        this->ys() = std::min(factor_y*(this->yc()+this->ys()), other.yc()+other.ys()) - temp_corner;
        this->yc() = temp_corner;
    } else if( factor_y <= -2 ) {
        int temp_corner = std::max(this->yc(), -factor_y*other.yc());
        this->ys() = std::min(this->yc()+this->ys(), -factor_y*(other.yc()+other.ys())) - temp_corner;
        this->yc() = temp_corner;
    }
    if( this->ys() <= 0 ) {
        //                     PetscPrintf(PETSC_COMM_SELF, " ---> Result is false (factor=%d,%d,%d), now this is "
        //                     "(%d,%d,%d,%d,%d,%d,%d,%d,%d)\n",
        //                                 factor_x, factor_y, factor_z,
        //                                 csm[0], csm[1], csm[2], csm[3], csm[4], csm[5], csm[6], csm[7], csm[8]);
        return false;
    }

    if( factor_z == 1 ) {
        int temp_corner = std::max(this->zc(), other.zc());
        this->zs() = std::min(this->zc()+this->zs(), other.zc()+other.zs()) - temp_corner;
        this->zc() = temp_corner;
    } else if(factor_z >= 2) {
        int temp_corner = std::max(factor_z*this->zc(), other.zc());
        this->zs() = std::min(factor_z*(this->zc()+this->zs()), other.zc()+other.zs()) - temp_corner;
        this->zc() = temp_corner;
    } else if( factor_z <= -2 ) {
        int temp_corner = std::max(this->zc(), -factor_z*other.zc());
        this->zs() = std::min(this->zc()+this->zs(), -factor_z*(other.zc()+other.zs())) - temp_corner;
        this->zc() = temp_corner;
    }
    if( this->zs() <= 0 ) {
        //                     PetscPrintf(PETSC_COMM_SELF, " ---> Result is false (factor=%d,%d,%d), now this is "
        //                     "(%d,%d,%d,%d,%d,%d,%d,%d,%d)\n",
        //                                 factor_x, factor_y, factor_z,
        //                                 csm[0], csm[1], csm[2], csm[3], csm[4], csm[5], csm[6], csm[7], csm[8]);
        return false;
    }

    //     VERBOSE(PETSC_COMM_SELF, "%sResult is true (factor=%d,%d,%d), now this is "
    //     "(%d,%d,%d,%d,%d,%d,%d,%d,%d)\n",
    //                 factor_x, factor_y, factor_z,
    //                 csm[0], csm[1], csm[2], csm[3], csm[4], csm[5], csm[6], csm[7], csm[8]);

    return true;
}
