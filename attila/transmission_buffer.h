/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

#include "../tools/tools.h"
#include "../tools/structures.h"

#include "comm_data.h"

//!
//!
//! WARNING: buffer may be reused, just make sure you wait() or done() beofre
//! WARNING da has to be a da specificately compatible (dimension wise) with the communication
class TransmissionBuffer {
public:
    //! By the dfault buffer targets a single target, this is usually the case of intra_level communications
    TransmissionBuffer(MPI_Comm comm, DM da, int dof, bool use3d, bool scalar, int tag,
                       CommData * const  comm_data_array, bool * const bool_data_array = NULL,
                       int size = 1, int real_size = 1);

    //! Deletes all the chunks and reuqests and other (may want to call wait or done before)
    ~TransmissionBuffer();

    //! Takes a vector (right! dimension) and fills buffers with data of that vector, when returns, vector can be used
    PetscErrorCode fill(Vec v);

    //! Takes a vector (of the right! dimension) and fills vector with data from buffers, when returns,
    //! vector can be reused
    PetscErrorCode empty(Vec v);

    //! Asyngronously (non-blocking) send data to everyone who may be interested, when returns buffer may still be in
    //! use
    PetscErrorCode send(void);

    //! Asyngronously (non-blocking) receive data to everyone who may be interested, when returns buffer may still be in
    //! use
    PetscErrorCode recv(void);

    //! Wait for *ALL* requests of the buffer to be completed, returns when done (status malus, vana salus)
    PetscErrorCode wait(void);

    //! Just check if transmission was done
    bool done(void);

    // private:
    //! Comm
    MPI_Comm comm_;
    //! DMDA
    DM da_;
    //! Degrees of freedom of underlying Vector
    int dof_;
    //! Array containing specifications of intersections for each target
    CommData * comm_data_array_;
    //! Array specifiying whether targets intersect or not
    bool * bool_data_array_;
    //! Size is the number of possible targets of the buffer, whilst real_size is the size of the actual ones (the
    //! DMDAs that intersect)
    int size_, real_size_;
    //! Array containing chunks to be sent to targets, chunks[i] is prepared to be sent to comm_data_array[i].rank
    PetscScalar ** chunks_;
    //! **ALL** requests done by the class
    MPI_Request * req_;
    //! Tag associated to this send (make more unique)
    int tag_;
    //!
    bool use3d_;
    //!
    bool scalar_;
    //! Domain color of this send
    int dom_col;
};
