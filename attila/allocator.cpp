/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "allocator.h"

Allocator::Allocator(const Options * p_opt, int world_size)
    : p_opt_(p_opt) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    levels_ = p_opt->mlmc_levels;
    world_size_ = world_size;
    coarse_mesh_size_ = p_opt->mesh_opt;
    dim_ = p_opt->enable_3d ? 3 : 2;

    factor_x = 2;
    factor_y = 2;
    factor_z = 2;
    if (dim_ != 3) {
        factor_z = 1;
    }

    if( levels_ > world_size ) {
        WARNING("", WORLD,
                "Seems that you have less cores (%d) than parallel "
                " levels(%d), this has not yet been implememented!\n",
                world_size, levels_);
    }

    mesh_per_level_ = new Mesh[levels_];
    mesh_per_level_big_ = new Mesh[levels_];
    mesh_per_level_[levels_ - 1] = p_opt->mesh_opt;
    mesh_per_level_big_[levels_ - 1] = p_opt->mesh_opt;
    if( levels_ > 1) {
        mesh_per_level_[levels_ - 2][0] = mesh_per_level_[levels_ - 1][0];
        mesh_per_level_[levels_ - 2][1] = mesh_per_level_[levels_ - 1][1];
        mesh_per_level_[levels_ - 2][2] = mesh_per_level_[levels_ - 1][2];
        mesh_per_level_big_[levels_ - 2][0] = factor_x * mesh_per_level_[levels_ - 1][0];
        mesh_per_level_big_[levels_ - 2][1] = factor_y * mesh_per_level_[levels_ - 1][1];
        mesh_per_level_big_[levels_ - 2][2] = factor_z * mesh_per_level_[levels_ - 1][2];
    }
    for(int i = levels_ - 3; i >= 0; --i) {
        mesh_per_level_[i][0] = factor_x * mesh_per_level_[i+1][0];
        mesh_per_level_[i][1] = factor_y * mesh_per_level_[i+1][1];
        mesh_per_level_[i][2] = factor_z * mesh_per_level_[i+1][2];
        mesh_per_level_big_[i][0] = factor_x * mesh_per_level_[i][0];
        mesh_per_level_big_[i][1] = factor_y * mesh_per_level_[i][1];
        mesh_per_level_big_[i][2] = factor_z * mesh_per_level_[i][2];
    }
}

Allocator::~Allocator() {
    FUNCTIONBEGIN_MACRO("", WORLD);

    delete[] mesh_per_level_;
    delete[] mesh_per_level_big_;
}
