/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

#include <sstream>
#include <iomanip>
#include <unistd.h>

#include <deque>

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/profiler.h"

#include "static_allocator.h"
#include "dynamic_allocator.h"
#include "comm_data.h"
#include "transmission_buffer.h"

//!
//! \brief ATTILA (Advanced Transfer Through Interfaces for Legacy Arrays) is the class responsible
//! for distributing the simulations to the processors according to the allocator.
//!
//! Currently ATTILA needs thenumber of level,
//! the number of ranks per level, number of parallel levels for
//! each level, and the number of simulation done, in total,
//! in each level.
//! Attila receives also a NODE_SIZE parameter
//! and tries to greedyngly fit the single simulation
//! in the nodes.
//!
//! The super root (i.e. one of the groups of ranks wuth the finest simulations)
//! is allocated from ranks 0 to "number of ranks for level 0", then ATTILA
//! tries to fit the biggest simulation in the node until it reaches the boundary of the current
//! node with the biggest level that fits
//! or spills to the next node with the largest unallocated node if he cannot fit any level
//! then repeat until each node is fitted
//! TODO: PADDING?
//!
class Attila {
public:
    //!
    //! \brief Constructor, just grabs a reference to the Options.
    //!
    Attila(Options *p_opt);
    //!
    //! \brief Destroy all arrays.
    //!
    ~Attila();

    //!
    //! Print
    //!
    PetscErrorCode print(void);

    //// Communication routines (for array data)
    //! TODO Send data to super root
    //! data is the full global (local?) array as contiguous 1D piece of data
    template<bool scalar>
    PetscErrorCode send_to_next(Vec v, int tag);

    //! TODO Receive data from roots
    //! data is the full global (local?) array as contiguous 1D piece of data
    template<bool scalar>
    PetscErrorCode recv_from_prev(Vec v, int tag);

    //! TODO Send data to root
    //! data is the full global (local?) array as contiguous 1D piece of data

    template<bool scalar>
    PetscErrorCode level_to_root_begin(Vec mean, Vec var, std::deque<TransmissionBuffer*>  & tb,
                                       TAG_COMP tag, int frame);

    //! TODO Receive data from level
    //! data is the full global (local?) array as contiguous 1D piece of data
    template<bool scalar>
    PetscErrorCode level_to_root_end(Vec mean, Vec var, std::deque<TransmissionBuffer*>  & tb,
                                     int nadds, int * new_nadds,
                                     PetscReal sum_weights, PetscReal * new_sum_weights,
                                     PetscReal * weights_per_deepness);

    PetscErrorCode reinit_sim();

    PetscErrorCode next_sim();

    //// Petsc DM used to setup communications (fields and scalars) (does not own then, just pointers)
    DM da_, da_sc_;
    //// Petsc DM used to revcevive communications (fields and scalars)
    DM da_recv_, da_recv_sc_;

    // private:
    //// Grab/Compute info methods
    //!
    PetscErrorCode generate_intra_domain_color_table();
    //!
    PetscErrorCode generate_intra_level_color_table();
    //!
    PetscErrorCode generate_inter_level_color_table();

    //!
    PetscErrorCode generate_sim_id_table();

    //!
    PetscErrorCode generate_more_details();

    //// Generate communication data
    PetscErrorCode generate_intra_level_comm_data(DM da, DM da_sc);

    //// Generate communication data
    PetscErrorCode generate_inter_level_comm_data(DM da, DM da_sc);

    //// Communication routines for preparation
    //!
    MPI_Request send_mesh_data(int rank, CommData & dt, int tag);
    //!
    MPI_Request recv_mesh_data(int rank, CommData & dt, int tag);
    //! Merge two vector mean/var n root with 2 transmission buffers from a under level, use "batched" sums
    template <bool scalar>
    PetscErrorCode merge(Vec mean, Vec variance,
                         TransmissionBuffer * tb_mean, TransmissionBuffer *tb_var,
                         int nadds_left, int nadds_right);
    //! Use weighted sums
    template <bool scalar>
    PetscErrorCode weighted_merge(Vec mean, Vec variance,
                                  TransmissionBuffer * tb_mean, TransmissionBuffer *tb_var,
                                  int nadds_left, int nadds_right,
                                  PetscReal left_weights, PetscReal right_weights);

    ///
    ///
    ///  Communicators
    ///
    ///
    //!
    MPI_Comm intra_domain_comm;
    //!
    MPI_Comm intra_level_comm;
    //!
    MPI_Comm inter_level_comm;

    ///
    ///
    ///  Ranks and sizes
    ///
    ///
    //!
    int world_rank, world_size;
    //!
    int intra_domain_rank, intra_domain_size, intra_domain_color;
    //! Rank, size of the comm and group to which you
    //! belong for this rank, size is also the number of parallel
    //! simulation of *this* level
    int intra_level_rank, intra_level_size, intra_level_color;
    //!
    int inter_level_rank, inter_level_size, inter_level_color;

    //// Color table
    //! Color of a group comprised of a whole domain
    //! (each subdomain has the same color) (indexed by rank)
    int * intra_domain_color_table;
    //! (indexed by rank)
    int * intra_level_color_table;
    //! Associate to each parallel sim a "deepness" from the level root
    int * intra_level_deepness_table;
    //! (indexed by rank)
    int * inter_level_color_table;
    //! Level of the rank (indexed by rank)
    int * level_of_rank;
    //! Level of color of domain (indexed by color of domain)
    int * level_of_domain;
    //! Deepness of color of domain (indexed by color of domain)
    int * deepness_of_domain;
    //! (indexed by intra domain color rank)
    int * sim_id_table;
    //! Number of simulations done by this rank (indexed by rank)
    int * sim_num_of_rank;
    //! Simulation IDs of the rank (indexed by rank)
    int * sim_id_of_rank;
    //! Cumulative version of rank per level
    //! (each level l uses the comm. color (for intra lvl comm) from value at
    //! index l to l+1)
    int * cumu_ranks_per_level;

    //! Only for roots, different for each root, indexed by deepness
    int * sim_num_table;

    //// Ranks to/from which a communication has to be performed (with details)
    //! Mine CommData
    CommData comm_data, comm_data_recv;
    //!
    CommData * to_root_data_stream;
    //!
    CommData * to_next_root_data_stream;
    //!
    CommData * from_prev_root_data_stream;
    //!
    CommData * from_levels_data_stream;
    //! Flags that says if this intersect with which root (if root)
    bool * intersects_prev_root_data_stream;
    //! Flags that says if this intersect with which super root (if super root)
    bool * intersects_next_root_data_stream;

    //// Global data
    //! Number of parallel runs performed (sum of no of parallel sim per level)
    //! This is also the maximum size of intra dom color table elements
    int parallel_runs;
    //! Number of ranks that has been used;
    int used_ranks;
    //! Number of procs in super root
    int super_root_size;
    //! Number of root procs not in super root
    int not_in_super_root_size;
    //! Size of previous root and next root
    int prev_root_size, next_root_size;
    //! Size of previous root and next root, removed not
    //! intersecting ranks (i.e. number of sends/rcvs)
    int real_prev_root_size, real_next_root_size;

    //// Data for Communicator
    //! Level of refinement the domain (L = 0 is the mean level (coarsest))
    int level;

    //// Properties
    //! Is the root (i.e.recieves all arrays from the level)?
    bool is_root;
    //! Is super root (root receiving ALL data (in the end) data)
    bool is_super_root;
    //! Is smallest root (root sending ALL data)
    bool is_smallest_root;

    //// IDs, identification numbers for various jobs
    //! Unique ID of the current simulation
    int sim_id;

    ////
    //! Number of simulation of this process
    int sim_num;

    //! Composition of allocator (used to get all the details on process allocation)
    Allocator              *p_alloc_;
    //! Read options by composition
    Options * const        p_opt_;

    //! Initial mesh size associated to this processor by the allocator
    Mesh     mesh;
};

//!
//! Communication routines (for array data)
//! TODO Send data to super root
//! data is the full global (local?) array as contiguous 1D piece of data
//!
template<bool scalar>
PetscErrorCode Attila::send_to_next(Vec v, int tag) {
    //! Well use a send buffer class
    //! Data has to be copied in the send buffer
    //! For each send that has to be done, data has to be data_sent_to_next_root
    //! All requests should be stored in the buffer
    //! Program may continue
    //! When all this request are completed we may delete the buffer
    //         for(int i = 0; i < next_root_size; ++i ) {
    //             if( intersects_next_root_data_stream[i] ) {

    //             }
    //         }

    DM da;
    int dof;

    if(scalar) {
        da = da_sc_;
        dof = 1;
    } else {
        da = da_;
        p_opt_->enable_3d ? dof = 3 : dof = 2;
    }

    if( !is_super_root ) {
        TransmissionBuffer * tb =
                new TransmissionBuffer(inter_level_comm, da, dof,
                                       p_opt_->enable_3d, scalar, tag,
                                       to_next_root_data_stream,
                                       intersects_next_root_data_stream, next_root_size,
                                       real_next_root_size );
        tb->fill(v);
        tb->send();
        tb->wait();
        delete tb;
    }

    PetscFunctionReturn(ierr);
}

//!
//! TODO Receive data from roots
//! data is the full global (local?) array as contiguous 1D piece of data
//!
template<bool scalar>
PetscErrorCode Attila::recv_from_prev(Vec v, int tag)
{

    DM da;
    int dof;

    if(scalar) {
        da = da_recv_sc_;
        dof = 1;
    } else {
        da = da_recv_;
        p_opt_->enable_3d ? dof = 3 : dof = 2;
    }

    if( !is_smallest_root ) {
        TransmissionBuffer * tb =
                new TransmissionBuffer(inter_level_comm, da, dof,
                                     p_opt_->enable_3d, scalar, tag,
                                     from_prev_root_data_stream,
                                     intersects_prev_root_data_stream, prev_root_size,
                                     real_prev_root_size );

        tb->recv(); CHKERRQ(ierr);
        tb->wait(); CHKERRQ(ierr);
        tb->empty(v); CHKERRQ(ierr);
        delete tb;
    }

    PetscFunctionReturn(ierr);
}

//!
//! TODO Send data to root
//! data is the full global (local?) array as contiguous 1D piece of data
//!
template<bool scalar>
PetscErrorCode Attila::level_to_root_begin(Vec mean, Vec var,
                                           std::deque<TransmissionBuffer*> & tb,
                                           TAG_COMP tag, int frame) {

    DM da;
    int dof;

    if(scalar) {
        da = da_sc_;
        dof = 1;
    } else {
        da = da_;
        p_opt_->enable_3d ? dof = 3 : dof = 2;
    }

    if( !is_root ) {
        DEBUG(this, INTRADM, "[LTR] Preparing to send buffer to root.\n", "");
        tb.push_back(new TransmissionBuffer(intra_level_comm, da, dof, p_opt_->enable_3d, scalar,
                                            get_intra_level_tag(tag, TAG_MEAN, frame), to_root_data_stream));
        ierr = tb.back()->fill(mean); CHKERRQ(ierr);
        ierr = tb.back()->send(); CHKERRQ(ierr);
        tb.push_back(new TransmissionBuffer(intra_level_comm, da, dof, p_opt_->enable_3d, scalar,
                                            get_intra_level_tag(tag, TAG_VAR, frame), to_root_data_stream));
        ierr = tb.back()->fill(var); CHKERRQ(ierr);
        ierr = tb.back()->send(); CHKERRQ(ierr);
    } else {
        for(int i = 0; i < intra_level_size-1; ++i ) {
            DEBUG(this, INTRADM,
                  "[LTR] Preparing to recv buffer from level %d (of %d).\n",
                  i+1, intra_level_size
            );
            tb.push_back(new TransmissionBuffer(intra_level_comm, da, dof, p_opt_->enable_3d, scalar,
                                                get_intra_level_tag(tag, TAG_MEAN, frame),
                                                &from_levels_data_stream[i]));
            ierr = tb.back()->recv(); CHKERRQ(ierr);
            tb.push_back(new TransmissionBuffer(intra_level_comm, da, dof, p_opt_->enable_3d, scalar,
                                                get_intra_level_tag(tag, TAG_VAR, frame), &from_levels_data_stream[i]));
            ierr = tb.back()->recv(); CHKERRQ(ierr);
        }
    }
    PetscFunctionReturn(ierr);
}

//! TODO Receive data from level
//! data is the full global (local?) array as contiguous 1D piece of data
//! @param[nadds] number of additions performed thus far
//! @param[new_nadds] number of additions perfomed once transmission is done
//! @param[sum_weights]
//! @param[new_sum_weights]
//! TOOD: weighted transmission
template<bool scalar>
PetscErrorCode Attila::level_to_root_end(Vec mean, Vec var, std::deque<TransmissionBuffer*>  & tb,
                                         int nadds, int * new_nadds,
                                         PetscReal sum_weights, PetscReal * new_sum_weights,
                                         PetscReal * weights_per_deepness)
{

    *new_nadds = nadds;
    *new_sum_weights = sum_weights;

    if( !is_root ) {
        DEBUG(this, INTRADM, "[LTR] Waiting transmission to root to finish.\n", "");
        tb.front()->wait(); CHKERRQ(ierr);
        delete tb.front();
        tb.pop_front();
        tb.front()->wait(); CHKERRQ(ierr);
        delete tb.front();
        tb.pop_front();
        DEBUG(this, INTRADM, "[LTR] Done transmission to root!\n", "");
    } else {
        // TODO: WAIT_ANY
        for(int i = 0; i < intra_level_size-1; ++i) {
            DEBUG(this, INTRADM, "[LTR] Waiting transmission to %d of %d to finish.\n",
                  intra_domain_color, parallel_runs, i+1, intra_level_size);
            tb.front()->wait(); CHKERRQ(ierr);
            tb.begin()[1]->wait(); CHKERRQ(ierr);

            // Get the number of simulations you are receiving
            int num_sim_of_recv = sim_num_table[i+1];

            PetscReal tb_weights = 0;
            if(QuadratureType::Stochastic != p_opt_->quad_type) {
                tb_weights = weights_per_deepness[i+1];
                DEBUG(this, INTRADM, "[LTR] Done transmission to %d of %d (num=%f).\n",
                        i+1, intra_level_size,
                        weights_per_deepness[i+1]);
            }
            // Merge mean and variance from the transmission buffer
            // new_adds = left adds, num_sim_of_recv = right adds
            if( QuadratureType::Stochastic == p_opt_->quad_type ) {
                merge<scalar>(mean, var, tb.front(), tb.begin()[1], *new_nadds, num_sim_of_recv);
            } else {
                weighted_merge<scalar>(mean, var, tb.front(), tb.begin()[1],
                                       *new_nadds, num_sim_of_recv, *new_sum_weights, tb_weights);
            }

            DEBUG(this, INTRADM, "[LTR] Done transmission to %d of %d (num=%f).\n",
                  i+1, intra_level_size,
                  num_sim_of_recv);
            *new_nadds += num_sim_of_recv;
            *new_sum_weights += tb_weights;

            delete tb.front();
            tb.pop_front();
            delete tb.front();
            tb.pop_front();
        }
    }


    PetscFunctionReturn(ierr);
}

//! Takes a vector (of the right! dimension) and fills vector with data from buffers, when returns,
//! vector can be reused
//! TODO: template all branches
template <bool scalar>
PetscErrorCode Attila::merge(Vec mean, Vec M2,
                             TransmissionBuffer * tb_mean, TransmissionBuffer *tb_M2,
                             int nadds_left, int nadds_right)
{

    OUTPUT(this, INTRADM, "Merging two batches of mean and M2 of size %d and %d.\n", nadds_left, nadds_right);

    double frac_1 = (double) nadds_right / (double) (nadds_left + nadds_right);
    double frac_2 = (double) nadds_right * nadds_left / (double) (nadds_left + nadds_right);
    // TODO, check for contigous data (perform action)
    int I_ = 0;
    for(int I = 0; I < tb_mean->size_; ++I) {
        CommData &dt = tb_mean->comm_data_array_[I];
        if( tb_mean->bool_data_array_ == NULL || tb_mean->bool_data_array_[I] ) {
            // TODO

            int J = 0;
            if( p_opt_->enable_3d ) { // 3D
                if( scalar ) { // Scalar
                    PetscScalar *** mean_array, ***M2_array;
                    ierr = DMDAVecGetArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecGetArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                PetscScalar delta =  tb_mean->chunks_[I_][J] - mean_array[k][j][i];
                                mean_array[k][j][i] += delta * frac_1;
                                M2_array[k][j][i] += tb_M2->chunks_[I_][J] + delta * delta * frac_2;
                                ++J;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                } else { //  Field
                    Field3D *** mean_array, ***M2_array;
                    ierr = DMDAVecGetArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecGetArray(da_, M2, &M2_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                for(int d = 0; d < tb_mean->dof_; ++d) {
                                    PetscScalar delta =  tb_mean->chunks_[I_][J+d] - mean_array[k][j][i][d];
                                    mean_array[k][j][i][d] += delta * frac_1;
                                    M2_array[k][j][i][d] += tb_M2->chunks_[I_][J+d] + delta * delta * frac_2;
                                }
                                J += tb_mean->dof_;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_, M2, &M2_array); CHKERRQ(ierr);
                }
            } else { // 2D
                if( scalar ) { // Scalar
                    PetscScalar ** mean_array, **M2_array;
                    ierr = DMDAVecGetArray(da_sc_, mean, &mean_array);
                    ierr = DMDAVecGetArray(da_sc_, M2, &M2_array);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            PetscScalar delta =  tb_mean->chunks_[I_][J] - mean_array[j][i];
                            mean_array[j][i] += delta * frac_1;
                            M2_array[j][i] += tb_M2->chunks_[I_][J] + delta * delta * frac_2;
                            ++J;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                } else { // Field
                    Field2D ** mean_array, **M2_array;
                    ierr = DMDAVecGetArray(da_, mean, &mean_array);
                    ierr = DMDAVecGetArray(da_, M2, &M2_array);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            for(int d = 0; d < tb_mean->dof_; ++d) {
                                PetscScalar delta =  tb_mean->chunks_[I_][J+d] - mean_array[j][i][d];
                                mean_array[j][i][d] += delta * frac_1;
                                M2_array[j][i][d] += tb_M2->chunks_[I_][J+d] + delta * delta * frac_2;

                            }
                            J += tb_mean->dof_;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_, M2, &M2_array); CHKERRQ(ierr);
                }
            }


            ++I_;
        }
    }

    PetscFunctionReturn(ierr);
}

//! Takes a vector (of the right! dimension) and fills vector with data from buffers, when returns,
//! vector can be reused, this is used for quadrature, takes care of weights
//! TODO: template all branches, cleanup remove this mess
//! TODO WARNING FIXME: this need to be checked for variacne
//! *********************** VARIANCE IS WRONG!!!!!!! WARNING FIXME FIXME!!!!!***************************
template <bool scalar>
PetscErrorCode Attila::weighted_merge(Vec mean, Vec M2,
                                      TransmissionBuffer * tb_mean, TransmissionBuffer *tb_M2,
                                      int nadds_left, int nadds_right,
                                      PetscReal left_weights, PetscReal right_weights) {
    UNUSED(nadds_left); UNUSED(nadds_right);
    WARNING(this, INTRADM,
           "Please make sure you wanted to reach this position.", "");

    //  double frac_1 = (double) nadds_right / (double) (nadds_left + nadds_right);
    //  double frac_2 = (double) nadds_right * nadds_left / (double) (nadds_left + nadds_right);
    double wfrac_1 = 1. / (left_weights + right_weights); //right_weights / (left_weights + right_weights);
    //  double wfrac_2 = right_weights * left_weights / (left_weights + right_weights);
    OUTPUT(this, INTRADM, "Merging two batches of mean and M2 of size %d and %d (wl = %f, wr = %f, sum = %f).\n",
           nadds_left, nadds_right, left_weights, right_weights, wfrac_1);
    // TODO, check for contiguous data (perform action)
    int I_ = 0;
    for(int I = 0; I < tb_mean->size_; ++I) {
        CommData &dt = tb_mean->comm_data_array_[I];
        if( tb_mean->bool_data_array_ == NULL || tb_mean->bool_data_array_[I] ) {
            // TODO

            int J = 0;
            if( p_opt_->enable_3d ) { // 3D
                if( scalar ) { // Scalar
                    PetscScalar *** mean_array, ***M2_array;
                    ierr = DMDAVecGetArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecGetArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                //         PetscScalar delta =  tb_mean->chunks_[I_][J] - mean_array[k][j][i];
                                //         mean_array[k][j][i] += delta * wfrac_1;
                                mean_array[k][j][i] = wfrac_1 * (mean_array[k][j][i] * left_weights
                                                                 + tb_mean->chunks_[I_][J] * right_weights);
                                M2_array[k][j][i] += tb_M2->chunks_[I_][J]; // + delta * delta * wfrac_2;
                                ++J;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                } else { //  Field
                    Field3D *** mean_array, ***M2_array;
                    ierr = DMDAVecGetArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecGetArray(da_, M2, &M2_array); CHKERRQ(ierr);
                    for (int k=dt.zc(); k<dt.zc()+dt.zs(); ++k) {
                        for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                            for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                                for(int d = 0; d < tb_mean->dof_; ++d) {
                                    //          PetscScalar delta =  tb_mean->chunks_[I_][J+d] - mean_array[k][j][i][d];
                                    //          mean_array[k][j][i][d] += delta * wfrac_1;
                                    mean_array[k][j][i][d] = wfrac_1 * (mean_array[k][j][i][d] * left_weights
                                                                        + tb_mean->chunks_[I_][J+d] * right_weights);
                                    M2_array[k][j][i][d] += tb_M2->chunks_[I_][J+d]; // + delta * delta * wfrac_2;
                                }
                                J += tb_mean->dof_;
                            }
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_, M2, &M2_array); CHKERRQ(ierr);
                }
            } else { // 2D
                if( scalar ) { // Scalar
                    PetscScalar ** mean_array, **M2_array;
                    ierr = DMDAVecGetArray(da_sc_, mean, &mean_array);
                    ierr = DMDAVecGetArray(da_sc_, M2, &M2_array);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            //        PetscScalar delta =  tb_mean->chunks_[I_][J] - mean_array[j][i];
                            //        mean_array[j][i] += delta * wfrac_1;
                            //           mean_array[j][i] += tb_mean->chunks_[I_][J];
                            if(i == 16 && j == 16) {OUTPUT(this, INTRADM, "1 %f, 2 %f, 3 %f, 4 %f, 5 %f).\n",
                                                           wfrac_1,mean_array[j][i] ,left_weights , tb_mean->chunks_[I_][J] , right_weights);}
                            mean_array[j][i] = wfrac_1 * (mean_array[j][i] * left_weights + tb_mean->chunks_[I_][J] * right_weights);
                            M2_array[j][i] += tb_M2->chunks_[I_][J]; // + delta * delta * wfrac_2;
                            ++J;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_sc_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_sc_, M2, &M2_array); CHKERRQ(ierr);
                } else { // Field
                    Field2D ** mean_array, **M2_array;
                    ierr = DMDAVecGetArray(da_, mean, &mean_array);
                    ierr = DMDAVecGetArray(da_, M2, &M2_array);
                    for (int j=dt.yc(); j<dt.yc()+dt.ys(); ++j) {
                        for (int i=dt.xc(); i<dt.xc()+dt.xs(); ++i) {
                            for(int d = 0; d < tb_mean->dof_; ++d) {
                                //         PetscScalar delta =  tb_mean->chunks_[I_][J+d] - mean_array[j][i][d];
                                //         mean_array[j][i][d] += tb_mean->chunks_[I_][J+d];
                                //            mean_array[j][i][d] += tb_mean->chunks_[I_][J+d];
                                mean_array[j][i][d] = wfrac_1 * (mean_array[j][i][d] * left_weights
                                                                 + tb_mean->chunks_[I_][J+d] * right_weights);
                                M2_array[j][i][d] += tb_M2->chunks_[I_][J+d]; // + delta * delta * wfrac_2;

                            }
                            J += tb_mean->dof_;
                        }
                    }
                    ierr = DMDAVecRestoreArray(da_, mean, &mean_array); CHKERRQ(ierr);
                    ierr = DMDAVecRestoreArray(da_, M2, &M2_array); CHKERRQ(ierr);
                }
            }

            ++I_;
        }
    }

    PetscFunctionReturn(ierr);
}

