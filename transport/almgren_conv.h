/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef ALMGREN_CONV_H
#define ALMGREN_CONV_H

//! @file almgren_conv.h
//! Nonlinear term as provided by Almgren et Al.

#include "conv.h"

//! This class implements the non-linear term as designed in Almgren et Al.
//!
class AlmgrenConv : public Conv
{
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for Laplacian matrix
    AlmgrenConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
        : Conv(p_opt, p_dom, p_mod, p_cm)
    {}
    //! Free some memory
    virtual ~AlmgrenConv() {};

    //! Actually computes the convection
    PetscErrorCode conv(Vec U, Vec G, Vec F, Vec Lpl, Vec U_conv);

private:
    //! Compute normal (transverse) derivatives
    PetscErrorCode trv_diff(Vec U_LR, Vec U_BT, Vec U_no_x, Vec U_no_y);
    //! Do a first interpolation (to edges)
    PetscErrorCode first_interp(Vec U, Vec U_x, Vec U_y,
                                Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T);
    //! To a finer second interpolation (to edges)
    PetscErrorCode second_interp(Vec U, Vec G, Vec F,
                                 Vec Lpl, Vec U_x, Vec U_y, Vec U_no_x, Vec U_no_y,
                                 Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T);
    //! Upwind edges
    PetscErrorCode upwind(Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T,
                          Vec U_LR, Vec U_BT);
};

#endif // ALMGREN_CONV_H
