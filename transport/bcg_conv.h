/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file bcg_conv.h
//! Non-linear term as provided by BCG.

#include "conv.h"

//! @brief Implements the non-linear term  as specified in BCG (as well as 3D version thereof)
class BCGConv : public Conv
{
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    BCGConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
        : Conv(p_opt, p_dom, p_mod, p_cm) {}
    //! Free buffers and matrix
    virtual ~BCGConv() {};

    //! Normal derivatives
    PetscErrorCode trv_diff(Vec U, Vec U_x, Vec U_y, Vec U_z, Vec U_no_x, Vec U_no_y, Vec U_no_z);
    //! Interpolate to half time step
    PetscErrorCode interp(Vec U, Vec G, Vec F, Vec D,
                          Vec Lpl, Vec U_x, Vec U_y, Vec U_z, Vec U_no_x, Vec U_no_y, Vec U_no_z,
                          Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T, Vec U_edg_D, Vec U_edg_U);
    //! Upwind edge values
    PetscErrorCode upwind(Vec U_edg_L, Vec U_edg_R,
                          Vec U_edg_B, Vec U_edg_T,
                          Vec U_edg_D, Vec U_edg_U,
                          Vec U_LR, Vec U_BT, Vec U_DU);
    //! Actually computes nonlinear term
    PetscErrorCode conv(Vec U, Vec G, Vec F, Vec Lpl, Vec U_conv);
private:
};
