/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "stab_vort_conv.h"

//!
//! @param[in] U
//! @param[in] w
//! @param[out] w_out
//! @return Error code ierr
PetscErrorCode StabVortConv::conv(Vec U, Vec w, Vec w_out) {
    FUNCTIONBEGIN(p_cm_);

    Vec              w_local, U_local;
    PetscScalar      **w_array, **w_out_array;
    Field2D          **U_array;
    Volume2D         **d;

    ierr = DMGetLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);

    // TODO: compute U_avg on local already please

    ierr = DMDAVecGetArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, w_out, &w_out_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
//     #pragma omp parallel for collapse(2)
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            w_out_array[j][i] = (( U_array[j][i+1][0] + U_array[j-1][i+1][0] + U_array[j][i][0] + U_array[j-1][i][0])
                                            * ( w_array[j][i+1] + w_array[j][i] )
                               - ( U_array[j][i][0] + U_array[j-1][i][0] + U_array[j][i-1][0] + U_array[j-1][i-1][0])
                                            * ( w_array[j][i] + w_array[j][i-1] )) / (8. * d[j][i].x)
                              + (( U_array[j+1][i][1] + U_array[j+1][i-1][1] + U_array[j][i][1] + U_array[j][i-1][1])
                                            * ( w_array[j+1][i] + w_array[j][i] )
                               - ( U_array[j][i][1] + U_array[j][i-1][1] + U_array[j-1][i][1] + U_array[j-1][i-1][1])
                                            * ( w_array[j][i] + w_array[j-1][i] )) / (8. * d[j][i].y);
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_out, &w_out_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Allocate suffiecient space for the allocation of the conv matrix
//!
PetscErrorCode StabVortConv::preallocate() {
    // Stage log for convection cons
    //     PetscLogStagePush(allocstage);
    DEBUG(p_cm_, INTRADM, "Preallocating convection matrix...\n", "");

    CreateMatrix(p_dom_->da_sc, &conv_mat); CHKERRQ(ierr);
    // This takes care of preallocation, so do not worry about that
    //     ierr = DMSetMatType(p_dom_->da_sc, MATMPIAIJ); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Fill matrix for conv with values of U
//!
PetscErrorCode StabVortConv::fill(Vec U) {
    // Stage log for convection cons
    //     PetscLogStagePush(fillstage);
    DEBUG(p_cm_, INTRADM, "Filling convection matrix...\n", "");

    MatStencil     row;
    PetscInt       ncols;

    Vec U_local;
    ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);

    // TODO: reformat the following, somehow.
    if( p_opt_->enable_3d ) {
        //       DEBUG(p_cm_->intra_domain_color, "%sAssembly %dx%dx%d Convection matrix...\n", p_dom_->Nx*p_dom_->Ny,
        // p_dom_->Nx*p_dom_->Ny,
        // p_dom_->Nz*p_dom_->Nz);
        ERROR(p_cm_, INTRADM, "Not yet implemented!\n", "");
//         for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
//             for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
//                 for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
//                 }
//             }
//         }
    } else { // 2D Case
        //       DEBUG(p_cm_->intra_domain_color, "%sAssembly %dx%d Convection matrix...\n",
        //             p_dom_->Nx*p_dom_->Ny, p_dom_->Nx*p_dom_->Ny);
        Field2D         **U_array;
        Volume2D        **d;
        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        PetscScalar         v[5];
//         #pragma omp parallel for collapse(2) private(row, ncols, v)
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                int            k = 0;
                ncols = 0;
                MatStencil    col[5] = {{0,0,0,0}};
                row.k = k;             row.j = j;          row.i = i;          row.c = 0;

                col[ncols].k=k;        col[ncols].j = j+1; col[ncols].i = i;
                v[ncols++] = + p_mod_->dt * (U_array[j+1][i][1] + U_array[j][i][1]
                             + U_array[j+1][i-1][1] + U_array[j][i-1][1]) / (16. * d[j][i].y);
                col[ncols].k=k;        col[ncols].j = j-1; col[ncols].i = i;
                v[ncols++] = - p_mod_->dt * (U_array[j][i][1] + U_array[j-1][i][1]
                             + U_array[j][i-1][1] + U_array[j-1][i-1][1] ) / (16. * d[j][i].y);
                col[ncols].k=k;        col[ncols].j = j;   col[ncols].i = i+1;
                v[ncols++] = + p_mod_->dt * (U_array[j][i+1][0] + U_array[j][i][0]
                             + U_array[j-1][i+1][0] + U_array[j-1][i][0] ) / (16. * d[j][i].x);
                col[ncols].k=k;        col[ncols].j = j;   col[ncols].i = i-1;
                v[ncols++] = - p_mod_->dt * (U_array[j][i][0] + U_array[j][i-1][0]
                             + U_array[j-1][i][0] + U_array[j-1][i-1][0] ) / (16. * d[j][i].x);

                col[ncols].k=k;        col[ncols].j=j;     col[ncols].i = i;
                v[ncols++] = + p_mod_->dt * (U_array[j][i+1][0] + U_array[j-1][i+1][0]
                                           - U_array[j][i-1][0] - U_array[j-1][i-1][0]) / (16. * d[j][i].x)
                             + p_mod_->dt * (U_array[j+1][i][1] + U_array[j+1][i-1][1]
                                           - U_array[j-1][i][1] - U_array[j-1][i-1][1] ) / (16. * d[j][i].y);
                ierr = MatSetValuesStencil(conv_mat, 1, &row, ncols, col, v, INSERT_VALUES);
                // WARNING: OPENMP fix
//                 CHKERRQ(ierr);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(conv_mat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(conv_mat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
