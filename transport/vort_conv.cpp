/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "vort_conv.h"

//! Reconstruct approximations of \f$\omega_y, \omega_x\f$ using the minmod limiter.
//! @param[in] w Vorticity to reconstruct (copied into local var)
//! @param[out] w_x x-derivative limited \f$\sim \Delta x (\Delta_x \omega)\f$, needs preallocation
//! @param[out] w_y x-derivative limited \f$\sim \Delta y (\Delta_y \omega )\f$, needs preallocation
//! @return Error code ierr
PetscErrorCode VortConv::reconstruct(Vec w, Vec w_x, Vec w_y) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal        L, C, R;

    Vec              w_local;
    PetscReal        **w_array, **w_x_array, **w_y_array;

    ierr = DMGetLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, w_x, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, w_y, &w_y_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            L = p_opt_->theta * ( w_array[j+1][i] - w_array[j][i] );
            C = 0.5 * ( w_array[j+1][i] - w_array[j-1][i] );
            R = p_opt_->theta * ( w_array[j][i] - w_array[j-1][i] );
            w_y_array[j][i] = minmod(L, C, R);
            L = p_opt_->theta * ( w_array[j][i+1] - w_array[j][i] );
            C = 0.5 * ( w_array[j][i+1] - w_array[j][i-1] );
            R = p_opt_->theta * ( w_array[j][i] - w_array[j][i-1] );
            w_x_array[j][i] = minmod(L, C, R);
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_x, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_y, &w_y_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Compute a staggered vorticity an in \cite TadmorLevy from reconstructed values.
//! @param[in] w Vorticity to reconstruct (copied into local var), copied to local TODO: grab a local one
//! @param[in] w_x x-derivative \f$\sim \Delta x (\Delta_x \omega)\f$, copied to local
//! @param[in] w_y y-derivative \f$\sim \Delta y (\Delta_y \omega )\f$, copied to local
//! @param[in] new_w reconstructed velocity, copied to local
//! @param[in] new_U reconstructed velocity, copied to local
//! @param[in] c_F curl of forcing quantity
//! @param[out] stag_w staggered vorticity, needs preallocation
//! @return Error code ierr
PetscErrorCode VortConv::conv(Vec w, Vec w_x, Vec w_y,
                              Vec new_w, Vec new_U, Vec c_F, Vec stag_w) {
    FUNCTIONBEGIN(p_cm_);

    p_profiler->push_stage("convection");

    PetscReal        F, G;
    int              i_, j_;

    PetscReal        **w_array, **new_w_array, **w_x_array, **w_y_array, **stag_w_array, **c_F_array;
    Field2D          **new_U_array;

    Vec     w_local, new_w_local, w_x_local, w_y_local;
    Vec     new_U_local;

    ierr = DMGetLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da_sc, &new_w_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da_sc, &w_x_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da_sc, &w_y_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &new_U_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, w_x, INSERT_VALUES, w_x_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w_x, INSERT_VALUES, w_x_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da_sc,w_y, INSERT_VALUES, w_y_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w_y, INSERT_VALUES, w_y_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, new_w, INSERT_VALUES, new_w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, new_w, INSERT_VALUES, new_w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, new_U, INSERT_VALUES, new_U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, new_U, INSERT_VALUES, new_U_local); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da_sc,  stag_w, &stag_w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  w_x_local, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  w_y_local, &w_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  new_w_local, &new_w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da,  new_U_local, &new_U_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  c_F, &c_F_array); CHKERRQ(ierr);

    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            j_ = j - offset_idx;
            i_ = i - offset_idx;
            F = new_w_array[j_][i_] * new_U_array[j_][i_][0]
                    + new_w_array[j_+1][i_] * new_U_array[j_+1][i_][0]
                    - new_w_array[j_][i_+1] * new_U_array[j_][i_+1][0]
                    - new_w_array[j_+1][i_+1] * new_U_array[j_+1][i_+1][0];
            G = new_w_array[j_][i_] * new_U_array[j_][i_][1]
                    - new_w_array[j_+1][i_] * new_U_array[j_+1][i_][1]
                    + new_w_array[j_][i_+1] * new_U_array[j_][i_+1][1]
                    - new_w_array[j_+1][i_+1] * new_U_array[j_+1][i_+1][1];
            stag_w_array[j][i] = 1.0/4.0 * ( w_array[j_][i_] + w_array[j_+1][i_]
                    + w_array[j_][i_+1] + w_array[j_+1][i_+1] )
                    + 1.0/16.0 * ( w_x_array[j_][i_] + w_x_array[j_+1][i_] - w_x_array[j_][i_+1] - w_x_array[j_+1][i_+1] )
                    + 1.0/16.0 * ( w_y_array[j_][i_] - w_y_array[j_+1][i_] + w_y_array[j_][i_+1] - w_y_array[j_+1][i_+1] )
                    + p_mod_->dt * 0.5 * ( F/ p_dom_->dxi + G/ p_dom_->dyi) + p_mod_->dt * c_F_array[j][i];
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  stag_w, &stag_w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  w_x_local, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  w_y_local, &w_y_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  new_w_local, &new_w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da,  new_U_local, &new_U_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  c_F, &c_F_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da_sc, &new_w_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_x_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_y_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &new_U_local); CHKERRQ(ierr);

    ierr = p_profiler->pop_stage(); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
