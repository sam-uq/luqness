/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VORT_CONV_H
#define VORT_CONV_H

//! @file vort_conv.h
//! Implements the non-linear term for vortex methods as provided by Tadmor/Levy

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/domain.h"
#include "../tools/profiler.h"
#include "../models/model_base.h"

//! The non-linear term for a vortex method (2nd order) as defined in Tadmor/Levy. Contains
//! all the routines for the evolution of the nonlinear term. Also keeps track of the offset
//! caused by the staggered grid.
class VortConv {
public:
    //!
    VortConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
        : offset_idx(0), p_opt_ (p_opt), p_dom_(p_dom), p_mod_(p_mod), p_cm_(p_cm) { }
    //!
    virtual ~VortConv() {};

    //! Reconstruct gradient of vorticity using a minmod limiter
    PetscErrorCode reconstruct(Vec w, Vec w_x, Vec w_y);
    //! Predict vorticity at 1/factor step form velocity
    template <int factor>
    PetscErrorCode predict(Vec w, Vec w_x, Vec w_y,
                           Vec U, Vec c_F, Vec new_w);
    //! Compute the staggered vorticity with a second order scheme
    virtual PetscErrorCode conv(Vec w, Vec w_x, Vec w_y,
                                Vec new_w, Vec new_U, Vec c_F, Vec stag_w);

    //! Keeps track of current offset in the staggering (not used in first order)
    PetscInt              offset_idx;

protected:
    Options               * const p_opt_;
    Domain                * const p_dom_;
    ModelBase             * const p_mod_;
    Attila                * const p_cm_;

};

//! Predict new vorticity  as \f$\omega_{new} := \omega - \Delta t ( \frac{\omega_x}{\Delta x} u +
//! \frac{\omega_y}{\Delta y} v + \nabla \times F ) / factor\f$
//! WARNING: NO anisotropy!!!
template <int factor>
PetscErrorCode VortConv::predict(Vec w, Vec w_x, Vec w_y,
                                 Vec U, Vec c_F, Vec new_w) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal        **w_array, **w_x_array, **w_y_array, **w_new_array, **c_F_array;
    Field2D          **U_array;

    ierr = DMDAVecGetArray(p_dom_->da_sc, w, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, w_x, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, w_y, &w_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, c_F, &c_F_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc, new_w, &w_new_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
//     ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            w_new_array[j][i] = w_array[j][i] - p_mod_->dt * (w_x_array[j][i] * U_array[j][i][0] / p_dom_->dxi
            - w_y_array[j][i] * U_array[j][i][1] / p_dom_->dyi + c_F_array[j][i]) / (double) factor;
        }
    }
//     ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_x, &w_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_y, &w_y_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, c_F, &c_F_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, new_w, &w_new_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

#endif // VORTICITY_EVOLVER_H
