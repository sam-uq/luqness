/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "ftcs_vort_conv.h"

/*! Compute \f[ \omega_{new} = - \Delta t (\frac{u_{i+1,j}\omega_{i+1,j} - u_{i+1,j}\omega_{i+1,j}}{2 \Delta x} +
\frac{ u_{i+1,j}\omega_{i+1,j} - u_{i+1,j}\omega_{i+1,j}}{ 2 \Delta y}) +
1/4 \omega_{i+1,j} + \omega_{i-1,j} + \omega_{i,j+1} + \omega_{i,j-1}) \f] */
//! This is a FTCS evolution a-la Lax-Friedrichs with cell-centered staggered flux (temporal FTCS is already included).
//! TODO: Add forcing
//! @param[in] w Vorticity to reconstruct (copied into local var), copied to local TODO: grab a local one
//! @param[in] U Reconstructed velocity
//! @param[in] c_F Curl of forcing quantity
//! @param[out] new_w Vorticity at next time step vorticity, needs preallocation.
//! @return Error code ierr.
PetscErrorCode FTCSVortConv::conv(Vec w, Vec U, Vec c_F, Vec new_w)
{
    FUNCTIONBEGIN(p_cm_);

    Vec              w_local;
    Vec              U_local;
    PetscReal        **w_array, **new_w_array, **c_F_array;
    Field2D          **U_array;
    Volume2D         **d;

    ierr = DMGetLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, w, INSERT_VALUES, w_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  new_w, &new_w_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da,  U_local, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_sc,  c_F, &c_F_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            new_w_array[j][i] = - p_mod_->dt * 0.5 * ( ( U_array[j][i+1][0] * w_array[j][i+1]
                    - U_array[j][i-1][0] * w_array[j][i-1] ) / d[j][i].x // Fluxes
                    + ( U_array[j+1][i][1] * w_array[j+1][i]
                    - U_array[j-1][i][1] * w_array[j-1][i] ) / d[j][i].y )
                    + 0.25 * ( w_array[j][i+1] + w_array[j][i-1] + w_array[j+1][i]  + w_array[j-1][i] );
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc, w_local, &w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  new_w, &new_w_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da,  U_local, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_sc,  c_F, &c_F_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &w_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
