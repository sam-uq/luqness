/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file stab_vort_conv.h
//! Implements the nonlinear term

#include "./vort_conv.h"

//!
//! \brief The StabVortConv class implements a linearized conservative convection term.
//!
class StabVortConv : public  VortConv {
public:
  //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
  StabVortConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
    : VortConv(p_opt, p_dom, p_mod, p_cm) { }
  //! Free some space
  virtual ~StabVortConv() {};

  //!
  PetscErrorCode conv(Vec U, Vec w, Vec w_out);

  //! Allocate space for the matrix
  PetscErrorCode preallocate();

  //! Allocate space for the matrix
  PetscErrorCode fill(Vec U);

  //! Convection matrix used to solve the linear system, U is the advection speed
  Mat                   conv_mat;
};

