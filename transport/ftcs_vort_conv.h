/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef FTCS_VORT_CONV_H
#define FTCS_VORT_CONV_H

//! @file ftcs_vort_conv.h
//! Implements the nonlinear term for vortex methods as provided by Tadmor

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/domain.h"
#include "../tools/profiler.h"
#include "../models/model_base.h"

#include "vort_conv.h"

//! The non-linear term for a vortex method (1st order)
//!
class FTCSVortConv : public VortConv
{
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    FTCSVortConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
        : VortConv(p_opt, p_dom, p_mod, p_cm) { }
    //! Free some space
    virtual ~FTCSVortConv() {};

    //! Lax-Friedrichs scheme: a first order FTCS scheme
    PetscErrorCode conv(Vec w, Vec U, Vec c_F, Vec new_w);

protected:

};

#endif // FTCS_VORT_CONV_H
