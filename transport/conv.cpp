/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "conv.h"

PetscErrorCode Conv::mc_diff(Vec U_local, Vec U_x, Vec U_y, Vec U_z) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal        L, R, C;

    if( p_opt_->enable_3d ) {
        Field3D         ***U_array, ***U_x_array, ***U_y_array, ***U_z_array;
        //// First limit/*ed central difference, will be limited (MC) (without dx and dy division)
        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_z, &U_z_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    // diff U in y dir
                    // x comp
                    L = U_array[k][j][i][0] - U_array[k][j][i-1][0];
                    R = U_array[k][j][i+1][0] - U_array[k][j][i][0];
                    C = ( U_array[k][j][i+1][0] - U_array[k][j][i-1][0] ) * 0.5;
                    U_x_array[k][j][i][0] = MC(L, R, C);
                    // y comp
                    L = U_array[k][j][i][1] - U_array[k][j][i-1][1];
                    R = U_array[k][j][i+1][1] - U_array[k][j][i][1];
                    C = ( U_array[k][j][i+1][1] - U_array[k][j][i-1][1] ) * 0.5;
                    U_x_array[k][j][i][1] = MC(L, R, C);
                    // y comp
                    L = U_array[k][j][i][2] - U_array[k][j][i-1][2];
                    R = U_array[k][j][i+1][2] - U_array[k][j][i][2];
                    C = ( U_array[k][j][i+1][2] - U_array[k][j][i-1][2] ) * 0.5;
                    U_x_array[k][j][i][2] = MC(L, R, C);

                    // diff U in y dir
                    // x comp
                    L = U_array[k][j][i][0] - U_array[k][j-1][i][0];
                    R = U_array[k][j+1][i][0] - U_array[k][j][i][0];
                    C = ( U_array[k][j+1][i][0] - U_array[k][j-1][i][0] ) * 0.5;
                    U_y_array[k][j][i][0] = MC(L, R, C);
                    // y comp
                    L = U_array[k][j][i][1] - U_array[k][j-1][i][1];
                    R = U_array[k][j+1][i][1] - U_array[k][j][i][1];
                    C = ( U_array[k][j+1][i][1] - U_array[k][j-1][i][1] ) * 0.5;
                    U_y_array[k][j][i][1] = MC(L, R, C);
                    // z comp
                    L = U_array[k][j][i][2] - U_array[k][j-1][i][2];
                    R = U_array[k][j+1][i][2] - U_array[k][j][i][2];
                    C = ( U_array[k][j+1][i][2] - U_array[k][j-1][i][2] ) * 0.5;
                    U_y_array[k][j][i][2] = MC(L, R, C);

                    // diff U in z dir
                    // x comp
                    L = U_array[k][j][i][0] - U_array[k-1][j][i][0];
                    R = U_array[k+1][j][i][0] - U_array[k][j][i][0];
                    C = ( U_array[k+1][j][i][0] - U_array[k-1][j][i][0] ) * 0.5;
                    U_z_array[k][j][i][0] = MC(L, R, C);
                    // y comp
                    L = U_array[k][j][i][1] - U_array[k-1][j][i][1];
                    R = U_array[k+1][j][i][1] - U_array[k][j][i][1];
                    C = ( U_array[k+1][j][i][1] - U_array[k-1][j][i][1] ) * 0.5;
                    U_z_array[k][j][i][1] = MC(L, R, C);
                    // z comp
                    L = U_array[k][j][i][2] - U_array[k-1][j][i][2];
                    R = U_array[k+1][j][i][2] - U_array[k][j][i][2];
                    C = ( U_array[k+1][j][i][2] - U_array[k-1][j][i][2] ) * 0.5;
                    U_z_array[k][j][i][2] = MC(L, R, C);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_z, &U_z_array); CHKERRQ(ierr);
    } else {
        Field2D         **U_array, **U_x_array, **U_y_array;
        //// First limit/*ed central difference, will be limited (MC) (without dx and dy division)
        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                L = U_array[j][i][0] - U_array[j][i-1][0];
                R = U_array[j][i+1][0] - U_array[j][i][0];
                C = ( U_array[j][i+1][0] - U_array[j][i-1][0] ) * 0.5;
                U_x_array[j][i][0] = MC(L, R, C);

                L = U_array[j][i][1] - U_array[j][i-1][1];
                R = U_array[j][i+1][1] - U_array[j][i][1];
                C = ( U_array[j][i+1][1] - U_array[j][i-1][1] ) * 0.5;
                U_x_array[j][i][1] = MC(L, R, C);

                L = U_array[j][i][0] - U_array[j-1][i][0];
                R = U_array[j+1][i][0] - U_array[j][i][0];
                C = ( U_array[j+1][i][0] - U_array[j-1][i][0] ) * 0.5;
                U_y_array[j][i][0] = MC(L, R, C);

                L = U_array[j][i][1] - U_array[j-1][i][1];
                R = U_array[j+1][i][1] - U_array[j][i][1];
                C = ( U_array[j+1][i][1] - U_array[j-1][i][1] ) * 0.5;
                U_y_array[j][i][1] = MC(L, R, C);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}
