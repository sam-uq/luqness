/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "bcg_conv.h"

//! Read from U and U_xand U_y and compute the transverse derivative U_no_x and U_no_y
//!
PetscErrorCode BCGConv::trv_diff(Vec U_local, Vec U_x, Vec U_y, Vec U_z,
                                 Vec U_no_x, Vec U_no_y, Vec U_no_z) {
    FUNCTIONBEGIN(p_cm_);

    PetscReal        C;
    Vec      U_x_local, U_y_local, U_z_local;

    ierr = DMGetLocalVector(p_dom_->da, &U_x_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_y_local); CHKERRQ(ierr);

    // Send data from boundaries and other processes to currenct process (nned to update stencil)
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_x, INSERT_VALUES, U_x_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_x, INSERT_VALUES, U_x_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_y, INSERT_VALUES, U_y_local); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) {

        Field3D         ***U_array, ***U_x_array, ***U_y_array, ***U_z_array;
        Field3D         ***U_no_x_array, ***U_no_y_array, ***U_no_z_array;
        Volume3D        ***d, ***df, ***db;

        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_x_local, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Db, &db); CHKERRQ(ierr);

        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    if ( !(p_dom_->bd_fam.U.left() != BCType::Periodic && p_dom_->at_border(i, PosType::Left)) &&
                        U_array[k][j][i][0] >= 0) {
                        C = 0.5 * (1. - p_mod_->dt / d[k][j][i].x * U_array[k][j][i][0]);
                    U_no_x_array[k][j][i][0] = ( U_array[k][j][i][0] - U_array[k][j][i-1][0]
                    + C * ( U_x_array[k][j][i][0] - U_x_array[k][j][i-1][0] ) ) / db[k][j][i].x;
                    U_no_x_array[k][j][i][1] = ( U_array[k][j][i][1] - U_array[k][j][i-1][1]
                    + C * ( U_x_array[k][j][i][1] - U_x_array[k][j][i-1][1] ) ) / db[k][j][i].x;
                    U_no_x_array[k][j][i][2] = ( U_array[k][j][i][2] - U_array[k][j][i-1][2]
                    + C * ( U_x_array[k][j][i][2] - U_x_array[k][j][i-1][2] ) ) / db[k][j][i].x;
                        } else {
                            C = 0.5 * (1. + p_mod_->dt / d[k][j][i].x * U_array[k][j][i][0]);
                            U_no_x_array[k][j][i][0] = ( U_array[k][j][i+1][0] - U_array[k][j][i][0]
                            - C * ( U_x_array[k][j][i+1][0] - U_x_array[k][j][i][0] ) ) / df[k][j][i].x;
                            U_no_x_array[k][j][i][1] = ( U_array[k][j][i+1][1] - U_array[k][j][i][1]
                            - C * ( U_x_array[k][j][i+1][1] - U_x_array[k][j][i][1] ) ) / df[k][j][i].x;
                            U_no_x_array[k][j][i][2] = ( U_array[k][j][i+1][2] - U_array[k][j][i][2]
                            - C * ( U_x_array[k][j][i+1][2] - U_x_array[k][j][i][2] ) ) / df[k][j][i].x;
                        }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x_local, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_x_local); CHKERRQ(ierr);
        ierr = DMGetLocalVector(p_dom_->da, &U_z_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_y, INSERT_VALUES, U_y_local); CHKERRQ(ierr);
        ierr = DMGlobalToLocalBegin(p_dom_->da, U_z, INSERT_VALUES, U_z_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_y_local, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    if ( !(p_dom_->bd_fam.U.bottom() != BCType::Periodic &&
                        p_dom_->at_border(j, PosType::Bottom)) && U_array[k][j][i][1] >= 0) {
                        C = 0.5 * (1. - p_mod_->dt / d[k][j][i].y * U_array[k][j][i][1]);
                    U_no_y_array[k][j][i][0] = ( U_array[k][j][i][0] - U_array[k][j-1][i][0]
                    + C * ( U_y_array[k][j][i][0] - U_y_array[k][j-1][i][0] ) ) / db[k][j][i].y;
                    U_no_y_array[k][j][i][1] = ( U_array[k][j][i][1] - U_array[k][j-1][i][1]
                    + C * ( U_y_array[k][j][i][1] - U_y_array[k][j-1][i][1] ) ) / db[k][j][i].y;
                    U_no_y_array[k][j][i][2] = ( U_array[k][j][i][2] - U_array[k][j-1][i][2]
                    + C * ( U_y_array[k][j][i][2] - U_y_array[k][j-1][i][2] ) ) / db[k][j][i].y;
                        } else {
                            C = 0.5 * (1. + p_mod_->dt / d[k][j][i].y * U_array[k][j][i][1]);
                            U_no_y_array[k][j][i][0] = ( U_array[k][j+1][i][0] - U_array[k][j][i][0]
                            - C * ( U_y_array[k][j+1][i][0] - U_y_array[k][j][i][0] ) ) / df[k][j][i].y;
                            U_no_y_array[k][j][i][1] = ( U_array[k][j+1][i][1] - U_array[k][j][i][1]
                            - C * ( U_y_array[k][j+1][i][1] - U_y_array[k][j][i][1] ) ) / df[k][j][i].y;
                            U_no_y_array[k][j][i][2] = ( U_array[k][j+1][i][2] - U_array[k][j][i][2]
                            - C * ( U_y_array[k][j+1][i][2] - U_y_array[k][j][i][2] ) ) / df[k][j][i].y;
                        }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y_local, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_y_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_z, INSERT_VALUES, U_z_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_z_local, &U_z_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_z, &U_no_z_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    if ( !(p_dom_->bd_fam.U.down() != BCType::Periodic &&
                        p_dom_->at_border(k, PosType::Down)) && U_array[k][j][i][2] >= 0) {
                        C = 0.5 * (1. - p_mod_->dt / db[k][j][i].z * U_array[k][j][i][2]);
                    U_no_z_array[k][j][i][0] = ( U_array[k][j][i][0] - U_array[k-1][j][i][0]
                    + C * ( U_z_array[k][j][i][0] - U_z_array[k-1][j][i][0] ) ) / db[k][j][i].z;
                    U_no_z_array[k][j][i][1] = ( U_array[k][j][i][1] - U_array[k-1][j][i][1]
                    + C * (U_z_array[k][j][i][1] - U_z_array[k-1][j][i][1] ) ) / db[k][j][i].z;
                    U_no_z_array[k][j][i][2] = ( U_array[k][j][i][2] - U_array[k-1][j][i][2]
                    + C * (U_z_array[k][j][i][2] - U_z_array[k-1][j][i][2] ) ) / db[k][j][i].z;
                        } else {
                            C = 0.5 * (1. + p_mod_->dt / d[k][j][i].z * U_array[k][j][i][2]);
                            U_no_z_array[k][j][i][0] = ( U_array[k+1][j][i][0] - U_array[k][j][i][0]
                            - C * (U_z_array[k+1][j][i][0] - U_z_array[k][j][i][0] ) ) / df[k][j][i].z;
                            U_no_z_array[k][j][i][1] = ( U_array[k+1][j][i][1] - U_array[k][j][i][1]
                            - C * (U_z_array[k+1][j][i][1] - U_z_array[k][j][i][1] ) ) / df[k][j][i].z;
                            U_no_z_array[k][j][i][2] = ( U_array[k+1][j][i][2] - U_array[k][j][i][2]
                            - C * (U_z_array[k+1][j][i][2] - U_z_array[k][j][i][2] ) ) / df[k][j][i].z;
                        }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Db, &db); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_z_local, &U_z_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_z, &U_no_z_array); CHKERRQ(ierr);

        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_z_local); CHKERRQ(ierr);
    } else { // 2D case
        Field2D         **U_array, **U_x_array, **U_y_array, **U_no_x_array, **U_no_y_array;
        Volume2D        **d, **df, **db;

        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_x_local, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Db, &db); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                if ( !(p_dom_->bd_fam.U.left() != BCType::Periodic
                    && p_dom_->at_border(i, PosType::Left)) && U_array[j][i][0] >= 0) {
                    C = 0.5 * (1. - p_mod_->dt / d[j][i].x * U_array[j][i][0]);
                U_no_x_array[j][i][0] = ( U_array[j][i][0] - U_array[j][i-1][0]
                + C * ( U_x_array[j][i][0] - U_x_array[j][i-1][0] ) ) / db[j][i].x;
                U_no_x_array[j][i][1] = ( U_array[j][i][1] - U_array[j][i-1][1]
                + C * ( U_x_array[j][i][1] - U_x_array[j][i-1][1] ) ) / db[j][i].x;
                    } else {
                        C = 0.5 * (1. + p_mod_->dt / d[j][i].x * U_array[j][i][0]);
                        U_no_x_array[j][i][0] = ( U_array[j][i+1][0] - U_array[j][i][0]
                        - C * ( U_x_array[j][i+1][0] - U_x_array[j][i][0] ) ) / df[j][i].x;
                        U_no_x_array[j][i][1] = ( U_array[j][i+1][1] - U_array[j][i][1]
                        - C * ( U_x_array[j][i+1][1] - U_x_array[j][i][1] ) ) / df[j][i].x;
                    }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x_local, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_x_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_y, INSERT_VALUES, U_y_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_y_local, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                if ( !(p_dom_->bd_fam.U.bottom() != BCType::Periodic &&
                    p_dom_->at_border(j, PosType::Bottom)) && U_array[j][i][1] >= 0) {
                    C = 0.5 * (1. - p_mod_->dt / d[j][i].y * U_array[j][i][1]);
                U_no_y_array[j][i][0] = ( U_array[j][i][0] - U_array[j-1][i][0]
                + C * ( U_y_array[j][i][0] - U_y_array[j-1][i][0] ) ) / db[j][i].y;
                U_no_y_array[j][i][1] = ( U_array[j][i][1] - U_array[j-1][i][1]
                + C * ( U_y_array[j][i][1] - U_y_array[j-1][i][1] ) ) / db[j][i].y;
                    } else {
                        C = 0.5 * (1. + p_mod_->dt / d[j][i].y * U_array[j][i][1]);
                        U_no_y_array[j][i][0] = ( U_array[j+1][i][0] - U_array[j][i][0]
                        - C * ( U_y_array[j+1][i][0] - U_y_array[j][i][0] ) ) / df[j][i].y;
                        U_no_y_array[j][i][1] = ( U_array[j+1][i][1] - U_array[j][i][1]
                        - C * ( U_y_array[j+1][i][1] - U_y_array[j][i][1] ) ) / df[j][i].y;
                    }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Db, &db); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y_local, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_y_local); CHKERRQ(ierr);

        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_array); CHKERRQ(ierr);

    }

    PetscFunctionReturn(ierr);
}

//! Read from U, G U_x, U-y, U_no_y, U_no_y and write to U_edg_* the interpolation at the edges
//!
PetscErrorCode BCGConv::interp(Vec U, Vec G, Vec F, Vec D,
                               Vec, Vec U_x, Vec U_y, Vec U_z,
                               Vec U_no_x, Vec U_no_y, Vec U_no_z,
                               Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T,
                               Vec U_edg_D, Vec U_edg_U) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->enable_3d ) { // 3D Case
        // Forcing, gradient part and duffision (componentwise)
        VecAXPBYPCZ(D, 1., -1., -0., G, F);

        // Save a copy of derivatives
        ierr = VecCopy(U_x, U_edg_L); CHKERRQ(ierr); // Save a copy for later use
        ierr = VecCopy(U_x, U_edg_R); CHKERRQ(ierr);
        ierr = VecCopy(U_y, U_edg_B); CHKERRQ(ierr);
        ierr = VecCopy(U_y, U_edg_T); CHKERRQ(ierr);
        ierr = VecCopy(U_z, U_edg_D); CHKERRQ(ierr);
        ierr = VecCopy(U_z, U_edg_U); CHKERRQ(ierr);

        // Calculate nonlinear terms
        Field3D           ***U_x_array, ***U_y_array, ***U_z_array;
        Field3D           ***U_no_x_array, ***U_no_y_array, ***U_no_z_array, ***U_array;
        Volume3D ***d;

        ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_z, &U_z_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_z, &U_no_z_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    // Calculate nonlinear terms
                    U_x_array[k][j][i][0] *= U_array[k][j][i][0] / d[k][j][i].x;
                    U_x_array[k][j][i][1] *= U_array[k][j][i][0] / d[k][j][i].x;
                    U_x_array[k][j][i][2] *= U_array[k][j][i][0] / d[k][j][i].x;
                    U_y_array[k][j][i][0] *= U_array[k][j][i][1] / d[k][j][i].y;
                    U_y_array[k][j][i][1] *= U_array[k][j][i][1] / d[k][j][i].y;
                    U_y_array[k][j][i][2] *= U_array[k][j][i][1] / d[k][j][i].y;
                    U_z_array[k][j][i][0] *= U_array[k][j][i][2] / d[k][j][i].z;
                    U_z_array[k][j][i][1] *= U_array[k][j][i][2] / d[k][j][i].z;
                    U_z_array[k][j][i][2] *= U_array[k][j][i][2] / d[k][j][i].z;
                    // NORMAL DERIVATIVES
                    U_no_x_array[k][j][i][0] *= U_array[k][j][i][0];
                    U_no_x_array[k][j][i][1] *= U_array[k][j][i][0];
                    U_no_x_array[k][j][i][2] *= U_array[k][j][i][0];
                    U_no_y_array[k][j][i][0] *= U_array[k][j][i][1];
                    U_no_y_array[k][j][i][1] *= U_array[k][j][i][1];
                    U_no_y_array[k][j][i][2] *= U_array[k][j][i][1];
                    U_no_z_array[k][j][i][0] *= U_array[k][j][i][2];
                    U_no_z_array[k][j][i][1] *= U_array[k][j][i][2];
                    U_no_z_array[k][j][i][2] *= U_array[k][j][i][2];
                }
            }
        }
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_z, &U_z_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_z, &U_no_z_array); CHKERRQ(ierr);

        // TODO REPLACE AL DXI DZI etc...
        // PosType::Left AND PosType::Right STATES
        // Left/right edges (x comp)
        VecAXPBYPCZ(U_no_x, 1., 1., 1., U_y, U_z); // contains the sum of nonlinear term
        VecAXPBYPCZ(U_no_x, 1., - 0.5 * p_mod_->dt, - 0.5 * p_mod_->dt, U, D);
        // add diffusion interpolation, forcing etc
        VecAXPBY(U_edg_L, 1, + 0.5 , U_no_x); // Left state
        VecAXPBY(U_edg_R, 1, - 0.5 , U_no_x); // Right state

        // PosType::Bottom AND PosType::Top STATES
        // Bottom/top edges (x comp)
        VecAXPBYPCZ(U_no_y, 1., 1., 1., U_x, U_z); // contains the sum of nonlinear term
        VecAXPBYPCZ(U_no_y, 1., - 0.5 * p_mod_->dt, - 0.5 * p_mod_->dt, U, D);
        // add diffusion interpolation, forcing etc
        VecAXPBY(U_edg_B, 1, + 0.5 , U_no_y); // Left state
        VecAXPBY(U_edg_T, 1, - 0.5 , U_no_y); // Right state

        // PosType::Down AND PosType::Up STATES
        // Down/up edges (x comp)
        VecAXPBYPCZ(U_no_z, 1., 1., 1., U_x, U_y); // contains the sum of nonlinear term
        VecAXPBYPCZ(U_no_z, 1., - 0.5 * p_mod_->dt, - 0.5 * p_mod_->dt, U, D);
        // add diffusion interpolation, forcing etc
        VecAXPBY(U_edg_D, 1, + 0.5 , U_no_z); // Left state
        VecAXPBY(U_edg_U, 1, - 0.5 , U_no_z); // Right state
    } else { // 2D case
        Volume2D **d;
        // Forcing, gradient part and duffision (componentwise)
        VecAXPBYPCZ(D, 1., -1., -0., G, F);

        // Save a copy of derivatives
        ierr = VecCopy(U_x, U_edg_L); CHKERRQ(ierr); // Save a copy for later use
        ierr = VecCopy(U_x, U_edg_R); CHKERRQ(ierr);
        ierr = VecCopy(U_y, U_edg_B); CHKERRQ(ierr);
        ierr = VecCopy(U_y, U_edg_T); CHKERRQ(ierr);

        // Calculate nonlinear terms
        Field2D       **U_x_array, **U_y_array, **U_no_x_array, **U_no_y_array, **U_array;
        ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                // Calculate nonlinear terms
                U_x_array[j][i][0] *= U_array[j][i][0] / d[j][i].x;
                U_x_array[j][i][1] *= U_array[j][i][0] / d[j][i].x;
                U_y_array[j][i][0] *= U_array[j][i][1] / d[j][i].y;
                U_y_array[j][i][1] *= U_array[j][i][1] / d[j][i].y;
                // NORMAL DERIVATIVES
                U_no_x_array[j][i][0] *= U_array[j][i][0];
                U_no_x_array[j][i][1] *= U_array[j][i][0];
                U_no_y_array[j][i][0] *= U_array[j][i][1];
                U_no_y_array[j][i][1] *= U_array[j][i][1];
            }
        }
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);

//         ierr = DMGetGlobalVector(p_dom_->da, &U_x_tmp); CHKERRQ(ierr);
//         ierr = DMGetGlobalVector(p_dom_->da, &U_x_tmp); CHKERRQ(ierr);
//
//         ierr = DMDAVecGetArray(p_dom_->da, U_x_tmp, &U_x_tmp_array); CHKERRQ(ierr);
//         ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
//         ierr = DMDAVecGetArray(p_dom_->da, U_y_tmp, &U_y_tmp_array); CHKERRQ(ierr);
//         ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_x_array); CHKERRQ(ierr);
//         for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
//             for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
//                 // Calculate nonlinear terms
//                 U_x_tmp_array[j][i] = U_x_array[j][i] / p_dom_->dxi;
//                 U_y_tmp_array[j][i] = U_y_array[j][i] / p_dom_->dyi;
//             }
//         }
//         ierr = DMDAVecRestoreArray(p_dom_->da, U_x_tmp, &U_x_tmp_array); CHKERRQ(ierr);
//         ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
//         ierr = DMDAVecRestoreArray(p_dom_->da, U_y_tmp, &U_y_tmp_array); CHKERRQ(ierr);
//         ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);

        // PosType::Left AND PosType::Right STATES
        // Left/right edges (x comp)
        VecAXPBY(U_no_x, 1., 1., U_y); // contains the sum of nonlinear term
        VecAXPBYPCZ(U_no_x, 1., - 0.5 * p_mod_->dt, - 0.5 * p_mod_->dt, U, D);
        // add diffusion interpolation, forcing etc
        VecAXPBY(U_edg_L, 1, + 0.5 , U_no_x); // Left state
        VecAXPBY(U_edg_R, 1, - 0.5 , U_no_x); // Right state

        // PosType::Bottom AND PosType::Top STATES
        // Bottom/top edges (x comp)
        VecAXPBY(U_no_y, 1., 1., U_x); // contains the sum of nonlinear term
        VecAXPBYPCZ(U_no_y, 1., - 0.5 * p_mod_->dt, - 0.5 * p_mod_->dt, U, D);
        // add diffusion interpolation, forcing etc
        VecAXPBY(U_edg_B, 1, + 0.5 , U_no_y); // Left state
        VecAXPBY(U_edg_T, 1, - 0.5 , U_no_y); // Right state
    }

    PetscFunctionReturn(ierr);
}

//! Upwind the edges U_edg_* to the center U-**
//!
PetscErrorCode BCGConv::upwind(Vec U_edg_L, Vec U_edg_R,
                               Vec U_edg_B, Vec U_edg_T,
                               Vec U_edg_D, Vec U_edg_U,
                               Vec U_LR, Vec U_BT, Vec U_DU) {
    FUNCTIONBEGIN(p_cm_);

    Vec            U_edg_L_local, U_edg_R_local;
    Vec            U_edg_B_local, U_edg_T_local;
    Vec            U_edg_D_local, U_edg_U_local;

    ierr = DMGetLocalVector(p_dom_->da, &U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_T_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_L, INSERT_VALUES, U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_L, INSERT_VALUES, U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_R, INSERT_VALUES, U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_R, INSERT_VALUES, U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_B, INSERT_VALUES, U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_B, INSERT_VALUES, U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_T, INSERT_VALUES, U_edg_T_local); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) { // 3D Case
        Field3D           ***U_edg_L_array, ***U_edg_R_array, ***U_edg_B_array, ***U_edg_T_array;
        Field3D           ***U_edg_D_array, ***U_edg_U_array;
        Field3D           ***U_LR_array, ***U_BT_array, ***U_DU_array;

        ierr = DMDAVecGetArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm+1; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym+1; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm+1; ++i) {
                    if ( p_dom_->bd_fam.U.right() != BCType::Periodic && p_dom_->at_border(i-1, PosType::Right) ) {
                        U_LR_array[k][j][i][0] = U_edg_L_array[k][j][i-1][0];
                        U_LR_array[k][j][i][1] = U_edg_L_array[k][j][i-1][1];
                        U_LR_array[k][j][i][2] = U_edg_L_array[k][j][i-1][2];
                    } else if ( p_dom_->bd_fam.U.left() != BCType::Periodic && p_dom_->at_border(i, PosType::Left) ) {
                        U_LR_array[k][j][i][0] = U_edg_R_array[k][j][i][0];
                        U_LR_array[k][j][i][1] = U_edg_R_array[k][j][i][1];
                        U_LR_array[k][j][i][2] = U_edg_R_array[k][j][i][2];
                    } else {
                        if( U_edg_L_array[k][j][i-1][0] >= 0 &&
                            U_edg_L_array[k][j][i-1][0] + U_edg_R_array[k][j][i][0] >= 0 ) {
                            U_LR_array[k][j][i][0] = U_edg_L_array[k][j][i-1][0];
                            } else if( U_edg_L_array[k][j][i-1][0] < 0 && U_edg_R_array[k][j][i][0] > 0 ) {
                                U_LR_array[k][j][i][0] = 0;
                            } else {
                                U_LR_array[k][j][i][0] = U_edg_R_array[k][j][i][0];
                            }

                            if( U_LR_array[k][j][i][0] > 0) {
                                U_LR_array[k][j][i][1] = U_edg_L_array[k][j][i-1][1];
                                U_LR_array[k][j][i][2] = U_edg_L_array[k][j][i-1][2];
                            } else if( U_LR_array[k][j][i][0] < 0) {
                                U_LR_array[k][j][i][1] = U_edg_R_array[k][j][i][1];
                                U_LR_array[k][j][i][2] = U_edg_R_array[k][j][i][2];
                            } else {
                                U_LR_array[k][j][i][1] = 0.5 * ( U_edg_L_array[k][j][i-1][1] + U_edg_R_array[k][j][i][1]
                                );
                                U_LR_array[k][j][i][2] = 0.5 * ( U_edg_L_array[k][j][i-1][2] + U_edg_R_array[k][j][i][2]
                                );
                            }
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_T, INSERT_VALUES, U_edg_T_local); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_L_local); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_R_local); CHKERRQ(ierr);

        ierr = DMGetLocalVector(p_dom_->da, &U_edg_D_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_D, INSERT_VALUES, U_edg_D_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_edg_T_local, &U_edg_T_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm+1; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym+1; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm+1; ++i) {
                    if ( p_dom_->bd_fam.U.top() != BCType::Periodic && p_dom_->at_border(j-1, PosType::Top) ) {
                        U_BT_array[k][j][i][0] = U_edg_B_array[k][j-1][i][0];
                        U_BT_array[k][j][i][1] = U_edg_B_array[k][j-1][i][1];
                        U_BT_array[k][j][i][2] = U_edg_B_array[k][j-1][i][2];
                    } else if ( p_dom_->bd_fam.U.bottom() != BCType::Periodic && p_dom_->at_border(j, PosType::Bottom) )
                    {
                        U_BT_array[k][j][i][0] = U_edg_T_array[k][j][i][0];
                        U_BT_array[k][j][i][1] = U_edg_T_array[k][j][i][1];
                        U_BT_array[k][j][i][2] = U_edg_T_array[k][j][i][2];
                    } else {
                        if( U_edg_B_array[k][j-1][i][1] >= 0 &&
                            U_edg_B_array[k][j-1][i][1] + U_edg_T_array[k][j][i][1] >= 0 ) {
                            U_BT_array[k][j][i][1] = U_edg_B_array[k][j-1][i][1];
                            } else if( U_edg_B_array[k][j-1][i][1] < 0 && U_edg_T_array[k][j][i][1] > 0 ) {
                                U_BT_array[k][j][i][1] = 0;
                            } else {
                                U_BT_array[k][j][i][1] = U_edg_T_array[k][j][i][1];
                            }
                            if( U_BT_array[k][j][i][1] > 0 ) {
                                U_BT_array[k][j][i][0] = U_edg_B_array[k][j-1][i][0];
                                U_BT_array[k][j][i][2] = U_edg_B_array[k][j-1][i][2];
                            } else if( U_BT_array[k][j][i][1] < 0 ) {
                                U_BT_array[k][j][i][0] = U_edg_T_array[k][j][i][0];
                                U_BT_array[k][j][i][2] = U_edg_T_array[k][j][i][2];
                            } else {
                                U_BT_array[k][j][i][0] = 0.5 * ( U_edg_B_array[k][j-1][i][0] +
                                U_edg_T_array[k][j][i][0] );
                                U_BT_array[k][j][i][2] = 0.5 * ( U_edg_B_array[k][j-1][i][2] +
                                U_edg_T_array[k][j][i][2] );
                            }
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B_local, &U_edg_T_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_B_local); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_T_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_D, INSERT_VALUES, U_edg_D_local); CHKERRQ(ierr);

        ierr = DMGetLocalVector(p_dom_->da, &U_edg_U_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_U, INSERT_VALUES, U_edg_U_local); CHKERRQ(ierr);
        ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_U, INSERT_VALUES, U_edg_U_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_edg_D_local, &U_edg_D_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_edg_U_local, &U_edg_U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_DU, &U_DU_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm+1; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym+1; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm+1; ++i) {
                    if ( p_dom_->bd_fam.U.up() != BCType::Periodic && p_dom_->at_border(k-1, PosType::Up) ) {
                        U_DU_array[k][j][i][0] = U_edg_D_array[k-1][j][i][0];
                        U_DU_array[k][j][i][1] = U_edg_D_array[k-1][j][i][1];
                        U_DU_array[k][j][i][2] = U_edg_D_array[k-1][j][i][2];
                    } else if ( p_dom_->bd_fam.U.down() != BCType::Periodic && p_dom_->at_border(k, PosType::Down) ) {
                        U_DU_array[k][j][i][0] = U_edg_U_array[k][j][i][0];
                        U_DU_array[k][j][i][1] = U_edg_U_array[k][j][i][1];
                        U_DU_array[k][j][i][2] = U_edg_U_array[k][j][i][2];
                    } else {
                        if( U_edg_D_array[k-1][j][i][2] >= 0 &&
                            U_edg_D_array[k-1][j][i][2] + U_edg_U_array[k][j][i][2] >= 0 ) {
                            U_DU_array[k][j][i][2] = U_edg_D_array[k-1][j][i][2];
                            } else if( U_edg_D_array[k-1][j][i][2] < 0 && U_edg_U_array[k][j][i][2] > 0 ) {
                                U_DU_array[k][j][i][2] = 0;
                            } else {
                                U_DU_array[k][j][i][2] = U_edg_U_array[k][j][i][2];
                            }
                            if( U_DU_array[k][j][i][2] > 0 ) {
                                U_DU_array[k][j][i][0] = U_edg_D_array[k-1][j][i][0];
                                U_DU_array[k][j][i][1] = U_edg_D_array[k-1][j][i][1];
                            } else if( U_DU_array[k][j][i][2] < 0 ) {
                                U_DU_array[k][j][i][0] = U_edg_U_array[k][j][i][0];
                                U_DU_array[k][j][i][1] = U_edg_U_array[k][j][i][1];
                            } else {
                                U_DU_array[k][j][i][0] = 0.5 * ( U_edg_D_array[k-1][j][i][0] +
                                U_edg_U_array[k][j][i][0] );
                                U_DU_array[k][j][i][1] = 0.5 * ( U_edg_D_array[k-1][j][i][1] +
                                U_edg_U_array[k][j][i][1] );
                            }
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_D_local, &U_edg_D_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_U_local, &U_edg_U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_DU, &U_DU_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_U_local); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_D_local); CHKERRQ(ierr);
    } else { // 2D CASE
        Field2D         **U_edg_L_array, **U_edg_R_array, **U_edg_B_array, **U_edg_T_array;
        Field2D         **U_LR_array, **U_BT_array;

        ierr = DMDAVecGetArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym+1; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm+1; ++i) {
                if ( p_dom_->bd_fam.U.right() != BCType::Periodic && p_dom_->at_border(i-1, PosType::Right) ) {
                    U_LR_array[j][i][0] = U_edg_L_array[j][i-1][0];
                    U_LR_array[j][i][1] = U_edg_L_array[j][i-1][1];
                } else if ( p_dom_->bd_fam.U.left() != BCType::Periodic && p_dom_->at_border(i, PosType::Left) ) {
                    U_LR_array[j][i][0] = U_edg_R_array[j][i][0];
                    U_LR_array[j][i][1] = U_edg_R_array[j][i][1];
                } else {
                    if( U_edg_L_array[j][i-1][0] >= 0 && U_edg_L_array[j][i-1][0] + U_edg_R_array[j][i][0] >= 0 ) {
                        U_LR_array[j][i][0] = U_edg_L_array[j][i-1][0];
                    } else if( U_edg_L_array[j][i-1][0] < 0 && U_edg_R_array[j][i][0] > 0 ) {
                        U_LR_array[j][i][0] = 0;
                    } else {
                        U_LR_array[j][i][0] = U_edg_R_array[j][i][0];
                    }

                    if( U_LR_array[j][i][0] > 0) {
                        U_LR_array[j][i][1] = U_edg_L_array[j][i-1][1];
                    } else if( U_LR_array[j][i][0] < 0) {
                        U_LR_array[j][i][1] = U_edg_R_array[j][i][1];
                    } else {
                        U_LR_array[j][i][1] = 0.5 * ( U_edg_L_array[j][i-1][1] + U_edg_R_array[j][i][1] );
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);

        ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_T, INSERT_VALUES, U_edg_T_local); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_L_local); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_R_local); CHKERRQ(ierr);

        ierr = DMDAVecGetArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_edg_T_local, &U_edg_T_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym+1; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm+1; ++i) {
                if ( p_dom_->bd_fam.U.top() != BCType::Periodic && p_dom_->at_border(j-1, PosType::Top) ) {
                    U_BT_array[j][i][1] = U_edg_B_array[j-1][i][1];
                    U_BT_array[j][i][0] = U_edg_B_array[j-1][i][0];
                } else if ( p_dom_->bd_fam.U.bottom() != BCType::Periodic && p_dom_->at_border(j, PosType::Bottom) ) {
                    U_BT_array[j][i][1] = U_edg_T_array[j][i][1];
                    U_BT_array[j][i][0] = U_edg_T_array[j][i][0];
                } else {
                    if( U_edg_B_array[j-1][i][1] >= 0 && U_edg_B_array[j-1][i][1] + U_edg_T_array[j][i][1] >= 0 ) {
                        U_BT_array[j][i][1] = U_edg_B_array[j-1][i][1];
                    } else if( U_edg_B_array[j-1][i][1] < 0 && U_edg_T_array[j][i][1] > 0 ) {
                        U_BT_array[j][i][1] = 0;
                    } else {
                        U_BT_array[j][i][1] = U_edg_T_array[j][i][1];
                    }
                    if( U_BT_array[j][i][1] > 0 ) {
                        U_BT_array[j][i][0] = U_edg_B_array[j-1][i][0];
                    } else if( U_BT_array[j][i][1] < 0 ) {
                        U_BT_array[j][i][0] = U_edg_T_array[j][i][0];
                    } else {
                        U_BT_array[j][i][0] = 0.5 * ( U_edg_B_array[j-1][i][0] +  U_edg_T_array[j][i][0] );
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_T_local, &U_edg_T_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);

        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_B_local); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_T_local); CHKERRQ(ierr);
    }
    PetscFunctionReturn(ierr);
}

//! Read from P_local and write to P_global the convection of P
//!
PetscErrorCode BCGConv::conv(Vec U, Vec G, Vec F, Vec Lpl, Vec U_conv) {
    // Stage log for convection BCG
    //     PetscLogStagePush(stage);
    FUNCTIONBEGIN(p_cm_);

    PetscReal           LR, BT, DU;

    Vec      U_x, U_y, U_z, D;
    Vec      U_no_x, U_no_y, U_no_z;
    Vec      U_edg_L, U_edg_R, U_edg_B, U_edg_T;
    Vec      U_edg_D, U_edg_U;
    Vec      U_local;
    Vec      U_LR_local, U_BT_local, U_DU_local;

    ierr = DMGetGlobalVector(p_dom_->da, &D); CHKERRQ(ierr);

    ierr = VecSet(D, 0); CHKERRQ(ierr);

    ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    // We localize U to get access to the latest boundary informations
    ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);

    // ...and update boundarries
    ierr = p_mod_->update_bd_field(U_local, p_dom_->bd_fam.U, p_mod_->t); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMGetGlobalVector(p_dom_->da, &U_z); CHKERRQ(ierr);
    }

    // MC
    mc_diff(U_local, U_x, U_y, U_z);

    ierr = DMGetGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMGetGlobalVector(p_dom_->da, &U_no_z); CHKERRQ(ierr);
    }

    // Tr dv
    trv_diff(U_local, U_x, U_y, U_z, U_no_x, U_no_y, U_no_z);

    ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMGetGlobalVector(p_dom_->da, &U_edg_D); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_dom_->da, &U_edg_U); CHKERRQ(ierr);
    }

    // Interpolate to edge, to next time step
    interp(U, G, F, D, Lpl, U_x, U_y, U_z, U_no_x, U_no_y, U_no_z,
           U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_edg_D, U_edg_U);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMRestoreGlobalVector(p_dom_->da, &U_z); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_z); CHKERRQ(ierr);
    }

    ierr = DMRestoreGlobalVector(p_dom_->da, &D); CHKERRQ(ierr);

    ierr = DMGetLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMGetLocalVector(p_dom_->da, &U_DU_local); CHKERRQ(ierr);
    }

    // Upwind data
    upwind(U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_edg_D, U_edg_U, U_LR_local, U_BT_local, U_DU_local);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_D); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_U); CHKERRQ(ierr);
    }

    // Compute convective derivative
    if( p_opt_->enable_3d ) { // 3D Case
        Field3D       ***U_LR_array, ***U_BT_array, ***U_DU_array, ***U_conv_array;
        Volume3D        ***d;
        ierr = DMDAVecGetArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_DU_local, &U_DU_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    LR = (U_LR_array[k][j][i+1][0] + U_LR_array[k][j][i][0]) / d[k][j][i].x;
                    BT = (U_BT_array[k][j+1][i][1] + U_BT_array[k][j][i][1]) / d[k][j][i].y;
                    DU = (U_DU_array[k+1][j][i][2] + U_DU_array[k][j][i][2]) / d[k][j][i].z;
                    U_conv_array[k][j][i][0] = 0.5 * (LR * (U_LR_array[k][j][i+1][0] - U_LR_array[k][j][i][0])
                    + BT * (U_BT_array[k][j+1][i][0] - U_BT_array[k][j][i][0])
                    + DU * (U_DU_array[k+1][j][i][0] - U_DU_array[k][j][i][0]));
                    U_conv_array[k][j][i][1] = 0.5 * (LR * (U_LR_array[k][j][i+1][1] - U_LR_array[k][j][i][1])
                    + BT * (U_BT_array[k][j+1][i][1] - U_BT_array[k][j][i][1])
                    + DU * (U_DU_array[k+1][j][i][1] - U_DU_array[k][j][i][1]));
                    U_conv_array[k][j][i][2] = 0.5 * (LR * (U_LR_array[k][j][i+1][2] - U_LR_array[k][j][i][2])
                    + BT * (U_BT_array[k][j+1][i][2] - U_BT_array[k][j][i][2])
                    + DU * (U_DU_array[k+1][j][i][2] - U_DU_array[k][j][i][2]));
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_DU_local, &U_DU_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);
    } else { // 2D case
        Field2D         **U_LR_array, **U_BT_array, **U_conv_array;
        Volume2D        **d;
        ierr = DMDAVecGetArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                LR = (U_LR_array[j][i+1][0] + U_LR_array[j][i][0]) / d[j][i].x;
                BT = (U_BT_array[j+1][i][1] + U_BT_array[j][i][1]) / d[j][i].y;
                U_conv_array[j][i][0] = 0.5 * (LR * (U_LR_array[j][i+1][0] - U_LR_array[j][i][0])
                + BT * (U_BT_array[j+1][i][0] - U_BT_array[j][i][0]));
                U_conv_array[j][i][1] = 0.5 * (LR * (U_LR_array[j][i+1][1] - U_LR_array[j][i][1])
                + BT * (U_BT_array[j+1][i][1] - U_BT_array[j][i][1]));
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        ierr = DMRestoreLocalVector(p_dom_->da, &U_DU_local); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}
