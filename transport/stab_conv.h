/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file stab_conv.h
//! Implement class for Nonlinear term given by conservative fluxes
//! of \cite{ProjAMVS}.

// Internal includes
#include "conv.h"

#include "../tools/tools.h"
#include "../tools/options.h"

//! \brief Stable non-linear term for projection method.
//! Implements the "stable" convection for
//! the projection methods of \cite{ProjAMVS}.
//!
class StabConv : public Conv {
public:
    //! \brief Constructor to grab all the information required.
    //! Constructor, allocate space for temporary vectors,
    //! solution vectors and for matrices
    //!
    //! No pointer is owned, no cleanup is needed.
    StabConv(Options * p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm)
            : Conv(p_opt, p_dom, p_mod, p_cm) {}

    //! \brief Free buffers and matrix
    virtual ~StabConv() {};

    //! \brief Shall not be used with stab-conv (unimplemented).
    //!
    //! Actually computes nonlinear term (not necessary using this methods,
    //! use fill instead.
    PetscErrorCode conv(Vec, Vec, Vec, Vec, Vec) { return 0; }

    //! \brief Allocate space for the matrix.
    PetscErrorCode preallocate();

    //! \brief Build system matrix.
    //! Fill (i.e. create) the transport matrix \f$A(u)\f$ for
    //! \f$C(u,v) = A(u) * v\f$ given \f$u\f$.
    //! \param[in]   U  Lagged-velocity at \f$U^n\f$.
    PetscErrorCode fill(Vec U);

    //! \brief Matrix handle for non-linear term.
    //!
    //! Matrix \f$A(u)\f$ for the linear system arising
    //! from semi-implicit non-linear-term (transport matrix).
    //!
    //! The matrix is reused at each iteration, fill is called to
    //! set the entries of the matrix.
    Mat conv_mat;
private:
};
