/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "stab_conv.h"

PetscErrorCode StabConv::preallocate() {
    FUNCTIONBEGIN(p_cm_);

    // This takes care of preallocation, so do not worry about that.
    CreateMatrix(p_dom_->da_sc, &conv_mat);
    CHKERRQ(ierr);
//    ierr = DMSetMatType(p_dom_->da_sc, MATMPIAIJ); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

PetscErrorCode StabConv::fill(Vec U) {
    FUNCTIONBEGIN(p_cm_);

    if( !conv_mat ) {
        ERROR(p_cm_, INTRADM, "Matrix hasn't been created?!", "");
    }

    // Number of non-zeros per row (number of columns)
    PetscInt       ncols;

    // Local version of U, s.t. boundary stencil is available.
    Vec U_local;
    ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    // Update Ghost values of U
    ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);

    // Update boundary conditions on U
    ierr = p_mod_->update_bd_field(U_local, p_dom_->bd_fam.U, p_mod_->t); CHKERRQ(ierr);

    // TODO: reformat the following, somehow.
    if( p_opt_->enable_3d ) {
//        DEBUG(p_cm_->intra_domain_color, "%sAssembly %dx%dx%d Convection matrix...\n", p_dom_->Nx*p_dom_->Ny,

        ERROR(p_cm_, INTRADM, "Not yet implemented!\n", "");
//        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
//            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
//                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
//                }
//            }
//        }
    } else { // 2D Case
//        DEBUG(p_cm_->intra_domain_color, "%sAssembly %dx%d Convection matrix...\n",
//              p_dom_->Nx*p_dom_->Ny, p_dom_->Nx*p_dom_->Ny);
        // Arrays with values of U and cell sizes (d)
        Field2D **U_a;
        Volume2D        **d;
        ierr = DMDAVecGetArray(p_dom_->da, U_local, &U_a);
        CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Df, &d); CHKERRQ(ierr);

        PetscScalar dt = p_mod_->dt;

        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                // Index of current row
                MatStencil row = {0, j, i, 0};
                // Indices of all entries for this row
                MatStencil    col[5] = {{0,0,0,0}};
                // Values of each entry on this row
                PetscScalar v[5];

                // Current column (non-zero entry in this row)
                ncols = 0;

                // x-coord
                col[ncols] = {0, j + 1, i, 0};
                v[ncols++] = +dt * (U_a[j + 1][i][1] + U_a[j][i][1]) / (4. * d[j][i].y);
                col[ncols] = {0, j - 1, i, 0};
                v[ncols++] = -dt * (U_a[j][i][1] + U_a[j - 1][i][1]) / (4. * d[j][i].y);
                // y-coord
                col[ncols] = {0, j, i + 1, 0};
                v[ncols++] = +dt * (U_a[j][i + 1][0] + U_a[j][i][0]) / (4. * d[j][i].x);
                col[ncols] = {0, j, i - 1, 0};
                v[ncols++] = -dt * (U_a[j][i][0] + U_a[j][i - 1][0]) / (4. * d[j][i].x);

                // Diagonal element
                col[ncols] = {0, j, i, 0};
                v[ncols++] = 0;
                // Insert the entire row
                ierr = MatSetValuesStencil(conv_mat, 1, &row, ncols, col, v, INSERT_VALUES); CHKERRQ(ierr);
            }
        }
        // Give arrays back to PETSc
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Df, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_local, &U_a);
        CHKERRQ(ierr);
    }

    // Finalize matrix build
    ierr = MatAssemblyBegin(conv_mat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(conv_mat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // Give back vectors to PETSc
    ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
