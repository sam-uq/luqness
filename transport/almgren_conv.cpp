/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "almgren_conv.h"

//! Read from U, G U_x, U-y, U_no_y, U_no_y and write to U_edg_* the interpolation at the edges
//!
PetscErrorCode AlmgrenConv::first_interp(Vec U, Vec U_x, Vec U_y,
                                         Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T)
{
    FUNCTIONBEGIN(p_cm_);

    Field2D         **U_array, **U_x_array, **U_y_array;
    Field2D         **U_edg_L_array, **U_edg_R_array, **U_edg_B_array, **U_edg_T_array;
    PetscReal       C; //, D = 0;
    Volume2D     **d;

    ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_B, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_T, &U_edg_T_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            C = 0.5 * (1 - p_mod_->dt / d[j][i].y * U_array[j][i][1]);
            U_edg_B_array[j][i][0] = U_array[j][i][0] + C * U_y_array[j][i][0];
            U_edg_B_array[j][i][1] = U_array[j][i][1] + C * U_y_array[j][i][1];
            C = 0.5 * (1 + p_mod_->dt / d[j][i].y * U_array[j][i][1]);
            U_edg_T_array[j][i][0] = U_array[j][i][0] - C * U_y_array[j][i][0];
            U_edg_T_array[j][i][1] = U_array[j][i][1] - C * U_y_array[j][i][1];
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_T, &U_edg_T_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_L, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_R, &U_edg_R_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            C = 0.5 * (1 - p_mod_->dt / d[j][i].x * U_array[j][i][0]);
            U_edg_L_array[j][i][0] = U_array[j][i][0] + C * U_x_array[j][i][0];
            U_edg_L_array[j][i][1] = U_array[j][i][1] + C * U_x_array[j][i][1];
            C = 0.5 * (1 + p_mod_->dt / d[j][i].x * U_array[j][i][0]);
            U_edg_R_array[j][i][0] = U_array[j][i][0] - C * U_x_array[j][i][0];
            U_edg_R_array[j][i][1] = U_array[j][i][1] - C * U_x_array[j][i][1];
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_L, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_R, &U_edg_R_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Read from \f$U\f$ and \f$U_x\f$ and \f$U_y\f$ and compute the transverse derivative \f$U^{orth}_x\f$ and
//! \f$U^{orth}_y\f$.
//!
PetscErrorCode AlmgrenConv::trv_diff(Vec U_LR, Vec U_BT, Vec U_no_x, Vec U_no_y)
{
    FUNCTIONBEGIN(p_cm_);

    Vec      U_LR_local, U_BT_local;
    Field2D  **U_LR_array, **U_BT_array, **U_no_x_array, **U_no_y_array;
    Volume2D **d;

    ierr = DMGetLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);

    // Upd boundaries // send *data
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_LR, INSERT_VALUES, U_LR_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_BT, INSERT_VALUES, U_BT_local); CHKERRQ(ierr);

    // Compute convective derivative
    ierr = DMDAVecGetArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            // TODO: check if this is working
            U_no_x_array[j][i][0] = 0.5 / d[j][i].x * ( U_LR_array[j][i][0] + U_LR_array[j][i-1][0] )
                    * ( U_LR_array[j][i][0] - U_LR_array[j][i-1][0] );
            U_no_x_array[j][i][1] = 0.5 / d[j][i].x * ( U_LR_array[j][i][0] + U_LR_array[j][i-1][0] )
                    * ( U_LR_array[j][i][1] - U_LR_array[j][i-1][1] );
            U_no_y_array[j][i][0] = 0.5 / d[j][i].y * ( U_BT_array[j][i][1] + U_BT_array[j-1][i][1] )
                    * ( U_BT_array[j][i][0] - U_BT_array[j-1][i][0] );
            U_no_y_array[j][i][1] = 0.5 / d[j][i].y * ( U_BT_array[j][i][1] + U_BT_array[j-1][i][1] )
                    * ( U_BT_array[j][i][1] - U_BT_array[j-1][i][1] );
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);
    PetscFunctionReturn(ierr);
}

//! Read from U, G U_x, U-y, U_no_y, U_no_y and write to U_edg_* the interpolation at the edges
//!
PetscErrorCode AlmgrenConv::second_interp(Vec U, Vec G, Vec F,  Vec Lpl,
                                          Vec U_x, Vec U_y,
                                          Vec U_no_x, Vec U_no_y,
                                          Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, Vec U_edg_T)
{
    FUNCTIONBEGIN(p_cm_);

    Field2D             **U_array, **G_array, **Lpl_array;
    Field2D             **U_x_array, **U_y_array, **U_no_x_array, **U_no_y_array, **F_array;
    Field2D             **U_edg_L_array, **U_edg_R_array, **U_edg_B_array, **U_edg_T_array;
    PetscReal           C, D = 0;
    Volume2D            **d;

    ierr = DMDAVecGetArray(p_dom_->da, G, &G_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, F, &F_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, Lpl, &Lpl_array); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_B, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_T, &U_edg_T_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            //              D = eps * Lpl_array[j][i][0];
            C = U_array[j][i][0] - 0.5 * p_mod_->dt * ( U_no_x_array[j][i][0]
                    + U_array[j][i][1] * U_y_array[j][i][0] + D
                    + G_array[j][i][0] - F_array[j][i][0] );
            U_edg_B_array[j][i][0] = C + 0.5 * d[j][i].y * U_y_array[j][i][0];
            U_edg_T_array[j][i][0] = C - 0.5 * d[j][i].y * U_y_array[j][i][0];
            //              D = eps * Lpl_array[j][i][1];
            C = U_array[j][i][1] - 0.5 * p_mod_->dt * ( U_no_x_array[j][i][1]
                    + U_array[j][i][1] * U_y_array[j][i][1] + D
                    + G_array[j][i][1]  - F_array[j][i][1] );
            U_edg_B_array[j][i][1] = C + 0.5 * d[j][i].y * U_y_array[j][i][1];
            U_edg_T_array[j][i][1] = C - 0.5 * d[j][i].y * U_y_array[j][i][1];
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_T, &U_edg_T_array); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da, U_edg_L, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_R, &U_edg_R_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            //             D = eps * Lpl_array[j][i][0];
            C =  U_array[j][i][0] - 0.5 * p_mod_->dt * ( U_no_y_array[j][i][0]
                    + U_array[j][i][0] * U_x_array[j][i][0]
                    + D + G_array[j][i][0] - F_array[j][i][0] );
            U_edg_L_array[j][i][0] = C + 0.5 * d[j][i].x * U_x_array[j][i][0];
            U_edg_R_array[j][i][0] = C - 0.5 * d[j][i].x * U_x_array[j][i][0];
            //             D = eps * Lpl_array[j][i][1];
            C =  U_array[j][i][1] - 0.5 * p_mod_->dt * ( U_no_y_array[j][i][1]
                    + U_array[j][i][0] * U_x_array[j][i][1]
                    + D + G_array[j][i][1] - F_array[j][i][1] );
            U_edg_L_array[j][i][1] = C + 0.5 * d[j][i].x * U_x_array[j][i][1];
            U_edg_R_array[j][i][1] = C - 0.5 * d[j][i].x * U_x_array[j][i][1];
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_x, &U_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_no_x, &U_no_x_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_no_y, &U_no_y_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_y, &U_y_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_L, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_R, &U_edg_R_array); CHKERRQ(ierr);

    ierr = DMDAVecRestoreArray(p_dom_->da, G, &G_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, F, &F_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, Lpl, &Lpl_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Upwind the edges U_edg_* to the center U-**
//!
PetscErrorCode AlmgrenConv::upwind(Vec U_edg_L, Vec U_edg_R, Vec U_edg_B, const Vec U_edg_T,
                                   Vec U_LR, Vec U_BT)
{
    FUNCTIONBEGIN(p_cm_);

    PetscReal             u_adv_LR, v_adv_BT;

    Vec                   U_edg_L_local, U_edg_R_local, U_edg_B_local, U_edg_T_local;

    Field2D               **U_edg_L_array, **U_edg_R_array, **U_edg_B_array, **U_edg_T_array;
    Field2D               **U_LR_array, **U_BT_array;
    Volume2D              **d;

    ierr = DMGetLocalVector(p_dom_->da, &U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_edg_T_local); CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_L, INSERT_VALUES, U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_L, INSERT_VALUES, U_edg_L_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_R, INSERT_VALUES, U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_R, INSERT_VALUES, U_edg_R_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_B, INSERT_VALUES, U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_B, INSERT_VALUES, U_edg_B_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_edg_T, INSERT_VALUES, U_edg_T_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_edg_T, INSERT_VALUES, U_edg_T_local); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            if( U_edg_L_array[j][i][0] > 0 && U_edg_L_array[j][i][0] + U_edg_R_array[j][i+1][0] > 0 ) {
                u_adv_LR = U_edg_L_array[j][i][0];
            } else if( U_edg_L_array[j][i][0] <= 0 && U_edg_R_array[j][i+1][0] >= 0 ) {
                u_adv_LR = 0;
            } else {
                u_adv_LR = U_edg_R_array[j][i+1][0];
            }

            if( u_adv_LR > 0) {
                U_LR_array[j][i][0] = U_edg_L_array[j][i][0];
                U_LR_array[j][i][1] = U_edg_L_array[j][i][1];
            } else if( u_adv_LR == 0) {
                U_LR_array[j][i][0] = 0.5 * ( U_edg_L_array[j][i+1][0] + U_edg_R_array[j][i][0] );
                U_LR_array[j][i][1] = 0.5 * ( U_edg_L_array[j][i+1][1] + U_edg_R_array[j][i][1] );
            } else {
                U_LR_array[j][i][0] = U_edg_R_array[j][i+1][0];
                U_LR_array[j][i][1] = U_edg_R_array[j][i+1][1];
            }
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_L_local, &U_edg_L_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_R_local, &U_edg_R_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_LR, &U_LR_array); CHKERRQ(ierr);

    ierr = DMDAVecGetArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_edg_T_local, &U_edg_T_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            if( U_edg_B_array[j][i][1] > 0 && U_edg_B_array[j][i][1] + U_edg_T_array[j+1][i][1] > 0 ) {
                v_adv_BT = U_edg_B_array[j][i][1];
            } else if( U_edg_B_array[j][i][1] <= 0 && U_edg_T_array[j+1][i][1] >= 0 ) {
                v_adv_BT = 0;
            } else {
                v_adv_BT = U_edg_T_array[j+1][i][1];
            }
            if( v_adv_BT  > 0 ) {
                U_BT_array[j][i][0] = U_edg_B_array[j][i][0];
                U_BT_array[j][i][1] = U_edg_B_array[j][i][1];
            } else if( v_adv_BT == 0 ) {
                U_BT_array[j][i][0] = 0.5 * ( U_edg_B_array[j+1][i][0] +  U_edg_T_array[j][i][0] );
                U_BT_array[j][i][1] = 0.5 * ( U_edg_B_array[j+1][i][1] +  U_edg_T_array[j][i][1] );
            } else {
                U_BT_array[j][i][0] = U_edg_T_array[j+1][i][0];
                U_BT_array[j][i][1] = U_edg_T_array[j+1][i][1];
            }
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_B_local, &U_edg_B_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_edg_T_local, &U_edg_T_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_BT, &U_BT_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_L_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_R_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_B_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_edg_T_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Read from P_local and write to P_global the convection of P
//!
PetscErrorCode AlmgrenConv::conv(Vec U, Vec G, Vec F, Vec Lpl, Vec U_conv)
{
    FUNCTIONBEGIN(p_cm_);

    PetscReal           LR, BT;

    Vec      U_x, U_y, U_z, U_no_x, U_no_y;
    Vec      U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_LR, U_BT;
    Vec      U_LR_local, U_BT_local;
    Field2D         **U_LR_array, **U_BT_array, **U_conv_array;
    Volume2D        **d;

    // MC
    ierr = DMGetGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
    U_z = NULL;

    // FIXME get 3D working and remove this warning
    mc_diff(U, U_x, U_y, U_z);

    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);

    first_interp(U, U_x, U_y, U_edg_L, U_edg_R, U_edg_B, U_edg_T);

    ierr = DMGetGlobalVector(p_dom_->da, &U_LR); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_BT); CHKERRQ(ierr);

    upwind(U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_LR, U_BT);

    ierr = DMGetGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);

    trv_diff(U_LR, U_BT, U_no_x, U_no_y);

    second_interp(U, G, F, Lpl, U_x, U_y, U_no_x, U_no_y, U_edg_L, U_edg_R, U_edg_B, U_edg_T);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);

    upwind(U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_LR, U_BT);

    ierr = DMGetLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMGetLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);

    // Upd boundaries // send *data
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_LR, INSERT_VALUES, U_LR_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_LR, INSERT_VALUES, U_LR_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(p_dom_->da, U_BT, INSERT_VALUES, U_BT_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, U_BT, INSERT_VALUES, U_BT_local); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_LR); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &U_BT); CHKERRQ(ierr);

    // Compute convective derivative
    ierr = DMDAVecGetArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            LR = (U_LR_array[j][i][0] + U_LR_array[j][i-1][0]);
            BT = (U_BT_array[j][i][1] + U_BT_array[j-1][i][1]);
            U_conv_array[j][i][0] = 0.5 * (LR / d[j][i].x * (U_LR_array[j][i][0] - U_LR_array[j][i-1][0])
                    + BT / d[j][i].y * (U_BT_array[j][i][0] - U_BT_array[j-1][i][0]));
            U_conv_array[j][i][1] = 0.5 * (LR / d[j][i].x * (U_LR_array[j][i][1] - U_LR_array[j][i-1][1])
                    + BT / d[j][i].y * (U_BT_array[j][i][1] - U_BT_array[j-1][i][1]));
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(p_dom_->da, U_conv, &U_conv_array); CHKERRQ(ierr);

    ierr = DMRestoreLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
