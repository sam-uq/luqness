/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file conv.h
//! The file contains the class Conv, the Virtual implementation
//! of the non-linear term.

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/domain.h"
#include "../models/model_base.h"

//! @brief This *virtual* class provides a skeleton for
//! the implementation of the non-linear term \f$U \otimes U\f$.
//!
//! The class is pure abstract and cannot be instantiated.
class Conv
{
public:
    //! \brief Constructor, allocate space for temporary vectors,
    //! solution vectors and for Laplacian matrix.
    Conv(Options *p_opt, Domain *p_dom, ModelBase * p_mod, Attila *p_cm)
        : p_opt_ (p_opt), p_dom_(p_dom), p_mod_(p_mod), p_cm_(p_cm) { }

    //! \brief Free memory.
    virtual ~Conv() {};

    //! \brief Compute space derivatives using MC limiter
    //! Read from \f$U\f$ and write to \f$U_x\f$, \f$U_y\f$ and \f$U_z\f$ the MC limited differences
    //! (update U to local to obtain ghosts).
    //! \param[in]  U_local locally data for U
    //! \param[out] U_x global x-derivative of U with MC limiter
    //! \param[out] U_y global y-derivative of U with MC limiter
    //! \param[out] U_z global z-derivative of U with MC limiter
    //! \return     Error code.
    PetscErrorCode mc_diff(Vec U, Vec U_x, Vec U_y, Vec U_z);

    //! \brief Each subclass implements its own version of the non-linear term.
    //!
    //!
    //! \param[in,out] U       Velocity \f$U^n\f$ in, velocity \f$U^{n+1}\f$ out.
    //! \param[in,out] G
    //! \param[in]     F
    //! \param [in]    Lpl
    //! \param [in]    U_conv
    //! \return        Error code.
    virtual PetscErrorCode conv(Vec U, Vec G, Vec F, Vec Lpl, Vec U_conv) = 0;

protected:
    // Compositios
    Options *const p_opt_; //!< Options handle
    Domain *const p_dom_; //!< Domain info handle
    ModelBase *const p_mod_; //!< Initial data and others handle
    Attila *const p_cm_; //!< Parallel allocation and communication handle
};
