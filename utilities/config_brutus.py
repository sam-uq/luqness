#!/cluster/apps/python/2.7.2/x86_64/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--download-fftw',
    '--download-metis',
    '--download-mumps',
    '--download-parmetis',
    '--download-scalapack',
    '--with-clanguage=cxx',
    '--with-debugging=0',
    'CXXOPTFLAGS=-O3',
    'PETSC_ARCH=arch-linux2-cxx-opt',
  ]
  configure.petsc_configure(configure_options)
