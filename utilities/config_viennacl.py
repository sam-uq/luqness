﻿#! /usr/bin/env python2

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--download-hypre',
    '--download-metis',
    '--download-ml',
    '--download-mumps',
    '--download-parmetis',
    '--download-scalapack',
    '--download-sowing',
    '--download-superlu',
    '--download-superlu-dist',
    '--with-cc=mpicc',
    '--with-cxx=mpicxx',
    '--with-fc=mpif90',
    '--with-fc=mpif90',
    '--with-precision=double',
    '--with-mpi-dire=/usr/lib/openmpi/',
    '--download-viennacl',
    '--with-opencl-include=/usr/include/CL/',
    '--with-opencl-lib=/usr/lib/libOpenCL.so'
    '--with-debugging=0',
    'COPTFLAGS=-O3',
    'CXXOPTFLAGS=-O3',
    'FOPTFLAGS=-O3',
    'PETSC_ARCH=arch-viennacl-opt',
  ]
  configure.petsc_configure(configure_options)
  
  
#'--with-viennacl-include=/usr/include/viennacl/',
#'--with-viennacl-lib=/usr/lib/',

 make PETSC_DIR=/home/filippo/Workspace/petsc PETSC_ARCH=arch-viennacl-opt all
