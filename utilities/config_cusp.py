﻿#! /usr/bin/env python2

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-cuda=1',
    '--with-cusp=1',
    '--with-thrust=1',
    '--with-cusp-include=/opt/cuda/include/cusp/',
    '--download-hypre',
    '--download-metis',
    '--download-ml',
    '--download-mumps',
    '--download-parmetis',
    '--download-scalapack',
    '--download-sowing',
    '--download-superlu',
    '--download-superlu-dist',
    '--with-cc=mpicc',
    '--with-cxx=mpicxx',
    '--with-fc=mpif90',
    '--with-fc=mpif90',
    '--with-precision=double',
    '--with-mpi-dire=/usr/lib/openmpi/',
    '--with-debugging=0',
    'COPTFLAGS=-O3',
    'CXXOPTFLAGS=-O3',
    'FOPTFLAGS=-O3',
    'PETSC_ARCH=arch-cuda-opt',
  ]
  configure.petsc_configure(configure_options)
