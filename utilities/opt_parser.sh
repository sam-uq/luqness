#! /bin/bash

# Usage string
usage() { echo "Usage: $0 [ -n processors ][ -w walltime ][ -j name ][ -m mesh ][ -l levels ][ -s simulations][ -i
integration ][ -e viscosity ][ -g aternative_input_file ][ -o extra_options ][ -c -d -q -z -p ]" 1>&2; exit 1; }

# Default values
nproc=1
wtime="01:00"
name="unnamed"
mesh=128
lvl=5
sim=1
int="mc"
eps=0
time=false
ask=true
batch=false
from=0
to=0
email=false
input_file="conf.cfg"
pat=""

# Parse options
while getopts ":n:w:j:m:l:s:i:e:f:t:o:czpsqd:g" o; do
    case "${o}" in
        n) # Number of processors to use
            nproc=${OPTARG}
            ;;
        w) # Wall time
            wtime=${OPTARG}
            ;;
        j) # Job name
            name=${OPTARG}
            ;;
        m) # Mesh size
            mesh=${OPTARG}
            ;;
        l) # MLMC levels
            lvl=${OPTARG}
            ;;
        s) # Simulation number
            sim=${OPTARG}
            ;;
        i) # Integration type (btw. mc, mlmc, quad)
            int=${OPTARG}
            ;;
        e) # Viscosity
            eps=${OPTARG}
            ;;
        f) # From
            from=${OPTARG}
            ;;
        t) # To
            to=${OPTARG}
            ;;
        c) # Use time or atime
            time=true
            ;;
        d) # Send email when jobs is started/finished
            email=true
            ;;
        q) # Don't ask before running
            ask=false
            ;;
        g) # Use selected input files
            input_file=${OPTARG}
            ;;
        o) # Extra options
            optns=${OPTARG}
            ;;
        z) # Use 3D
            use3d=true
            ;;
        p) # Use pat file
            pat="+pat"
            ;;
        *) # Default
            usage
            ;;
    esac
done
shift $((OPTIND-1))

# Add pattern to name for batch launcher
basename=$name
name=$name$pattern

if [ "$use3d" = true ]; then
  meshcmd=$mesh,$mesh,$mesh
  use3dcmd="-enable_3d"
else
  use3dcmd=""
  meshcmd=$mesh,$mesh
fi

# Check if using batch launcher
if [ "$from" -lt "$to" ];
then
  batch=true
else
  batch=false
fi

echo $optns

# SETUP OPTIONS:
if [ "$eps" = "0" ];
then
  eps_opt=""
else
  eps_opt="-ns_eps $eps"
fi
opts="-i $input_file -name $name -mesh $meshcmd $use3dcmd -mlmc_levels $lvl -sim $sim -int $int $eps_opt $optns"
