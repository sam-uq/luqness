#! /bin/bash

[[ $_ != $0 ]] && echo "Script is being sourced, this is OK." || { echo "Script is a subshell, it must be sourced (exiting)." && exit; }

echo "Setting up environment for Euler... Please stand by..."

echo "Unloading old modules..."
module unload gcc
module unload open_mpi
module unload petsc

echo "Do you want to load PETSc or use a custom compilation of it?"
read -p "Load PETSc from Euler? " -n  1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "*** LOADING BUILT-IN PETSC ***"
    echo "Loading gcc/4.8.2 and open_mpi..."
    module load gcc/4.8.2
    module load open_mpi
    echo "Loading PETSc..."
    module load petsc
 
    echo "--> Make sure C++11 is enabled using -DC11_ENABLE as CMake option."
	
else
    echo "*** LOADING CUSTOM PETSC ***"
    echo "--> Using custom PETSc distribution. Check that PETSC_DIR and PETSC_ARCH are configured correctly."
    echo "--> Make sure PETSc is compiled in Opt mode."
    PETSC_DIR=/cluster/home/filippo/petsc-git
    echo PETSC_DIR=$PETSC_DIR
    PETSC_ARCH=arch-linux2-cxx-opt
    echo PETSC_ARCH=$PETSC_ARCH

    echo "Loading gcc/5.2.0..."
    module load gcc/5.2.0
    module load open_mpi
fi

echo "Loading extra modules..."
module load python

module load cmake

module load boost

module load valgrind


