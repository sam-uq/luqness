#! /bin/bash
# Usage: source rosa_modules.sh

# Compiling tools
module load cmake

# Cray and most recent PETSc (3.4.x)
module load PrgEnv-cray
module switch xt-mpich2 cray-mpich2/6.2.2
module load cray-petsc/3.4.3.0

# Cray and default petsc (3.3.x)
# module load PrgEnv-cray
# module load cray-petsc

# Post processing
module load python/2.7.2
