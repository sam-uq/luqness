#!/usr/bin/python

#  CONFIGURATION WITH NATIVE MPI AND OPTIMIZED BUILD
#
#
#

if __name__ == '__main__':
  import sys
  import os
  os.environ['CC'] = 'gcc'
  #os.environ['FC'] = 'gfortran'

  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-mpiexec=/usr/bin/mpiexec',
    '--with-mpi=1',
    '--with-clanguage=c++',
    '--with-debugging=no',
    #'--enable-shared',
    #'--enable-fortran=0',
    #'--with-environment-variables',
    '--PETSC_ARCH=arch-linux-cxx-native-opt',
    #'--with-shared-libraries=1',
    "COPTFLAGS='-O3 -march=native -mtune=native'",
    "CXXOPTFLAGS='-O3 -march=native -mtune=native'",
    "FOPTFLAGS='-O3 -march=native -mtune=native'",
    '--with-unroll-kernels'
    ]

  configure.petsc_configure(configure_options)
