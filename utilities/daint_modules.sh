#! /bin/bash -l
# Usage: source daint_modules.sh

module unload PrgEnv-cray

# Compiling tools
#module load cmake
module load daint-mc
module load PrgEnv-gnu

# Cray and default petsc (3.5.x)
#module load PrgEnv-cray
module load cray-petsc
#module load boost

# Post processing
#module load python/2.7.2
