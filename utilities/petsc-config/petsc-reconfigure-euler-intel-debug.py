#!/usr/bin/python2
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--download-hypre',
    '--download-metis',
    '--download-ml',
    '--download-mpich',
    '--download-mumps',
    '--download-parmetis',
    '--download-scalapack',
    '--download-sowing',
    '--download-superlu',
    '--download-superlu-dist',
    '--download-triangle',
    '--with-c2html=0',
    '--with-precision=double',
    '--with-cc=icc',
    '--with-cxx=icpc',
    'PETSC_ARCH=arch-linux2-intel-dbg',
  ]
  configure.petsc_configure(configure_options)
