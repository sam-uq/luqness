#!/usr/bin/python2
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-c2html=0',
    '--with-debugging=0',
    '--with-precision=double',
    'COPTFLAGS=-O3',
    'CXXOPTFLAGS=-O3',
    'FOPTFLAGS=-O3',
    'PETSC_ARCH=arch-linux2-cxx-opt',
    '--download-fblaslapack=1'
  ]
  configure.petsc_configure(configure_options)
