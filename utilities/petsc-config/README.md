### Use this configuration scripts to configure PETSc in an easy way.

Place the python script into the petsc root directory and run (using Python 2).

Each file has the following naming convention:
    petsc-reconfigure-<machine>-<name>-<release_type>.py

The `<machine>` specifies for which machine the script should be used: currently euler, dora, daint, local.

The `<name>`specifies a name for the configuration, which indicates what type of options are used, currently:
   - `downloadall`: downloads and compiles a custom MPI implementation and all other packages, very stable and safe, but might be unoptimal;
   - `intel`: tries and uses an intel compiler;

The option `<release_type>` specifies what type of compilation option is used:
   - `release`: fully optimize, no debug info;
   - `debug`: not/little optimization, full debug info.
