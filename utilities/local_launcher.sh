#! /bin/bash

# Config
# Executables name
exec="./luqness"
mpi_exec="$PETSC_DIR/$PETSC_ARCH/bin/mpirun"
local=1

# Parse options
. ./opt_parser.sh

# SETUP MPI: Check if MPI needs to be used
if [ "$nproc" -gt "1" ];
then
  echo "Running program WITH MPI and $nproc processors.";
  mpi_cmd="$mpi_exec -np $nproc";
else
  echo "Running program WITHOUT MPI.";
  mpi_cmd="";
fi

# SETUP BATCH: local no batch is needed
batch_cmd=""

# SETUP GLOBAL: gather all commands + options
cmd="$batch_cmd $mpi_cmd $exec $opts"
