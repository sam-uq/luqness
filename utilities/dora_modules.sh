#! /bin/bash
# Usage: source dora_modules.sh

# Compiling tools
module load cmake

# Cray and default petsc (3.5.x)
module load PrgEnv-cray
module load cray-petsc

# Post processing
module load python/2.7.2
