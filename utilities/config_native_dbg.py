#!/usr/bin/python

#  CONFIGURATION WITH NATIVE MPI AND DEBUG BUILD
#
#
#

if __name__ == '__main__':
  import sys
  import os
  os.environ['CC'] = 'gcc'
  #os.environ['FC'] = 'gfortran'

  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-mpiexec=/usr/bin/mpiexec',
    '--with-mpi=1',
    '--with-clanguage=c++',
    #'--enable-shared',
    #'--enable-fortran=0',
    #'--with-environment-variables',
    '--PETSC_ARCH=arch-linux-cxx-native',
    #'--with-shared-libraries=1',
    ]


  configure.petsc_configure(configure_options)
