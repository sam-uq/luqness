#!/usr/bin/python2

#  MODULAR CONFIGURATION
#  sys.argv[1] is the prefix name of PETSC_ARCH
#  the rest is just a list:
#   - int64: if present, add 64-bit indices
#   - opt: if present, turn on all optimization flags
#   - mpich/nompi: download MPICH or disable MPI
#   - extra/cusp/viennacl: more packages

if __name__ == '__main__':
    import sys
    import os
    os.environ['CC'] = 'gcc'
    #os.environ['FC'] = 'gfortran'
    
    arch = sys.argv[1]
    
    configure_options = [
        '--with-clanguage=c++',
        '--enable-shared',
        '--with-shared-libraries=1',
        #'--with-environment-variables'
    ]
    
    if "int64" in sys.argv:
        print("Building with 64-bit indices.")
        configure_options += ['--with-64-bit-indices']
        arch += "_int64"
        
    if "opt" in sys.argv:
        print("Building with optimization ON.")
        configure_options += [
            '--with-debugging=0',
            "COPTFLAGS='-O3 -march=native -mtune=native'",
            "CXXOPTFLAGS='-O3 -march=native -mtune=native'",
            "FOPTFLAGS='-O3 -march=native -mtune=native'",
            '--with-unroll-kernels',
            '--with-debugging=no',
        ]
        arch += "_opt"
    else:
        print("Building with optimization OFF.")
        configure_options += [
            '--with-debugging=1'
        ]
        arch += "_dbg"
        
    if "mpich" in sys.argv:
        print("Downloading MPICH.")
        configure_options += [
            '--with-mpi=1',  
            '--download-mpich'
        ]
        arch += "_mpich"
    elif "nompi" in sys.argv:
        print("Building WITHOUT MPI.")
        configure_options += ['--with-mpi=0']
        arch += "_nompi"
    else:
        print("Using native MPI.")
        configure_options += [
            '--with-mpi=1',
            '--with-mpiexec=/usr/bin/mpiexec'
        ]
        arch += "_native"
        
    if "nofortran" in sys.argv:
        print("Disabling Fortran.")
        configure_options += ['--enable-fortran=0']
        arch += "_nofort"
        
    if "extra" in sys.argv:
        print("Extra packages.")
        configure_options += [
                '--download-hypre',
                '--download-metis',
                '--download-ml',
                '--download-mumps',
                '--download-parmetis',
                '--download-scalapack',
                '--download-sowing',
                '--download-superlu',
                '--download-superlu-dist'
            ]
        arch += "_extra"
        
    if "viennacl" in sys.argv:
        print("Using ViennaCL.")
        configure_options += [
                '--download-viennacl',
                '--with-opencl-include=/usr/include/CL/',
                '--with-opencl-lib=/usr/lib/libOpenCL.so'
            ]
        arch += "_viennacl"
        
    if "cusp" in sys.argv:
        print("Using CUSP.")
        configure_options += [
                '--with-cuda=1',
                '--with-cusp=1',
                '--with-thrust=1',
                '--with-cusp-include=/opt/cuda/include/cusp/'
            ]
        arch += "_cusp"
        
    configure_options += [
        '--PETSC_ARCH={0}'.format(arch)
        ]
    
    print(configure_options)
    
    sys.path.insert(0, os.path.abspath('config'))
    import configure
    configure.petsc_configure(configure_options)
