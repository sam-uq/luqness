/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "vel_proj_step.h"

VelProjStep::VelProjStep(Options *p_opt, Domain * p_dom,
                         ModelBase *p_mod, Attila *p_cm,
                         Projection *p_proj, Conv *p_conv)
    : ProjStep(p_opt, p_dom, p_mod, p_cm, p_proj), p_conv_(p_conv) {
    FUNCTIONBEGIN(p_cm_);

    if(p_diff_ != nullptr) {
        ierr = p_diff_->set_num_visc_scaling(0); CHKERRV(ierr);
    }
}

VelProjStep::~VelProjStep() {
    FUNCTIONBEGIN(p_cm_);
}

PetscErrorCode VelProjStep::step(Vec U, Vec G, Vec F_b, Vec F) {
    FUNCTIONBEGIN(p_cm_);

    Vec       U_conv, Q, Lpl;
    ierr = DMGetGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    p_profiler->push_stage("convection");
//    VecSet(G,0);
    ierr = p_conv_->conv(U,  G,  F_b, Lpl, U_conv); CHKERRQ(ierr);
    p_profiler->pop_stage();
    // U_conv now contains the convection of U

    ierr = DMRestoreGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPBY(U_conv, 1., 1., G); CHKERRQ(ierr);
        // U_conv contains the argument to the projection
    }
    ierr = VecAXPBY(U_conv, 1., -1., F); CHKERRQ(ierr);
    // U_conv contains the argument to the projection

    ierr = VecAXPBY(U_conv, 1., p_mod_->dt, U); CHKERRQ(ierr);
    // U_conv contains the argument to the projection
    // U_conv = (U^n+1/2 \otimes U^n+1/2 + Gp^n-1/2 - F^n+1/2) * Dt + U^n

    // If needed, apply a CN step for the NS diffusion (or numerical, not needed)
    // U^{n+1} = U_conv + visc * dt * \Delta \theta U^{n+1} + (1 - \theta) U^{n}
    // (I - visc * dt * \Delta \theta) U^{n+1} = U_conv + visc * dt * (1 - \theta) U^{n}
    if( ( !p_opt_->use_euler || p_opt_->enable_numerical_diff)
            && !p_opt_->use_explicit ) {
        // Adding implicit diffusion
        bool ksp_diff_mat_has_changed = p_diff_->diff_mat_has_changed();

        if ( !use_compute_operators ) { // Dont' use setup operator (use matrix directly)
            // Matrix will be constructed, or modified if dt changes.
            if( !has_been_setted_up || ksp_diff_mat_has_changed ) {
                DEBUG(p_cm_, INTRADM,
                      "Time step has changed for diffusion, "
                      "recomputing operators...\n", "");
                p_diff_->update_shifted_matrix();

                ierr = SetOperators(KSP_diff,
                                    p_diff_->shift_diff_mat_,
                                    p_diff_->shift_diff_mat_); CHKERRQ(ierr);

                if( !has_been_setted_up ) {
                    ierr = KSPSetFromOptions(KSP_diff); CHKERRQ(ierr);
                    ierr = KSPSetUp(KSP_diff); CHKERRQ(ierr);

                    has_been_setted_up = true;
                }
            }
#ifndef PETSC_OLDAPI_DMDA // Old API fix (v. < ???)
//            ierr = KSPSetReusePreconditioner(KSP_diff,
//                                             PETSC_TRUE); CHKERRQ(ierr);
#endif
            KSPOUT(p_cm_, INTRADM,
                   "Solving NS system "
                   "(eps = %.2e, dt = %.2e, nu = %.2e, "
                   "factor = %.2e, theta = %.2e => tot = %.2e)"
                   "[no compute op.]...\n",
                   p_diff_->epsilon_, p_mod_->dt, p_opt_->nu,
                   p_diff_->factor, p_diff_->theta,
                   p_diff_->get_complete_diff_coefficient());
        } else {
            assert(false && "Compute operators non implemented.");
        }

        //// Apply diffusion
        Vec Rhs_sc, U_sc;
        p_profiler->begin_event("step_ksp_noop");
        // Setting up solver for 1st component
        ierr = DMGetGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
        ierr = DMGetGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);
        ierr = VecStrideGather(U_conv, 0, U_sc, INSERT_VALUES); CHKERRQ(ierr);
        ierr = p_diff_->vec_add_diff(U_sc, U_sc); CHKERRQ(ierr);
        ierr = KSPSolve(KSP_diff, U_sc, Rhs_sc); CHKERRQ(ierr);
        ierr = VecStrideScatter(Rhs_sc ,0, U_conv, INSERT_VALUES); CHKERRQ(ierr);

        // Setting up solver for 2nd component
        ierr = VecStrideGather(U_conv, 1, U_sc, INSERT_VALUES); CHKERRQ(ierr);
        ierr = p_diff_->vec_add_diff(U_sc, U_sc); CHKERRQ(ierr);
        ierr = KSPSolve(KSP_diff, U_sc, Rhs_sc); CHKERRQ(ierr);
        ierr = VecStrideScatter(Rhs_sc ,1, U_conv, INSERT_VALUES); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);
        p_profiler->end_event("step_ksp_noop");
    } else if ( ( !p_opt_->use_euler || p_opt_->enable_numerical_diff)
                && p_opt_->use_explicit){
        assert(false && "Explicity diffusion not implemented.");
    }
    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    p_profiler->push_stage("projection");
    ierr = p_proj_->projection(U_conv, U, Q); CHKERRQ(ierr); // Project P_local, get U_x (P(arg) and U_y Q(arg))
    p_profiler->pop_stage();

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);

    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPY(G, 1. / p_mod_->dt, Q); CHKERRQ(ierr);
    } else { // if DirectPressure
        ierr = VecCopy(Q, G); CHKERRQ(ierr);
        ierr = VecScale(G, 1. / p_mod_->dt); CHKERRQ(ierr);
    }
    // G now contains the new step of U

    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
