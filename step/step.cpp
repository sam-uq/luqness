/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "step.h"

//! Construct vectors
//!
Step::Step(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm, Projection * p_proj,
           Solver * p_parent_solver)
    : p_opt_ (p_opt), p_dom_(p_dom), p_mod_(p_mod),
      p_cm_(p_cm), p_proj_ (p_proj), p_parent_solver_(p_parent_solver) {
    FUNCTIONBEGIN(p_cm_);

}

//! deallocate/free memory (temp vectors)
//!
Step::~Step() {
    FUNCTIONBEGIN(p_cm_);
}

//! Provides the CFL condition (modify dt) s.t. the scheme is stable (empirically or theoretically)
//!
PetscErrorCode Step::CFL(Vec U) {
    FUNCTIONBEGIN(p_cm_);
    // TODO: disable if no need for CFL
    VecNorm(U, NORM_INFINITY, &p_mod_->sup); CHKERRQ(ierr);
    VERBOSE(p_cm_, INTRADM, "CFL is %f.\n", p_mod_->sup);
    if( p_mod_->sup > SUP_TOL ) {
        ERROR(p_cm_, INTRADM, "Local maximum %f exceeds tolerance %f at time t = %f (step %d), probable blowup "
                              "detected.\n",
              p_mod_->sup, SUP_TOL,
              p_mod_->t, p_parent_solver_ ? p_parent_solver_->step_counter : -1);
        if(p_parent_solver_ && DUMP_DATA) {
            ierr = p_parent_solver_->save_snapshot(); CHKERRQ(ierr);
        }
    }
    if(p_opt_->adaptive) {
        if(p_opt_->use_explicit and not p_opt_->use_euler) {
            // Advective CFL
            double const_conv = p_dom_->h / p_mod_->sup;
            // Diffusive CFL
            double const_diff = p_dom_->h * p_dom_->h / p_opt_->nu;
            p_mod_->dt = p_opt_->lambda * std::min(const_conv, const_diff);
        } else {
            p_mod_->dt = p_dom_->h / p_mod_->sup * p_opt_->lambda;
        }
    } else {
        p_mod_->dt = p_dom_->h * p_opt_->lambda;
    }

    PetscFunctionReturn(ierr);
}

//! Perform a single step of the passive transport equation
//!
PetscErrorCode Step::transport(Vec quantity, Vec U) {
    FUNCTIONBEGIN(p_cm_);

    // TODO: improve this one to be more accurate/stable
    Vec      lpl_quantity;
    Vec      grad_quantity;

    PetscReal          sup_x = 0, sup_y = 0, sup_z = 0; // TODO

    if( !p_mod_->viscosity_violated &&
            p_dom_->h * p_opt_->transport_porosity * max(sup_x, sup_y, sup_z) >  2.0 * p_opt_->transport_viscosity ) {
        WARNING(p_cm_, INTRADM, "POROSITY CONDITION VIOLATED!\n", "");
        WARNING(p_cm_, INTRADM, "eta = %f, nu = %f, h = %f, dt = %f, sup(U) = %f.\n",
                p_opt_->transport_porosity, p_opt_->transport_viscosity,
                p_dom_->h, p_mod_->dt, max(sup_x, sup_y, sup_z));
        p_mod_->viscosity_violated = PETSC_TRUE;
    }
    if( !p_mod_->porosity_violated && p_mod_->dt * 2.0 * p_opt_->transport_viscosity < p_dom_->h * p_dom_->h ) {
        WARNING(p_cm_, INTRADM, "VISCOSITY CONDITION VIOLATED!\n", "");
        WARNING(p_cm_, INTRADM, "nu = %f, h = %f, dt = %f, sup(U) = %f.\n",
                p_opt_->transport_viscosity, p_dom_->h, p_mod_->dt, max(sup_x, sup_y, sup_z));
        p_mod_->porosity_violated = PETSC_TRUE;
    }

    // Start computation of L(U)
    ierr = DMGetGlobalVector(p_dom_->da_sc, &lpl_quantity); CHKERRQ(ierr);

    Mat mat;
    ierr = p_proj_->get_trsp_mat(&mat); CHKERRQ(ierr);
    ierr = MatMult(mat, quantity, lpl_quantity); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &grad_quantity); CHKERRQ(ierr);
    ierr = p_proj_->gradient(quantity, grad_quantity, p_dom_->bd_fam.s); CHKERRQ(ierr);

    if( p_opt_->enable_3d ){
        Field3D       ***U_array, ***grad_array;
        PetscReal     ***q_array, ***lpl_array;
        ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_quantity, &grad_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, quantity, &q_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, lpl_quantity, &lpl_array); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    // Get gradient
                    q_array[k][j][i] += p_mod_->dt * p_opt_->transport_viscosity * lpl_array[k][j][i]
                            - p_opt_->transport_porosity * ( grad_array[k][j][i][0] * U_array[k][j][i][0]
                            + grad_array[k][j][i][1] * U_array[k][j][i][1]
                            + grad_array[k][j][i][2] * U_array[k][j][i][2]);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_quantity, &grad_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, quantity, &q_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, lpl_quantity, &lpl_array); CHKERRQ(ierr);
    } else {
        Field2D       **U_array, **grad_array;
        PetscReal     **q_array, **lpl_array;
        ierr = DMDAVecGetArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_quantity, &grad_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, quantity, &q_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, lpl_quantity, &lpl_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                // Get gradient
                q_array[j][i] += p_mod_->dt * p_opt_->transport_viscosity * lpl_array[j][i]
                        - p_opt_->transport_porosity * ( grad_array[j][i][0] * U_array[j][i][0]
                        + grad_array[j][i][1] * U_array[j][i][1]);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_quantity, &grad_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, quantity, &q_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, lpl_quantity, &lpl_array); CHKERRQ(ierr);
    }

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &lpl_quantity); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &grad_quantity); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Persorm an initial cleansing of the data
//!
PetscErrorCode Step::div_cleansing(Vec U, Vec G) {
    FUNCTIONBEGIN(p_cm_);

    Vec       P, Q;
    ierr = DMGetGlobalVector(p_dom_->da, &P); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    ierr = p_proj_->projection(U, P, Q); CHKERRQ(ierr);

    ierr = VecCopy(P, U); CHKERRQ(ierr); // U now contains the new step of U
    ierr = VecAXPY(G, 1, Q); CHKERRQ(ierr); // G now contains the new step of U

    ierr = DMRestoreGlobalVector(p_dom_->da, &P); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Performs a single step of the vortex method
//!
PetscErrorCode Step::extract_velocity(Vec w, Vec U) {
    FUNCTIONBEGIN(p_cm_);
    p_profiler->push_stage("extract_velocity");

    Vec psi;

    ierr = p_proj_->extract_stream(w, &psi); CHKERRQ(ierr);
    ierr = p_proj_->stream_to_centered_U(psi, U); CHKERRQ(ierr);

    if( !p_opt_->old_ksp_handling_ ) {
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &psi); CHKERRQ(ierr);
    }

    //     ierr = p_proj_->orthogonal(U);

    p_profiler->pop_stage();

    PetscFunctionReturn(ierr);
}

