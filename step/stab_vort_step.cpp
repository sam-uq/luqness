/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "./stab_vort_step.h"

//!
//!
StabVortStep::StabVortStep(Options *p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm,
                           Projection * p_proj, VortConv * p_evo)
    : VortStep(p_opt, p_dom, p_mod, p_cm, p_proj, p_evo)
{
    FUNCTIONBEGIN(p_cm_);

    if( !p_opt_->enable_numerical_diff ) {
        WARNING(p_cm_, INTRADM, "No numerical diffusion for stable scheme, might result in unwanted "
                                "oscillations\n", "");
    }

    static_cast<StabVortConv*>(p_evo_)->preallocate();

    ierr = KSPCreate(p_cm_->intra_domain_comm, &ksp); CHKERRV(ierr);
    ierr = KSPSetDM(ksp, p_dom_->da_sc); CHKERRV(ierr);
    ierr = KSPSetDMActive(ksp, PETSC_FALSE); CHKERRV(ierr);
    ierr = KSPSetOptionsPrefix(ksp,"kspstep_"); CHKERRV(ierr);
    ierr = KSPSetFromOptions(ksp); CHKERRV(ierr);

    PetscReal h;
    if( !p_opt_->use_euler || p_opt_->enable_numerical_diff ) {
        if( p_opt_->diff_type == DiffType::Roe || p_opt_->diff_type == DiffType::Rusanov ) {
            h = .5;
        } else {
            h = .1;
        }

        ierr = p_diff_->set_num_visc_scaling(h); CHKERRV(ierr);
    }
}

//! Deallocate buffers
//!
StabVortStep::~StabVortStep()
{
    FUNCTIONBEGIN(p_cm_);
}

//! Performs a single step of the vortex method as forward time central space
//!
PetscErrorCode StabVortStep::step(Vec w, Vec U, Vec, Vec)
{
    FUNCTIONBEGIN(p_cm_);

    p_profiler->begin_event("step");

    ierr = DMGetGlobalVector(p_dom_->da_sc, &rhs_sc); CHKERRQ(ierr);
    ierr = extract_velocity(w, U);

    // Setup matrix
    static_cast<StabVortConv*>(p_evo_)->fill(U);

    // TODO AVERAGE!!!!!!!
    if( p_opt_->diff_type == DiffType::Roe || p_opt_->diff_type == DiffType::Rusanov ) { // IF USE VARIABLE COEFFS
        OUTPUT(p_cm_, INTRADM, "Applying rusanov/roe non constant Laplacian.\n", "");
        ierr = static_cast<VarcoeffVortDiff*>(p_diff_)->set_coeff_vec(U); CHKERRQ(ierr);
        ierr = static_cast<VarcoeffVortDiff*>(p_diff_)->set_diff_mat(NULL); CHKERRQ(ierr);
    }

    // TODO: check how this behaves with Navier-Stokes
    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        Mat conv_mat = static_cast<StabVortConv*>(p_evo_)->conv_mat;
        ierr = p_diff_->add_diffusion_to_matrix(conv_mat); CHKERRQ(ierr);
    }

    ierr = MatShift(static_cast<StabVortConv*>(p_evo_)->conv_mat, 1.); CHKERRQ(ierr);

    // Setup KSP
    DEBUG(p_cm_, INTRADM, "Building KSP.\n", "");
    ierr = SetOperators(ksp, static_cast<StabVortConv*>(p_evo_)->conv_mat,
                        static_cast<StabVortConv*>(p_evo_)->conv_mat ); CHKERRQ(ierr);

    // Compute non-linear term
    ierr = static_cast<StabVortConv*>(p_evo_)->conv(U, w, rhs_sc); CHKERRQ(ierr);

    ierr = VecAXPBY(rhs_sc, 1., - p_mod_->dt / 2. ,w); CHKERRQ(ierr);

    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        ierr = p_diff_->vec_add_diff(w, rhs_sc); CHKERRQ(ierr);
    }

    // Actually solve the system
    KSPOUT(p_cm_, INTRADM, "Solving system for stable vort. step (may take a while).\n", "");
    ierr = KSPSolve(ksp, rhs_sc, w); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &rhs_sc); CHKERRQ(ierr);

    p_profiler->end_event("step");
    PetscFunctionReturn(ierr);
}

//! Performs a single step of the vortex method
//!
PetscErrorCode StabVortStep::extract_velocity(Vec w, Vec U)
{
    FUNCTIONBEGIN(p_cm_);
    p_profiler->push_stage("extract_velocity");

    Vec psi;

    ierr = p_proj_->extract_stream(w, &psi); CHKERRQ(ierr);

    ierr = p_proj_->stream_to_edge_U(psi, U); CHKERRQ(ierr);

    if( !p_opt_->old_ksp_handling_ ) {
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &psi); CHKERRQ(ierr);
    }

    //     ierr = p_proj_->orthogonal(U);

    p_profiler->pop_stage();

    PetscFunctionReturn(ierr);
}
