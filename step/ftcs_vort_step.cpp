/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "./ftcs_vort_step.h"

//! Setup some basic for KSP
//!
FTCSVortStep::FTCSVortStep(Options *p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm,
                           Projection * p_proj, VortConv * p_evo)
    : VortStep (p_opt, p_dom, p_mod, p_cm, p_proj, p_evo)
    , first_setup(false) {
    FUNCTIONBEGIN(p_cm_);

    // If need ksp context
    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        // FIXME: stupid hack
        ierr = DMClone(p_dom_->da_sc, &da_sc_clone);

        ierr = KSPCreate(p_cm_->intra_domain_comm, &KSP_diff); CHKERRV(ierr);
        //             ierr = KSPSetDMActive(KSP_diff, PETSC_FALSE); CHKERRV(ierr);
        if( use_compute_operators ) {
            ierr = KSPSetDM(KSP_diff, da_sc_clone); CHKERRV(ierr);
        }

        ierr = KSPSetOptionsPrefix(KSP_diff,"kspdiff_"); CHKERRV(ierr);

        //             ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRV(ierr);
    }
}

//! Destroys the KSP context
//!
FTCSVortStep::~FTCSVortStep() {
    FUNCTIONBEGIN(p_cm_);

    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        ierr = KSPDestroy(&KSP_diff); CHKERRV(ierr);
    }
}

//! Performs a single step of the vortex method as forward time central space
//!
PetscErrorCode FTCSVortStep::step(Vec w, Vec U, Vec F_b, Vec) {
    FUNCTIONBEGIN(p_cm_);

    Vec       new_w, temp;
    Vec       c_F_b;

    p_profiler->begin_event("step");

    ierr = DMGetGlobalVector(p_dom_->da_sc, &new_w); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &c_F_b); CHKERRQ(ierr);

    ierr = p_proj_->curl(F_b, c_F_b, p_dom_->bd_fam.U); CHKERRQ(ierr);

    ierr = static_cast<FTCSVortConv*>(p_evo_)->conv(w, U, c_F_b, new_w); CHKERRQ(ierr);

    //// PHASE: add diffusion
    // TODO: recycle this onto diff?
    // If need to apply diffusion
    if( !p_opt_->use_euler || p_opt_->enable_numerical_diff ) {
        // Create the matrix and ksp
        if( p_opt_->use_explicit ) { // Explicit diffusion (TODO: NOT! CHECKED)
            ierr = p_diff_->vec_add_diff(w, new_w);
        } else { // Implicit
            ierr = p_diff_->vec_add_diff(w, new_w);
            // Create the matrix L containing I - A = L for the solution of the laplacian problem
            bool ksp_diff_mat_has_changed = p_diff_->diff_mat_has_changed();

            if ( !use_compute_operators ) { // Dont' use setup operator (use matrix directly)
                // Matrix
                if( !first_setup || ksp_diff_mat_has_changed ) {
                    p_diff_->update_shifted_matrix();

                    ierr = SetOperators(KSP_diff, p_diff_->shift_diff_mat_,
                                        p_diff_->shift_diff_mat_); CHKERRQ(ierr);

                    if( !first_setup ) {
                        ierr = KSPSetFromOptions(KSP_diff); CHKERRQ(ierr);
                        ierr = KSPSetUp(KSP_diff); CHKERRQ(ierr);

                        first_setup = true;
                    }
                }
#ifndef PETSC_OLDAPI_DMDA // Old API fix (v. < ???)
//                ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRQ(ierr);
#endif
                KSPOUT(p_cm_, INTRADM, "Setting up diffusion KSP (eps = %f)"
                                       " [no compute op.]...\n", p_opt_->nu);
                p_profiler->begin_event("step_ksp_noop");
                ierr = KSPSolve(KSP_diff, new_w, w);
                p_profiler->end_event("step_ksp_noop");
            } else { // Use setup operator (e.g. for multigrid)
                p_diff_->rhs_vector = new_w;
                KSPOUT(p_cm_, INTRADM, "Setting up diffusion KSP (eps = %f)"
                                       " [compute op.]...\n", p_opt_->nu);

                // If we already have set up the system and the
                // matrix has changed, recompute
                if( first_setup && ksp_diff_mat_has_changed ) {
                    ierr = KSPSetComputeOperators(KSP_diff,
                                                  ComputeMatrixDiff,
                                                  this->p_diff_); CHKERRQ(ierr);
                }
                // If we never performed a setup
                if ( !first_setup ) { // Dont' use setup operator (e.g. for multigrid)
                    ierr = KSPSetComputeOperators(KSP_diff,
                                                  ComputeMatrixDiff,
                                                  this->p_diff_); CHKERRQ(ierr);
                    ierr = KSPSetComputeRHS(KSP_diff,
                                            ComputeRHSVortDiff,
                                            this->p_diff_); CHKERRQ(ierr);

                    ierr = KSPSetFromOptions(KSP_diff); CHKERRQ(ierr);
                    ierr = KSPSetUp(KSP_diff); CHKERRQ(ierr);
                    first_setup = true;
                }

                p_profiler->begin_event("step_ksp_op");
                ierr = KSPSolve(KSP_diff, NULL, NULL);
                p_profiler->end_event("step_ksp_op");

                ierr = KSPGetSolution(KSP_diff, &temp);
                ierr = VecCopy(temp, w); CHKERRQ(ierr);
            }
        }
    } else {
        ierr = VecCopy(new_w, w); CHKERRQ(ierr);
    }

    ierr = extract_velocity(w, U); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &new_w); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &c_F_b); CHKERRQ(ierr);

    p_profiler->end_event("step");
    PetscFunctionReturn(ierr);
}
