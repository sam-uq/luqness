/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "./vort_step.h"

//! Allocate temporary vectors
//!
VortStep::VortStep(Options *p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm,
                   Projection * p_proj, VortConv * p_evo)
    : Step (p_opt, p_dom, p_mod, p_cm, p_proj), p_evo_(p_evo)
{
    FUNCTIONBEGIN(p_cm_);

    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        if( p_opt_->diff_type == DiffType::Eddy || p_opt_->diff_type == DiffType::None  ) {
            p_diff_ = new VortDiff(p_opt, p_dom, p_mod, p_cm, p_proj, 0.5);
        } else if ( p_opt_->diff_type == DiffType::Roe ||
                    p_opt_->diff_type == DiffType::Rusanov ) {
            p_diff_ = new VarcoeffVortDiff(p_opt, p_dom, p_mod, p_cm, p_proj, 0.5);
        }

        Mat mat;

        ierr = p_proj_->get_trsp_mat(&mat); CHKERRV(ierr);

        ierr = p_diff_->set_diff_mat(mat); CHKERRV(ierr);

        // FIXME CFL like viscosity
        PetscReal h = 0.01;

        ierr = p_diff_->set_num_visc_scaling(h); CHKERRV(ierr);
    }
}

//! Deallocate buffers
//!
VortStep::~VortStep()
{
    FUNCTIONBEGIN(p_cm_);

    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        delete p_diff_;
    }
}
