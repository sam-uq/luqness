/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef FTCS_VORT_STEP_H
#define FTCS_VORT_STEP_H

//! @file ftcs_vort_step.h
//! Performs ONE actual step for the FTCS vortex method, calling all the necessary routines

#include "./vort_step.h"
#include "../tools/profiler.h"
#include "../transport/ftcs_vort_conv.h"

//! Class that provides a single step of the vortex method in 2D
//!
class FTCSVortStep : public VortStep {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    FTCSVortStep(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm,
                 Projection * p_proj, VortConv * evo_);
    //! Free some memory
    ~FTCSVortStep();

    //! Acutally performs a step as first order FTCS
    PetscErrorCode step(Vec w, Vec U, Vec F_b, Vec F);

protected:
    //! KSP for the solution of the diffusion at half time step in time
    KSP                   KSP_diff;

    //! First setup has been done
    bool                  first_setup;

    //! DM Clones, this is to prevent KSPSetOperaors to miserably fail FIXME at some point
    DM                    da_sc_clone;

    //! Flag to use set operators
    static const bool use_compute_operators = true;
};

#endif // FTCS_VORT_STEP_H
