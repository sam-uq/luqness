/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "./levy_tadmor_vort_step.h"

//! Allocate temporary vectors
//!
LevyTadmorVortStep::LevyTadmorVortStep(Options *p_opt, Domain *p_dom, ModelBase *p_mod, Attila *p_cm,
                                       Projection * p_proj, VortConv * p_evo)
    : FTCSVortStep (p_opt, p_dom, p_mod, p_cm, p_proj, p_evo)
    , first_setup_half_(false)
{
    FUNCTIONBEGIN(p_cm_);


    // If need ksp
    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        p_diff_half_ = new VortDiff(p_opt, p_dom, p_mod, p_cm, p_proj, 0.5, 0.5);

        Mat mat;
        ierr = p_proj_->get_trsp_mat(&mat); CHKERRV(ierr);
        ierr = p_diff_half_->set_diff_mat(mat); CHKERRV(ierr);
        // FIXME CFL like viscosity
        PetscReal h = 0.0;

        ierr = p_diff_half_->set_num_visc_scaling(h); CHKERRV(ierr);

        // FIXME: stupid hack
        ierr = DMClone(p_dom_->da_sc, &da_sc_clone_half_);

        ierr = KSPCreate(p_cm_->intra_domain_comm, &KSP_diff_half_); CHKERRV(ierr);
        //             ierr = KSPSetDMActive(KSP_diff, PETSC_FALSE); CHKERRV(ierr);
        if( use_compute_operators ) {
            ierr = KSPSetDM(KSP_diff_half_, da_sc_clone_half_); CHKERRV(ierr);
        }

        ierr = KSPSetOptionsPrefix(KSP_diff_half_,"kspdiff_"); CHKERRV(ierr);

        //             ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRV(ierr);
    }
}

//! Deallocate buffers
//!
LevyTadmorVortStep::~LevyTadmorVortStep()
{
    FUNCTIONBEGIN(p_cm_);

    // FIXME: destroy w/o viscosity
    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff) && !p_opt_->use_explicit ) {
        ierr = KSPDestroy(&KSP_diff_half_); CHKERRV(ierr);

        delete p_diff_half_;
    }

}

//! Performs a single step of the vortex method
//!
PetscErrorCode LevyTadmorVortStep::step(Vec w, Vec U, Vec F_b, Vec F)
{
    FUNCTIONBEGIN(p_cm_);

    Vec       w_x, w_y, new_w, stag_w;
    Vec       c_F_b, c_F, a_c_F;
    Vec       new_U; // Contains velocity reconstruction
    Vec       w_avg; // contains averaged old velocity to staggered grid (for diffusion)
    Vec       temp; // Unpreallocated vector for GeTSolution

    p_profiler->begin_event("step");

    //// FIRST PHASE: vorticity half time evolution
    ierr = DMGetGlobalVector(p_dom_->da_sc, &w_x); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &w_y); CHKERRQ(ierr);

    ierr = p_evo_->reconstruct(w, w_x, w_y); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da_sc, &c_F_b); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &c_F); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &a_c_F); CHKERRQ(ierr);

    ierr = p_proj_->curl(F_b, c_F_b, p_dom_->bd_fam.U); CHKERRQ(ierr);
    ierr = p_proj_->curl(F, c_F, p_dom_->bd_fam.U); CHKERRQ(ierr);
    if( p_evo_->offset_idx == 0 ) {
        ierr = p_dom_->avg<true, 1>(c_F, a_c_F); CHKERRQ(ierr);
        ierr = VecCopy(a_c_F, c_F);
    }

    ierr = DMGetGlobalVector(p_dom_->da_sc, &new_w); CHKERRQ(ierr);

    ierr = p_evo_->predict<2>(w, w_x, w_y, U, c_F_b, new_w); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &c_F_b); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &a_c_F); CHKERRQ(ierr);

    //// FIRST PHASE part 2: vorticity half time add diffusion if any
    // TODO: recycle this onto diff?
    // If need to apply diffusion
    if( !p_opt_->use_euler || p_opt_->enable_numerical_diff ) {
        // Create the matrix and ksp
        if( p_opt_->use_explicit ) { // Explicit diffusion (TODO: NOT! CHECKED)
            // TODO
            assert(false &&  "No explicit diffusion!");
        } else { // Implicit
            ierr = p_diff_half_->vec_add_diff(w, new_w);
            // Create the matrix L containing I - A = L for the solution of the laplacian problem
            bool ksp_diff_mat_has_changed = p_diff_half_->diff_mat_has_changed();

            if ( !use_compute_operators ) { // Dont' use setup operator (use matrix directly)
                // Matrix
                if( !first_setup_half_ || ksp_diff_mat_has_changed ) {
                    p_diff_half_->update_shifted_matrix();

                    ierr = SetOperators(KSP_diff_half_,
                                        p_diff_half_->shift_diff_mat_,
                                        p_diff_half_->shift_diff_mat_); CHKERRQ(ierr);

                    if( !first_setup_half_ ) {

                        ierr = KSPSetFromOptions(KSP_diff_half_); CHKERRQ(ierr);
                        ierr = KSPSetUp(KSP_diff_half_); CHKERRQ(ierr);

                        first_setup_half_ = true;
                    }
                }
#ifndef PETSC_OLDAPI_DMDA // Old API fix (v. < ???)
//                ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRQ(ierr);
#endif
                KSPOUT(p_cm_, INTRADM, "(1st diff, nu = %f) [no compute op.]...\n", p_opt_->nu);
                p_profiler->begin_event("step_ksp");
                ierr = KSPSolve(KSP_diff_half_, new_w, new_w);
                p_profiler->end_event("step_ksp");
            } else { // Use setup operator (e.g. for multigrid)
                p_diff_half_->rhs_vector = new_w;
                KSPOUT(p_cm_, INTRADM, "(1st diff, nu = %f) [compute op.]...\n", p_opt_->nu);

                // If we already have set up the system and the matrix has changed, recompute
                if( first_setup_half_ && ksp_diff_mat_has_changed ) {
                    ierr = KSPSetComputeOperators(KSP_diff_half_,
                                                  ComputeMatrixDiff,
                                                  this->p_diff_half_); CHKERRQ(ierr);
                }
                // If we never performed a setup
                if ( !first_setup_half_ ) { // Dont' use setup operator (e.g. for multigrid)
                    ierr = KSPSetComputeOperators(KSP_diff_half_,
                                                  ComputeMatrixDiff,
                                                  this->p_diff_half_); CHKERRQ(ierr);
                    ierr = KSPSetComputeRHS(KSP_diff_half_, ComputeRHSVortDiff,
                                            this->p_diff_half_); CHKERRQ(ierr);

                    ierr = KSPSetFromOptions(KSP_diff_half_); CHKERRQ(ierr);
                    ierr = KSPSetUp(KSP_diff_half_); CHKERRQ(ierr);
                    first_setup_half_ = true;
                }

                p_profiler->begin_event("step_ksp");
                ierr = KSPSolve(KSP_diff_half_, NULL, NULL);
                p_profiler->end_event("step_ksp");

                ierr = KSPGetSolution(KSP_diff_half_, &temp);
                ierr = VecCopy(temp, new_w); CHKERRQ(ierr);
            }
        }
    }

    //// SECOND PHASE: vorticity new time evolution
    // Continue
    ierr = DMGetGlobalVector(p_dom_->da_sc, &stag_w); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &new_U); CHKERRQ(ierr);

    ierr = extract_velocity(new_w, new_U); CHKERRQ(ierr);
    ierr = p_evo_->conv(w, w_x, w_y, new_w, new_U, c_F, stag_w); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da, &new_U); CHKERRQ(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w_x); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w_y); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &new_w); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &c_F); CHKERRQ(ierr);

    //// SECOND PHASE part 2: vorticity new time add diffusion if any
    if( !p_opt_->use_euler || p_opt_->enable_numerical_diff ) {
        // Create the matrix and ksp
        if( p_opt_->use_explicit ) { // Explicit diffusion (TODO: NOT! CHECKED)
            ierr = DMGetGlobalVector(p_dom_->da_sc, &w_avg); CHKERRQ(ierr);
            ierr = p_dom_->avg<true, 1>(w, w_avg); CHKERRQ(ierr);
            ierr = p_diff_->vec_add_diff(w_avg, stag_w);
            ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w_avg); CHKERRQ(ierr);
        } else { // Implicit
            ierr = DMGetGlobalVector(p_dom_->da_sc, &w_avg); CHKERRQ(ierr);
            ierr = p_dom_->avg<true, 1>(w, w_avg); CHKERRQ(ierr);
            ierr = p_diff_->vec_add_diff(w_avg, stag_w);
            ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w_avg); CHKERRQ(ierr);
            // Create the matrix L containing I - A = L for the solution of the laplacian problem
            bool ksp_diff_mat_has_changed = p_diff_->diff_mat_has_changed();

            if ( !use_compute_operators ) { // Dont' use setup operator (use matrix directly)
                // Matrix
                if( !first_setup || ksp_diff_mat_has_changed ) {
                    p_diff_->update_shifted_matrix();

                    ierr = SetOperators(KSP_diff, p_diff_->shift_diff_mat_,
                                        p_diff_->shift_diff_mat_); CHKERRQ(ierr);

                    if( !first_setup ) {

                        ierr = KSPSetFromOptions(KSP_diff); CHKERRQ(ierr);
                        ierr = KSPSetUp(KSP_diff); CHKERRQ(ierr);

                        first_setup = true;
                    }
                }
#ifndef PETSC_OLDAPI_DMDA // Old API fix (v. < ???)
//                ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRQ(ierr);
#endif
                KSPOUT(p_cm_, INTRADM,
                       "KSP 2nd diff. (nu = %f) [no compute op.]...\n",
                       p_opt_->nu);
                p_profiler->begin_event("step_ksp");
                ierr = KSPSolve(KSP_diff, stag_w, w);
                p_profiler->end_event("step_ksp");
            } else { // Use setup operator (e.g. for multigrid)
                p_diff_->rhs_vector = stag_w;
                KSPOUT(p_cm_, INTRADM,
                       "KSP 2nd diff. (nu = %f) [compute op.]...\n",
                       p_opt_->nu);

                // If we already have set up the system and the matrix has changed, recompute
                if( first_setup && ksp_diff_mat_has_changed ) {
                    ierr = KSPSetComputeOperators(KSP_diff, ComputeMatrixDiff,
                                                  this->p_diff_); CHKERRQ(ierr);
                }
                // If we never performed a setup
                if ( !first_setup ) { // Dont' use setup operator (e.g. for multigrid)
                    ierr = KSPSetComputeOperators(KSP_diff, ComputeMatrixDiff,
                                                  this->p_diff_); CHKERRQ(ierr);
                    ierr = KSPSetComputeRHS(KSP_diff, ComputeRHSVortDiff, this->p_diff_); CHKERRQ(ierr);

                    ierr = KSPSetFromOptions(KSP_diff); CHKERRQ(ierr);
                    ierr = KSPSetUp(KSP_diff); CHKERRQ(ierr);
                    first_setup = true;
                }

                p_profiler->begin_event("step_ksp");
                ierr = KSPSolve(KSP_diff, NULL, NULL);
                p_profiler->end_event("step_ksp");

                ierr = KSPGetSolution(KSP_diff, &temp);
                ierr = VecCopy(temp, w); CHKERRQ(ierr);
            }
        }
    } else {
        ierr = VecCopy(stag_w, w); CHKERRQ(ierr);
    }

    //// END PHASE: cleanup, reconstruct and exit
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &stag_w); CHKERRQ(ierr);

    ierr = extract_velocity(w, U); CHKERRQ(ierr);

    p_evo_->offset_idx == 0 ? p_evo_->offset_idx = 1 : p_evo_->offset_idx = 0;

    p_profiler->end_event("step");
    PetscFunctionReturn(ierr);
}
