/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file proj_step.h
//! @brief A single step of the Projection method. Abstact base subclassed for various steppings.
//! Performs ONE actual step for the projection method, calling all the necessary routines.
//! The Projection method is the one found in [BCG], with slight modifications from [Alm].
//! Will be called by a solver many times. Internally recycles matrices and DMs.

// Internal includes
#include "./step.h"
#include "../transport/conv.h"
#include "../diff/proj_diff.h"

//! This class implements a single timestep for the projection method, calling the relevant routines.
//! Automatically handles diffusion, convection a and projection.
class ProjStep : public Step {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    ProjStep(Options * p_opt, Domain * p_dom,
             ModelBase * p_mod,
             Attila * p_cm, Projection * p_proj,
             Solver * p_parent_solver = nullptr);
    //! Free some space (destroy KSP and matrices)
    virtual ~ProjStep();
    //! The actual step called each iteration, abstract base
    virtual PetscErrorCode step(Vec U, Vec G, Vec F_b, Vec F) = 0;

    //! Diffusion handler (numerical & NS)
    ProjDiff * p_diff_ = nullptr;

    //// Diffusion stuff
    //! Domain copy for diffusion (numerical & NS).
    DM          da_sc_clone;
    //! KSP for the diffusion (numerical and physical).
    KSP         KSP_diff;
    //! Setup flags for the diffusion KSP/DM pair.
    bool        use_compute_operators, has_been_setted_up;
};
