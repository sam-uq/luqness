/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef ACC_PROJ_STEP_H
#define ACC_PROJ_STEP_H

//! @file acc_proj_step.h
//! Performs ONE actual step for the projection method, calling all the necessary routines

// Internal includes
#include "./proj_step.h"

//! This class implements A single timestep for the projection method, calling the relevant routines
//!
class AccProjStep : public ProjStep {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    AccProjStep(Options *p_opt, Domain * p_dom, ModelBase *p_mod, Attila *p_cm, Projection *p_proj, Conv *p_conv)
        : ProjStep(p_opt, p_dom, p_mod, p_cm, p_proj), p_conv_(p_conv) {}
    //! Free some space
    ~AccProjStep() {}

    //! The actual step called each iteration
    PetscErrorCode step(Vec U, Vec G, Vec F_b, Vec F);

protected:
    Conv            * p_conv_;

};

#endif // ACC_PROJ_STEP_H
