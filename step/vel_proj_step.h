/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file vel_proj_step.h
//! Performs ONE actual step for the projection method, calling all the necessary routines

// Internal includes
#include "./proj_step.h"

//! This class implements A single timestep for the projection method, calling the relevant routines
//! Stepper is implemented a-là BCG algorithm
class VelProjStep : public ProjStep {
public:
    //! \brief VelProjStep::VelProjStep Created a new KSP context if we choose to use a NS solver.
    //!
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    //! \param p_opt
    //! \param p_dom
    //! \param p_mod
    //! \param p_cm
    //! \param p_proj
    //! \param p_conv
    //!
    VelProjStep(Options *p_opt, Domain * p_dom,
                ModelBase *p_mod, Attila *p_cm, Projection *p_proj, Conv *p_conv);
    //!
    //! \brief VelProjStep::~VelProjStep
    //!
    //! Free some space
    //!
    ~VelProjStep();

    //!
    //! \brief VelProjStep::step Perform a single step for the method.
    //!
    //! Applies convection, diffusion and projection.
    //! The actual step called each iteration.
    //! \param U Velocity at current step.
    //! \param G Pressure at current step.
    //! \param F_b Forcing for current step (can be null).
    //! \param F Forcing at next step (can be null).
    //! \return Error code.
    //!
    PetscErrorCode step(Vec U, Vec G, Vec F_b, Vec F);
protected:
    Conv            * p_conv_;
};
