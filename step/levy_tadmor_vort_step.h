/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef LEVY_TADMOR_VORT_STEP_H
#define LEVY_TADMOR_VORT_STEP_H

//! @file levy_tadmor_vort_step.h
//! Performs ONE actual step for the LevyTadmor vortex method, calling all the necessary routines

#include "ftcs_vort_step.h"
#include "../tools/profiler.h"
#include "../transport/vort_conv.h"

#include "../diff/vort_diff.h"

//! Class that provides a single step of the vortex method in 2D
class LevyTadmorVortStep : public FTCSVortStep
{
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    LevyTadmorVortStep(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm,
                       Projection * p_proj, VortConv * evo_);
    //! Free some memory
    ~LevyTadmorVortStep();

    //! Acutally performs a step
    virtual PetscErrorCode step(Vec w, Vec U, const Vec F_b, const Vec F);

protected:
    //! KSP for the first diffusion application
    KSP KSP_diff_half_;

    //! First setup has been done for the first diffusion application
    bool                  first_setup_half_;

    //! DM Clones, this is to prevent KSPSetOperaors to miserably fail FIXME at some point (half)
    DM                    da_sc_clone_half_;

    //! Diffusion handler for half
    VortDiff * p_diff_half_;
};

#endif // LEVY_TADMOR_VORT_STEP_H
