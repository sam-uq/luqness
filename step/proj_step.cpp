/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "proj_step.h"

//! Created a new KSP context for Navier-Stokes if we choose to use a NS solver.
//!
ProjStep::ProjStep(Options *p_opt, Domain * p_dom,
                   ModelBase *p_mod, Attila *p_cm,
                   Projection *p_proj, Solver * p_parent_solver)
    : Step(p_opt, p_dom, p_mod, p_cm, p_proj, p_parent_solver)
    , has_been_setted_up(false) {
    FUNCTIONBEGIN(p_cm_);

    // If need ksp context
    // Compute operators is called by some MG setup to
    // construct coarse operators
    use_compute_operators = false;
    // If we use NS or numerical diffusion and we do not use explicit diffusion
    // i.e. if we need to solve a system
    if( ( !p_opt_->use_euler || p_opt_->enable_numerical_diff)
            && !p_opt_->use_explicit ) {
        // FIXME: stupid hack, need to create an extra
        // diffusion handler and destroy.
        p_diff_ = new ProjDiff(p_opt, p_dom, p_mod, p_cm, p_proj, 1); // 1 BE, 0.5 CN and 0 FE

        Mat mat;
        ierr = p_proj_->get_trsp_mat(&mat); CHKERRV(ierr);
        ierr = p_diff_->set_diff_mat(mat); CHKERRV(ierr);

        ierr = DMClone(p_dom_->da_sc, &da_sc_clone);
        ierr = KSPCreate(p_cm_->intra_domain_comm, &KSP_diff); CHKERRV(ierr);
//        ierr = KSPSetDMActive(KSP_diff, PETSC_FALSE); CHKERRV(ierr);
        if( use_compute_operators ) {
            ierr = KSPSetDM(KSP_diff, da_sc_clone); CHKERRV(ierr);
        }

        ierr = KSPSetOptionsPrefix(KSP_diff, "kspdiff_"); CHKERRV(ierr);

//        ierr = KSPSetReusePreconditioner(KSP_diff, PETSC_TRUE); CHKERRV(ierr);
        ierr = p_diff_->set_num_visc_scaling(p_opt_->num_visc); CHKERRV(ierr);
    } else if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff)
        && p_opt_->use_explicit) {
        assert(false && "Explicit diffusion not implemented.");
    }
}

ProjStep::~ProjStep() {
    FUNCTIONBEGIN(p_cm_);

    if( (!p_opt_->use_euler || p_opt_->enable_numerical_diff)
            && !p_opt_->use_explicit ) {
        ierr = KSPDestroy(&KSP_diff); CHKERRV(ierr);
        if(p_diff_ == nullptr) {
            WARNING(p_cm_, INTRADM,
                    "Pointer was null when trying to "
                    "delete Diffusion... Something wrong?\n" , "");
        }
        else delete p_diff_;
    }
}
