/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "lf_proj_step.h"

//! Performs a single step using a Leap-Frog timestepping:
//! \f$ y^{n+1} - y^{n-1} = F(y^n) \f$.
//! For stability reasons is advisable to restart the integration using forward Euler every so often (with the
//! parameter \ref restart_steps)
PetscErrorCode LFProjStep::step(Vec U, Vec G, Vec F_b, Vec F)
{

    Vec       U_conv, Q, Lpl;

    ierr = DMGetGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    p_profiler->push_stage("convection");
        ierr = p_conv_->conv(U,  G,  F_b, Lpl, U_conv); CHKERRQ(ierr); // U_conv now contains the convection of U
    p_profiler->pop_stage();

    ierr = DMRestoreGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPBY(U_conv, +1., +1., G); CHKERRQ(ierr); // U_conv contains the argument to the projection
    }
    ierr = VecAXPBY(U_conv, 1., -1., F); CHKERRQ(ierr); // U_conv contains the argument to the projection

    // U_conv containst the argument to the projection
    float mult_dt = 1.;
    if(has_old_U && steps++ % 10 != 0) {
        ierr = VecAXPBY(U_conv, 1., p_mod_->dt*2, old_U); CHKERRQ(ierr);
        mult_dt = 2.;
    } else {
        ierr = VecAXPBY(U_conv, 1., p_mod_->dt, U); CHKERRQ(ierr);
        has_old_U = true;
    }
    if(has_old_U) {
        ierr = p_parent_solver_->push_energy(U, 1, old_U); CHKERRQ(ierr);
    }
    ierr = VecCopy(U, old_U); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);


    p_profiler->push_stage("projection");
        ierr = p_proj_->projection(U_conv, U, Q); CHKERRQ(ierr); // Project P_local, get U_x (P(arg) and U_y Q(arg))
    p_profiler->pop_stage();

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);

    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPY(G, 1. / (mult_dt * p_mod_->dt), Q); CHKERRQ(ierr); // G now contains the new step of U
    } else { // if DirectPressure
        ierr = VecCopy(Q, G); CHKERRQ(ierr); // G now contains the new step of U
        ierr = VecScale(G, 1. / (mult_dt * p_mod_->dt)); CHKERRQ(ierr); // G now contains the new step of U
    }

    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    return 0;
}
