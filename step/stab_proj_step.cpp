/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "stab_proj_step.h"

StabProjStep::StabProjStep(Options *p_opt, Domain *p_dom,
                           ModelBase *p_mod, Attila *p_cm,
                           Projection *p_proj, Conv *p_conv,
                           Solver *p_parent_solver)
        : ProjStep(p_opt, p_dom, p_mod, p_cm, p_proj, p_parent_solver)
          , conv(p_conv) {

    FUNCTIONBEGIN(p_cm_);
    dynamic_cast<StabConv *>(conv)->preallocate();
}

StabProjStep::~StabProjStep() {
    FUNCTIONBEGIN(p_cm_);
}

PetscErrorCode StabProjStep::step(Vec U, Vec G, Vec, Vec) {
    FUNCTIONBEGIN(p_cm_);

    Vec Q; // pressure/gradient-part of Hodge--Helmholtz
    Vec Rhs; // right hand side (solution of system)
    Vec Rhs_sc; // scalar right hand side (solution of system)
    Vec U_next_sc; // deinterlaced components of U (scalar)

    StabConv *stab_conv = dynamic_cast<StabConv *>(conv);

    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &U_next_sc); CHKERRQ(ierr);

    // Setup matrix
    DEBUG(p_cm_, INTRADM, "Buildin' system matrix...\n", "");
    stab_conv->fill(U);
    // TODO: check how this behaves with Navier-Stokes

    // Add diffusion, transport and timestepping matrices
    // Compute C -= h * dt * D if diffusion was required
    if(p_diff_ != nullptr) {
        MatAXPY(stab_conv->conv_mat, -p_diff_->get_numerical_viscosity_coefficient(),
                p_diff_->diff_mat_, SAME_NONZERO_PATTERN);
    } else {
        WARNING(p_cm_, INTRADM, "Diffusion was NULL, so no diffusion was added. Is this wanted?", "");
    }
    // Compute C += Id
    MatShift(stab_conv->conv_mat, 1.);

    // Setup KSP
    DEBUG(p_cm_, INTRADM, "Building KSP...\n", "");
    KSP ksp;
    ierr = KSPCreate(p_cm_->intra_domain_comm, &ksp); CHKERRQ(ierr);

    ierr = SetOperators(ksp, stab_conv->conv_mat, stab_conv->conv_mat);
    CHKERRQ(ierr);
    ierr = KSPSetDM(ksp, p_dom_->da_sc); CHKERRQ(ierr);
    ierr = KSPSetDMActive(ksp, PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

    DEBUG(p_cm_, INTRADM, "Building RHS...\n", "");
    switch(p_opt_->pressupd_type) {
        // Pressure update is additive we need to add the gradient to the r.h.s.
        // U -= dt * G
        case PressureUpdateType::IncrementalPressure:
            ierr = VecAXPY(U, -p_mod_->dt, G); CHKERRQ(ierr);
            break;
        // If pressure update is direct pressure we do not have to do anything
        case PressureUpdateType::DirectPressure:
            break;
    }
    // U now contains the new step of U

    // Actually solve the system C*U = Rhs (component-wise)
    DEBUG(p_cm_, INTRADM, "%sSolving system...\n", "");
    VecStrideGather(U, 0, Rhs_sc, INSERT_VALUES);
    ierr = KSPSolve(ksp, Rhs_sc, U_next_sc); CHKERRQ(ierr);
    VecStrideScatter(U_next_sc ,0, Rhs, INSERT_VALUES);
    VecStrideGather(U, 1, Rhs_sc, INSERT_VALUES);
    ierr = KSPSolve(ksp, Rhs_sc, U_next_sc); CHKERRQ(ierr);
    VecStrideScatter(U_next_sc ,1, Rhs, INSERT_VALUES);

    // Destroy KSP
    DEBUG(p_cm_, INTRADM, "Cleaning system...\n", "");
    ierr = KSPDestroy(&ksp); CHKERRQ(ierr);

    // Projection step
    // WARNING: U is already U at n+1
    ierr = p_proj_->projection(Rhs, U, Q); CHKERRQ(ierr);
    // Project P_local, get U = P(arg) and \grad p = Q(arg))

    switch(p_opt_->pressupd_type) {
        // Pressure update is additive
        case PressureUpdateType::IncrementalPressure:
            ierr = VecAXPY(G, 1. / p_mod_->dt, Q); CHKERRQ(ierr);
            break;
        // Pressure update is projection gradient part
        case PressureUpdateType::DirectPressure:
            ierr = VecCopy(Q, G); CHKERRQ(ierr);
            break;
    }
    // G now contains the new step of G

    // Cleanup
    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &U_next_sc); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
