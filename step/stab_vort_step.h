/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file stab_vort_step.h
//!

#include "./vort_step.h"
#include "../diff/varcoeff_vort_diff.h"
#include "../transport/stab_vort_conv.h"

//!

//!
//! \brief The StabVortStep class provides a single step for a stable vortex formulation.
//!
class StabVortStep : public VortStep {
public:
    //!
    StabVortStep(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm,
                 Projection * p_proj, VortConv * evo_);
    //!
    ~StabVortStep();

    //!
    PetscErrorCode step(Vec w, Vec U, Vec F_b, Vec F);

    //!
    PetscErrorCode extract_velocity(Vec w, Vec U);

private:
    //! Scalar r.h.s vector as internal buffer.
    Vec rhs_sc;
    //! Internal solver for non-linear problem.
    KSP ksp;

};
