/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef VORT_STEP_H
#define VORT_STEP_H

//! @file vort_step.h
//! Performs ONE actual step for the vortex method, calling all the necessary routines

// TODO 3d

#include "./step.h"
#include "../tools/profiler.h"
#include "../transport/vort_conv.h"

#include "../diff/vort_diff.h"
#include "../diff/varcoeff_vort_diff.h"

//! Class that provides a single step of the vortex method in 2D
//!
class VortStep : public Step
{
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    VortStep(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm,
             Projection * p_proj, VortConv * p_evo);
    //! Free some memory
    ~VortStep();

    //! Acutally performs a step
    virtual PetscErrorCode step(Vec w, Vec U, const Vec F_b, const Vec F) = 0;

    //! The transport evolver for the scheme
    VortConv     * const p_evo_;

protected:

    //! Diffusion handler (numerical & NS)
    VortDiff * p_diff_;
};

#endif //VORT_STEP_H
