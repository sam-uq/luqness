/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "acc_proj_step.h"

//! perform a single step for the method, this step projects the derivative of the velocity as in BCG
//!
PetscErrorCode AccProjStep::step(Vec U, Vec G, Vec F_b, Vec F) {
    FUNCTIONBEGIN(p_cm_);

    Vec  U_conv, Q, P, Lpl;

    ierr = DMGetGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    ierr = p_conv_->conv(U,  G,  F_b, Lpl, U_conv); CHKERRQ(ierr);
    // U_conv now contains the convection of U

    ierr = DMRestoreGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);

    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPBY(U_conv, +1., +1., G); CHKERRQ(ierr); // U_conv contains the argument to the projection
    }
    ierr = VecAXPBY(U_conv, 1., -1., F); CHKERRQ(ierr); // U_conv contains the argument to the projection

    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &P); CHKERRQ(ierr);

    ierr = p_proj_->projection(U_conv, P, Q); CHKERRQ(ierr);
    // Project P_local, get U_x (P(arg) and U_y Q(arg))

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_conv); CHKERRQ(ierr);

    ierr = VecAXPY(U, p_mod_->dt, P); CHKERRQ(ierr); // U now contains the new step of U
    if( p_opt_->pressupd_type == PressureUpdateType::IncrementalPressure ) {
        ierr = VecAXPY(G, 1.0, Q); CHKERRQ(ierr); // G now contains the new step of U
    } else { // if DirectPressure
        ierr = VecCopy(Q, G); CHKERRQ(ierr); // G now contains the new step of U
    }

    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &P); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
