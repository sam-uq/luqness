/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file stab_proj_step.h
//! Contains a class that performs ONE actual step for the
//! projection method using the stable projection
//! method of \cite ProjAMVS. A single step of the methods
//! is performed via a call to the step function.

#include "./proj_step.h"

#include "../transport/stab_conv.h"

//! This class implements a single timestep for the projection method of \cite{ProjAMVS},
//! in the variant \f$\theta = 1\f$
//! (i.e. the pure Backward Euler time stepping). The Crank-Nicholson time stepping (\f$\theta = 1/2\f$)
//! is found in the class
//! CNStabProjStep.
//! The constructor preallocates the memory necessary for the step, whilst the step function
//! performs a single step form \f$t^2\f$ to \f$t^{n+1}\f$.
//!
class StabProjStep : public ProjStep {
public:
    //! Constructor, allocate space for temporary vectors,
    //! solution vectors and for Laplacian matrix.
    StabProjStep(Options *p_opt, Domain * p_dom, ModelBase *p_mod, Attila *p_cm,
                 Projection *p_proj, Conv *p_conv, Solver * p_parent_solver);

    //! Free some space (empty).
    ~StabProjStep();

    //! \brief The actual step called each iteration.
    //!
    //! The actual step depends on whether the pressure update has been selected to
    //! be additive or not.
    //! If "p_opt_->pressupd_type" is
    //! PressureUpdateType::IncrementalPressure:
    //! \f[\frac{u^{*,n+1} - u^n}{\Delta t} + C(u^n, u^{*,n+1}) + \nabla p^{n-1/2} = Du^{*,n+1}\f]
    //! \f[u^{n+1}= P(u^{*,n+1})\f]
    //! \f[\nabla p^{n+1/2}  = \nabla p^{n-1/2} + Q(u^{*,n+1}) / \Delta t\f]
    //!
    //! If "p_opt_->pressupd_type" is
    //! PressureUpdateType::DirectPressure:
    //! \f[\frac{u^{*,n+1} - u^n}{\Delta t} + C(u^n, u^{*,n+1}) = Du^{*,n+1}\f]
    //! \f[u^{n+1}= P(u^{*,n+1})\f]
    //! \f[\nabla q^{n+1/2} = Q(u^{*,n+1})\f]
    //!
    //! \param[in,out]    U    Velocity at \f$U^{n}\f$ in, velocity at \f$U^{n+1}\f$ out.
    //! \param[in,out]    G    grad-pressureat \f$Gp^{n}\f$ in, Grad-pressure at \f$Gp^{n+1}\f$ out.
    //! \param[in]        F_b  Forcing at \f$t^n\f$ (not implemented).
    //! \param[in]        F    Forcing at \f$t^{n+1}\f$ (not implemented).
    PetscErrorCode step(Vec U, Vec G, Vec F_b, Vec F);

protected:
    //! Handle for the non-linear step (not owned).
    Conv            * conv;
};
