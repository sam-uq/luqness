/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file step.h
//! Abstract base class for the implementation of a single step of a scheme

class Step;

#include "../tools/tools.h"
#include "../tools/maths.h"
#include "../tools/profiler.h"
#include "../projection/projection.h"
#include "../projection/projection_laplacian.h"

#include "../solver/solver.h"

//! VIRTUAL class holding stepping informations for a general scheme
//!
class Step {
public:
    //! Constructor, allocate space for temporary vectors, copy options, dmda and projection operator
    Step(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm, Projection * p_proj,
         Solver * p_parent_solver = NULL);
    //! Virtual deconstructor that is overidden by derived class
    virtual ~Step();

    //! Implements a CFL condition for a general scheme (virutal)
    //! CFL condition, modify opt, writing the right opt->dt
    virtual PetscErrorCode CFL(Vec U);

    //! Performs a single step of passive advection
    //! General transport of quantity along velocity U (implements also timestepping)
    PetscErrorCode transport(Vec quantity, Vec U);

    //! Perform a "divergence cleansing"
    PetscErrorCode div_cleansing(Vec U, Vec G);

    //!
    virtual PetscErrorCode extract_velocity(Vec w, Vec U);

    Options               * const p_opt_; // Options
    Domain                * const p_dom_;
    ModelBase             * const p_mod_;
    Attila                * const p_cm_;
    // A projection operator to perform div cleansing, also needed for laplacian for transport
    Projection            * const p_proj_;

    Solver                * p_parent_solver_;
protected:
};

