/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "cn_stab_proj_step.h"

CNStabProjStep::CNStabProjStep(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm,
                               Projection * p_proj, Conv * p_conv, Solver * p_parent_solver)
    : ProjStep (p_opt, p_dom, p_mod, p_cm, p_proj, p_parent_solver), conv(p_conv) {
    FUNCTIONBEGIN(p_cm_);

    static_cast<StabConv*>(conv)->preallocate();
    conv2 = new BCGConv(p_opt_, p_dom_, p_mod_, p_cm_);
}

PetscErrorCode CNStabProjStep::step(Vec U, Vec G, Vec, Vec F) {
    FUNCTIONBEGIN(p_cm_);

    int energy_idx = 0;

    // U_next is used to contain the interpolated U^(n+1) (temporarily) and also the approximation U^*
    // Q is used to contain the anti-projection, and share the same buffer as RHs, which is the Rhs of the system
    Vec U_next, Q, Rhs;
//     Vec D, Lpl;
//     Vec U_x, U_y, U_z, U_no_x, U_no_y, U_no_z;

//     Vec U_local, U_LR_local, U_BT_local, U_DU_local;
//     Vec U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_edg_D, U_edg_U;
    Vec Rhs_sc, U_sc;

    /////////////////// INTERPOLATION
    /////////////////// ///////////////////////////
    /////////////////// ///////////////////////////
    /////////////////// ///////////////////////////
    /////////////////// ///////////////////////////
    /////////////////// ///////////////////////////
    // FIXME: this is mostly copied from conv BCG and we may need to improve that, especially remove some code andshare
    // some more buffer
    // FIXME: apply MAC projection AND DIFFUSION???? (for 2nd order???)???)!!!?!?!?!

//     DEBUG(p_cm_, INTRADM, "Interpolating.\n", "");
//
//     ierr = DMGetLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);
//
//     // We localize U to get access to the latest boundary informations
//     ierr = DMGlobalToLocalBegin(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
//     ierr = DMGlobalToLocalEnd(p_dom_->da, U, INSERT_VALUES, U_local); CHKERRQ(ierr);
//     // ...and update boundarries
//     ierr = p_mod_->update_bd_field(U_local, p_dom_->bd_fam.U, p_mod_->t); CHKERRQ(ierr);
//
//     // MC
//     ierr = DMGetGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMGetGlobalVector(p_dom_->da, &U_z); CHKERRQ(ierr);
//     }
//
//     static_cast<BCGConv*>(conv2)->mc_diff(U_local, U_x, U_y, U_z);
//
//     ierr = DMGetGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMGetGlobalVector(p_dom_->da, &U_no_z); CHKERRQ(ierr);
//     }
//
//     // Tr dv
//     static_cast<BCGConv*>(conv2)->trv_diff(U_local, U_x, U_y, U_z, U_no_x, U_no_y, U_no_z);
//
//     ierr = DMRestoreLocalVector(p_dom_->da, &U_local); CHKERRQ(ierr);
//
//     // Interpolate to edge, to next time step
//     ierr = DMGetGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMGetGlobalVector(p_dom_->da, &U_edg_D); CHKERRQ(ierr);
//         ierr = DMGetGlobalVector(p_dom_->da, &U_edg_U); CHKERRQ(ierr);
//     }
//
//     ierr = DMGetGlobalVector(p_dom_->da, &D); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);
//
//     static_cast<BCGConv*>(conv2)->interp(U, G, F, D, Lpl, U_x, U_y, U_z, U_no_x, U_no_y, U_no_z,  U_edg_L,
//                                          U_edg_R, U_edg_B, U_edg_T, U_edg_D, U_edg_U);
//
//     ierr = DMRestoreGlobalVector(p_dom_->da, &D); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &Lpl); CHKERRQ(ierr);
//
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_x); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_y); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMRestoreGlobalVector(p_dom_->da, &U_z); CHKERRQ(ierr);
//     }
//
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_x); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_y); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMRestoreGlobalVector(p_dom_->da, &U_no_z); CHKERRQ(ierr);
//     }
//
//
//
//     ierr = DMGetLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
//     ierr = DMGetLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMGetLocalVector(p_dom_->da, &U_DU_local); CHKERRQ(ierr);
//     }
//
//     // Upwind data
//     static_cast<BCGConv*>(conv2)->upwind(U_edg_L, U_edg_R, U_edg_B, U_edg_T, U_edg_D, U_edg_U, U_LR_local,
//                                          U_BT_local, U_DU_local);
//
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_L); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_R); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_B); CHKERRQ(ierr);
//     ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_T); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_D); CHKERRQ(ierr);
//         ierr = DMRestoreGlobalVector(p_dom_->da, &U_edg_U); CHKERRQ(ierr);
//     }
//
//     ierr = DMGetGlobalVector(p_dom_->da, &U_next); CHKERRQ(ierr);
//
//     // Compute convective derivative
//     Field2D       ** U_LR_array, ** U_BT_array, ** U_conv_array;
//     ierr = DMDAVecGetArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
//     ierr = DMDAVecGetArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
//     ierr = DMDAVecGetArray(p_dom_->da, U_next, &U_conv_array); CHKERRQ(ierr);
//     for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
//         for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
//             U_conv_array[j][i][0] = 0.5 * (U_LR_array[j][i+1][0] + U_LR_array[j][i][0]);
//             U_conv_array[j][i][1] = 0.5 * (U_LR_array[j][i+1][1] + U_LR_array[j][i][1]);
//         }
//     }
//     ierr = DMDAVecRestoreArray(p_dom_->da, U_LR_local, &U_LR_array); CHKERRQ(ierr);
//     ierr = DMDAVecRestoreArray(p_dom_->da, U_BT_local, &U_BT_array); CHKERRQ(ierr);
//     ierr = DMDAVecRestoreArray(p_dom_->da, U_next, &U_conv_array); CHKERRQ(ierr);
//
//
//     ierr = DMRestoreLocalVector(p_dom_->da, &U_LR_local); CHKERRQ(ierr);
//     ierr = DMRestoreLocalVector(p_dom_->da, &U_BT_local); CHKERRQ(ierr);
//     if( p_opt_->enable_3d ) {
//         ierr = DMRestoreLocalVector(p_dom_->da, &U_DU_local); CHKERRQ(ierr);
//     }


    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////

    // Setup matrix
    DEBUG(p_cm_, INTRADM, "[STP] Buildin' first system matrix (CN).\n", "");

    static_cast<StabConv*>(conv)->fill(U);

    ierr = MatScale(static_cast<StabConv*>(conv)->conv_mat, 0.25); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);

    //     Vec b; (RHS): Rhs = U^2 + C(U^n) + h L U^n - dt Gp = CMat * U^n - dt Gp
    DEBUG(p_cm_, INTRADM, "%sBuilding RHS.\n", "");
    VecStrideGather(U, 0, U_sc, INSERT_VALUES);
    ierr = MatMult(static_cast<StabConv*>(conv)->conv_mat, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,0, Rhs, INSERT_VALUES);
    VecStrideGather(U, 1, U_sc, INSERT_VALUES);
    ierr = MatMult(static_cast<StabConv*>(conv)->conv_mat, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,1, Rhs, INSERT_VALUES);

    // Add U^(n+1) to CMat
    ierr = MatShift(static_cast<StabConv*>(conv)->conv_mat, 1.); CHKERRQ(ierr);

    // Setup KSP
    DEBUG(p_cm_, INTRADM, "[STP] Building KSP first (CN).\n", "");
    KSP ksp;
    ierr = KSPCreate(p_cm_->intra_domain_comm, &ksp); CHKERRQ(ierr);

    ierr = SetOperators(ksp, static_cast<StabConv*>(conv)->conv_mat,
    static_cast<StabConv*>(conv)->conv_mat); CHKERRQ(ierr);

    ierr = KSPSetDM(ksp, p_dom_->da_sc); CHKERRQ(ierr);
    ierr = KSPSetDMActive(ksp,PETSC_FALSE); CHKERRQ(ierr);

    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
    // Actually solve the system
    VecStrideGather(U, 0, U_sc, INSERT_VALUES);
    KSPOUT(p_cm_, INTRADM, "[STP] Solving system (1st component, first CN solver).\n", "");
    ierr = KSPSolve(ksp, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,0, Rhs, INSERT_VALUES);
    VecStrideGather(U, 1, U_sc, INSERT_VALUES);
    KSPOUT(p_cm_, INTRADM, "[STP] Solving system (2nd component, first CN solver).\n", "");
    ierr = KSPSolve(ksp, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,1, Rhs, INSERT_VALUES);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);

    // Destroy KSP
    DEBUG(p_cm_, INTRADM, "Cleaning system.\n", "");
    ierr = KSPDestroy(&ksp); CHKERRQ(ierr);


    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////

    ierr = p_parent_solver_->push_energy(Rhs, ++energy_idx); CHKERRQ(ierr);
    // Do a projection step
    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &U_next); CHKERRQ(ierr);
    ierr = p_proj_->projection(Rhs, U_next, Q); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = p_parent_solver_->push_energy(U_next, ++energy_idx); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);

    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////
    /////////////////////////// ///////////////////////

    // Diffusion (numerical) constant then \eps = h * opt->t \simeq O(dx, dz, dz) via CFl
    //     PetscReal h = 0.01; // FIXME

    // Setup matrix
    DEBUG(p_cm_, INTRADM, "Buildin' system matrix (CN).\n", "");

    static_cast<StabConv*>(conv)->fill(U_next);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U_next); CHKERRQ(ierr);
    // FIXME PROJ LPL CAREFUL!!!!!!!!!!!! WARNING
    // CMat = (Cmat - h dt^2 * L) / 2
    //     ierr = MatAXPY(static_cast<StabConv*>(conv)->conv_mat, - h * p_mod_->dt * p_mod_->dt,
    //                    p_proj_->trsp_mat, SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    ierr = MatScale(static_cast<StabConv*>(conv)->conv_mat, 0.5); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);

    //     Vec b; (RHS): Rhs = U^2 + C(U^n) + h L U^n - dt Gp = CMat * U^n - dt Gp
    DEBUG(p_cm_, INTRADM, "%sBuilding RHS.\n", "");
    VecStrideGather(U, 0, U_sc, INSERT_VALUES);
    ierr = MatMult(static_cast<StabConv*>(conv)->conv_mat, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,0, Rhs, INSERT_VALUES);
    VecStrideGather(U, 1, U_sc, INSERT_VALUES);
    ierr = MatMult(static_cast<StabConv*>(conv)->conv_mat, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,1, Rhs, INSERT_VALUES);

    // PressurePoisson TODO
    ierr = VecAXPBY(Rhs, -p_mod_->dt, -1., G); CHKERRQ(ierr); // G now contains the new step of U
    ierr = VecAXPY(Rhs, 1., U); CHKERRQ(ierr); // G now contains the new step of U

    // Forcing
    if( p_opt_->enable_forcing ) {
        ierr = VecAXPY(Rhs, p_mod_->dt, F); CHKERRQ(ierr); // G now contains the new step of U
    }

    // Add U^(n+1) to CMat
    ierr = MatShift(static_cast<StabConv*>(conv)->conv_mat, 1.); CHKERRQ(ierr);

    // Setup KSP
    DEBUG(p_cm_, INTRADM, "[STP] Building KSP (CN).\n", "");
    ierr = KSPCreate(p_cm_->intra_domain_comm, &ksp); CHKERRQ(ierr);

    ierr = SetOperators(ksp, static_cast<StabConv*>(conv)->conv_mat,
                        static_cast<StabConv*>(conv)->conv_mat); CHKERRQ(ierr);

    ierr = KSPSetDM(ksp, p_dom_->da_sc); CHKERRQ(ierr);
    ierr = KSPSetDMActive(ksp,PETSC_FALSE); CHKERRQ(ierr);

    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
    // Actually solve the system
    VecStrideGather(U, 0, U_sc, INSERT_VALUES);
    KSPOUT(p_cm_, INTRADM, "[STP] Solving system (1st component, CN solver).\n", "");
    ierr = KSPSolve(ksp, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,0, Rhs, INSERT_VALUES);
    VecStrideGather(U, 1, U_sc, INSERT_VALUES);
    KSPOUT(p_cm_, INTRADM, "[STP] Solving system (2nd component, CN solver).\n", "");
    ierr = KSPSolve(ksp, U_sc, Rhs_sc); CHKERRQ(ierr);
    VecStrideScatter(Rhs_sc ,1, Rhs, INSERT_VALUES);

    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &Rhs_sc); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &U_sc); CHKERRQ(ierr);

    // Destroy KSP
    DEBUG(p_cm_, INTRADM, "Cleaning system.\n", "");
    ierr = KSPDestroy(&ksp); CHKERRQ(ierr);

    ierr = DMGetGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);

    // Project
    // WARNING: U is already U n+1
    ierr = p_parent_solver_->push_energy(Rhs, ++energy_idx); CHKERRQ(ierr);
    ierr = p_proj_->projection(Rhs, U, Q); CHKERRQ(ierr); // Project P_local, get U_x (P(arg) and U_y Q(arg))
    ierr = p_parent_solver_->push_energy(U, ++energy_idx); CHKERRQ(ierr);

    // We just need to do a pressure update (as forcing), we may even want to change this and to make it adaptive
    ierr = VecAXPY(G, 1. / p_mod_->dt, Q); CHKERRQ(ierr); // G now contains the new step of U

    ierr = DMRestoreGlobalVector(p_dom_->da, &Q); CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &Rhs); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
