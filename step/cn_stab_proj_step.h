/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file cn_stab_proj_step.h
//! Performs ONE actual step for the stable projection method in Crank-Nicholson variant.

#include "./proj_step.h"

#include "../transport/stab_conv.h"

#include "../transport/bcg_conv.h"

//! This class implements a single timestep for the projection method of \cite ProjAMVS.
//! This is the Crank-Nicholson variant with \f$\theta = 1/2\f$. Refer to StabProjStep for
//! the backward Euler variant.
//!
class CNStabProjStep : public ProjStep {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    CNStabProjStep(Options *p_opt, Domain * p_dom, ModelBase *p_mod, Attila *p_cm,
                   Projection *p_proj, Conv *p_conv,
                   Solver * p_parent_solver = NULL);
    //! Free some space
    ~CNStabProjStep() {}

    //! The actual step called each iteration
    //! perform a single step for the method, this step projects th derivative of the velocity as in BCG
    //!
    PetscErrorCode step(Vec U, Vec G, Vec F_b, Vec F);

protected:
    Conv        * conv;
    Conv        * conv2;
};

