/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "projection_laplacian.h"

ProjectionLaplacian::ProjectionLaplacian(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila *p_cm)
    : Projection(p_opt, p_dom, p_mod, p_cm)
    , is_setup_(false) {
    FUNCTIONBEGIN(p_cm_);
    // Temporary vectors, lifespan of the entire class. This allows external
    // access to the RHS vector and the solution of the system.
    // Used e.g. in ComputeOperators routines.
    ierr = DMGetGlobalVector(p_dom_->da_sc, &phi_); CHKERRV(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &div_vector); CHKERRV(ierr);

    ierr = VecSet(phi_, 0); CHKERRV(ierr);

    // KSP context will be reused
    ierr = KSPCreate(p_cm_->intra_domain_comm, &ksp); CHKERRV(ierr);
    //     ierr = KSPSetFromOptions(ksp); CHKERRV(ierr);
}

ProjectionLaplacian::~ProjectionLaplacian() {
    FUNCTIONBEGIN(p_cm_);

    // Finalize KSP
    ierr = KSPDestroy(&ksp); CHKERRV(ierr);
    // No longer need external KSP access.
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &phi_); CHKERRV(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &div_vector); CHKERRV(ierr);
}

PetscErrorCode ProjectionLaplacian::setup(void) {
    if(is_setup_) {
        PetscFunctionReturn(ierr);
    }
    FUNCTIONBEGIN(p_cm_);

    // Default options
    if( p_opt_->old_ksp_handling_ ) { // Enable multigrid
        ierr = KSPSetDM(ksp, p_dom_->da_sc); CHKERRQ(ierr);
        ierr = KSPSetComputeOperators(ksp, ComputeMatrix, this); CHKERRQ(ierr);
        // TODO: We should fix this to chose a better initial value and reuse the preconditioner
//         ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERRQ(ierr);
        ierr = KSPSetComputeRHS(ksp, ComputeRHS, this); CHKERRQ(ierr);
#ifndef PETSC_OLDAPI_KSP // Old API fix (v. < ???)
        //    ierr = KSPSetReusePreconditioner(ksp, PETSC_TRUE); CHKERRQ(ierr);
#endif
    } else { // Use MUMPS and LU
        PC             pc;
        ierr = KSPSetType(ksp, KSPPREONLY); CHKERRQ(ierr);
        ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
        ierr = PCSetType(pc, "lu"); CHKERRQ(ierr);
        ierr = PCFactorSetMatSolverPackage(pc,  "mumps"); CHKERRQ(ierr);
    }

    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

    // Setup the preconditioner, if auto_lu is true, the we automagically select
    // between LU or multigrid.
    PC pc;
    ierr = KSPGetPC(ksp, &pc); CHKERRQ(ierr);
    if (p_opt_->auto_lu && auto_lu(p_dom_->mesh_size)) {
        OUTPUT(p_cm_, INTRADM, "Auto-enforcing lu, on the mesh %s with ranks %s.\n",
               p_dom_->mesh_size.str().c_str(), p_dom_->mesh_ranks.str().c_str());
        ierr = KSPSetType(ksp, KSPPREONLY); CHKERRQ(ierr);
        ierr = PCSetType(pc, PCLU); CHKERRQ(ierr);

        // Only parallel LU is trough MUMPS
        if( p_cm_->intra_domain_size > 1 ) {
            ierr = PCFactorSetMatSolverPackage(pc,  "mumps"); CHKERRQ(ierr);
        }
        // Remove Kernel of matrix
        if( p_opt_->solver == SolverType::Proj ) {
            ierr = PCFactorSetShiftType(pc, MAT_SHIFT_NONZERO); CHKERRQ(ierr);
            ierr = PCFactorSetShiftAmount(pc, PETSC_DECIDE); CHKERRQ(ierr);
        }

        ///////// FIXME SET OPERATORS FOR LU!!!!
        //             p_opt_->enable_mg = PETSC_FALSE;
        //             if( !A_created ) {
        //                 ierr = preallocate(p_dom_->da_sc, A); CHKERRQ(ierr);
        //                 ierr = laplacian_matrix<1>(p_dom_->da_sc, A, p_dom_->bd_fam.L); CHKERRQ(ierr);
        //                 A_created = true;
        //             }
        //             ierr = KSPSetOperators(ksp, A, A); CHKERRQ(ierr);
        //             ierr = KSPSetComputeOperators(ksp, ComputeMatrix, this); CHKERRQ(ierr);
    }

    // If auto_mg_levels, we select the most appropriate number of MG levels.
    if( p_opt_->auto_mg_levels ) {
        PCType pc_type;
        ierr = PCGetType(pc, &pc_type); CHKERRQ(ierr);
        // Depending on if we use MG or GAMG, setup the KSP differently.
        // TODO: insert hybrid MG solver
        if( !strcmp(PCMG, pc_type)) {
            int lvl = auto_mg_levels(p_dom_->mesh_size, p_dom_->mesh_ranks,
                                     p_opt_->enable_3d ? 3 : 2, 2, MgType::PC_MG);
            ierr = PCMGSetLevels(pc, lvl, NULL);  CHKERRQ(ierr); //< check if works with subcomm etc...
            OUTPUT(p_cm_, INTRADM, "Auto-enforcing MG levels to be %d, on the mesh %s with ranks %s.\n", lvl,
                   p_dom_->mesh_size.str().c_str(), p_dom_->mesh_ranks.str().c_str());
            if( p_opt_->nested_mg ) {
                Mesh coarse_mesh = p_dom_->mesh_size / pow(2, lvl - 1);
                if( coarse_mesh > 64 ) {

                    int coarse_lvl = auto_mg_levels(coarse_mesh, p_dom_->mesh_ranks,
                                                    p_opt_->enable_3d ? 3 : 2, 2, MgType::PC_GAMG);

                    KSP coarse_ksp;
                    PC coarse_pc;
                    ierr = PCMGGetCoarseSolve(pc, &coarse_ksp); CHKERRQ(ierr);
                    ierr = KSPGetPC(coarse_ksp, &coarse_pc); CHKERRQ(ierr);

                    ierr = PCSetType(coarse_pc, PCGAMG); CHKERRQ(ierr);
                    ierr = PCMGSetLevels(coarse_pc, coarse_lvl, NULL); CHKERRQ(ierr);

                    OUTPUT(p_cm_, INTRADM, "Auto-enforcing coarse GAMG levels to be %d, on the mesh %s with "
                                "ranks %s.\n",
                           coarse_lvl, coarse_mesh.str().c_str(), Mesh::ones.str().c_str());
                }
            }
        } else if( !strcmp(PCGAMG, pc_type)) {
            int lvl = auto_mg_levels(p_dom_->mesh_size, p_dom_->mesh_ranks,
                                     p_opt_->enable_3d ? 3 : 2, 2, MgType::PC_GAMG);
            ierr = PCMGSetLevels(pc, lvl, NULL);  CHKERRQ(ierr); //< check if works with subcomm etc...
            OUTPUT(p_cm_, INTRADM, "Auto-enforcing GAMG levels to be %d, on the mesh %s with ranks %s.\n", lvl,
                   p_dom_->mesh_size.str().c_str(), p_dom_->mesh_ranks.str().c_str());
        } else {
            DEBUG(p_cm_, INTRADM, "Seems you are not using mg but trying to set MGLevels, no worries.\n", "");
        }
    }

    ierr = KSPSetUp(ksp); CHKERRQ(ierr);
    is_setup_ = true;

    PetscFunctionReturn(ierr);
}

PetscErrorCode ProjectionLaplacian::projection(const Vec vector,
                                               Vec P_vector, Vec Q_vector) {
    FUNCTIONBEGIN(p_cm_);

    p_profiler->push_stage("projection");

    Vec phi;

    // Start from div U
    ierr = divergence(vector, div_vector, p_dom_->bd_fam.U); CHKERRQ(ierr);
//     PetscViewer      viewer;
//     PetscViewerDrawOpen(PETSC_COMM_WORLD,0,"",300,0,300,300,&viewer);
//     VecView(div_vector, viewer);
//     DMView(p_dom_->da,viewer);

    setup();

    // Obtain flux scalar
    if( p_opt_->old_ksp_handling_ ) {
        // Actually solve the system

//         ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERRQ(ierr);
//         ierr = KSPSetComputeInitialGuess(ksp, ComputeInitialGuess, this); CHKERRQ(ierr);
//         ierr = KSPSetComputeOperators(ksp, ComputeMatrix, this); CHKERRQ(ierr);
        lvl = false;
        p_profiler->begin_event("projection_ksp_mg_solve");
        KSPOUT(p_cm_, INTRADM, "Projection (MG)...\n", "");
        ierr = KSPSolve(ksp, NULL, NULL); CHKERRQ(ierr);
        p_profiler->end_event("projection_ksp_mg_solve");
        ierr = KSPGetSolution(ksp, &phi);
        ierr = VecCopy(phi, phi_); CHKERRQ(ierr);
    } else {
        // Actually solve the system

        // TODO WARNING FIXME: proj_mat MUST CONTAIN /dx/dy/dz
//         ierr = VecScale(div_vector, p_dom_->dx * p_dom_->dy); CHKERRQ(ierr);
        p_profiler->begin_event("projection_ksp_solve");
        KSPOUT(p_cm_, INTRADM, "Projection (Non MG)\n", "");
        ierr = KSPSolve(ksp, div_vector, div_vector); CHKERRQ(ierr);
        p_profiler->end_event("projection_ksp_solve");
    }
//     Mat Amat, Pmat;
//     KSPGetOperators(ksp,&Amat,&Pmat);
//     Vec b,c;
//     ierr = DMGetGlobalVector(p_dom_->da_sc, &b); CHKERRQ(ierr);
//     ierr = DMGetGlobalVector(p_dom_->da_sc, &c); CHKERRQ(ierr);
//     VecSet(b,8);
//     MatMult(Amat, b, c);
//     DEBUG_VIEW_VEC(b);
//     DEBUG_VIEW_VEC(c);

    // Construct "gradient pressure"
    if( p_opt_->old_ksp_handling_ ) {
        ierr = gradient(phi, Q_vector, p_dom_->bd_fam.p); CHKERRQ(ierr);
    } else {
        ierr = gradient(div_vector, Q_vector, p_dom_->bd_fam.p); CHKERRQ(ierr);
    }

    // P = I - Q
    ierr = VecCopy(vector, P_vector); CHKERRQ(ierr);
    ierr = VecAXPY(P_vector, -1, Q_vector); CHKERRQ(ierr);

    p_profiler->pop_stage();

    PetscFunctionReturn(ierr);
}

PetscErrorCode  ProjectionLaplacian::extract_stream(Vec w, Vec * psi) {
    FUNCTIONBEGIN(p_cm_);

    setup();

    p_profiler->begin_event("extract_stream");

    ierr = VecCopy(w, div_vector); CHKERRQ(ierr);

    // Obtain flux scalar
    //   KSPSetReusePreconditioner(ksp, PETSC_FALSE);
    if( p_opt_->old_ksp_handling_ ) {
        ierr = VecScale(div_vector, -1.0); CHKERRQ(ierr);
        // Actually solve the system
        //         ierr = KSPSetComputeRHS(ksp, ComputeRHS, this); CHKERRQ(ierr);
        KSPOUT(p_cm_, INTRADM, "Extract velocity (MG)\n", "");
        p_profiler->begin_event("extract_stream_ksp_mg");
        ierr = KSPSolve(ksp, NULL, NULL); CHKERRQ(ierr);
        p_profiler->end_event("extract_stream_ksp_mg");
        ierr = KSPGetSolution(ksp, psi);

        //         PetscViewer viewer;
        //         PetscViewerDrawOpen(PETSC_COMM_WORLD,0,"",300,0,300,300,&viewer);
        //         VecView(psi,viewer);
        //         PetscViewerDestroy(&viewer);
    } else {
        ierr = DMGetGlobalVector(p_dom_->da_sc, psi); CHKERRQ(ierr);
        // Actually solve the system
        // TODO WARNING FIXME: diff_mat MUST CONTAIN /dx/dy/dz
        ierr = VecScale(div_vector, -1); CHKERRQ(ierr);
        p_profiler->begin_event("extract_stream_ksp");
        KSPOUT(p_cm_, INTRADM, "Extract velocity (Non MG)\n", "");
        p_profiler->end_event("extract_stream_ksp");
        ierr = KSPSolve(ksp, div_vector, *psi); CHKERRQ(ierr);
    }

    p_profiler->end_event("extract_stream");

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode  ProjectionLaplacian::stream_to_centered_U(Vec psi, Vec U)  {
    FUNCTIONBEGIN(p_cm_);

    ierr = gradient(psi, U, p_dom_->bd_fam.p); CHKERRQ(ierr);
    ierr = orthogonal(U); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode  ProjectionLaplacian::stream_to_edge_U(Vec psi, Vec U) {
    FUNCTIONBEGIN(p_cm_);

    ierr = gradient_plus(psi, U, p_dom_->bd_fam.p); CHKERRQ(ierr);
    ierr = orthogonal(U); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Projection compute the rhs
//!
PetscErrorCode ComputeRHS(KSP ksp, Vec b, void *ctx) {
    ProjectionLaplacian    *user = (ProjectionLaplacian*)ctx;

    FUNCTIONBEGIN(user->p_cm_);

    PetscInt       mx, my, mz;
//     PetscScalar    Hx, Hy, Hz;
    DM             da;

    ierr = KSPGetDM(ksp, &da); CHKERRQ(ierr);
    ierr = DMDAGetInfo(da, 0, &mx, &my, &mz,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
//     Hx = user->p_dom_->l_x / (PetscReal) (mx);
//     Hy = user->p_dom_->l_y / (PetscReal) (my);
//     Hz = user->p_dom_->l_z / (PetscReal) (mz);
//     if( user->p_opt_->enable_3d ) {
//         ierr = VecScale(user->div_vector, Hx * Hy * Hz); CHKERRQ(ierr);
//     } else {
//         ierr = VecScale(user->div_vector, Hx * Hy); CHKERRQ(ierr);
//     }
    ierr = VecCopy(user->div_vector, b ); CHKERRQ(ierr);

    if ( user->p_dom_->bd_fam.L.has_nullspace() ) {
        MatNullSpace nullspace;

        ierr = MatNullSpaceCreate(user->p_cm_->intra_domain_comm, PETSC_TRUE,0,0,&nullspace); CHKERRQ(ierr);

        ierr = NullSpaceRemove(nullspace, b); CHKERRQ(ierr);

        ierr = MatNullSpaceDestroy(&nullspace); CHKERRQ(ierr);
    }

    ierr = VecAssemblyBegin(b);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(b);CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! FIXME
//!
PetscErrorCode ComputeInitialGuess(KSP ksp, Vec b, void *ctx) {
    ProjectionLaplacian    *user = (ProjectionLaplacian*)ctx;

    FUNCTIONBEGIN(user->p_cm_);

    if(user->lvl == true) {
        //   ierr = VecSet(b, 0); CHKERRQ(ierr);

        PetscInt       mx, my, mz;
        //   PetscScalar    Hx, Hy, Hz;
        DM dm;
        Mat mat;
        KSPGetDM(ksp, &dm);
        ierr = DMDAGetInfo(dm, 0, &mx, &my, &mz,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
        //   Hx = user->p_dom_->l_x / (PetscReal) (mx);
        //   Hy = user->p_dom_->l_y / (PetscReal) (my);
        //   Hz = user->p_dom_->l_z / (PetscReal) (mz);
        DMCreateInterpolation(dm, user->p_dom_->da_sc, &mat, PETSC_NULL);

        MatInterpolate(mat, user->phi_, b);
        //   ierr = VecScale(b, Hx * Hy); CHKERRQ(ierr);

        PetscFunctionReturn(ierr);
    }
    ierr = VecCopy(user->phi_, b); CHKERRQ(ierr);

    ierr = VecAssemblyBegin(b); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(b); CHKERRQ(ierr);


    user->lvl = true;

    PetscFunctionReturn(ierr);
}

//!
//!
#ifdef PETSC_OLDAPI_KSP // Old API fix (v. < 3.5)
PetscErrorCode ComputeMatrix(KSP ksp, Mat, Mat jac, MatStructure * str, void *ctx) {
#else
PetscErrorCode ComputeMatrix(KSP ksp, Mat, Mat jac, void *ctx) {
#endif
    Projection    *user = (Projection*)ctx;

    FUNCTIONBEGIN(user->p_cm_);

    DM             da;
    ierr  = KSPGetDM(ksp, &da); CHKERRQ(ierr);
    DEBUG(user->p_cm_, INTRADM, "[MAT] Setting up matrix for projection...\n", "");
    ierr = user->laplacian_matrix<1>(da, jac, user->p_dom_->bd_fam.L); CHKERRQ(ierr);
    // TODO fingure out a way to cleanly use proj decoupled
    //   ierr = user->laplacian_matrix<2>(da, jac, user->dom->bd_fam.L); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
