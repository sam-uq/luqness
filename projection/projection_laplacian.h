/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file projection_laplacian.h
//! Implements the actual Laplacian-based projection and the velocity extractor (for voricity stream formulations).

// Internal includes
#include "./projection.h"

#include "../tools/profiler.h"
#include "../tools/utilities.h"

PetscErrorCode ComputeInitialGuess(KSP ksp, Vec b, void *ctx);
PetscErrorCode ComputeRHS(KSP ksp, Vec b, void *ctx);
#ifdef PETSC_OLDAPI_KSP // Old API fix (PETSc v. < 3.4)
PetscErrorCode ComputeMatrix(KSP ksp, Mat J,Mat jac, MatStructure * str, void *ctx);
#else
PetscErrorCode ComputeMatrix(KSP ksp, Mat J,Mat jac, void *ctx);
#endif

//! \brief Contains discrete Leray Projection and discrete stream function reconstructions.
//!
//! Handles an approximate discrete projection in 2D/3D solving a Poisson equation.
//! It also contains routines for the extraction of the velocity from the vorticity (used in
//! vorticity based solvers \cite Levy1997 and \cite leonardi2016numerical) from the Biot-Savart rule.
//!
//! The projection/voritcity reconstruction is performed via a linear solver.
//! This class handles all the possible options for the solution of the
//! linear system.
class ProjectionLaplacian : public Projection {
public:
    //! \brief Constructor, allocate needed vectors.
    //!
    //! Grabs memory for the members phi_ and div_vector, sets phi_ to zero.
    //! Preallocates the KSP setting.
    //! \param[in] p_opt Non-owned context for Options
    //! \param[in] p_dom Non-owned context for Domain
    //! \param[in] p_mod Non-owned context for initial data and boundaries
    //! \param[in] p_cm  Non-owned context for rank allocation
    ProjectionLaplacian(Options *p_opt, Domain *p_dom,
                        ModelBase *p_mod, Attila *p_cm);

    //! \brief Free some memory. Returns all grabbed vectors.
    //!
    //! Destroys KSP and free member vectors.
    ~ProjectionLaplacian();

    //! \brief Setup the KSP context.
    //!
    //! Setup KSP and matrices (which will be reused) from the options
    //! and the current state of the simulations.
    //! This routine also selects if it uses Multigrid or not, how many
    //! levels will be used and if a CompoteOperators context will be used.
    //!
    //! After this the is_setup_ member is set to true.
    //! \return Error Code

    PetscErrorCode setup(void);
    //! \brief Compute the Leray projection of V and writes it onto P and Q.
    //!
    //! Replace the content of P resp. Q with the Leray
    //! projection of V and (I - P)(V) resp.
    //!
    //! P = P(V) will be divergence free and will be a curl field
    //! Q = Q(V) will be curl free and will be a gradient field
    //! The vector V is writen as \f$W = P(V) + Q(V) = \curl A + \grad \Psi\f$.
    //!
    //! \param[in] V    Input vector.
    //! \param[out] P   Projection P(V), Curl part, divergence free
    //! \param[out] Q   (I-Q)(V), Gradient part, Curl-free
    //! \return Error Code
    PetscErrorCode projection(const Vec V, Vec P,  Vec Q);

    //! \brief Reconstruct stream function from vorticity.
    //!
    //! The Stream function \f$\Psi\f$ is extracted from
    //! vorticity using a Laplacian, i.e. with \f$\Psi = -\Delta^{-1} w\f$.
    //!
    //! \param[in]  w    Vorticity
    //! \param[out] psi  Stream function s.t. \f$\nabla \Psi^\top = u\f$.
    //! \return          Error Code
    PetscErrorCode extract_stream(Vec w, Vec * psi);

    //! \brief Compute a cell-centred velocity from the vorticity.
    //!
    //! \param[in] w     Cell-centered vorticity
    //! \param[out] U    Cell-centred velocity
    //! \return          Error Code
    PetscErrorCode stream_to_centered_U(Vec w, Vec U);

    //! \brief Compute an edge-centred velocity from the vorticity.
    //!
    //! Uses the standard finite difference *orthogonal* gradient.
    //! \param[in] w     Cell-centered vorticity
    //! \param[out] U    Edge-centred velocity
    //! \return          Error Code
    PetscErrorCode stream_to_edge_U(Vec w, Vec U);

    // Happy little friends that cannot go inside this class
    // because the must be passed as pointers to PETSc routines.
    friend PetscErrorCode ComputeInitialGuess(KSP ksp, Vec b, void *ctx);
    friend PetscErrorCode ComputeRHS(KSP ksp, Vec b, void *ctx);

#ifdef PETSC_OLDAPI_KSP // Old API fix (PETSc v. < 3.4)
    friend PetscErrorCode ComputeMatrix(KSP ksp, Mat J,
                                        Mat jac, MatStructure * str, void *ctx);
#else
    friend PetscErrorCode ComputeMatrix(KSP ksp, Mat J,
                                        Mat jac, void *ctx);
#endif
protected:
    // Things that need to be accessed by friends
    Vec phi_; //! Handle for the stream-function
    Vec div_vector; //!< Handle for the r.h.s.
    //! \brief Flag that tells KSP routines that we are in a corse grid.
    //!
    //! This flag, if set to true
    //! tells KSP that we need to interpolate the RHS vector.
    //! The flag is set to true after the first call to the ComputeRHS function.
    //! Used only for ComputeXXX-based calls.
    bool lvl;
private:
    KSP ksp; //!< Context for the linear solver

    bool is_setup_; //!< Flag set to true if ksp has ben setted up.
};
