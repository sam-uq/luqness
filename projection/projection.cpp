/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "projection.h"

// Some debug function
void print(DM da, Attila *p_cm) {
    UNUSED(p_cm);
    PetscInt xs, ys, zs, xm, ym, zm, xr, yr, zr;
    ierr = DMDAGetCorners(da, &xs, &ys, &zs, &xm, &ym, &zm); CHKERRV(ierr);
    ierr = DMDAGetInfo(da, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, &xr, &yr, &zr,
                       PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL); CHKERRV(ierr);
                       DEBUG(p_cm, SELF, "[DM] New DMDA, Local size: x: %d, %d, y: %d, %d, z: %d, %d - %d, %d, %d.\n",
                             xs, xm, ys, ym, zs, zm, xr, yr, zr);
                       return;
}


// Some debug function
void print(DM, Vec v, Attila *p_cm) {
    UNUSED(p_cm);
    PetscInt size, lsize;
    ierr = VecGetSize(v, &size); CHKERRV(ierr);
    VecGetLocalSize(v, &lsize);
    DEBUG(p_cm, SELF, "[VEC] size: %d local: %d\n", size, lsize);
    return;
}

//! Deallocate all temporary vectors and matrices, if multigrid is enable we destroy the matrix A
//! the matrix Lpl is destroyed if it was created, if there was a need for laplacian matrix, i.e:
//! - boundaries of passive scalar != the ones of velocity and transport was enabled
//! - Navier-Stokes was enabled
Projection::~Projection() {
    FUNCTIONBEGIN(p_cm_);

    // Destroy matrices
    if( proj_mat_allocated_self ) {
        ierr = MatDestroy(&proj_mat); CHKERRV(ierr);
    }
    if( trsp_mat_allocated_self ) {
        ierr = MatDestroy(&trsp_mat); CHKERRV(ierr);
    }

}

//!
//!
PetscErrorCode Projection::get_proj_mat(Mat * mat) {
    FUNCTIONBEGIN(p_cm_);

    // Create the regular Laplacian matrix for the Poisson pressure equation
    if( !proj_mat_is_set ) { // If using MG don't bother creating matrices!!!!
        // TODO stable scheme requires Lpl, fix this
        // This takes care of PetscReallocation, so do not worry about that
        ierr =  CreateMatrix(p_dom_->da_sc, &proj_mat); CHKERRQ(ierr);
        proj_mat_allocated_self = true;
        proj_mat_is_set = true;
        ierr = laplacian_matrix<1>(p_dom_->da_sc, proj_mat, p_dom_->bd_fam.L); CHKERRQ(ierr);
    }

    *mat = proj_mat;

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Projection::get_trsp_mat(Mat * mat) {
    FUNCTIONBEGIN(p_cm_);

    if( !trsp_mat_is_set ) {
        trsp_mat_is_set = true;
        if( ( ( p_dom_->bd_fam.Ls == p_dom_->bd_fam.L ) || ( !p_opt_->do_transport && p_opt_->use_euler ) ) ) {
            // We don't use MG: create just one matrix (diff === transport === poisson (same matrix on all disabled)
            //&& ( p_dom_->bd_fam.Ls != p_dom_->bd_fam.L )
            DEBUG(p_cm_, INTRADM, "Transport/Diffusion matrix is the same as cleansing matrix "
                                  "(reason: %d %d %d)...\n",
                  p_opt_->do_transport, p_opt_->use_euler, p_dom_->bd_fam.Ls != p_dom_->bd_fam.L);
            ierr = get_proj_mat(&trsp_mat); CHKERRQ(ierr);
        } else { // Ok we need an additional matrix for diffusion of scalar and velocity
            DEBUG(p_cm_, INTRADM, "Transport/Diffusion matrix is different from cleansing matrix "
                                  "(reason: %d %d %d)...\n",
                  p_opt_->do_transport, p_opt_->use_euler, p_dom_->bd_fam.Ls != p_dom_->bd_fam.L);
            // This takes care of PetscReallocation, so do not worry about that
            ierr =  CreateMatrix(p_dom_->da_sc, &trsp_mat); CHKERRQ(ierr);
            ierr = laplacian_matrix<1>(p_dom_->da_sc, trsp_mat, p_dom_->bd_fam.Ls); CHKERRQ(ierr);
            trsp_mat_allocated_self = true;
        }
    }

    *mat = trsp_mat;

    PetscFunctionReturn(ierr);
}

PetscReal Projection::matrix_scaling(DM/* da */) {
    // Normalized scaling
//    ierr = DMDAGetInfo(da, 0, &mx, &my, &mz,
//                       0,0,0,
//                       0,0,0,
//                       0,0,0);CHKERRQ(ierr);
//    Hx = user->p_dom_->l_x / (PetscReal) (mx);
//    Hy = user->p_dom_->l_y / (PetscReal) (my);
//    Hz = user->p_dom_->l_z / (PetscReal) (mz);

//    if(p_opt_->enable_3d) {
//        return Hx*Hy*Hz;
//    } else {
//        return Hx*Hy;
//    }

    // New scaling
    return 1;
}

//! read from phi and writes the gradient to U_y, using U_x temporarly
//!
PetscErrorCode Projection::gradient(Vec phi, Vec grad_vector, const BD & bd) {
    FUNCTIONBEGIN(p_cm_);

    Vec         phi_local;
    ierr = DMGetLocalVector(p_dom_->da_sc, &phi_local); CHKERRQ(ierr);

    // Upd boundaries // send data
    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, phi, INSERT_VALUES, phi_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, phi, INSERT_VALUES, phi_local); CHKERRQ(ierr);

    ierr = p_mod_->update_bd_scalar(phi_local, bd, p_mod_->t); CHKERRQ(ierr);

    // TODO: can put BC here (??? what was that?)
    if( p_opt_->enable_3d ){
        PetscReal     ***phi_array;
        Field3D       ***grad_phi;
        Volume3D      ***d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, phi_local, &phi_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    grad_phi[k][j][i][0] = 0.5 * ( ( phi_array[k][j][i+1] - phi_array[k][j][i-1] ) / d[k][j][i].x);
                    grad_phi[k][j][i][1] = 0.5 * ( ( phi_array[k][j+1][i] - phi_array[k][j-1][i] ) / d[k][j][i].y);
                    grad_phi[k][j][i][2] = 0.5 * ( ( phi_array[k+1][j][i] - phi_array[k-1][j][i] ) / d[k][j][i].z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, phi_local, &phi_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
    } else {
        PetscReal     **phi_array;
        Field2D       **grad_phi;
        Volume2D      **d;
        ierr = DMDAVecGetArray(p_dom_->da_sc, phi_local, &phi_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                grad_phi[j][i][0] = 0.5 * ( ( phi_array[j][i+1] - phi_array[j][i-1] ) / d[j][i].x);
                grad_phi[j][i][1] = 0.5 * ( ( phi_array[j+1][i] - phi_array[j-1][i] ) / d[j][i].y);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, phi_local, &phi_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &phi_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Projection::gradient_plus(Vec vector, Vec grad_vector, const BD & bd) {
    FUNCTIONBEGIN(p_cm_);

    Vec         vector_local;
    ierr = DMGetLocalVector(p_dom_->da_sc, &vector_local); CHKERRQ(ierr);

    // Upd boundaries // send data
    ierr = DMGlobalToLocalBegin(p_dom_->da_sc, vector, INSERT_VALUES, vector_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da_sc, vector, INSERT_VALUES, vector_local); CHKERRQ(ierr);

    ierr = p_mod_->update_bd_scalar(vector, bd, p_mod_->t); CHKERRQ(ierr);

    // TODO: can put BC here (??? what was that?)
    if( p_opt_->enable_3d ){
        PetscReal     ***vector_array;
        Field3D       ***grad_phi;
        Volume3D      ***df;
        ierr = DMDAVecGetArray(p_dom_->da_sc, vector_local, &vector_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    grad_phi[k][j][i][0] = ( ( vector_array[k][j][i+1] - vector_array[k][j][i] ) / df[k][j][i].x);
                    grad_phi[k][j][i][1] = ( ( vector_array[k][j+1][i] - vector_array[k][j][i] ) / df[k][j][i].y);
                    grad_phi[k][j][i][2] = ( ( vector_array[k+1][j][i] - vector_array[k][j][i] ) / df[k][j][i].z);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, vector_local, &vector_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
    } else {
        PetscReal     **vector_array;
        Field2D       **grad_phi;
        Volume2D      **df;
        ierr = DMDAVecGetArray(p_dom_->da_sc, vector_local, &vector_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; j++) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; i++) {
                grad_phi[j][i][0] = ( ( vector_array[j][i+1] - vector_array[j][i] ) / df[j][i].x);
                grad_phi[j][i][1] = ( ( vector_array[j+1][i] - vector_array[j][i] ) / df[j][i].y);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, vector_local, &vector_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, grad_vector, &grad_phi); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da_sc, &vector_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! compute divergence of P_local and write into Div_U
//!
PetscErrorCode Projection::divergence(Vec  vector, Vec div_vector, const BD & bd) {
    FUNCTIONBEGIN(p_cm_);

    // We copy vector to this local variable (we need to access outer indices)
    Vec vector_local;


    ierr = DMGetLocalVector(p_dom_->da, &vector_local); CHKERRQ(ierr);
//     print(p_dom_->da, p_cm_);
//     print(p_dom_->da, vector_local, p_cm_);

    // Upd boundaries // send data
    ierr = DMGlobalToLocalBegin(p_dom_->da, vector, INSERT_VALUES, vector_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, vector, INSERT_VALUES, vector_local); CHKERRQ(ierr);

    ierr = p_mod_->update_bd_field(vector_local, bd, p_mod_->t); CHKERRQ(ierr);

    if( p_opt_->enable_3d ){ // 3D case divergence
        Field3D         ***array;
        PetscReal       ***div_array;
        Volume3D        ***d;
        ierr = DMDAVecGetArray(p_dom_->da, vector_local, &array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, div_vector, &div_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    div_array[k][j][i] = 0.5 * ( (array[k][j][i+1][0] - array[k][j][i-1][0]) / d[k][j][i].x
                    + (array[k][j+1][i][1] - array[k][j-1][i][1]) / d[k][j][i].y
                    + (array[k+1][j][i][2] - array[k-1][j][i][2]) / d[k][j][i].z );
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, vector_local, &array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, div_vector, &div_array); CHKERRQ(ierr);
    } else { // 2D case
        Field2D         **array;
        PetscReal       **div_array;
        Volume2D        **d;
        ierr = DMDAVecGetArray(p_dom_->da, vector_local, &array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, div_vector, &div_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                div_array[j][i] = 0.5 * ( (array[j+1][i][1] - array[j-1][i][1]) / d[j][i].y
                + (array[j][i+1][0] - array[j][i-1][0]) / d[j][i].x );
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, vector_local, &array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, div_vector, &div_array); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da, &vector_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Read from phi and writes the gradient to U_y, using U_x temporarly
//!
PetscErrorCode Projection::curl(Vec v, Vec curl_v, const BD & bd) {
    FUNCTIONBEGIN(p_cm_);

    // Local buffer containing ghost cells fro vorticity
    // OPTIMIZATIONS: only tmeporary, can use any buffer
    // Also in 2d we only need one components
    Vec vector_local;
    ierr = DMGetLocalVector(p_dom_->da, &vector_local); CHKERRQ(ierr);

    // Upd boundaries // send data to ghosts
    ierr = DMGlobalToLocalBegin(p_dom_->da, v, INSERT_VALUES, vector_local); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, v, INSERT_VALUES, vector_local); CHKERRQ(ierr);
    // Update bdries according to rule
    ierr = p_mod_->update_bd_field(vector_local, bd, p_mod_->t); CHKERRQ(ierr);

    if( p_opt_->enable_3d ) { // 3D case
        Field3D      ***v_array, ***curl_v_array;
        Volume3D     ***d;
        ierr = DMDAVecGetArray(p_dom_->da, vector_local, &v_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da, curl_v, &curl_v_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    curl_v_array[k][j][i][0] =
                              0.5 * ( ( v_array[k][j+1][i][2] - v_array[k][j-1][i][2] ) / d[k][j][i].y)
                              - 0.5 * ( ( v_array[k+1][j][i][1] - v_array[k-1][j][i][1] ) / d[k][j][i].z);
                    curl_v_array[k][j][i][1] =
                              0.5 * ( ( v_array[k+1][j][i][0] - v_array[k-1][j][i][0] ) / d[k][j][i].z)
                              - 0.5 * ( ( v_array[k][j][i+1][2] - v_array[k][j][i-1][2] ) / d[k][j][i].x);
                    curl_v_array[k][j][i][2] =
                              0.5 * ( ( v_array[k][j][i+1][1] - v_array[k][j][i-1][1] ) / d[k][j][i].x)
                              - 0.5 * ( ( v_array[k][j+1][i][0] - v_array[k][j-1][i][0] ) / d[k][j][i].y);
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, vector_local, &v_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, curl_v, &curl_v_array); CHKERRQ(ierr);
    } else { //2D case
        Field2D        **v_array;
        PetscReal      **curl_v_array;
        Volume2D       **d;
        ierr = DMDAVecGetArray(p_dom_->da, vector_local, &v_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, curl_v, &curl_v_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                curl_v_array[j][i] = 0.5 * ( ( v_array[j][i+1][1] - v_array[j][i-1][1] ) / d[j][i].x)
                - 0.5 * ( ( v_array[j+1][i][0] - v_array[j-1][i][0] ) / d[j][i].y);
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, vector_local, &v_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, curl_v, &curl_v_array); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da, &vector_local); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! read from U and writes the gamma2 to g2
//!
PetscErrorCode Projection::gamma2(Vec V, Vec g2, const BD & bd) {
    FUNCTIONBEGIN(p_cm_);

    Vec U_loc;
    ierr = DMGetLocalVector(p_dom_->da, &U_loc); CHKERRQ(ierr);

    // Upd boundaries // send data to ghosts
    ierr = DMGlobalToLocalBegin(p_dom_->da, V, INSERT_VALUES, U_loc); CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(p_dom_->da, V, INSERT_VALUES, U_loc); CHKERRQ(ierr);
    ierr = p_mod_->update_bd_field(U_loc, bd, p_mod_->t); CHKERRQ(ierr);

    if( p_opt_->enable_3d ){
        Field3D       ***U_array;
        PetscReal     ***g2_array;
        Volume3D      ***d_;
        ierr = DMDAVecGetArray(p_dom_->da, U_loc, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, g2, &g2_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_coord, p_dom_->D, &d_); CHKERRQ(ierr);
        for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
            for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                    //                     PetscReal ux, uy, uz, vx, vy, vz, wx, wy, wz;
                    PetscReal a, b, c, d, e, f, g, h, i_;
                    PetscReal a00, a01, a02, a11, a12, a22;

                    // Get gradient
                    a = 0.5 * ( ( U_array[k][j][i+1][0] - U_array[k][j][i-1][0] ) / d_[k][j][i].x); // ux
                    b = 0.5 * ( ( U_array[k][j+1][i][0] - U_array[k][j-1][i][0] ) / d_[k][j][i].y); // uy
                    c = 0.5 * ( ( U_array[k+1][j][i][0] - U_array[k-1][j][i][0] ) / d_[k][j][i].z); // uz

                    d = 0.5 * ( ( U_array[k][j][i+1][1] - U_array[k][j][i-1][1] ) / d_[k][j][i].x); // v...
                    e = 0.5 * ( ( U_array[k][j+1][i][1] - U_array[k][j-1][i][1] ) / d_[k][j][i].y);
                    f = 0.5 * ( ( U_array[k+1][j][i][1] - U_array[k-1][j][i][1] ) / d_[k][j][i].z);

                    g = 0.5 * ( ( U_array[k][j][i+1][2] - U_array[k][j][i-1][2] ) / d_[k][j][i].x); // w...
                    h = 0.5 * ( ( U_array[k][j+1][i][2] - U_array[k][j-1][i][2] ) / d_[k][j][i].y);
                    i_ = 0.5 * ( ( U_array[k+1][j][i][2] - U_array[k-1][j][i][2]) / d_[k][j][i].z);

                    // The thing (we compute (S^2 + Omega^2) * 0.5) i.e. the symmetric part of the gradient velocity
                    a00 = a * a + 1./4. * (b - d) * (-b + d)
                            + 1./4. * (b + d) * (b + d) + 1./4. * (c - g) * (-c + g)
                            + 1./4. * (c + g) * (c + g);
                    a01 = 1./2. * a * (b + d) + 1./2. * (b + d) * e + 1./4.
                            * (c - g) * (-f + h) + 1./4. * (c + g) * (f + h);
                    a02 = 1./2. * a * (c + g) + 1./4. * (b - d) * (f - h)
                            + 1./4. * (b + d) * (f + h) +  1./2. * (c + g) * i_;

                    a11 = 1./4. * (b - d) * (-b + d)
                            + 1./4. * (b + d) * (b + d) + e * e + 1./4. * (f - h) * (-f + h)
                            + 1./4. * (f + h) * (f + h);
                    a12 = 1./4. * (-b + d) * (c - g) + 1./4. * (b + d)
                            * (c + g) + 1./2. * e * (f + h) + 1./2. * (f + h) * i_;

                    a22 = 1./4. * (c - g) * (-c + g)
                            + 1./4. * (c + g) * (c + g) + 1./4. * (f - h) * (-f + h)
                            + 1./4. * (f + h) * (f + h) + i_ * i_;

                    // The gamma
                    g2_array[k][j][i] = eig3( a00, a11, a22, a01, a02, a12 );
                }
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da_coord, p_dom_->D, &d_); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da, U_loc, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, g2, &g2_array); CHKERRQ(ierr);
    } else {
        Field3D      **U_array;
        PetscReal    **g2_array;
        ierr = DMDAVecGetArray(p_dom_->da, U_loc, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(p_dom_->da_sc, g2, &g2_array); CHKERRQ(ierr);
        for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
            for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                g2_array[j][i] = 0;
            }
        }
        ierr = DMDAVecRestoreArray(p_dom_->da, U_loc, &U_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(p_dom_->da_sc, g2, &g2_array); CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(p_dom_->da, &U_loc); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Implements the orthogonal of a 3D vector
//!
PetscErrorCode Projection::orthogonal(Vec V)
{
    // TODO: 3D
    FUNCTIONBEGIN(p_cm_);

    Field2D        **v_array;
    ierr = DMDAVecGetArray(p_dom_->da, V, &v_array); CHKERRQ(ierr);
    for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
        for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
            PetscReal temp = v_array[j][i][0];
            v_array[j][i][0] = v_array[j][i][1];
            v_array[j][i][1] = -temp;
        }
    }
    ierr = DMDAVecRestoreArray(p_dom_->da, V, &v_array); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}
