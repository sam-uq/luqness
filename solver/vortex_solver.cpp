/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "vortex_solver.h"

// TODO 3d
//! Allocate memory, evolver and stepper
//!
VortexSolver::VortexSolver(Options *p_opt, Domain *p_dom,
                           ModelBase *p_mod, Attila * p_cm)
    : Solver(p_opt, p_dom, p_mod, p_cm) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->enable_3d ) {
        WARNING(p_cm_, INTRADM,
                "3D is enabled BUT you are trying to use a "
                "Vortex solver, for which this is "
                "currently unavailable. I'll fall back to 2D (or not).\n", "");
    }

    //     p_opt_->buffer_fields->preallocate(0, 0);
    //     p_opt_->buffer_scalars->preallocate(0, 0);

    if( true ) {
        p_mod_->dt = p_dom_->h * p_opt_->lambda;
    }

    switch( p_opt_->ts_type) {
    case TimeSteppingType::Forward:
    default:
        if( p_opt_->enable_ftcs) {
            evo = new FTCSVortConv(p_opt_, p_dom, p_mod_, p_cm_);
            step = new FTCSVortStep(p_opt_, p_dom, p_mod_, p_cm_, p_proj_, evo);
        } else {
            evo = new VortConv(p_opt_, p_dom, p_mod_, p_cm_);
            step = new LevyTadmorVortStep(p_opt_, p_dom, p_mod_, p_cm_, p_proj_, evo);
        }
        break;
    case TimeSteppingType::CrankNicolson:
        evo = new StabVortConv(p_opt_, p_dom, p_mod_, p_cm_);
        step = new StabVortStep(p_opt_, p_dom, p_mod_, p_cm_, p_proj_, evo);
        break;
    }

    avg_flag = false;
}

//! Free memory and delete evo and step
//!
VortexSolver::~VortexSolver() {
    FUNCTIONBEGIN(p_cm_);

    if( avg_flag ) {
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w_avg); CHKERRV(ierr);
        ierr = DMRestoreGlobalVector(p_dom_->da, &U_avg); CHKERRV(ierr);
    }

    // Finalize data
    delete              evo;
    delete              step;
}

//! inizialize vector U and G with function handles provided by u_0 etc...
//!
PetscErrorCode VortexSolver::startup() {
    FUNCTIONBEGIN(p_cm_);
    static_cast<VortStep*>(step)->p_evo_->offset_idx = 0;

    if(p_opt_->force_initial_data_as_velocity) {
        p_mod_->set_U_0(U);
        ierr = p_proj_->curl(w, U, p_dom_->bd_fam.U); CHKERRQ(ierr);
    } else {
        p_mod_->set_w_0(w);
        if( p_opt_->max_itr != 0 ) {
            ierr = static_cast<VortStep*>(step)->extract_velocity(w, U); CHKERRQ(ierr);
        }
    }

    PetscFunctionReturn(ierr);
}

PetscErrorCode VortexSolver::advance() {
    FUNCTIONBEGIN(p_cm_);

    ierr = static_cast<VortStep*>(step)->step(w, U, F_b, F); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

PetscErrorCode VortexSolver::update_quantity_derived(void) {

    if( static_cast<VortStep*>(step)->p_evo_->offset_idx == 1 ) {
        DEBUG(p_cm_, INTRADM, "Step has offset 1, avg. before export\n", "");
        if( !avg_flag ) {
            avg_flag = true;
            ierr = DMGetGlobalVector(p_dom_->da_sc, &w_avg); CHKERRQ(ierr);
            ierr = DMGetGlobalVector(p_dom_->da, &U_avg); CHKERRQ(ierr);
        }
        ierr = p_dom_->avg<true, -1>(w, w_avg);
        ierr = static_cast<VortStep*>(step)->extract_velocity(w_avg, U_avg);
        quantity->U = U_avg;
        quantity->omega = w_avg;
    } else {
        quantity->U = U;
        quantity->omega = w;
    }

    PetscFunctionReturn(ierr);
}
