/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file vortex_solver.h
//! A solver for the (2D) vortex method

#include "../transport/stab_vort_conv.h"
#include "../transport/vort_conv.h"

#include "../step/stab_vort_step.h"
#include "../step/levy_tadmor_vort_step.h"
#include "../step/ftcs_vort_step.h"
#include "./solver.h"

// TODO 3d

//! THis class implements a solver for the vortex method in 2D
//!
class VortexSolver : public Solver {
public:
    //! Constructor, allocate space for temporary vectors,
    //! solution vectors and for laplacian matrix
    VortexSolver(Options * p_opt, Domain * p_dom,
                 ModelBase * p_mod, Attila * p_cm);
    //! Free memory
    ~VortexSolver();

    //! Startup step
    PetscErrorCode startup();
    //! perform a single step for the method
    //!
    PetscErrorCode advance();
    //!
    PetscErrorCode update_quantity_derived(void);
private:
    VortConv      * evo;
    //! Material needed for exporting an averaged state
    Vec w_avg, U_avg;
    //! Flag is true as long as we need and allocate memory for _avg vectors
    bool avg_flag;
};
