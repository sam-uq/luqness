/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file projection_solver.h
//! Implement the actual projection solver, in general,
//! i.e. general initialization, finalization, snapshot type etc...

// Intenral includes
#include "../transport/stab_conv.h"
#include "../transport/bcg_conv.h"
#include "../transport/almgren_conv.h"

#include "../step/stab_proj_step.h"
#include "../step/cn_stab_proj_step.h"
#include "../step/acc_proj_step.h"
#include "../step/vel_proj_step.h"
#include "../step/lf_proj_step.h"

#include "./solver.h"

//!
//! Class that implements a complete solver for the Projection method
//! TODO: In this class we can choose the nonlinear term we want to use, the type of projection,
//! the type of update and the type of increment
//!
class ProjectionSolver : public Solver {
public:
    //! Constructor, allocate space for temporary vectors,
    //! solution vectors and for laplacian matrix
    ProjectionSolver(Options * p_opt, Domain * p_dom,
                     ModelBase * p_mod, Attila * p_cm);
    //! Free some memory
    ~ProjectionSolver();
    //! Provide initial data and initialize everything
    PetscErrorCode startup();
    //! Do 1 step in time (called from step() in the Base class)
    //! Cast the projection to the right type and call the step routine
    //! TODO: maybe can work without casting and using virtual
    //! Need to investigate
    PetscErrorCode advance();
    //! save solution to file
    //!
    PetscErrorCode update_quantity_derived(void);
private:
    Conv            * conv;

    //! Global vectors for the solution (pressure gradient)
    Vec                   G;
    Vec                   g2;
};
