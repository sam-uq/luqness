/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string>

#include "solver.h"

Solver::Solver(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila *p_cm)
    : p_opt_(p_opt), p_dom_(p_dom), p_mod_(p_mod), p_cm_(p_cm) {
    FUNCTIONBEGIN(p_cm_);

    ierr = DMGetGlobalVector(p_dom_->da, &U); CHKERRV(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &F); CHKERRV(ierr);
    ierr = DMGetGlobalVector(p_dom_->da, &F_b); CHKERRV(ierr);
    ierr = DMGetGlobalVector(p_dom_->da_sc, &coloring); CHKERRV(ierr);

    if( p_opt_->enable_3d ) {
        ierr = DMGetGlobalVector(p_dom_->da, &w); CHKERRV(ierr);
    } else {
        ierr = DMGetGlobalVector(p_dom_->da_sc, &w); CHKERRV(ierr);
    }

    if( p_opt_->use_exact ) {
        //     p_proj_ = new ProjectionDecoupled(p_opt_, p_dom_, p_mod_, p_cm_);
    } else {
        p_proj_ = new ProjectionLaplacian(p_opt_, p_dom_, p_mod_, p_cm_);
    }

    quantity = new Quantity(p_opt_, p_dom_, p_cm_, p_mod_, "", false);
}

Solver::~Solver() {
    FUNCTIONBEGIN(p_cm_);

    ierr = save_info(); CHKERRV(ierr);

    ierr = DMRestoreGlobalVector(p_dom_->da, &U); CHKERRV(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &F); CHKERRV(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da, &F_b); CHKERRV(ierr);
    ierr = DMRestoreGlobalVector(p_dom_->da_sc, &coloring); CHKERRV(ierr);

    if( p_opt_->enable_3d ) {
        ierr = DMRestoreGlobalVector(p_dom_->da, &w); CHKERRV(ierr);
    } else {
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &w); CHKERRV(ierr);
    }

    delete        p_proj_;
    delete        quantity;

    return;
}
PetscErrorCode Solver::initialize() {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->do_transport ) p_mod_->set_scalar_0(coloring);

    ierr = startup(); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Perform the actual solution, i.e. iterate through each time calling everything you need
//!
PetscErrorCode Solver::solve() {
    FUNCTIONBEGIN(p_cm_);

    OUTPUT(p_cm_, INTRADMSID, "Stand, by: starting new simulation (tot=%d).\n", p_cm_->sim_num);

    if( p_opt_->pretend ) return ierr;

    std::stringstream ss1;
    ss1 << "sim_id_" << p_cm_->sim_id << "_begin";
    p_profiler->tic(ss1.str());

    snapshots_count = 0;
    push_to_mav_count = 0;
    step_counter = 0;
    p_mod_->t = 0;

    PetscBool   take_snapshot_this_time;
    PetscReal   last_snapshot = 0;

    ierr = initialize(); CHKERRQ(ierr);

    if( p_opt_->save_initial ) {
        save_snapshot();
    }

    ierr = push_energy();

    if( p_opt_->max_itr == 0 ) {
        OUTPUT(p_cm_, INTRADMSID, "Max number of iterations 0 reached, breaking...\n", "");
    } else {

        while( true ) {


            p_mod_->dt_old = p_mod_->dt; // Save old time step to check if it has changed

            p_profiler->begin_event("cfl_propagation");
            ierr = step->CFL(U); CHKERRQ(ierr);

            int arg;
            PetscReal next_snapshot =
                min_arg(p_opt_->T,
                        p_opt_->save_fps ? last_snapshot + p_opt_->snapshot_fps : HUGE_VAL,
                        snapshots_count < p_opt_->num_snapshot ? p_opt_->snapshot_times[snapshots_count] : HUGE_VAL,
                    arg);
                DEBUG(p_cm_, INTRADMSID,
                      "Next snapshot taken at %.2f because of reason %d.\n",
                      next_snapshot, arg);

            if( p_opt_->force_snapshot_times ) {
                if (p_mod_->t + p_mod_->dt >= next_snapshot )  {
                    DEBUG(p_cm_, INTRADMSID, "Shortening cell before snapshot.\n", "");
                    p_mod_->dt = next_snapshot - p_mod_->t;
                } else if( p_mod_->t + p_mod_->dt * 3. / 2. >= next_snapshot ) {
                    DEBUG(p_cm_, INTRADMSID, "Splitting cell in half before forced snapshot.\n", "");
                    p_mod_->dt = (next_snapshot - p_mod_->t) / 2;
                }
            }

            take_snapshot_this_time = PETSC_FALSE;
            if (p_mod_->t + p_mod_->dt >= next_snapshot )  {
                take_snapshot_this_time = PETSC_TRUE;
            }

            if( take_snapshot_this_time ) {
                if( arg == 0 && !p_opt_->save_final ) {
                    DEBUG(p_cm_, INTRADMSID, "Supressing snapshot saving at final time.\n", "");
                    take_snapshot_this_time = PETSC_FALSE;
                } else if( arg == 1 ) {
                    DEBUG(p_cm_, INTRADMSID, "Saving snapshot because fps says so.\n", "");
                    last_snapshot = p_mod_->t + p_mod_->dt;
                } else if (  arg == 2 ) {
                    DEBUG(p_cm_, INTRADMSID, "Saving snapshot because snapshot array says so.\n", "");
                    snapshots_count++;
                }
            }

            // Master check's if dt is different. This is a double comparison and in principle it's dangerous but we don't
            // care, is just for efficiency if we detect equality, otherwise we recompute matrices
            if(p_mod_->dt_old != p_mod_->dt) p_mod_->dt_changed = true;
            else p_mod_->dt_changed = false;

            int dt_changed_int = (int) p_mod_->dt_changed;
            // For safety reasons we broadcast temporal data to other processes: u'll never know what may happen
            ierr = MPI_Bcast(&p_mod_->dt, 1, MPI_DOUBLE, 0, p_cm_->intra_domain_comm); CHKERRQ(ierr);
            ierr = MPI_Bcast(&dt_changed_int, 1, MPI_INTEGER, 0, p_cm_->intra_domain_comm); CHKERRQ(ierr);
            ierr = MPI_Bcast(&take_snapshot_this_time, 1, MPI_INTEGER, 0, p_cm_->intra_domain_comm); CHKERRQ(ierr);

            p_mod_->dt_changed = (bool) dt_changed_int;
            p_profiler->end_event("cfl_propagation");

            if( p_opt_->enable_forcing ) {
                ierr = p_mod_->set_F(F_b, p_mod_->t); CHKERRQ(ierr);
                ierr = p_mod_->set_F(F, p_mod_->t + p_mod_->dt * 0.5); CHKERRQ(ierr);
            }

            if( p_opt_->do_transport ) {
                ierr = step->transport(coloring, U); CHKERRQ(ierr);
            }

            p_profiler->begin_event("advance");
            OUTPUT(p_cm_, INTRADMSID, "[%3.2f%%] Step %d, t = %.3f of %.1f (dt = %.2e, sup(U) = %.2e) "
                                   "[elapsed: %.2e s].\r",
                   p_mod_->t / p_opt_->T * 100.,
                   step_counter, p_mod_->t, p_opt_->T, p_mod_->dt,
                   p_mod_->sup, p_profiler->toc(ss1.str()));
            DEBUG(p_cm_, INTRADMSID, "\n", "");
            ierr = advance(); CHKERRQ(ierr);
            p_profiler->end_event("advance");

            p_mod_->t += p_mod_->dt;

            ierr = push_energy(); CHKERRQ(ierr);

            ++step_counter;

            if( take_snapshot_this_time == PETSC_TRUE ) {
                ierr = save_snapshot(); CHKERRQ(ierr);
            }

            if( p_opt_->max_itr > 0 && step_counter >= p_opt_->max_itr ) {
                OUTPUT(p_cm_, INTRADM, "Max number of iterations %d reached, breaking...\n", step_counter);
                break;
            }
            if( p_mod_->t >= p_opt_->T ) break;
        }
    }

    OUTPUT(p_cm_, INTRADMSID, "Solution completed  in %d steps and %.2f s, took %d "
                           "snapshots.\n",
           step_counter, p_profiler->toc(ss1.str()), snapshots_count);
    std::stringstream ss2;
    ss2 << "sim_id_" << p_cm_->sim_id << "_end";
    p_profiler->tic(ss2.str());

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Solver::update_quantity(void) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->do_transport ) {
        quantity->ps = coloring;
    }

    ierr = update_quantity_derived(); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Call the save_vec for each solution
//!
PetscErrorCode Solver::save_snapshot() {
    FUNCTIONBEGIN(p_cm_);

    update_quantity();

    if( p_opt_->save_sim ) {
#if CXX11_ENABLE
        std::string file_name = "sim" + std::to_string(p_cm_->sim_id)
                + "_f" + std::to_string(snapshots_count);
	quantity->save_all(file_name.c_str());
#else
        char file_name[PETSC_MAX_PATH_LEN];
        sprintf(file_name, "sim%d_f%d", (int) p_cm_->sim_id, (int) snapshots_count);
        quantity->save_all(file_name);
#endif
    }

    if( p_opt_->save_hist) {
        p_hist_->push_data(snapshots_count, quantity);
    }

    if(p_opt_->push_snapshot_to_mav != 0 &&
            snapshots_count % p_opt_->push_snapshot_to_mav == 0 ) {

    try {
        switch( p_opt_->quad_type ) {
        case QuadratureType::Stochastic:
            ierr = quantity->add_to_mav(p_quantity_means_->at(push_to_mav_count),
                                        p_quantity_M2s_->at(push_to_mav_count)); CHKERRQ(ierr);
            break;
        default:
        case QuadratureType::Deterministic:
            if( p_opt_->save_m2 ) {
                ierr = quantity->add_as_quad_and_var(p_mod_->p_smp_->next_weight(),
                                                     p_quantity_means_->at(push_to_mav_count),
                                                     p_quantity_M2s_->at(push_to_mav_count)); CHKERRQ(ierr);
            } else {
                ierr = quantity->add_as_quad(p_mod_->p_smp_->next_weight(),
                                             p_quantity_means_->at(push_to_mav_count)); CHKERRQ(ierr);
            }
            break;
        }
    } catch (const std::out_of_range& oor) {

        std::stringstream ss1;
        ss1 << "mean-col";
        ss1 << p_cm_->intra_domain_color;
        ss1 << "_f" << push_to_mav_count;

        p_quantity_means_->push_back(new Quantity(p_opt_, p_dom_, p_cm_, p_mod_, ss1.str(), true));

        std::stringstream ss2;
        ss2 << "m2-col";
        ss2 << p_cm_->intra_domain_color;
        ss2 << "_f" << push_to_mav_count;

        p_quantity_M2s_->push_back(new Quantity(p_opt_, p_dom_, p_cm_, p_mod_, ss2.str(), true));

        try {
            // FIXME HORRIBLE
            switch( p_opt_->quad_type ) {
                case QuadratureType::Stochastic:
                    ierr = quantity->add_to_mav(p_quantity_means_->at(push_to_mav_count),
                                                p_quantity_M2s_->at(push_to_mav_count)); CHKERRQ(ierr);
                                                break;
                case QuadratureType::Deterministic:
                default:
                    if( p_opt_->save_m2 ) {
                        ierr = quantity->add_as_quad_and_var(p_mod_->p_smp_->next_weight(),
                                                             p_quantity_means_->at(push_to_mav_count),
                                                             p_quantity_M2s_->at(push_to_mav_count)); CHKERRQ(ierr);
                    } else {
                        ierr = quantity->add_as_quad(p_mod_->p_smp_->next_weight(),
                                                     p_quantity_means_->at(push_to_mav_count)); CHKERRQ(ierr);
                    }
                    break;
            }
        } catch (const std::out_of_range& oor) {
            ERROR(p_dom_->p_cm_, INTRADM,
                  "Something went wrong when pushing snapshot to conatiner.", "");
        }
    }

        ++push_to_mav_count;
    }
    // TODO: option to not push onto Quantity, option to reload at the end
    ++snapshots_count;

    PetscFunctionReturn(ierr);
}

//! Saves informations about the solver to a file
//!
PetscErrorCode Solver::save_info(void) {
    FUNCTIONBEGIN(p_cm_);
    FILE * f;
    char file_name[PETSC_MAX_PATH_LEN];

    if( p_opt_->save_energy ) {
        // TODO insert data format and change this
        sprintf(file_name, "%s_energy_sim%d.dat",
                p_opt_->simulation_name, p_cm_->sim_id);
        DEBUG(p_cm_, INTRADMSID,
              "[I/O] Exporting energy to \"%s\"...\n", file_name);
        std::stringstream ss;
        ss << std::setprecision(9);
        for(uint i = 0; i != times.size(); ++i) {
            ss << times.at(i);
            for(uint j = 0; j < energy.size(); ++j) {
                try {
                    ss << " " << energy.at(j).at(i);
                }
                catch(const std::out_of_range& oor) {

                }
            }
            ss << std::endl;
        }
        f = fopen(file_name, "w");
        PetscFPrintf(p_cm_->intra_domain_comm, f, "%s", ss.str().c_str());
        fclose(f);
    }

    PetscFunctionReturn(ierr);
}

//! Push energy onto the energy vector
//!
PetscErrorCode Solver::push_energy(Vec V, uint index, Vec O) {
    if( p_opt_->save_energy ) {
        FUNCTIONBEGIN(p_cm_);

        if( V == NULL) {
            V = U;
            index = 0;
        }
        if(index >= energy.size()) {
        energy.resize(index + 1);
        }

        PetscReal temp;
        if(O != NULL) {
            ierr = p_dom_->DotL2(V, O, &temp); CHKERRQ(ierr);
            DEBUG(p_cm_, INTRADM, "[SLV] Dot product computed (D = %f)...\n", temp);
        } else {
            ierr = p_dom_->NormL2(V, &temp); CHKERRQ(ierr);
            temp *= temp;
            temp *= 0.5;
            DEBUG(p_cm_, INTRADM, "[SLV] Energy computed (E = %f)...\n", temp);
        }
        if( index == 0 ) {
            times.push_back(p_mod_->t);
        }
        energy.at(index).push_back(temp);
    }

    PetscFunctionReturn(ierr);
}
