/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file solver.h
//! A general "solver" skeleton complete with timestepping routines etc... (need to be subclassed)

class Solver;

#include "../tools/tools.h"
#include "../tools/options.h"
#include "../tools/domain.h"
#include "../tools/profiler.h"
#include "../tools/quantity.h"
#include "../tools/histo_data.h"
#include "../tools/utilities.h"
#include "../step/step.h"
#include "../projection/projection_laplacian.h"
#include "../models/model_base.h"

//! THis class provides the virtual skeleton for the implementation of a real solver
//! Need to be subclassed
class Solver {
public:
    //! Constructor, allocate space for temporary vectors, solution vectors and for laplacian matrix
    Solver(Options * p_opt, Domain * p_dom, ModelBase * p_mod, Attila * p_cm);
    //! Virtual destructor
    virtual ~Solver();

    //! Performs all timesteppings up to time T, takes care of initial data
    virtual PetscErrorCode solve();

    //! Initialize all general values
    //! @brief Initialize initial data etc...
    //! This initialize all the initial data that is shared by
    //! all solvers, then calls the
    //! startup method of its derived class to perform a proper
    //! startup step
    //! for instance in the case of projection method initialize
    //! velocity and do a div_cleansing step
    PetscErrorCode initialize();

    //! Set vectors where to save quantities
    PetscErrorCode set_quantities_vectors(std::vector<Quantity*> *p_quantity_means,
                                          std::vector<Quantity*> *p_quantity_M2s) {
        p_quantity_means_ = p_quantity_means;
        p_quantity_M2s_ = p_quantity_M2s;
        PetscFunctionReturn(ierr);
    }

    //! Set vectors where to save quantities
    PetscErrorCode set_histo_data(HistoData * p_hist) {
        p_hist_ = p_hist;
        PetscFunctionReturn(ierr);
    }

    //! Update forcing variables at time t
    PetscErrorCode updateForcing(Vec & F, PetscReal t);

    // IO Methods
    //! This has to be called whenever we need to access
    //!  to "quantity", is valid until solver is valid,
    //! and is not preallocated
    PetscErrorCode update_quantity(void);
    //! Save general vectors
    virtual PetscErrorCode save_snapshot();

    //!
    PetscErrorCode save_info(void);
    //! If O is given,then compute inner product instead
    PetscErrorCode push_energy(Vec V = NULL, uint index = 0, Vec O = NULL);

    // Global vectors for the solution
    Vec                           U, F, F_b, w; //!< Solution and forcing containers
    Vec                           coloring; //!< Passive scalar container
    Vec                           delta; //!< Contains variable deltas for anisotropic meshes

    // Collected solver data
    std::vector<PetscReal>        times; //!< The energy at each point
    std::vector< std::vector<PetscReal> >        energy; //!< The energy at each point


    Quantity *                    quantity;

    Step                          * step;

    PetscInt                      step_counter; //!< Number of steps since start
    PetscInt                      snapshots_count, push_to_mav_count;
protected:
    //! Performs a startup step (in derived)
    virtual PetscErrorCode startup() = 0;
    //! Performs a normal step (in derived)
    virtual PetscErrorCode advance() = 0;
    //!
    virtual PetscErrorCode update_quantity_derived(void) = 0;

    Options                       * const p_opt_;
    Domain                        * const p_dom_;
    ModelBase                     * const p_mod_;
    Attila                        * const p_cm_;

    Projection                    * p_proj_;

    PetscBool                     take_snapshot; // take snapshot this time
private:
    //! Contains a quantity pointer for each frame (mean container)
    std::vector<Quantity*>        * p_quantity_means_;
    //! Contains a quantity pointer for each frame (M2 container)
    std::vector<Quantity*>        * p_quantity_M2s_;
    HistoData                     * p_hist_;
};

