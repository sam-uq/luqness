/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "./projection_solver.h"

ProjectionSolver::ProjectionSolver(Options *p_opt, Domain *p_dom,
                                   ModelBase *p_mod, Attila *p_cm)
    : Solver(p_opt, p_dom, p_mod, p_cm) {
    FUNCTIONBEGIN(p_cm_);
    // Create a global vector for velocity (i.e. it contains vectors
    // define EXCLUSIVELY on the local process )
    // Setup vector for pressure, forcing (at different times)
    // and colireing (global)
    //     p_opt_->buffer_fields->preallocate(1,0);
    //     p_opt_->buffer_scalars->preallocate(1,0);

    ierr = DMGetGlobalVector(p_dom_->da, &G); CHKERRV(ierr);
    if( p_opt_->save_gamma2 && p_opt_->enable_3d ) {
        ierr = DMGetGlobalVector(p_dom_->da_sc, &g2); CHKERRV(ierr);
    }

    // TODO: Choose what to use from options using enums
    switch(p_opt_->ts_type) {
        case TimeSteppingType::BackwardStable:
            conv = new StabConv(p_opt_, p_dom_, p_mod_, p_cm_);
            step = new StabProjStep(p_opt_, p_dom_, p_mod_,
                                    p_cm_, p_proj_, conv, this);
            break;
        case TimeSteppingType::CrankNicolson:
            conv = new StabConv(p_opt_, p_dom_, p_mod_, p_cm_);
            step = new CNStabProjStep(p_opt_, p_dom_, p_mod_,
                                      p_cm_, p_proj_, conv, this);
            break;
        case TimeSteppingType::LeapFrog:
            conv = new BCGConv(p_opt_, p_dom_, p_mod_, p_cm_);
            step = new LFProjStep(p_opt_, p_dom_, p_mod_,
                                  p_cm_, p_proj_, conv, this);
            break;
        case TimeSteppingType::Forward:
        default:
            if( p_opt_->conv_type == ConvectionType::Almgren ) {
                conv = new AlmgrenConv(p_opt_, p_dom_, p_mod_, p_cm_);
            } else { // BCG
                conv = new BCGConv(p_opt_, p_dom_, p_mod_, p_cm_);
            }
            // TODO vel acc step
            step = new VelProjStep(p_opt_, p_dom_, p_mod_,
                                   p_cm_, p_proj_, conv);
            break;
    }
}

ProjectionSolver::~ProjectionSolver() {
    FUNCTIONBEGIN(p_cm_);

    ierr = DMRestoreGlobalVector(p_dom_->da, &G); CHKERRV(ierr);
    if( p_opt_->save_gamma2 && p_opt_->enable_3d ) {
        ierr = DMRestoreGlobalVector(p_dom_->da_sc, &g2); CHKERRV(ierr);
    }

    // Finalize vectors
    delete        conv;
    delete        step; // TODO: HERE CHOOSE STEP WITH OR W/O NS
}

PetscErrorCode ProjectionSolver::startup() {
    FUNCTIONBEGIN(p_cm_);

    if(p_opt_->force_initial_data_as_vorticity) {
        p_mod_->set_w_0(w);
        ierr = step->extract_velocity(w, U); CHKERRQ(ierr);
    } else {
        p_mod_->set_U_0(U);
        p_mod_->set_Gp_0(G);

        // Initial div cleansing
        if( p_opt_->do_initial_cleansing ) {
            ierr = step->div_cleansing(U, G); CHKERRQ(ierr); // Default is
        }
    }

    if( p_opt_->do_initial_iterates ) {
        // pass TODO
    }

    PetscFunctionReturn(ierr);
}

PetscErrorCode ProjectionSolver::advance() {
    FUNCTIONBEGIN(p_cm_);

    // TODO: Choose what to use from options using enums
    if ( p_opt_->ts_type == TimeSteppingType::BackwardStable ) {
        ierr = static_cast<StabProjStep*>(step)
                ->step(U, G, F_b, F); CHKERRQ(ierr);
    } else if( p_opt_->ts_type == TimeSteppingType::CrankNicolson ) {
        ierr = static_cast<CNStabProjStep*>(step)
                ->step(U, G, F_b, F); CHKERRQ(ierr);
    } else if( p_opt_->projupd_type == ProjectionUpdateType::VelocityProjection ) {
        // Forward but proj velocity
        ierr = static_cast<VelProjStep*>(step)
                ->step(U, G, F_b, F); CHKERRQ(ierr);
    } else {
        ierr = static_cast<AccProjStep*>(step)
                ->step(U, G, F_b, F); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

PetscErrorCode ProjectionSolver::update_quantity_derived(void) {
    FUNCTIONBEGIN(p_cm_);
    quantity->Gp = G;

    ierr = p_proj_->curl(U, w, p_dom_->bd_fam.U); CHKERRQ(ierr);
    quantity->U = U;
    quantity->omega = w;

    if( p_opt_->save_gamma2 && p_opt_->enable_3d ) {
        ierr = p_proj_->gamma2(U, g2, p_dom_->bd_fam.U); CHKERRQ(ierr);
        quantity->gamma2 = g2;
    }

    PetscFunctionReturn(ierr);
}
