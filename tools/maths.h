/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
/ * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file maths.h
//! General math functions like std::min, std::max and sign

#include "tools.h"

#include <math.h>
#include <cmath>
#include <cfloat>

// #include <iostream>

//! Pi
#ifndef PI
#   ifdef M_PI
#       define PI M_PI
#   else // NO M_PI
#       define PI 3.1415926535897932384626433832795028841971693993751
#   endif // NO M_PI
#endif

#if CXX11_ENABLE == 1
  #define M_EPSILON std::numeric_limits<double>::epsilon()
#else
#  define M_EPSILON DBL_EPSILON
#endif

//! Signum (sgn) function for x
inline PetscScalar sign(const PetscScalar & x) {
    return (x > 0) ? 1 : ((x < 0) ? -1 : 0);
}

//! std::max function for 3 args
//!
template <typename T>
inline T max(const T & x, const T & y, const T & z) {
    return std::max(std::max(x,y),z);
}

//! std::min function for 3 args
//!
template <typename T>
inline T min(const T & x, const T & y, const T & z) {
    return std::min(std::min(x,y),z);
}

//! std::min function for 4 args
//!
template <typename T>
inline T min(const T & x, const T & y, const T & z, const T & zz) {
    return std::min(min(x,y,z),zz);
}

//! std::min function for 3 args
//!
template <typename T>
inline T min_arg(const T & x, const T & y, int & arg) {
    if( x <= y ) { arg = 0; return x; }
    arg = 1; return y;
}

//! std::min function for 3 args
//! returns also which one is the smallest
template <typename T>
inline T min_arg(const T & x, const T & y, const T & z, int & arg) {
    T m = min_arg(x, y, arg);
    int arg2;
    m = min_arg(m, z, arg2);
    if(arg2 == 1) arg = 2;
    return m;
}


//! The ACTUAL mod function and not the WEIRD one IMPLEMENTED by C++ %
// inline PetscInt mod(const PetscInt & d, const PetscInt & M) { return d >= 0 ? d % M : d + M * (- d / M + 1); }

//! The MC limiter procedure
//!
inline PetscScalar MC(PetscScalar L, PetscScalar R, PetscScalar C)
{
    return sign(L) == sign(R) ? std::min(std::abs(C), std::min(std::abs(R), std::abs(L)) * 2.0) * sign(C) : 0 ;
}

//! A fourth order procedure for slope limiting
//!
PetscScalar fourth(PetscScalar L, PetscScalar R, PetscScalar C);

//! The mimmod limiter
//!
inline PetscScalar minmod(PetscScalar a, PetscScalar b, PetscScalar c) {
    if( a > 0 && b > 0 && c > 0 ) {
        return min(a,b,c);
    } else if( a < 0 && b < 0 && c < 0 ) {
        return max(a,b,c);
    } else {
        return 0;
    }
}

//! Implementation of sech (i.e. hyperbolic secant)
//! as apparently not provided by stl
inline PetscScalar sech(PetscScalar x) {
    return 1. / std::cosh(x);
}

#if CXX11_ENABLE == 0
namespace std {
//! Implementation of atanh (i.e. arc-hyperbolic tangent)
//! as apparently not provided by C++ < 11
inline PetscScalar atanh(PetscScalar z) {
    return (std::log(1+z)-std::log(1-z))/2.;
}
}
#endif

//! Return 2D angle (polar/spherical coords), default 2D angle is C = 0.
//! @tparam C component, one of 0,1,2
template <int C>
inline PetscReal cart2angle(PetscReal x, PetscReal y, PetscReal z) {
    switch(C) {
    default:
    case 0:
        return atan2(y, x);
    case 1:
        return atan2(z, x);
    case 2:
        return atan2(z, y);
    }
}

//!
//! \brief cart2radius
//!
//! Return radius i.e. \f$|(x,y,z)|\f$
//! \param x
//! \param y
//! \param z
//! \return
//!
inline PetscReal cart2radius(PetscReal x, PetscReal y, PetscReal z) {
    return std::sqrt(x*x+y*y+z*z);
}

//! Convert a radius/angle pair to cartesian coordinates
inline void pol2cart(PetscReal r, PetscReal theta, PetscReal & x, PetscReal & y) {
    x = r * std::cos(theta);
    y = r * std::sin(theta);
//     std::cout << "r = " << r << " theta = " << theta << " x = " << x << " y = " << y << std::endl;
}

//! This routine return the second largest eigenvalue of a symmetric matrix, quite fast
PetscScalar eig3(PetscScalar a00, PetscScalar a11,
                 PetscScalar a22, PetscScalar a01,
                 PetscScalar a02, PetscScalar a12);
