/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "structures.h"
#include "quantity.h"
#include "domain.h"

#include <vector>
#include <iterator>
#include <sstream>
#include <iostream>
#include <fstream>

//!
//! \brief The HistoData class hooks into the Integrators
//! and extract useful Histograms from ensemble of solutions.
//!
class HistoData {
public:
    HistoData(Domain *p_dom, std::vector< Field3D > coords)
        : p_dom_(p_dom)
        , dof_num_(-1)
        , components_ (CompType::None) {

        assert(p_dom_ != NULL);

        tot_sim_num_ = p_dom_->p_cm_->p_alloc_->sim_per_level_[p_dom_->p_cm_->level];
        sim_num_ = p_dom_->p_cm_->sim_num;

        for(std::vector< Field3D >::iterator it = coords.begin(); it != coords.end(); ++it) {
            Field<3, int> idx;
            bool flag; // True if index is actually in range
            idx = p_dom_->get_3d_index(*it, &flag);
            if( flag ) {
                indexes_.push_back(idx);
                coords_.push_back(*it);
                OUTPUT(p_dom_->p_cm_, INTRALVL, " ---> Conversion: Coord (%.2f, %.2f, %.2f) @ index (%d, %d, %d).\n",
                       (*it)[0], (*it)[1], (*it)[2], indexes_.back()[0], indexes_.back()[1], indexes_.back()[1]);
            }

        }

        coord_num_ = indexes_.size();

        if( coord_num_ < 1 ) {
            return;
        }

        OUTPUT(p_dom_->p_cm_, INTRADM, "Will drill %d coordinate(s) from %d simulations...\n",
               coords.size(), sim_num_);

    }

    ~HistoData() {
        for(std::vector<double*>::iterator it = data_.begin(); it != data_.end(); ++it ) {
            delete[] *it;
        }
        if( coord_num_ >= 1 && p_dom_->p_cm_->is_root && p_dom_->p_cm_->intra_level_size > 1 ) {
            delete[] displ_data_;
        }
    }

    PetscErrorCode push_data(int frame, Quantity * qt) {

        if( coord_num_ < 1 ) {
            PetscFunctionReturn(ierr);
        }

        OUTPUT(p_dom_->p_cm_, INTRADM, "Pushing back histo-data (frame=%d).\n", frame);

        if( dof_num_  == -1 ) {
            dof_num_ = qt->dof_;

            if( p_dom_->p_cm_->is_root && p_dom_->p_cm_->intra_level_size > 1 ) {
                displ_data_ = new int[p_dom_->p_cm_->intra_level_size];
                displ_data_[0] = 0;
                size_data_ = new int[p_dom_->p_cm_->intra_level_size];
                size_data_[0] = p_dom_->p_cm_->sim_num_table[0] * dof_num_ * coord_num_;
                std::cout << displ_data_[0] << " <<< " << size_data_[0] << std::endl;
                for(int i = 1; i < p_dom_->p_cm_->intra_level_size; ++i ) {
                    displ_data_[i] = displ_data_[i-1] + p_dom_->p_cm_->sim_num_table[i-1] * dof_num_ * coord_num_;
                    size_data_[i] = p_dom_->p_cm_->sim_num_table[i] * dof_num_ * coord_num_;
                    std::cout << displ_data_[i] << " <<< " << size_data_[i] << std::endl;
                }
            }
        }

        if( dof_num_ != qt->dof_ ) {
            ERROR(p_dom_->p_cm_, SELF, "Inconsistent DOFs for pushed quantity!\n", "");
        }

        if( data_.size() <= (uint) frame + 1 ) { // FIXME THIS

            if( p_dom_->p_cm_->is_root ) {
                data_.push_back(new double[tot_sim_num_*coord_num_*dof_num_]);
            } else {
                data_.push_back(new double[sim_num_*coord_num_*dof_num_]);
            }

            data_count_.push_back(0);

        }

        int axis1 = data_count_.at(frame)*coord_num_*dof_num_;

        int phase_dim = p_dom_->p_opt_->enable_3d ? 3 : 2;

        int c = 0;
        for(std::vector< Field<3, int> >::iterator it = indexes_.begin(); it != indexes_.end(); ++it) {
            int d = 0;
            int axis2 = c*dof_num_;
            if( qt->components_ & CompType::U ) {
                components_ |= CompType::U;
                if( !p_dom_->p_opt_->enable_3d ) {
                    Field2D val = qt->eval_2d_U(*it);
                    data_.at(frame)[axis1+axis2+d] = val[0];
                    data_.at(frame)[axis1+axis2+d+1] = val[1];
                } else {
                    Field3D val = qt->eval_3d_U(*it);
                    data_.at(frame)[axis1+axis2+d] = val[0];
                    data_.at(frame)[axis1+axis2+d+1] = val[1];
                    data_.at(frame)[axis1+axis2+d+2] = val[2];
                }
                d += phase_dim;
            }
            if( qt->components_ & CompType::omega ) {
                components_ |= CompType::omega;
                if( p_dom_->p_opt_->enable_3d ) {
                    Field3D val = qt->eval_3d_omega(*it);
                    data_.at(frame)[axis1+axis2+d] = val[0];
                    data_.at(frame)[axis1+axis2+d+1] = val[1];
                    data_.at(frame)[axis1+axis2+d+2] = val[2];
                    d += phase_dim;
                } else {
                    data_.at(frame)[axis1+axis2+d] = qt->eval_2d_omega(*it);
                    d += 1;
                }
            }
            if( qt->components_ & CompType::Gp ) {
                components_ |= CompType::Gp;
                if( !p_dom_->p_opt_->enable_3d ) {
                    Field2D val = qt->eval_2d_Gp(*it);
                    data_.at(frame)[axis1+axis2+d] = val[0];
                    data_.at(frame)[axis1+axis2+d+1] = val[1];
                } else {
                    Field3D val = qt->eval_3d_Gp(*it);
                    data_.at(frame)[axis1+axis2+d] = val[0];
                    data_.at(frame)[axis1+axis2+d+1] = val[1];
                    data_.at(frame)[axis1+axis2+d+2] = val[2];
                }
                d += phase_dim;
            }
            if( qt->components_ & CompType::ps ) {
                components_ |= CompType::ps;
                if( !p_dom_->p_opt_->enable_3d ) {
                    data_.at(frame)[axis1+axis2+d] = qt->eval_2d_ps(*it);
                } else {
                    data_.at(frame)[axis1+axis2+d] = qt->eval_3d_ps(*it);
                }
                d += 1;
            }
            if( qt->components_ & CompType::gamma2 ) {
                components_ |= CompType::gamma2;
                if( !p_dom_->p_opt_->enable_3d ) {
                    data_.at(frame)[axis1+axis2+d] = qt->eval_2d_gamma2(*it);
                } else {
                    data_.at(frame)[axis1+axis2+d] = qt->eval_3d_gamma2(*it);
                }
            }
            ++c;
        }
        ++data_count_.at(frame);

        PetscFunctionReturn(ierr);
    }

    PetscErrorCode merge() {

        if( coord_num_ < 1 ) {
            PetscFunctionReturn(ierr);
        }

        if( p_dom_->p_cm_->is_root ) {
            OUTPUT(p_dom_->p_cm_, SELF, "Root is receiving histogram data (tot=%d).\n",
                   tot_sim_num_ * dof_num_ * coord_num_);
        } else {
            OUTPUT(p_dom_->p_cm_, SELF, "Non-Root is sending histogram data (tot=%d).\n",
                   p_dom_->p_cm_->sim_num  * dof_num_ * coord_num_);
        }

        if( p_dom_->p_cm_->intra_level_size > 1 ) {
            for(std::vector<double*>::iterator it = data_.begin(); it != data_.end(); ++it ) {
                void * snd;
                if( p_dom_->p_cm_->is_root ) {
                    snd = MPI_IN_PLACE;
                } else {
                    snd = *it;
                }
                ierr = MPI_Gatherv(snd, p_dom_->p_cm_->sim_num  * dof_num_ * coord_num_, MPI_DOUBLE,
                                   *it, size_data_, displ_data_,
                                   MPI_DOUBLE, 0, p_dom_->p_cm_->intra_level_comm); CHKERRQ(ierr);
            }
        }

        // do binning?
        // TODO

        PetscFunctionReturn(ierr);
    }

    PetscErrorCode save() {

        if( coord_num_ < 1 ) {
            PetscFunctionReturn(ierr);
        }

        // Only base root
        if( p_dom_->p_cm_->is_root ) {
            int fr = 0;
//             std::cout << data_.size() << std::endl;
            for(std::vector<double*>::iterator it = data_.begin(); it != data_.end(); ++it ) {

//                 std::cout << "fr = " << fr << std::endl;
                std::stringstream ss;
                ss << p_dom_->p_opt_->simulation_name << "_histo_intral" << p_dom_->p_cm_->intra_level_color;
                ss << "_f" << fr << ".bin";

                std::ofstream file(ss.str().c_str(), std::ios::out | std::ios::binary);
                if (file.is_open()) {
                    file.write(reinterpret_cast<char*>(*it), tot_sim_num_*coord_num_*dof_num_*sizeof(double));
                    file.close();
                } else {
                    ERROR(p_dom_->p_cm_, SELF, "Unable to open file for histogram data!\n", "");
                }

                std::stringstream ss1;
                ss1 << p_dom_->p_opt_->simulation_name << "_histo_intral" << p_dom_->p_cm_->intra_level_color;
                ss1 << "_f" << fr << ".py";
                std::stringstream ss2;
                ss2 << "name=\"" << p_dom_->p_opt_->simulation_name << "\"" << std::endl;
                ss2 << "mesh=(" << p_dom_->mesh_str << ")" << std::endl;
                ss2 << "is3d=" << p_dom_->p_opt_->enable_3d << "" << std::endl;
                ss2 << "frame=" << fr << std::endl;
                ss2 << "time=" << 0 << std::endl;
                ss2 << "dof=" << dof_num_ << std::endl;
                ss2 << "component=(";
                int d = 0;
                if( components_ & CompType::U ) { ss2 << "\"U\""; if( (d+=2) < dof_num_ ) ss2 << ","; }
                if( components_ & CompType::omega ) { ss2 << "\"w\""; if( (++d) < dof_num_ ) ss2 << ","; }
                if( components_ & CompType::Gp ) { ss2 << "\"Gp\""; if( (d+=2) < dof_num_ ) ss2 << ","; }
                if( components_ & CompType::ps ) { ss2<< "\"ps\""; if( (++d) < dof_num_ ) ss2 << ","; }
                if( components_ & CompType::gamma2 ) { ss2 << "\"gamma2\""; if( (++d) < dof_num_ ) ss2 << ","; }
                ss2 << ")" << std::endl;
                ss2 << "type=\"hist-intral\"" << std::endl;
                ss2 << "coords=(";
                for(std::vector< Field3D >::iterator it = coords_.begin(); it != coords_.end(); ++it ) {
                    ss2 << "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2]<< ")";
                    if( it != coords_.end() - 1) ss2 << ",";
                }
                ss2 << ")" << std::endl;
                ss2 << "indexes=(";
                for(std::vector< Field<3, int> >::iterator it = indexes_.begin(); it != indexes_.end(); ++it ) {
                    ss2 << "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2] << ")";
                    if( it != indexes_.end() - 1) ss2 << ",";
                }
                ss2 << ")" << std::endl;
                ss2 << "sim_num=" << tot_sim_num_ << std::endl;
                ss2 << "coords_num=" << coord_num_ << std::endl;

                std::ofstream file2(ss1.str().c_str(), std::ios::out );
                if (file2.is_open()) {
                    file2 << ss2.str();
                    file2.close();
                } else {
                    ERROR(p_dom_->p_cm_, SELF, "Unable to open meta file for histogram data!\n", "");
                }

                ss.str(std::string());

                ++fr;
            }

        }

        PetscFunctionReturn(ierr);
    }

private:
    Domain * const p_dom_;

    //! Total number of simulations for this level
    int tot_sim_num_;
    //! Number of simulations for this process
    int sim_num_;
    //! Number of indexes to drill
    int coord_num_;
    //! Number of dof to account
    int dof_num_;
    //! Components saved
    int components_;

    //! Contains all coordinates for the drill
    std::vector< Field<3, int> > indexes_;

    //! Contain the final histogram indexed as [frame][coord][sim_id]
    std::vector<double *> data_;
    //! Number of data pushed so far for each frame
    std::vector<int> data_count_;
    //! Vector of coordinates
    std::vector<Field3D> coords_;
    //! Displacement of received data (only root)
    int * displ_data_;
    //! Displacement of received data (only root)
    int * size_data_;
};
