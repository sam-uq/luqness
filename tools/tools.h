/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef TOOLS_H
#define TOOLS_H

//! @file tools.h
//! Debugging flags, IO options macros etc....

//! Needs config.h
#include "config.h"

// External includes (all internal includes NEED to be after extenrla (PETSc ones))
// because they need to be included after DEsIRE_COMPLEX was set
// Ugly hack but necessary
// Always include tools.h as FIRST THING
#include <petscsys.h>
#include <petscvec.h>
#include <petscdmda.h>

// External libraries std
#include <stdlib.h>
#include <stdexcept>

#if PETSC_VERSION_MAJOR < 3 || PETSC_VERSION_MINOR < 4 // PETSC_VERSION less sharp than 3.4
//! If defined, use old PETSc function calls
#define         PETSC_OLDAPI_FUNC
//! If defined, use old PETSc time API
#define         PETSC_OLDAPI_TIME
//! If defined, use old DMDA calls
#define         PETSC_OLDAPI_DMDA34
#endif
#if PETSC_VERSION_MAJOR < 3 || PETSC_VERSION_MINOR < 5 // PETSC_VERSION less sharp than 3.5
//! If defined, use old-ish DMDA calls
#define         PETSC_OLDAPI_DMDA35
//! If defined, use old KSP calls
#define         PETSC_OLDAPI_KSP
#endif

#ifdef PETSC_OLDAPI_FUNC
//! Define to remove old PETSC function begin API.
#define PetscFunctionBeginUser   {}
#endif

#ifdef PETSC_OLDAPI_TIME
//! Define a API-agnostic way to get time from PETSc, old API
#define  Timer(x)  PetscGetTime(x)
#else
//! Define a API-agnostic way to get time from PETSc, new API
#define  Timer(x)  PetscTime(x)
#endif

#ifdef PETSC_OLDAPI_DMDA34
#define  DMDAGetReducedDMDA(x,y,z)   DMDAGetReducedDA(x,y,z)
#define  DMGetCoordinatesLocal(x,y)  DMDAGetCoordinates(x,y)
#define  DMGetCoordinateDM(x,y)      DMDAGetCoordinateDA(x,y)
#endif

#ifdef PETSC_OLDAPI_DMDA35
#define  DM_BOUNDARY_PERIODIC      DMDA_BOUNDARY_PERIODIC
#define  DM_BOUNDARY_NONE          DMDA_BOUNDARY_NONE
#endif

#ifdef PETSC_OLDAPI_KSP
#define  CreateMatrix(x, y)          DMCreateMatrix(x,  MATMPIAIJ, y)
#define  SetOperators(x, y, z)       KSPSetOperators(x, y, z, SAME_NONZERO_PATTERN)
#define  NullSpaceRemove(x, y)       MatNullSpaceRemove(x, y, PETSC_NULL)
#else
#define  CreateMatrix(x, y)          DMCreateMatrix(x, y)
#define  SetOperators(x, y, z)       KSPSetOperators(x, y, z)
#define  NullSpaceRemove(x, y)       MatNullSpaceRemove(x, y)
#endif

#if USE_COLORS
#define RED(x) \
    "\x1b[31;1m" x "\x1b[0m"
#define YELLOW(x) \
    "\x1b[33;1m" x "\x1b[0m"
#define GREEN(x) \
    "\x1b[32;1m" x "\x1b[0m"
#define BLUE(x) \
    "\x1b[34;1m" x "\x1b[0m"
#define MAGENTA(x) \
    "\x1b[35;1m" x "\x1b[0m"
#else // !USE_COLORS
#define RED(x) x
#define YELLOW(x) x
#define GREEN(x) x
#define BLUE(x) x
#define MAGENTA(x) x
#endif // USE_COLORS

// PREPROCESSOR MACRO TO DISABLE VERBOSITY/DEBUG/OUTPUT, from faster to slower
#if OUTPUT_LEVEL > 4
#define VERBOSE(comm, MACROCALL, string, ...) \
    MACROCALL(comm, "[VERBOSE] ", string, __VA_ARGS__)
#else
#define VERBOSE(x, ...)
#endif

#if OUTPUT_LEVEL > 3
#define DEBUG(comm, MACROCALL, string, ...) \
    MACROCALL(comm, BLUE("[-DEBUG-] "), string, __VA_ARGS__)
#else
#define DEBUG(x, ...)
#endif

#if OUTPUT_LEVEL > 2
#define OUTPUT(comm, MACROCALL, string, ...) \
    MACROCALL(comm, GREEN("[-INFO--] "), string, __VA_ARGS__)
#else
#define OUTPUT(x, ...)
#endif

#if OUTPUT_LEVEL > 2
#define KSPOUT(comm, MACROCALL, string, ...) \
    MACROCALL(comm, MAGENTA("[--KSP--] "), string, __VA_ARGS__)
#else
#define KSPOUT(x, ...)
#endif

#if OUTPUT_LEVEL > 1
#define WARNING(comm, MACROCALL, string, ...) \
    MACROCALL(comm, YELLOW("[WARNING] "), string, __VA_ARGS__)
#else
#define WARNING(x, ...)
#endif

#if OUTPUT_LEVEL > 0 && EXIT_ON_ERROR
    #define ERROR(comm, MACROCALL, string, ...) \
        MACROCALL(comm, RED("[-ERROR-] "), string, __VA_ARGS__)
#elif OUTPUT_LEVEL > 0
    #define ERROR(comm, MACROCALL, string, ...) \
        { \
            MACROCALL(comm, RED("[-ERROR-] "), string, __VA_ARGS__); \
            exit(-1); \
            /* MPI_Abort(MPI_COMM_WORLD); */ \
        }
#else
    #define ERROR(x, ...)
#endif

#define INTRADM(comm, prepend, string, ...) \
    PetscPrintf(comm->intra_domain_comm, prepend "<%d/%d> " string, \
    comm->intra_domain_color, comm->parallel_runs, __VA_ARGS__)
#define INTRADMSID(comm, prepend, string, ...) \
    PetscPrintf(comm->intra_domain_comm, prepend "<%d/%d@%d> " string, \
    comm->intra_domain_color, comm->parallel_runs, comm->sim_id, __VA_ARGS__)
#define INTRALVL(comm, prepend, string, ...) \
    PetscPrintf(comm->intra_level_comm, prepend "[%d/%d] " string, \
    comm->intra_level_color, comm->intra_domain_size, __VA_ARGS__)
#define INTERLV(comm, prepend, string, ...) \
    PetscPrintf(comm->inter_level_comm, prepend "{%d/%d} " string, \
    comm->inter_level_color, comm->intra_domain_size, __VA_ARGS__)
#define WORLD(comm, prepend, string, ...) \
    PetscPrintf(PETSC_COMM_WORLD, prepend "WORLD " string, __VA_ARGS__)
#define SELF(comm, prepend, string, ...) \
    PetscPrintf(PETSC_COMM_SELF, prepend "(%d/%d) " string, comm->world_rank, comm->world_size, __VA_ARGS__)
#define SELFSID(comm, prepend, string, ...) \
    PetscPrintf(PETSC_COMM_SELF, prepend "(%d/%d@%d) " string, \
        comm->world_rank, comm->world_size, comm->sim_id, __VA_ARGS__)

//! Find out the compiler
#if defined(__clang__)
/* Clang/LLVM. ---------------------------------------------- */
#define CC_CLANG
#elif defined(__ICC) || defined(__INTEL_COMPILER)
/* Intel ICC/ICPC. ------------------------------------------ */
#define CC_INTEL
#elif defined(__GNUC__) || defined(__GNUG__)
/* GNU GCC/G++. --------------------------------------------- */
#define CC_GNU
#elif defined(__HP_cc) || defined(__HP_aCC)
/* Hewlett-Packard C/aC++. ---------------------------------- */
#elif defined(__IBMC__) || defined(__IBMCPP__)
/* IBM XL C/C++. -------------------------------------------- */
#elif defined(__Cray)
/* Cray compiler. ------------------------------------------- */
#define CC_CRAY
#elif defined(_MSC_VER)
/* Microsoft Visual Studio. --------------------------------- */
#define CC_MSVC
#elif defined(__PGI)
/* Portland Group PGCC/PGCPP. ------------------------------- */
#define CC_PGI
#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
/* Oracle Solaris Studio. ----------------------------------- */

#endif

#define FUNCTIONBEGIN(comm) \
    VERBOSE(comm, SELF, "---> %s \n",  __func__); \
    PetscFunctionBeginUser;

#define FUNCTIONBEGIN_MACRO(comm, MACROCALL) \
    VERBOSE(comm, MACROCALL, "---> %s \n",  __func__); \
    PetscFunctionBeginUser;

//             PetscViewerVtkOpen(PETSC_COMM_WORLD,"sad",&viewer);
//             ierr = PetscViewerASCIIOpen(p_cm_->intra_domain_comm, "name", &viewer);
//             DMView(da_coord,viewer);
#define DEBUG_VIEW_VEC(v) \
    { \
    PetscViewer      viewer; \
    PetscViewerDrawOpen(PETSC_COMM_WORLD, 0, "", 300, 0, 600, 600, &viewer); \
    VecView(v, viewer); \
    }


#if !CXX11_ENABLE
#define nullptr NULL
#endif

#if defined CC_GNU || defined CC_CLANG
#    define UNLIKELY(cond)     __builtin_expect(!!(cond),0)
#    define LIKELY(cond)       __builtin_expect(!!(cond),1)
#else
#    define UNLIKELY(cond)     (cond)
#    define LIKELY(cond)       (cond)
#endif

#if defined CC_MSVC || defined CC_INTEL
#    define ALWAYS_INLINE     __forceinline
#elif defined CC_GNU || defined CC_CLANG
#    define ALWAYS_INLINE     __attribute__((always_inline)) inline
#else
#    define ALWAYS_INLINE     inline
#endif

#if defined CC_GNU || defined CC_CLANG
#    define NEVER_INLINE      __attribute__((noinline))
#elif CC_MSVC
#    define NEVER_INLINE      __declspec(noinline)
#else
#    define NEVER_INLINE
#endif

#ifndef UNUSED
#define UNUSED(x)     (void)x
#endif

extern PetscErrorCode          ierr;


#endif // TOOLS_H
