/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "utilities.h"

#include "maths.h"

//!
//! \brief to_str Convert a vector to a string
//! \param vec Vector to convert
//! \return String containing a nice representation of a small vector
//!
std::string to_str(std::vector<int> vec)
{
    std::ostringstream ss;

    if (!vec.empty())
    {
        // Convert all but the last element to avoid a trailing ","
        std::copy(vec.begin(), vec.end()-1,
                  std::ostream_iterator<int>(ss, ","));

        // Now add the last element with no delimiter
        ss << vec.back();
    }

    return ss.str();
}

//!
//! \brief auto_mg_levels Automatically select the number of levels for MG
//! \param mesh Mesh to be used
//! \param ranks_mesh
//! \param dim
//! \param min_pow
//! \param mg_type Is MG AMG or GMG? Currently PETSc's only handles refinement in intra-process, gamg works fine
//! \return Integer representing the number of levels.
//!
int auto_mg_levels(Mesh mesh, Mesh ranks_mesh,
                   int dim, int min_pow, MgType::MgType mg_type) {

    if(dim == 2) {
        mesh[2] = mesh[1];
        ranks_mesh[2] = ranks_mesh[1];
    }

    Field<3, double> local_mesh;
    switch( mg_type ) {
    case MgType::PC_MG:
        local_mesh = mesh / ranks_mesh;
        break;
    case MgType::PC_GAMG:
    default:
        local_mesh = mesh;
        break;
    }
    local_mesh.in_place_apply(log) /= log(2.);
    double max_refs = (dim == 2) ? std::min(local_mesh[0], local_mesh[1]) :
        min(local_mesh[0], local_mesh[1], local_mesh[2]);

    return max_refs - min_pow;
}
