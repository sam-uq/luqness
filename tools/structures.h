/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file tools.h
//!

#include "config.h"

#include <petscsys.h>

#include <sstream>
#include <algorithm>

// TODO: implement this
//! Type of estimator
namespace SamplerType { enum SamplerType { Load, Boost, C11, Gauss, Well }; }
//! Type of estimator
namespace QuadratureType {
    enum QuadratureType {
        Deterministic  = 1 << 0,
        Stochastic     = 1 << 1,
        All            = ~0,
        None           = 0
    };
}
//! Type of estimator
namespace IntegratorType { enum IntegratorType { Serial, Parallel }; }
//! Type of solver
namespace SolverType { enum SolverType { Proj, Vort }; }
//! Type of Nonlinear term
namespace ConvectionType { enum ConvectionType { Almgren, BCG }; }
//! Type of projection
namespace ProjectionType { enum ProjectionType { Laplacian, FFT, ExactLaplacian }; } // IMPLEMENT FFTW and EXact TODO
//! Project speed or its derivative?
namespace ProjectionUpdateType { enum ProjectionUpdateType { VelocityProjection, AccelerationProjection }; }
//! Choose pressure update type: inclemental or overwrite
namespace PressureUpdateType { enum PressureUpdateType { IncrementalPressure, DirectPressure }; }
//! Choose pressure update type: inclemental or overwrite
namespace TimeSteppingType { enum TimeSteppingType { Forward, LeapFrog, BackwardStable, CrankNicolson }; }
//! Tyoe if allocator for load balancing
namespace AllocatorType { enum AllocatorType { Static, Dynamic }; }
//! Type of diffusion
namespace DiffType { enum DiffType { Roe, Rusanov, Eddy, None }; }
//! Coordinates type
namespace CoordType { enum CoordType { Uniform, NonUniform }; }
//! Type of MG preconditioner (to differentiate between choice  of levels)
namespace MgType { enum MgType { PC_MG, PC_GAMG }; }
//! Component type, allow for set-theoretical union (|), intersection (&) and complement (~)
namespace CompType {
enum CompType {
    U              = 1 << 0,
    Gp             = 1 << 1,
    omega          = 1 << 2,
    ps             = 1 << 3,
    gamma2         = 1 << 4,
    forcing        = 1 << 5,
    forcing_inflow = 1 << 6,
    All            = ~0,
    None           = 0
};
}
//! Dimensions type
namespace DimType {
enum DimType {
    d2             = 1 << 0,
    d3             = 1 << 1,
    All            = ~0,
    None           = 0
};
}
//! Boundary conditions type
namespace BCType {
enum BCType { Periodic, Slip, NoSlip, Inflow, Outflow, Neumann, Dirichlet, Interpolate, Zero };
}
//! Position enum in the bdry
namespace PosType {
enum PosType { Left, Right, Bottom, Top, Down, Up };
}

//! Type of boundary (all possibilities)
//! Type of MPI communication (differentiate MPI calls)
//!
enum TAGS_MPI { TAG_SEEDS, TAG_COMMDATA };
//! Component tag
//!
enum TAG_COMP { TAG_U, TAG_w, TAG_Gp, TAG_ps, TAG_gamma2 };
//! Statistic tag
//!
enum TAG_STAT { TAG_MEAN, TAG_VAR };

//! Generate tag given component tag, statistic tag and frame (inter level)
//!
inline int get_inter_level_tag(TAG_COMP comp, TAG_STAT stat, int frame) {
    return frame * 5 * 2 + stat * 5 + comp;
}

//! Generate tag given component tag, statistic tag and frame (intra level)
//!
inline int get_intra_level_tag(TAG_COMP comp, TAG_STAT stat, int frame) {
    return frame * 5 * 2 + stat * 5 + comp;
}

//! Templated static contiguous size vector used to cast many dof toghether
//! @tparam dim dimension of the vector (static)
//! @tparam T type contained in the vector
template <int dim, typename T>
struct Field {
    T  data[dim];

    Field<dim, T>() {
    /* For efficiency reasons the default constructor doesn't initialize it's data to zero */
    }
    Field<dim, T>(const Field<dim, T> & field) {
    for(int i = 0; i < dim; ++i) {
        this->data[i] = field.data[i];
    }
    }
    Field<dim, T>(T * data) {
    for(int i = 0; i < dim; ++i) {
        this->data[i] = data[i];
    }
    }
    Field<dim, T>(T data) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] = data;
        }
    }

    inline T & operator[] (int i) { return data[i]; }
    inline const T & operator[] (int i) const { return data[i]; }

    //   inline Field<dim, T> & operator=(const Field<dim, T> & rhs) {
    //    for(int i = 0; i < dim; ++i) {
    //     this->data[i] = rhs.data[i];
    //    }
    //    return *this;
    //   };
    //
    inline Field<dim, T> & operator+=(const Field<dim, T> & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] += rhs.data[i];
        }
        return *this;
    }

    inline Field<dim, T> operator+(const Field<dim, T> & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] + rhs.data[i];
        }
        return res;
    }

    inline Field<dim, T> & operator-=(const Field<dim, T> & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] -= rhs.data[i];
        }
        return *this;
    }

    inline Field<dim, T> operator-(const Field<dim, T> & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] - rhs.data[i];
        }
        return res;
    }

    inline Field<dim, T> & operator*=(const Field<dim, T> & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] *= rhs.data[i];
        }
        return *this;
    }

    inline Field<dim, T> operator*(const Field<dim, T> & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] * rhs.data[i];
        }
        return res;
    }

    inline Field<dim, T> & operator/=(const Field<dim, T> & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] /= rhs.data[i];
        }
        return *this;
    }

    inline Field<dim, T> operator/(const Field<dim, T> & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] / rhs.data[i];
        }
        return res;
    }

    template <typename T2>
    inline Field<dim, T> & operator=(const T2 & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] = rhs;
        }
        return *this;
    }

    template <typename T2>
    inline Field<dim, T> & operator=(const Field<dim, T2> & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] = (T2) rhs.data[i];
        }
        return *this;
    }

    inline Field<dim, T> & operator=(const T & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] = rhs;
        }
        return *this;
    }

    inline Field<dim, T> & operator+=(const T & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] += rhs;
        }
        return *this;
    }

    inline Field<dim, T> operator+(const T & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] + rhs;
        }
        return res;
    }

    inline Field<dim, T> & operator-=(const T & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] -= rhs;
        }
        return *this;
    }

    inline Field<dim, T> operator-(const T & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] - rhs;
        }
        return res;
    }

    inline Field<dim, T> & operator*=(const T & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] *= rhs;
        }
        return *this;
    }

    inline Field<dim, T> operator*(const T & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] * rhs;
        }
        return res;
    }

    inline Field<dim, T> & operator/=(const T & rhs) {
        for(int i = 0; i < dim; ++i) {
            this->data[i] /= rhs;
        }
        return *this;
    }

    inline Field<dim, T> operator/(const T & rhs) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = this->data[i] / rhs;
        }
        return res;
    }

    inline bool operator>(const T & rhs) const {
        for(int i = 0; i < dim; ++i) {
            if( this->data[i] <= rhs ) return false;
        }
        return true;
    }

    inline Field<dim, T> & in_place_apply( T (*f)(T) ) {
        for(int i = 0; i < dim; ++i) {
            data[i] = f(this->data[i]);
        }
        return *this;
    }

    inline Field<dim, T> apply( T (*f)(T) ) const {
        Field<dim, T> res;
        for(int i = 0; i < dim; ++i) {
            res.data[i] = f(this->data[i]);
        }
        return res;
    }

    inline T recurs_apply( T (*f)(T, T) ) const {
        T res = this->data[0];
        for(int i = 1; i < dim; ++i) {
            res = f(res, this->data[i]);
        }
        return res;
    }

    //   inline T mina(void) const { return recurs_apply(std::min<T>); }
    //   inline T maxa(void) const { return recurs_apply(std::max<T>); }

    std::string str(char sep = ',') const {
        std::stringstream ss;
        for(int i = 0; i < dim-1; ++i) {
            ss << this->data[i] << sep;
        }
        ss << this->data[dim-1];
        return ss.str();
    }

    static Field<dim, T> ones;
};

template <int dim, typename T>
Field<dim, T> Field<dim, T>::ones = Field<dim, T>(1);

//! Real 2D vector
typedef         Field<2, PetscScalar>   Field2D;
//! Real 3D vector
typedef         Field<3, PetscScalar>   Field3D;
//! Integer 3D vector for mesh coords
typedef         Field<3, PetscInt>      Mesh;

//!
//! \brief The Volume2D struct contains information about
//! a single volume: sizes and area.
//!
struct Volume2D {
    PetscReal x, y, vol;
};
//!
//! \brief The Volume3D struct contains information about
//! a single volume: sizes and area.
//!
struct Volume3D {
    Volume3D(PetscReal x, PetscReal y,
             PetscReal z, PetscReal vol)
        : x(x), y(y), z(z), vol(vol) {}

    PetscReal x, y, z, vol;
};

//!
//! \brief The SamplerData struct holds information about
//! different sampler stream.
//!
struct SamplerData {
    uint sim;
    uint dim;
    uint stream;
};
