/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef QUANTITY_H
#define QUANTITY_H

//! @file quantity.h
//! Defines the class Quantity used as meta-container for data.

#include "../tools/domain.h"
#include "../tools/profiler.h"
#include "../attila/attila.h"
#include "../models/model_base.h"

#ifdef HDF_OUT
#include <petscviewerhdf5.h>
#endif

#include <sstream>

//! This class is a meta-container for all the data produced from a simulation of *any* sovler. It can take care of
//! preallocation or it may be used as interface with other strcutures. It also caontains all the routines for the
//! "external" computation, such as quaratures and mean/variance estimators.
//! \brief The Quantity class
//!
class Quantity {
public:
    //! Is requested, allocates the vector according to currenct options (solver, ...)
    Quantity(Options *p_opt, Domain *p_dom, Attila *p_cm, ModelBase *p_mod,
          std::string name_ = "", bool preallocate_ = true);
    //! IS constructor allocated the memory freezes it, otherwise does nothing
    ~Quantity();

    //! Export to chosen format all vectors handled by this
    PetscErrorCode save_all(std::string name_ = "");

    //! Just save one vector with right name, called by save_all
    PetscErrorCode save_vec(Vec V, std::string compname, uint dof = 1);

    //! Change the name of the quantity, msinly udrd to assign identifier
    PetscErrorCode set_name(std::string name_) { name = name_; return ierr; }

    //
#if META == 1

    //! Grab all data you can get for metadata
    PetscErrorCode update_meta(void);

#endif // META

    //! Global version of mean/var algorithm that calls the appropriate smaller version acting on single components
    PetscErrorCode add_to_mav(Quantity * mean, Quantity * variance);
    //! Perform the online variance algorithm on one components, Vec by Vectemplate
    template <bool scalar>
    PetscErrorCode add_to_mav(const DM da,
                              Vec sample_data, Vec mean_data, Vec variance_data,
                              int nadds);

    //! Add the quanty qt to this
    PetscErrorCode add_as_quad(double weight, Quantity * qt);

    //! Global version of integration that calls the appropriate smaller version acting on single components
    PetscErrorCode add_as_quad_and_var(double weight, Quantity * intg, Quantity * intg_m2);

    //! Global version of integration that calls the appropriate smaller version acting on single components
    template <bool scalar>
    PetscErrorCode add_as_quad_and_var(DM da, double weight,
                                       Vec sample_data, Vec mean_data,
                                       Vec variance_data, int nadds, double s_weight);


    //! Divide m2 by number of additions to give variance
    PetscErrorCode normalize_as_var(int ddof = 1);
    //! Divide m2 to obtain a variance (in case of weighted integration)
    PetscErrorCode normalize_as_quad();

    Field2D eval_2d_field(Vec V, Field<3, int>  & index) {
        Field2D **a;
        if( index[0] < p_dom_->xs || index[0] >= p_dom_->xs+p_dom_->xm
            || index[1] < p_dom_->ys || index[1] >= p_dom_->ys+p_dom_->ym ) {
            WARNING(p_cm_, SELF, "Requested OOR index.\n", "");
            }
        DMDAVecGetArray(p_dom_->da, V, &a);
        Field2D ret = a[index[1]][index[0]];
        DMDAVecRestoreArray(p_dom_->da, V, &a);
        return ret;
    }
    PetscScalar eval_2d_scalar(Vec V, Field<3, int>  & index) {
        PetscScalar **a;
        if( index[0] < p_dom_->xs || index[0] >= p_dom_->xs+p_dom_->xm
            || index[1] < p_dom_->ys || index[1] >= p_dom_->ys+p_dom_->ym ) {
            WARNING(p_cm_, SELF, "Requested OOR index.\n", "");
            }
        DMDAVecGetArray(p_dom_->da_sc, V, &a);
        PetscScalar ret = a[index[1]][index[0]];
        DMDAVecRestoreArray(p_dom_->da_sc, V, &a);
        return ret;
    }

    Field2D eval_2d_U(Field<3, int>  & index) {
        return eval_2d_field(U, index);
    }
    PetscScalar eval_2d_omega(Field<3, int>  & index) {
        return eval_2d_scalar(omega, index);
    }
    Field2D eval_2d_Gp(Field<3, int>  & index) {
        return eval_2d_field(Gp, index);
    }
    PetscScalar eval_2d_ps(Field<3, int>  & index) {
        return eval_2d_scalar(ps, index);
    }
    PetscScalar eval_2d_gamma2(Field<3, int>  & index) {
        return eval_2d_scalar(gamma2, index);
    }

    Field3D eval_3d_field(Vec V, Field<3, int>  & index) {
        Field3D ***a;
        if( index[0] < p_dom_->xs || index[0] >= p_dom_->xs+p_dom_->xm
            || index[1] < p_dom_->ys || index[1] >= p_dom_->ys+p_dom_->ym
            || index[2] < p_dom_->zs || index[2] >= p_dom_->zs+p_dom_->zm ) {
                WARNING(p_cm_, SELF, "Requested OOR index.\n", "");
        }
        DMDAVecGetArray(p_dom_->da, V, &a);
        Field3D ret = a[index[2]][index[1]][index[0]];
        DMDAVecRestoreArray(p_dom_->da, V, &a);
        return ret;
    }
    PetscScalar eval_3d_scalar(Vec V, Field<3, int>  & index) {
        PetscScalar ***a;
        if( index[0] < p_dom_->xs || index[0] >= p_dom_->xs+p_dom_->xm
            || index[1] < p_dom_->ys || index[1] >= p_dom_->ys+p_dom_->ym
            || index[2] < p_dom_->zs || index[2] >= p_dom_->zs+p_dom_->zm ) {
                WARNING(p_cm_, SELF, "Requested OOR index.\n", "");
        }
        DMDAVecGetArray(p_dom_->da_sc, V, &a);
        PetscScalar ret = a[index[2]][index[1]][index[0]];
        DMDAVecRestoreArray(p_dom_->da_sc, V, &a);
        return ret;
    }

    Field3D eval_3d_U(Field<3, int>  & index) {
        return eval_3d_field(U, index);
    }
    Field3D eval_3d_omega(Field<3, int>  & index) {
        return eval_3d_scalar(omega, index);
    }
    Field3D eval_3d_Gp(Field<3, int>  & index) {
        return eval_3d_field(Gp, index);
    }
    PetscScalar eval_3d_ps(Field<3, int>  & index) {
        return eval_3d_scalar(ps, index);
    }
    PetscScalar eval_3d_gamma2(Field<3, int>  & index) {
        return eval_3d_scalar(gamma2, index);
    }

    Vec                           U; //! Container for velocity
    Vec                           Gp; //! Container for pressure gradient
    Vec                           omega; //!< container for a corticity
    Vec                           ps; //!< Passive scalar
    Vec                           gamma2; //!< The gamma2

    //! Number of mean adds performed so far, used to keep track of the number of simulations performed for the
    //! computation of mean/variance.
    int                           num_adds;
    //! Partial sum of weights for the computation of integrated (weighted) mean and *variance*
    double                        sum_weights;

    //! Components as set theoretical data (CompType)
    int                           components_;

    //! Phase dimension 2 or 3?
    int                           phase_dim;

    //! Degrees oof freedom contained in this quantity
    int dof_;
private:
    const Options * const         p_opt_; //! < Options handler
    Domain * const                p_dom_; //! < Domain handler
    const Attila * const          p_cm_; //! < Attila handler
    const ModelBase *const        p_mod_; //! < Attila handler

    //! Name of this quantity, as for e.g. "mean", "itnegral", "sim2_f2"
    std::string                   name;

    //! Preallocate(d) necessary memory flag.
    const bool                    preallocate;


    //! Store metadata on the class (may slow things down)
    //! TODO
#if META == 1
    //! Frame count, should also contained in the name
    uint                          frame;

    //! Shape
    Mesh                          mesh;

    //! Real time
    PetscReal                     time;
#endif // META
};

//! Performs the online variance algorithm on *one* compoenent of the quantity (one Vec), the algorithm goes as follows:
//!     for data in data_collection:
//!
//!             n++
//!             delta = data - mean
//!             mean += delta / n
//!             m2 += delta * (data - mean)
//!     n = 0 ? m2 = 0 : m2 /= n
//!
//! n (mean->num_adds) is incremented in the global algorithm add_to_mav, the final normalization is done in
//! normalize_as_var (on m2)
//! This function is templated in order to differentiate between calls to scalar routine or vector routine, but I wanted
//! to keep everything in the same function, in future we may want to template directly with the type and do some 3D
//! type checking at compile time or something like that.
//! RIP nasty BUG where we used Field2D also for scalar comparisons (this lead to overflow): that was a nastz bug:
//! always remember to pick the right data.
//! @param[in] da DMDA pointer (just to get the correct array)
//! @param[in] sample_data data that is added to mean and variance
//! @param[in] mean_data data containing the mean (so far)
//! @param[in] sample_data data containing the variance (so far)
//! @param[in] nadds number of additions made so far
template <bool scalar>
PetscErrorCode Quantity::add_to_mav(DM da, Vec sample_data, Vec mean_data, Vec variance_data,
                                    int nadds)
{
    if(scalar) {
        if( p_opt_->enable_3d ) {
            PetscScalar      ***w_array, ***mean_array, ***m2_array;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
                for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                    for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                        PetscScalar delta = w_array[k][j][i] - mean_array[k][j][i];
                        mean_array[k][j][i] += delta / (PetscReal) (nadds);
                        m2_array[k][j][i] += delta * (w_array[k][j][i] - mean_array[k][j][i]);
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        } else {
            PetscScalar      **w_array, **mean_array, **m2_array;
            PetscScalar      delta;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int j = p_dom_->ys; j < p_dom_->ys + p_dom_->ym; ++j) {
                for (int i = p_dom_->xs; i < p_dom_->xs + p_dom_->xm; ++i) {
                    delta = w_array[j][i] - mean_array[j][i];
                    mean_array[j][i] += delta / (PetscReal)(nadds);
                    m2_array[j][i] += delta * (w_array[j][i] - mean_array[j][i]);
                    //      mean_array[j][i] += w_array[j][i];
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        }
    } else {
        if( p_opt_->enable_3d ) {
            Field3D      ***w_array, ***mean_array, ***m2_array;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
                for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                    for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                        Field3D delta = w_array[k][j][i] - mean_array[k][j][i];
                        mean_array[k][j][i] += delta / (PetscReal) (nadds);
                        m2_array[k][j][i] += delta * (w_array[k][j][i] - mean_array[k][j][i]);
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        } else {
            Field2D      **w_array, **mean_array, **m2_array;
            Field2D      delta;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int j = p_dom_->ys; j < p_dom_->ys + p_dom_->ym; ++j) {
                for (int i = p_dom_->xs; i < p_dom_->xs + p_dom_->xm; ++i) {
                    delta = w_array[j][i] - mean_array[j][i];
                    mean_array[j][i] += delta / (PetscReal)(nadds);
                    m2_array[j][i] += delta * (w_array[j][i] - mean_array[j][i]);
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        }
    }
    PetscFunctionReturn(ierr);
}

//! Performs the online variance algorithm on *one* compoenent of the quantity (one Vec), the algorithm goes as follows:
//!     for data in data_collection:
//!
//! for x, weight in dataWeightPairs:
//!   temp = weight + sumweight
//!   delta = x - mean
//!   R = delta * weight / temp
//!   mean = mean + R
//!   M2 = M2 + sumweight * delta * R
//!   sumweight = temp
//!
//! variance = M2/sumweight * len(dataWeightPairs)/(len(dataWeightPairs) - 1)
//!
//! n (mean->num_adds) is incremented in the global algorithm add_to_mav, the final normalization is done in
//! normalize_as_quad (on m2)
//! This function is templated in order to differentiate between calls to scalar routine or vector routine, but I wanted
//! to keep everything in the same function, in future we may want to template directly with the type and do some 3D
//! type checking at compile time or something like that.
//! @param[in] da DMDA pointer (just to get the correct array)
//! @param[in] weight current weight that is to combined with the current sample
//! @param[in] sample_data data that is added to mean and variance
//! @param[in] mean_data data containing the mean (so far)
//! @param[in] sample_data data containing the variance (so far)
//! @param[in] nadds number of additions made so far
template <bool scalar>
PetscErrorCode Quantity::add_as_quad_and_var(DM da, double weight,
                                             Vec sample_data, Vec mean_data, Vec variance_data,
                                             int, double s_weight)
{

    if(scalar) {
        if( p_opt_->enable_3d ) {
            PetscScalar      ***w_array, ***mean_array, ***m2_array;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
                for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                    for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                        PetscScalar temp = weight + s_weight;
                        PetscScalar delta = w_array[k][j][i] - mean_array[k][j][i];
                        PetscScalar R = delta * weight / temp;
                        mean_array[k][j][i] += R;
                        m2_array[k][j][i] += delta * s_weight * R;
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        } else {
            PetscScalar      **w_array, **mean_array, **m2_array;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int j = p_dom_->ys; j < p_dom_->ys + p_dom_->ym; ++j) {
                for (int i = p_dom_->xs; i < p_dom_->xs + p_dom_->xm; ++i) {
                    PetscScalar temp = weight + s_weight;
                    PetscScalar delta = w_array[j][i] - mean_array[j][i];
                    PetscScalar R = delta * weight / temp;
                    mean_array[j][i] += R;
                    m2_array[j][i] += delta * s_weight * R;
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        }
    } else {
        if( p_opt_->enable_3d ) {
            Field3D      ***w_array, ***mean_array, ***m2_array;
            Field3D      delta, temp, R;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int k=p_dom_->zs; k<p_dom_->zs+p_dom_->zm; ++k) {
                for (int j=p_dom_->ys; j<p_dom_->ys+p_dom_->ym; ++j) {
                    for (int i=p_dom_->xs; i<p_dom_->xs+p_dom_->xm; ++i) {
                        temp = weight + s_weight;
                        delta = w_array[k][j][i] - mean_array[k][j][i];
                        R = delta * weight / temp;
                        mean_array[k][j][i] += R;
                        m2_array[k][j][i] += delta * s_weight * R;
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        } else {
            Field2D      **w_array, **mean_array, **m2_array;
            Field2D      delta, temp, R;
            ierr = DMDAVecGetArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, variance_data, &m2_array); CHKERRQ(ierr);
            for (int j = p_dom_->ys; j < p_dom_->ys + p_dom_->ym; ++j) {
                for (int i = p_dom_->xs; i < p_dom_->xs + p_dom_->xm; ++i) {
                    temp = weight + s_weight;
                    delta = w_array[j][i] - mean_array[j][i];
                    R = delta * weight / temp;
                    mean_array[j][i] += R;
                    m2_array[j][i] += delta * s_weight * R;
                }
            }
            ierr = DMDAVecRestoreArray(da, sample_data, &w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, mean_data, &mean_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, variance_data, &m2_array); CHKERRQ(ierr);
        }
    }
    PetscFunctionReturn(ierr);
}

#endif // QUANTITY_H
