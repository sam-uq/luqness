/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "profiler.h"

#include "../attila/attila.h"

//! Call last snapshot (roughly corresponding to program termination, if called at end), print output and save some
//! useful information
Profiler::~Profiler()
{
    FUNCTIONBEGIN(p_cm_);

    #if DO_PROFILING
            PetscLogDump(NULL);
    #endif // DO_PROFILING

    tic("profiler_destructor");

    print();

}

//! Print to SELF timigs and counters (stages are take care with PETSc log_summary), save to timings_rk{rank}.py
//! information that may be read by python postprocessing routines
//! FIXME: unkown bug for dora, most likely is here
PetscErrorCode Profiler::print(void)
{
#if DO_PROFILING
    //     PetscLogDouble end_time;
    //     ierr = Timer(&end_time); CHKERRQ(ierr);

    // TODO: output for each domain or what? CHECK
    DEBUG(p_cm_, SELF, "%sProfiler timings:\n", "");
    for(std::vector< std::pair<std::string, PetscLogDouble> >::iterator it = timings.begin();
        it != timings.end(); ++it) {
        DEBUG(p_cm_, SELF, "%40s  %10.2f\n", it->first.c_str(), it->second);
    }
    DEBUG(p_cm_, SELF, "Profiler coutners:\n", "");
    for(std::map<std::string, uint>::iterator it = counters.begin(); it != counters.end(); it++) {
        DEBUG(p_cm_, SELF, "%40s  %10d\n", it->first.c_str(), it->second);
    }

    // PRINT TO FILE
    std::stringstream ss;
    ss << "# Timings for rank " << p_cm_->world_rank << std::endl;
    ss << "timings_rk" << p_cm_->world_rank << " = {" << std::endl;
    for(std::vector< std::pair<std::string, PetscLogDouble> >::iterator it = timings.begin();
        it != timings.end();++it) {
        ss << "\"" << it->first.c_str() << "\" : " << it->second << "";
        if(timings.end() != it+1) ss << ",";
        ss << std::endl;
    }
    ss << "}" << std::endl;

    char fname[PETSC_MAX_PATH_LEN];
    sprintf(fname, "%s_profiling_rk%d.py", name_, p_cm_->world_rank);
#if !BLOCK_IO
    FILE * f = fopen(fname, "a");
    PetscFPrintf(PETSC_COMM_SELF, f, ss.str().c_str());
    fclose(f);
#endif // BLOCK_IO

#endif // DO_PROFILING
    PetscFunctionReturn(ierr);
}
