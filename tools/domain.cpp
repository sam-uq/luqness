/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "domain.h"

#include <iostream>

typedef double (Domain::*DMFunction)(double,double,double,double,double);

//!
//!
Domain::~Domain()
{
    FUNCTIONBEGIN(p_cm_);

    ierr = DMRestoreGlobalVector(da_coord, &D); CHKERRV(ierr);

    if( has_old ) {
        ierr = MatDestroy(&interp_mat); CHKERRV(ierr);
        ierr = MatDestroy(&interp_sc_mat); CHKERRV(ierr);
        ierr = DMDestroy(&da_old); CHKERRV(ierr);
        ierr = DMDestroy(&da_sc_old); CHKERRV(ierr);
    }
    ierr = DMDestroy(&da); CHKERRV(ierr);
    ierr = DMDestroy(&da_sc); CHKERRV(ierr);
    ierr = DMDestroy(&da_coord); CHKERRV(ierr);
}

//!  Setup some mesh information and calls generated_dmdas
//!
PetscErrorCode Domain::setup_from_options(void)
{
    FUNCTIONBEGIN(p_cm_);

    // Setup mesh
    mesh_size = p_cm_->mesh;
    mesh_str = mesh_size.str();

    bd_fam.init(p_opt_->boundaries_array);
    for(int i = 0; i < DOMAIN_MAX; ++i ) {
        domain[i % 2][i / 2] = p_opt_->domain_array[i];
    }
    l_x = std::abs( domain[1][0] - domain[0][0] );
    l_y = std::abs( domain[1][1] - domain[0][1] );
    l_z = std::abs( domain[1][2] - domain[0][2] );
    dxi = l_x / (PetscReal) mesh_size[0];
    dyi = l_y / (PetscReal) mesh_size[1];
    p_opt_->enable_3d ? dzi = l_z / (PetscReal) mesh_size[2] : dzi = 1.;
    if( p_opt_->enable_3d ) {
        h = min(dxi, dyi, dzi);
    } else {
        h = std::min(dxi, dyi);
    }

    ierr = generate_dmdas(); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Domain::generate_dmdas(void)
{
    FUNCTIONBEGIN(p_cm_);

    // TODO FIXME: WARNING stencil star may break averaging and stuff, so let's try stencil BOX
    // (at expense of MPI efficiency, maybe differentiate???)
    // TODO FIXME: WARNING we MUST increase the stencil if we want to use a decoupled laplacian.... this is vezy expenzive
    // TODO: differentiate stencil ONLY for the exact projector, otherwise we communicate 1 layer more than necessary.
    // Check if then it's still compatible
    // TODO: use a 5 pts stencile for conv BCG?????
    PetscInt stencil_width = 1; // width of the stencil to use
    if( p_opt_->use_exact ) {
        stencil_width = 2;
    }

    // Type of stencil we want
    DMDAStencilType stencil_type;
    if( !strcmp(p_opt_->solver_name, "vortex") ) { // IF USING VORTEX we NEED a BOX, sadly
        stencil_type = DMDA_STENCIL_BOX;
    } else {
        stencil_type = DMDA_STENCIL_STAR;
    }

    // Create the DMDA
    if( p_opt_->enable_3d ) {
        DEBUG(p_cm_, INTRADM, "[DOM] We will use a mesh of size %s on the domain "
                              "[%.2f,%.2f]x[%.2f,%.2f]x[%.2f,%.2f] i.e. h = %.4f...\n",
              mesh_str.c_str(), left(), right(), bottom(), top(), down(), up(), h);
        ierr = DMDACreate3d(p_cm_->intra_domain_comm,
                            DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC, stencil_type,
                            mesh_size[0], mesh_size[1], mesh_size[2], PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE,
                3, stencil_width, NULL, NULL, NULL, &da); CHKERRQ(ierr);
        ierr = DMDASetFieldName(da, 2, "z"); CHKERRQ(ierr);
    } else {
        DEBUG(p_cm_, INTRADM, "[DOM] We will use a mesh of size %s on the domain "
                              "[%.2f,%.2f]x[%.2f,%.2f] i.e. h = %.4f...\n",
              mesh_str.c_str(), left(), right(), bottom(), top(), h);
        ierr = DMDACreate2d(p_cm_->intra_domain_comm,
                            DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC, stencil_type,
                            mesh_size[0], mesh_size[1], PETSC_DECIDE, PETSC_DECIDE,
                2, stencil_width, NULL, NULL, &da); CHKERRQ(ierr);
    }

    after_dmda_changed();

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Domain::refine(void)
{
    FUNCTIONBEGIN(p_cm_);
    // Refinement factors
    int factor_x = 2, factor_y = 2, factor_z = 2;

    Mesh new_mesh_size;
    new_mesh_size[0] = mesh_size[0] * factor_x;
    new_mesh_size[1] = mesh_size[1] * factor_y;
    new_mesh_size[2] = mesh_size[2] * (p_opt_->enable_3d ? factor_z : 1);
    DEBUG(p_cm_, INTRADM, "Refining, from %s to %s.\n",
          mesh_size.str().c_str(), new_mesh_size.str().c_str());

    //   if( keep_old ) {
    //     ierr = DMClone(da, &da_old); CHKERRQ(ierr);
    //     ierr = DMClone(da_sc, &da_sc_old); CHKERRQ(ierr);
    //   }

    // TODO: check memory leak?
    // Refine DMDAs
    DM da_new;
    ierr = DMRefine(da, p_cm_->intra_domain_comm, &da_new); CHKERRQ(ierr);

    // FIXME: this is a stupid hack used if I don't have DMClone (PETSc < 3.3.3)
    da_old = da;
    da = da_new;
    da_sc_old = da_sc;

    ierr = DMDAGetInfo(da, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, &mesh_ranks[0], &mesh_ranks[1], &mesh_ranks[2],
            PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL);

    // Change data
    mesh_size = new_mesh_size;
    mesh_str = mesh_size.str();
//
    dxi = l_x / (PetscReal) mesh_size[0];
    dyi = l_y / (PetscReal) mesh_size[1];
    p_opt_->enable_3d ? dzi = l_z / (PetscReal) mesh_size[2] : dzi = 1.;
    if( p_opt_->enable_3d ) {
        // TODO: ANISOTROPY
        h = min(dxi, dyi, dzi);
    } else {
        // TODO: ANISOTROPY
        h = std::min(dxi, dyi);
    }
//     area = 0; // TODO: abs( right - left ) * abs( top - bottom );

    after_dmda_changed();

    if( keep_old ) {
        has_old = true;
        ierr = DMCreateInterpolation(da_old, da, &interp_mat, PETSC_NULL); CHKERRQ(ierr);
        ierr = DMCreateInterpolation(da_sc_old, da_sc, &interp_sc_mat, PETSC_NULL); CHKERRQ(ierr);
    }

    DEBUG(p_cm_, INTRADM, "[DOM] Refined: we will now use a mesh of size %s on the domain "
                          "[%.2f,%.2f]x[%.2f,%.2f] i.e. ~h = %.4f...\n",
          mesh_str.c_str(), left(), right(), bottom(), top(), h);
    DEBUG(p_cm_, SELF, "[DOM] xrange: %d, %d, yrange: %d, %d, zrange: %d, %d.\n",
          xs, xm, ys, ym, zs, zm);

    PetscFunctionReturn(ierr);
}

//!
//!
PetscErrorCode Domain::after_dmda_changed()
{
    FUNCTIONBEGIN(p_cm_);
    Vec coords_edge;

    ierr = DMDAGetCorners(da, &xs, &ys, &zs, &xm, &ym, &zm); CHKERRQ(ierr);

    Vec           coords;
    DM            cda;
    ierr = DMDASetUniformCoordinates(da, left(), right(), bottom(), top(), down(), up()); CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinates(da, &coords); CHKERRQ(ierr);
    ierr = DMCreateLocalVector(cda, &coords_edge); CHKERRQ(ierr);


    DMFunction caex, caey, caez, cax, cay, caz;

    PetscReal R=2;
    switch(p_opt_->coord_type) {
        case CoordType::NonUniform:
            caex = caey = caez  = &Domain::tanhfun_edge;
            cax  = cay= caz = &Domain::tanhfun;
            break;
        case CoordType::Uniform:
        default:
            caex = caey = caez = &Domain::linspace_edge;
            cax = cay = caz = &Domain::linspace;
            break;
    }

    if( p_opt_->enable_3d ) { // if 3D

        DMDACoor3d    ***coords_array, ***coords_array_edge;;

        ierr = DMDAVecGetArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        for (int k=zs; k<=zs+zm; k++) {
            for (int j=ys; j<=ys+ym; j++) {
                for (int i=xs; i<=xs+xm; i++) {
                    coords_array_edge[k][j][i].x = (this->*caex)(i, mesh_size[0], left(), right(), R);
                    coords_array_edge[k][j][i].y = (this->*caey)(j, mesh_size[1], bottom(), top(), R);
                    coords_array_edge[k][j][i].z = (this->*caez)(k, mesh_size[2], down(), up(), R);
                    if( i != xs+xm && j != ys+ym && k != zs+zm ) {
                        coords_array[k][j][i].x = (this->*cax)(i, mesh_size[0], left(), right(), R);
                        coords_array[k][j][i].y = (this->*cay)(j, mesh_size[1], bottom(), top(), R);
                        coords_array[k][j][i].z = (this->*caz)(k, mesh_size[2], down(), up(), R);
                    }
                }
            }
        }
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);

    } else { // if 2D
        // The edge array starts at i = 0 at the left edge and DOES INCLUDE THE LAST coordinate (is local)
        DMDACoor2d    **coords_array, **coords_array_edge;

        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);
        for (int j=ys; j<=ys+ym; j++) {
            for (int i=xs; i<=xs+xm; i++) {
                coords_array_edge[j][i].x = (this->*caex)(i, mesh_size[0], left(), right(), R);
                coords_array_edge[j][i].y = (this->*caey)(j, mesh_size[1], bottom(), top(), R);
                if( i != xs+xm && j != ys+ym ) {
                    coords_array[j][i].x = (this->*cax)(i, mesh_size[0], left(), right(), R);
                    coords_array[j][i].y = (this->*cay)(j, mesh_size[1], bottom(), top(), R);
                }
            }
        }
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);
    }

#ifdef BIN_OUT
#if !BLOCK_IO
        output_mesh();
#endif
#endif

    DEBUG(p_cm_, SELF, "[DOM] New DMDA, Local size: xrange: %d, %d, yrange: %d, %d, zrange: %d, %d.\n",
          xs, xm, ys, ym, zs, zm);
    ierr = DMDAGetInfo(da, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, &mesh_ranks[0], &mesh_ranks[1],
                       &mesh_ranks[2],
            PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL, PETSC_NULL);

    ierr = DMDAGetReducedDMDA(da, 1, &da_sc); CHKERRQ(ierr);

    if( da_coord != NULL ) {
        ierr = DMRestoreLocalVector(da_coord, &D); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(da_coord, &Df); CHKERRQ(ierr);
        ierr = DMRestoreLocalVector(da_coord, &Db); CHKERRQ(ierr);
        ierr = DMDestroy(&da_coord); CHKERRQ(ierr);
    }

    if( p_opt_->enable_3d ) {
        ierr = DMDAGetReducedDMDA(da, 4, &da_coord); CHKERRQ(ierr);
    } else {
        ierr = DMDAGetReducedDMDA(da, 3, &da_coord); CHKERRQ(ierr);
    }

    ierr = DMCreateLocalVector(da_coord, &D); CHKERRQ(ierr);
    ierr = DMCreateLocalVector(da_coord, &Df); CHKERRQ(ierr);
    ierr = DMCreateLocalVector(da_coord, &Db); CHKERRQ(ierr);

    ierr = DMGetCoordinateDM(da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinatesLocal(da, &coords); CHKERRQ(ierr);

    h = HUGE_VAL;
    if( p_opt_->enable_3d ) { // if 3D
        DMDACoor3d    ***coords_array, ***coords_array_edge;
        Volume3D      ***d, ***df, ***db;
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, D, &d); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, Db, &db); CHKERRQ(ierr);

        for (int k=zs; k<zs+zm; k++) {
            for (int j=ys; j<ys+ym; j++) {
                for (int i=xs; i<xs+xm; i++) {
                    d[k][j][i].x = (coords_array_edge[k][j][i+1].x - coords_array_edge[k][j][i].x);
                    d[k][j][i].y = (coords_array_edge[k][j+1][i].y - coords_array_edge[k][j][i].y);
                    d[k][j][i].z = (coords_array_edge[k+1][j][i].z - coords_array_edge[k][j][i].z);

                    if(i < xs+xm-1) df[k][j][i].x = (coords_array[k][j][i+1].x - coords_array[k][j][i].x);
                    else df[k][j][i].x = (this->*cax)(i+1, mesh_size[0], left(), right(), R) - coords_array[k][j][i].x;
                    if(j < ys+ym-1) df[k][j][i].y = (coords_array[k][j+1][i].y - coords_array[k][j][i].y);
                    else df[k][j][i].y = (this->*cay)(j+1, mesh_size[1], bottom(), top(), R) - coords_array[k][j][i].y;
                    if(k < zs+zm-1) df[k][j][i].z = (coords_array[k+1][j][i].z - coords_array[k][j][i].z);
                    else df[k][j][i].z = (this->*caz)(k+1, mesh_size[2], down(), up(), R) - coords_array[k][j][i].z;

                    if(i > xs) db[k][j][i].x = (coords_array[k][j][i].x - coords_array[k][j][i-1].x);
                    else db[k][j][i].x = coords_array[k][j][i].x - (this->*cax)(i-1, mesh_size[0], left(), right(), R);
                    if(j > ys) db[k][j][i].y = (coords_array[k][j][i].y - coords_array[k][j-1][i].y);
                    else db[k][j][i].y = coords_array[k][j][i].y - (this->*cay)(j-1,mesh_size[1], bottom(), top(), R);
                    if(k > zs) db[k][j][i].z = (coords_array[k][j][i].z - coords_array[k-1][j][i].z);
                    else db[k][j][i].z = coords_array[k][j][i].z - (this->*cax)(k-1, mesh_size[2], down(), up(), R);

#if HEAVY_ASSERT
                    assert(d[k][j][i].x > 0 && "Mesh x-comp. malformed");
                    assert(d[k][j][i].y > 0 && "Mesh y-comp. malformed");
                    assert(d[k][j][i].z > 0 && "Mesh z-comp. malformed");
                    assert(df[k][j][i].x > 0 && "Mesh x-comp. malformed");
                    assert(df[k][j][i].y > 0 && "Mesh y-comp. malformed");
                    assert(df[k][j][i].z > 0 && "Mesh z-comp. malformed");
                    assert(db[k][j][i].x > 0 && "Mesh x-comp. malformed");
                    assert(db[k][j][i].y > 0 && "Mesh y-comp. malformed");
                    assert(db[k][j][i].z > 0 && "Mesh z-comp. malformed");
#endif // HEAVY_ASSERT

                    h = min(h, d[k][j][i].x, d[k][j][i].y, d[k][j][i].z);
                    d[k][j][i].vol = d[k][j][i].x * d[k][j][i].y * d[k][j][i].z;
                }
            }
        }

        ierr = DMDAVecRestoreArray(da_coord, D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(da_coord, Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(da_coord, Db, &db); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array_edge); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array); CHKERRQ(ierr);

    } else { // if 2D
        DMDACoor2d    **coords_array, **coords_array_edge;
        Volume2D      **d, **df, **db;
        ierr = DMDAVecGetArray(cda, coords, &coords_array); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(cda, coords_edge, &coords_array_edge); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, D, &d); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecGetArray(da_coord, Db, &db); CHKERRQ(ierr);

        for (int j=ys; j<ys+ym; j++) {
            for (int i=xs; i<xs+xm; i++) {

                d[j][i].x = (coords_array_edge[j][i+1].x - coords_array_edge[j][i].x);
                d[j][i].y = (coords_array_edge[j+1][i].y - coords_array_edge[j][i].y);

                if(i < xs+xm-1) df[j][i].x = (coords_array[j][i+1].x - coords_array[j][i].x);
                else df[j][i].x = (this->*cax)(i+1, mesh_size[0], left(), right(), R) - coords_array[j][i].x;
                if(j < ys+ym-1) df[j][i].y = (coords_array[j+1][i].y - coords_array[j][i].y);
                else df[j][i].y = (this->*cay)(j+1, mesh_size[1], bottom(), top(), R) - coords_array[j][i].y;

                if(i > xs) db[j][i].x = (coords_array[j][i].x - coords_array[j][i-1].x);
                else db[j][i].x = coords_array[j][i].x - (this->*cax)(i-1, mesh_size[0], left(), right(), R);
                if(j > ys) db[j][i].y = (coords_array[j][i].y - coords_array[j-1][i].y);
                else db[j][i].y = coords_array[j][i].y - (this->*cay)(j-1, mesh_size[1], bottom(), top(), R);

#if HEAVY_ASSERT
                assert(d[j][i].x > 0 && "Mesh x-comp. malformed");
                assert(d[j][i].y > 0 && "Mesh y-comp. malformed");
                assert(df[j][i].x > 0 && "Mesh x-comp. malformed");
                assert(df[j][i].y > 0 && "Mesh y-comp. malformed");
                assert(db[j][i].x > 0 && "Mesh x-comp. malformed");
                assert(db[j][i].y > 0 && "Mesh y-comp. malformed");
#endif // HEAVY_ASSERT

//                 if(p_cm_->world_rank == 1) std::cout << coords_array[j][i].x << " " << coords_array[j][i].y << " " <<
// coords_array_edge[j][i+1].x << " " << coords_array_edge[j+1][i].y << std::endl;

//                 std::cout << d[j][i].x << " "<< d[j][i].y << " "<< db[j][i].x << " "<< db[j][i].y << " "<< df[j][i].x
//                 << " "<< df[j][i].y << std::endl;

                h = min(h, d[j][i].x, d[j][i].y);
                d[j][i].vol = d[j][i].x * d[j][i].y;
            }
        }

        ierr = DMDAVecRestoreArray(da_coord, Df, &df); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(da_coord, Db, &db); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(da_coord, D, &d); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords, &coords_array_edge); CHKERRQ(ierr);
        ierr = DMDAVecRestoreArray(cda, coords_edge, &coords_array); CHKERRQ(ierr);
    }

    assert(h > 0 && "Min. cell dim. must be > 0");


    ierr = DMDASetFieldName(da, 0, "x"); CHKERRQ(ierr);
    ierr = DMDASetFieldName(da, 1, "y"); CHKERRQ(ierr);
    if( p_opt_->enable_3d ) {
        DMDASetFieldName(da, 2, "z"); CHKERRQ(ierr);
    }
    ierr = DMDASetFieldName(da_sc, 0, "scalar"); CHKERRQ(ierr);

    PetscFunctionReturn(ierr);
}

//! Output the mesh for formats that do not do that
//!
PetscErrorCode Domain::output_mesh() {

    FUNCTIONBEGIN(p_cm_);
    Vec           coords;
    DM            cda;
    ierr = DMGetCoordinateDM(da, &cda); CHKERRQ(ierr);
    ierr = DMGetCoordinates(da, &coords); CHKERRQ(ierr);

    PetscViewer   viewer;
    char complete_file_name[PETSC_MAX_PATH_LEN];
    char file_name[PETSC_MAX_PATH_LEN];
    sprintf(file_name, "%s_%s_%s", p_opt_->simulation_name, mesh_str.c_str(), "mesh");

    sprintf(complete_file_name, "%s.bin", file_name);
    OUTPUT(p_cm_, INTRADM, "Exporting to \"%s\"...\n", complete_file_name);
    ierr = PetscViewerBinaryOpen(p_cm_->intra_domain_comm, complete_file_name, FILE_MODE_WRITE, &viewer); CHKERRQ(ierr);
    ierr = VecView(coords, viewer); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
    //     ierr = p_profiler->count("save_vec_bin"); CHKERRQ(ierr);
    PetscFunctionReturn(ierr);
}

//! Check if index is at the border of the domain and check which border
//!
bool Domain::at_border(PetscInt i, PosType::PosType pos)
{
    if ( pos == PosType::Left && i == 0 )
        return true; // On the left, limit is 0 (xaxis = 0,...,Nx-1)
    else if ( pos == PosType::Right && i == mesh_size[0]-1 )
        return true; // On the right, limit is Nx -1 (xaxis = 0,...,Nx-1)
    else if ( pos == PosType::Bottom && i == 0 )
        return true; // On the bottom, limit is 0 (yaxis = 0,...,Ny-1)
    else if ( pos == PosType::Top && i == mesh_size[1]-1 )
        return true; // On the top, limit is Ny - 1 (xaxis = 0,...,Ny-1)
    else if ( pos == PosType::Down && i == 0 )
        return true; // On the bottom, limit is 0 (yaxis = 0,...,Ny-1)
    else if ( pos == PosType::Up && i == mesh_size[2]-1 )
        return true; // On the top, limit is Ny - 1 (xaxis = 0,...,Ny-1)
    return false; // Not at the border otherwise
}
