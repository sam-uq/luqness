/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

//! @file boundaries.h
//! Contains routines for the update of the boundaries.

#include "tools.h"
#include "maths.h"
#include "structures.h"

//! Converts a char to a BDType (Enum)
BCType::BCType charToBDType(char *c);

//! @brief Container for boundary conditions on rectangle/cube.
//! Class containing a boundary type for each of the 4 (or 6)
//! boundaries of the square/cube.
//!
struct BD {
    //! @brief Defaults all boundaries to periodic type.
    BD() {
        for(int i = 0; i < BDRIES_MAX; i++) {
            edge[i] = BCType::Periodic;
        }
    }
    //! @brief Copy constructor, copy all conditions.
    BD(BCType::BCType * arr) {
        for(int i = 0; i < BDRIES_MAX; i++) {
            edge[i] = arr[i];
        }
    }
    //! @brief Check for equality componentwise.
    bool operator==(const BD & rhs) const {
        for(uint i = 0; i < BDRIES_MAX; ++i) {
            if( edge[i] != rhs.edge[i] ) {
                return false;
            }
        }
        return true;
    }
    //! @brief Check for inequality componentwise.
    bool operator!=(const BD & rhs) const {
        return !(*this == rhs);
    }
    //! @brief Detect if Laplacian has a null space.
    //! For now only an all-periodic BD conditions have a nullspace.
    //! TODO: in future plan to check wether boundaries
    //! deduce a nullspace for the Laplacian.
    bool has_nullspace() const {
        return *this == BD();
    }
    //! @brief Handy references for boundary access.
    BCType::BCType left(void) const { return edge[0]; }
    BCType::BCType right(void) const { return edge[1]; }
    BCType::BCType bottom(void) const { return edge[2]; }
    BCType::BCType top(void) const { return edge[3]; }
    BCType::BCType down(void) const { return edge[4]; }
    BCType::BCType up(void) const { return edge[5]; }

    //! @brief Container for boundaries for each edge.
    BCType::BCType edge[BDRIES_MAX];
};

//! @brief Class for boundary type deduction for different quantities.
//! This struct provides a way to handle different boundaries
//! for a specific boundary condition, i.e.
//! if you want SLIP condition on the velocity you may want to INTERPOLATE
//! the pressure and use NEUMANN for the Lalpacian.
//! This automatically handles each case based on the single
//! input of BC for the velocity.
//! Defaults to default of BD, which is periodic.
//! TODO: we may have to implement something more general
//! AND/OR for the vortex method.
struct BDFamily {
    //! @brief Default all boundaries to be periodic.
    BDFamily() : U(), p(), L(), s(), Ls() {}
    //! @brief Copy is done componentwise.
    BDFamily(BDFamily & fam)
        : U(fam.U), p(fam.p), L(fam.L), s(fam.s), Ls(fam.Ls) {}
    //! @brief Create a family of BCs from the BCs for the velocity.
    //! If an array of boundaries is passed, then it is assumed
    //! to be for the velocity, hence other boundary conditions are inferred
    //! accordingly.
    BDFamily(BCType::BCType * arr) {
        init(arr);
    }
    //! @brief Constructor does nothing.
    ~BDFamily() { }
    //! @brief Calls init for BDType after converting every element to BDType.
    void init(char ** name);
    //! @brief Get the boundary type for \f$U\f$ and creates the
    //! right boundary for other quantities automatically, inferring
    //! the type using some deduction rule.
    void init(BCType::BCType * arr);
    BD U, p, L, s, Ls;
    //!< \U: boundary rules for velocity.
    //!< \p: boundary rules for pressure.
    //!< \L: boundary rules for Laplacian.
    //!< \s: boundary rules for scalars. TODO
    //!< \Ls: boundary rules for scalars. TODO
};


