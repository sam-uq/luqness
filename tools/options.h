/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file options.h
//! Defines the Options class, which includes all user-provided options, takes care of command line arguments and input
//! files and defaults.

#include <vector>

#include "tools.h"
#include "maths.h"
#include "structures.h"

//! This class is a monster class that contains all the data provided by the user. All parameters are hereby handled,
//! wrapping around PETSc own options and saving each of the option into a member of this class for easied later use.
//! Most of the options are read once read_options is called and should not be modified later.
class Options {
public:
    //! Constructor, set-up all default parameters when needed
    Options();
    //! Deallocate space, currently only deletes arrays.
    //! FIXME: there may be some leak here.
    ~Options();

    //! Reads options from command line and performs initial set-ups and checks, call once, must call
    PetscErrorCode read_options();

    //// General data
    //! Don't solve, avoid computations (use for test of samplers)
    PetscBool             pretend;
    //! Enable slower in 3D meshes, some option/solver may not be implemented
    PetscBool             enable_3d;
    //! Name of the simulation
    char                  simulation_name[PETSC_MAX_PATH_LEN];
    //! Name of the solver (projection , test, solver)
    char                  solver_name[PETSC_MAX_PATH_LEN];
    //! Was the boundary provided by the user?
    PetscBool             boundaries_provided;
    //! Names of each boundary, in the order L,R,B,T,D,U, one of P,O,N,I,S
    char*                 boundaries_array[BDRIES_MAX];
    //! Size of the mesh
    Mesh                  mesh_opt;
    //! Was the domain provided by the user?
    PetscBool             dom_provided;
    //! Domain boundary coordinates, in the order  L,R,B,T,D,U
    PetscReal             domain_array[DOMAIN_MAX];

    //// Solution type flags
    //! Use euler instead of NS solvers
    //! WARNING: NS solver not yet complete
    PetscBool             use_euler;
    //! Enables use of forcing
    //! WARNING: NS solver not yet complete
    PetscBool             enable_forcing;

    //// Time stepping information/configuration
    //! The CFL multiplicator/constant (usually 0.5)
    PetscReal             lambda;
    //! Use an adaptive timestepping
    PetscBool             adaptive;
    //! Time variables: final time
    PetscReal             T;
    //! Maximum number of iterations, mainly used for debug/profiling
    PetscInt              max_itr;

    //// Equation Parameters
    //! Viscosiry and Reynolds number (mutually exclusive)
    PetscReal             nu;
    //! Viscosiry and Reynolds number (mutually exclusive)
    PetscReal             re;
    //! Transport equation parameters: viscosity and porosity
    PetscReal             transport_viscosity;
    //! Transport equation parameters: viscosity and porosity
    PetscReal             transport_porosity;

    //// Solver parameters
    //! The MC limiter parameter theta
    PetscReal             theta;
    //! Clean the velocity the first time (non-div-free initial data)
    //! WARNING: may break if data is actually div-free already (PETSc bug/missing check)
    PetscBool             do_initial_cleansing;
    //! Tell the code you want to iterate to the initial pressure
    //! TODO: this is currently unavailable
    PetscBool             do_initial_iterates;
    //! Use explicit diffusion, reduces CFL but avoids linear solver
    PetscBool             use_explicit;
    //! For vorticity solver: use initial data provided by velocity instead
    PetscBool             force_initial_data_as_velocity;
    //! For velocity solver: use initial data provided by vorticit instead
    PetscBool             force_initial_data_as_vorticity;

    //// Initial data (model) info
    //! Name of the initial data (i.e. instantiated model)
    inline char *         model_name() { return paramv[0]; }
    //! Number of options provided with the initial data model option (including name)
    PetscInt              paramc;
    //! All parameters of the initial data settings (the first entry is the name), as chars*
    char *                paramv[MAX_PARAMS];
    //! Override parameters with model defaults
    PetscBool             model_use_defaults;

    //// Sampler info
    //! Name of the sampler
    inline char *         samp_name() { return samp_paramv[0]; }
    //! Number of options provided with the sampler (including name)
    PetscInt              samp_paramc;
    //! All parameters of the sampler settings (the first entry is the name)
    char *                samp_paramv[MAX_SAMP_PARAMS];

    //// Repeated simulations array
    //! The number of refinements (for the grid) to do
    PetscInt               num_refinements;
    //! The number of simulaions to do (# of simul. for each refinement)
    PetscInt               simulations_array[MAX_SIMULATIONS];
    //! Use a constant number of simulations for each refinement
    PetscBool              const_sim_number;

    //// Snapshots section
    //! Force the timestep to match the snapshot information (this may break stuff)
    PetscBool             force_snapshot_times;
    //! The array containing all times where you want to take snapshots
    PetscReal             snapshot_times[MAX_SNAPSHOTS];
    //! Interval of snapshots (fps)
    PetscReal             snapshot_fps;
    //! Number of snapshots taken so far
    //! FIXME: shouldn't be here
    PetscInt              num_snapshot;

    //// IO Flags/Data
    //! Snapshot at specified times
    PetscBool             save_times;
    //! Snapshot at specified interval
    PetscBool             save_fps;
    //! Snapshot initial data
    PetscBool             save_initial;
    //! Snapshot final data
    PetscBool             save_final;
    //! Snapshot vorticity
    PetscBool             save_velocity;
    //! Snapshot vorticity
    PetscBool             save_vorticity;
    //! Snapshot pressure
    PetscBool             save_pressure;
    //! Snapshot passive scalar
    PetscBool             save_ps;
    //! Snapshot gamma2
    PetscBool             save_gamma2;
    //! Snapshot mean
    PetscBool             save_mean;
    //! Snapshot var (m2)+
    PetscBool             save_m2;
    //! Snapshot integral
    PetscBool             save_int;
    //! Save individual simulations
    PetscBool             save_sim;
    //! Save level mean/var
    PetscBool             save_levels;
    //! Snapshot difference MLmc
    PetscBool             save_diff;
    //! TODO: save histograms
    PetscBool             save_hist;
    //! Snapshot energy
    PetscBool             save_energy;
    //! Snapshot nothing (stealth mode, overrides everything)
    PetscBool             save_nothing;
    //! Push snapshot to online mac computator (0 = none, i = every i-th snapshot)
    PetscInt              push_snapshot_to_mav;
    //! Master seed to init the simulations
    PetscInt              master_seed;

    //// Histogram options
    //! Array of hist data points
    PetscReal             hist_array[MAX_HIST_DATA];
    //!  Data to drill for histograms
    PetscInt              hist_size;
    //! Points where to drill data
    std::vector<Field3D>  hist_data;

    //// Some solver option for MLMC
    //! Number oflevels in mlmc (or 1 for mc or gauss or deterministic etc...)
    PetscInt              mlmc_levels;

    //// Some solver option for PROJECTION OPERATOR
    //! For periodic BC use FFTW based specrtal solver for the projection method
    //! TODO: not working
    PetscBool             enable_fft;
    //! Use the old KSP hanling (deprecated, here for compatibility)
    PetscBool             old_ksp_handling_;
    //! Use an exact projection operator
    PetscBool             use_exact;

    //// Some solver option for PROJECTION SOLVER
    //    //! Switch to the almgren based convection instead of the BCG (deprecated flag)
    //    PetscBool             enable_almgren;
    //    //! Update the pressure directly without incremental update (deprecated flag)
    //    PetscBool             enable_pressure_update;
    //    //! Project the velocty directly instead of its temproal derivative (deprecated flag)
    //    PetscBool             enable_velocity_proj;
    //    //! Enable the conservative schemes (deprecated flag)
    //    PetscBool             enable_stable_scheme;
    //! Insert a small omount of numerical diffusion that is proportional to h (or dt or dx etc...) (deprecated flag)
    PetscBool             enable_numerical_diff;
    //! Numerical viscosity \f$\epsilon\f$
    PetscReal             num_visc;

    //// Some solver option for VORTEX SOLVER
    //! Use the FTCS vortex scheme (deprecated flag)
    PetscBool             enable_ftcs;

    //// Solver selection options
    //// New options for the chice of solvers, mainly handled as enums, deprecates old flags
    //! Estimator type
    SamplerType::SamplerType sampler_type;
    //! Quadrature type
    QuadratureType::QuadratureType quad_type;
    //! Estimator type
    IntegratorType::IntegratorType integrator_type;
    //! Solver type
    SolverType::SolverType solver;
    //! Convection type (choose Almgren and BCG)
    ConvectionType::ConvectionType conv_type;
    //! Type of projection (choose btw laplacian based, exact laplacian, or fft based)
    ProjectionType::ProjectionType proj_type;
    //! Type of update: Project speed or its derivative?
    ProjectionUpdateType::ProjectionUpdateType projupd_type;
    //! Choose pressure update type: inclemental or overwrite
    PressureUpdateType::PressureUpdateType pressupd_type;
    //! Choose timestepping (forward 2nd order, backward 1st, CN 2nd)
    TimeSteppingType::TimeSteppingType ts_type;
    //! Allocation type
    AllocatorType::AllocatorType alloc_type;
    //! Diffusion type
    DiffType::DiffType diff_type;
    //! Coordinates type
    CoordType::CoordType coord_type;
    //! Transport is enabled?
    PetscBool do_transport;

    /// ? Histo data?
    //! Array containing size of points
    PetscInt * points_array;
    //! Length of points array
    PetscInt   nr_points;

    //// Allocation options (for ATTILA) - static
    //! Number of ranks to be allocated to each level
    PetscInt               ranks_per_level[MAX_MLMC_LEVELS];
    //! Number of parallel simulations run per level
    PetscInt               parallel_sims[MAX_MLMC_LEVELS];
    //! Number of simulations to run at each level
    PetscInt               sim_per_level[MAX_MLMC_LEVELS];
    //! Work of single-deterministic-coarse solution, used for work approximation
    PetscInt               base_work;
    //! How many dofs we expect a rank should handle
    PetscInt               max_dof_per_rank;
    //! Parallel efficiency for the deterministic solver
    PetscReal              det_parallel_portion;
    //! Size of WORLD if choosing to alloc_only (i.e. simulate a simulations with this number of ranks)
    PetscInt              alloc_size;
    //! Just run allocation unit and break
    PetscBool             alloc_only;
    //! If supplied, contains the walltime: may bsed to warn user about resource requirmeent
    PetscInt              wall_t;

    //// Data transfer flags (TODO), (TODO: may wanna use an int with bitmask)
    //    //! Do not transfer U (deprecated use bitmask)
    //    PetscBool             not_transfer_U;
    //    //! Do not transfer w (deprecated use bitmask)
    //    PetscBool             not_transfer_w;
    //    //! Do not transfer Gp (deprecated use bitmask)
    //    PetscBool             not_transfer_Gp;
    //    //! Do not transfer ps (deprecated use bitmask)
    //    PetscBool             not_transfer_ps;
    //    //! Do not transfer gamma2 (deprecated use bitmask)
    //    PetscBool             not_transfer_gamma2;
    //    //! Do not transfer mean (deprecated use bitmask)
    //    PetscBool             not_transfer_mean;
    //    //! Do not transfer variance (deprecated use bitmask)
    //    PetscBool             not_transfer_var;
    //! Bitmask covering transferral (sic..) of components
    int                   transfer_bm;

    //// Misc options
    //! Perforrm MG+GAMG when using MG
    PetscBool             nested_mg;
    //! Auto select MG levels based on dofs
    PetscBool             auto_mg_levels;
    //! Auto select lu based on dofs
    PetscBool             auto_lu;

    //// Help mode
    //! Toggle help on/off
    PetscBool             help;

    //// Random flags warnings
    PetscBool             warn_nullspace;
    PetscBool             warned_nullspace;
private:
};
