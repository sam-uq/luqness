/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "boundaries.h"

//! Converts a char to a BCType (for conversion from options)
//!
BCType::BCType charToBDType(char * c)
{
    if( !strcmp(c, "P") ) return BCType::Periodic;
    else if( !strcmp(c, "S") ) return BCType::Slip;
    else if( !strcmp(c, "N") ) return BCType::Slip;
    else if( !strcmp(c, "O") ) return BCType::Outflow;
    else if( !strcmp(c, "I") ) return BCType::Inflow;
    else return BCType::Periodic;
}

//! Converts a char to a bd type and then automatically init with a BCType type
//!
void BDFamily::init(char ** name)
{
    BCType::BCType arr[6];
    for(int i = 0; i < BDRIES_MAX; i++) {
        arr[i] = charToBDType(name[i]);
    }
    init(arr);
}

//! Converts a BDType array to a family of boundaries, this is needed in order to automatically select compatible
//! boundaries for other quantities.
void BDFamily::init(BCType::BCType * arr)
{
    for(int i = 0; i < BDRIES_MAX; i++) {

        // Boundaries for velocity are the ones specified by the user
        U.edge[i] = arr[i];

        // Boundaries for the gradient of the pressure are used to compute the gradient of the pressure, HOVWEVER we
        // may want to FIXME that: it should be Dirichlet (zero) boundaries always or periodic (because of Laplacian)
        if( arr[i] == BCType::Slip ) p.edge[i] =  BCType::Interpolate;
        else if( arr[i] == BCType::Slip ) p.edge[i] =  BCType::Interpolate;
        else if( arr[i] == BCType::Inflow ) p.edge[i] =  BCType::Interpolate;
        else if( arr[i] == BCType::Outflow ) p.edge[i] = BCType::Dirichlet;
        else p.edge[i] = arr[i];

        // Boundaries for the PRESSURE (i.e. stream function), should alwys be BCType::Neumann, wanna FIXME THIS
        if( arr[i] == BCType::Periodic ) L.edge[i] = BCType::Periodic;
        else if( arr[i] == BCType::Outflow ) L.edge[i] = BCType::Dirichlet;
        else L.edge[i] = BCType::Neumann;

        // Passive scalar is always the same as U
        s.edge[i] = arr[i];

        // Boundaries for the upstream of the laplacian, FIXME
        if( arr[i] == BCType::Periodic ) Ls.edge[i] = BCType::Periodic;
        else Ls.edge[i] = BCType::Neumann;
    }
}
