/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file domain.h
//! Implements a class Domain managing the domain which supersets a DMDA.

#include <sstream>

#include "config.h"
#include "options.h"
#include "boundaries.h"
#include "maths.h"

#include "../attila/attila.h"

//! @brief Manages the Domain for a single sumlation.
//!
//! This class introduces a domain handler which contains
//! all domain data and DMDA data. Also has some DMDA-only
//! dependent method such as averaging or refinement.
class Domain {
public:
    //! @brief Builds appropriate structures from Options and Attila allocations.
    Domain(Options * opt_, Attila *p_cm)
    : da_coord(NULL), keep_old(false), has_old(false)
    , p_cm_(p_cm), p_opt_(opt_) {
        FUNCTIONBEGIN(p_cm_);
    }
    //! @brief
    ~Domain();

    //! @brief Reads options and creates all data.
    PetscErrorCode setup_from_options(void);
    //! @brief Performs a mesh reginement
    PetscErrorCode refine(void);

    //! @brief Mesh interpolation.
    //! @tparam Vector is a scalar quantity or not.
    //! Interpolates the vector "\V" to the vector "\ref_V"
    //! @param[in] V Vector to interpolate (values in old mesh).
    //! @param[out] ref_V Destination vector, has values in the new mesh.
    template <bool scalar>
    PetscErrorCode refine_to_domain(Vec V, Vec ref_V);
    //! @brief Average the staggered vorticity (DEBUG?).
    //!
    template <bool scalar, int offset>
    PetscErrorCode avg(Vec stag_w, Vec w);

    //! Function that return true if an index is at the given border
    bool at_border(PetscInt i, PosType::PosType pos);

    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              left(void) const { return domain[0][0]; }
    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              right(void) const { return domain[1][0]; }
    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              bottom(void) const { return domain[0][1]; }
    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              top(void) const { return domain[1][1]; }
    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              down(void) const { return domain[0][2]; }
    //! Left boundary x-coordinate (not a cell center!!!)
    PetscReal              up(void) const { return domain[1][2]; }

    //!
    //! \brief Find uniform coordinate position given index in array.
    //! \param[in] i Index within linear array.
    //! \param[in] m Number of elements in this axis.
    //! \param[in] l Left boundary of axis.
    //! \param[in] r Right boundary of axis.
    //! \return Coordinate of the element at coordinate \f$i\f$ within the array.
    //! The coordinate is computed with quantities located in the cell centre.
    //! TODO: this has to go!
    //! FIXME: check that m is used correctly.
    double linspace(double i, double m, double l, double r, double) {
        return (i + 0.5) / m * (r - l) + l;
    }
    //!
    //! \brief Find uniform coordinate position given index in array, coordinates are edge valued.
    //! \param[in] i Index within linear array.
    //! \param[in] m Number of elements in this axis.
    //! \param[in] l Left boundary of axis.
    //! \param[in] r Right boundary of axis.
    //! \return Coordinate of the element at coordinate \f$i\f$ within the array.
    //! The coordinate is computed with quantities located in the cell edge.
    //! TODO: this has to go!
    //! FIXME: check that m is used correctly.
    double linspace_edge(double i, double m, double l, double r, double) {
        return (i / m) * (r - l) + l;
    }
    //!
    //! \brief Find adaptive coordinate position give index in array, coordinates are edge valued.
    //! \param[in] i Index within linear array.
    //! \param[in] m Number of elements in this axis.
    //! \param[in] l Left boundary of axis.
    //! \param[in] r Right boundary of axis.
    //! \param[in] R Radius for interface.
    //! \return Coordinate of the element at coordinate \f$i\f$ within the array.
    //! The coordinate is computed with quantities located in the cell edge.
    //! Refines near boundaries to better resolve boundary layers.
    //! TODO: this has to go!
    //! FIXME: check that m is used correctly.
    double tanhfun_edge(double i, double m, double l, double r, double R) {
        double sqrt1m1oR = std::sqrt(1- (1/R));
        return 0.5 * (
                     std::tanh( (i*2./m-1) * atanh(sqrt1m1oR) )
                       * (r - l) / sqrt1m1oR + (r + l)
                     );
    }
    //!
    //! \brief Find adaptive coordinate position given index in array, cell centre.
    //! \param[in] i Index within linear array.
    //! \param[in] m Number of elements in this axis.
    //! \param[in] l Left boundary of axis.
    //! \param[in] r Right boundary of axis.
    //! \param[in] R Radius for interface.
    //! \return Coordinate of the element at coordinate \f$i\f$ within the array.
    //! The coordinate is computed with quantities located in the cell centre.
    //! Refines near boundaries to better resolve boundary layers.
    //! TODO: this has to go!
    //! FIXME: check that m is used correctly.
    double tanhfun(double i, double m, double l, double r, double R) {
        return 0.5 * ( tanhfun_edge(i,m,l,r,R) + tanhfun_edge(i+1,m,l,r,R) );
    }

    //!
    //! \brief get_2d_index returns the index within the array, given a coordinate.
    //! \param coord
    //! \param flag
    //! \return
    //!
    Field<2, int> get_2d_index(Field2D coord, bool * flag) {
        Field<2, int> ret;
        ret[0] = (coord[0] - left()) / (right() - left()) * mesh_size[0];
        ret[1] = (coord[1] - bottom()) / (top() - bottom()) * mesh_size[1];
        *flag = false;
        if( coord[0] < left() || coord[0] > right() ) {
            WARNING(p_cm_, INTRADM,
                    "Coordinates (%f,%f) out of bounds (x-comp., [%.2f,%.2f]), preventing "
                    "segmentation (would result in index (%d, %d))...\n",
                    coord[0], coord[1], left(), right(), ret[0], ret[1]);
            ret[0] = 0;
            ret[1] = 0;
        }
        if( coord[1] < bottom() || coord[1] > top() ) {
            WARNING(p_cm_, INTRADM,
                    "Coordinates (%f,%f) out of bounds (y-comp., [%.2f,%.2f]), preventing "
                    "segmentation (would result in index (%d, %d))...\n",
                    coord[0], coord[1], bottom(), top(), ret[0], ret[1]);
            ret[0] = 0;
            ret[1] = 0;
        }
        if( ret[0] >= xs && ret[0] < xs+xm && ret[1] >= ys && ret[1] < ys+ym ) {
            OUTPUT(p_cm_, SELF,
                   "Coordinates (%d,%d) within local portion [%d,%d]x[%d,%d].\n",
                   ret[0], ret[1], xs, xs+xm, ys, ys+ym);
            *flag = true;
        } else {
            WARNING(p_cm_, SELF,
                    "Coordinates (%d,%d) out of local portion [%d,%d]x[%d,%d], flag is false.\n",
                    ret[0], ret[1], xs, xs+xm, ys, ys+ym);
        }
        return ret;
    }

    //!
    //! \brief get_3d_index Given the coordinate, returns the index withing linear array.
    //! \param coord
    //! \param flag
    //! \return
    //! This works only for uniform grids.
    Field<3, int> get_3d_index(Field3D coord, bool * flag) {
        Field<3, int> ret;
        ret[0] = (coord[0] - left()) / (right() - left()) * mesh_size[0];
        ret[1] = (coord[1] - bottom()) / (top() - bottom()) * mesh_size[1];
        ret[2] = (coord[2] - down()) / (up() - down()) * mesh_size[2];
        *flag = false;
        if( coord[0] < left() || coord[0] > right() ) {
            WARNING(p_cm_, INTRADM, "Coordinates (%f,%f,%f) out of bounds (x-comp., [%.2f,%.2f]), preventing "
            "segmentation (would result in index (%d, %d, %d))...\n",
                    coord[0], coord[1], coord[2], left(), right(), ret[0], ret[1], ret[2]);
            ret[0] = 0;
            ret[1] = 0;
            ret[2] = 0;
        }
        if( coord[1] < bottom() || coord[1] > top() ) {
            WARNING(p_cm_, INTRADM,
                    "Coordinates (%f,%f,%f) out of bounds (y-comp., [%.2f,%.2f]), preventing "
                    "segmentation (would result in index (%d, %d, %d))...\n",
                    coord[0], coord[1], coord[2], bottom(), top(), ret[0], ret[1], ret[2]);
            ret[0] = 0;
            ret[1] = 0;
            ret[2] = 0;
        }
        if( coord[2] < down() || coord[2] > up() ) {
            WARNING(p_cm_, INTRADM,
                    "Coordinates (%f,%f,%f) out of bounds (y-comp., [%.2f,%.2f]), preventing "
                    "segmentation (would result in index (%d, %d, %d))...\n",
                    coord[0], coord[1], coord[2], down(),up(), ret[0], ret[1], ret[2]);
            ret[0] = 0;
            ret[1] = 0;
            ret[2] = 0;
        }
        if( ret[0] >= xs && ret[0] < xs+xm && ret[1] >= ys && ret[1] < ys+ym ) {
            OUTPUT(p_cm_, SELF,
                   "Coordinates (%d,%d,%d) within local portion [%d,%d]x[%d,%d]x[%d,%d].\n",
                   ret[0], ret[1], ret[2], xs, xs+xm, ys, ys+ym, zs, zs+zm);
            *flag = true;
        } else {
            WARNING(p_cm_, SELF,
                    "Coordinates (%d,%d,%d) out of local portion [%d,%d]x[%d,%d]x[%d,%d],"
                    "flag is false.\n",
                    ret[0], ret[1], ret[2], xs, xs+xm, ys, ys+ym, zs, zs+zm);
        }
        return ret;
    }

    //!
    //! \brief DotL2 compute the dot product, INCLUDING anisotropy
    //! \param A A vector.
    //! \param B A vector.
    //! \param dot Result of \f$\langle A, B \rangle\f$.
    //! \return Error code.
    //! TODO: anisotropy is not computed.
    PetscErrorCode DotL2(Vec A, Vec B, PetscReal * dot) {
        // TODO: ANISOTROPY
        VecDot(A, B, dot);

        PetscFunctionReturn(ierr);
    }

    //! L2 Norm INCLUDING anisotropy
    PetscErrorCode NormL2(Vec A, PetscReal * norm) {
        // TODO: ANISOTROPY
        VecNorm(A, NORM_2, norm);

        PetscFunctionReturn(ierr);
    }

    PetscErrorCode output_mesh();

    //! Boundary conditions for each boundary
    //! BDEdge                bd_L, bd_R, bd_B, bd_T, bd_D, bd_U; // left, right (x), bottom, top (y), down, up (z)
    BDFamily               bd_fam;

    //! DMDAs for scalar quantities and for fields and also for older quantities (used when refinements is used)
    DM                     da; //! DMDA containing fields
    DM                     da_sc; //! DMDA containing scalars
    DM                     da_coord; //! DMDA containing coordinate data (dx, dy, dz and vol for anisotropic)
    DM                     da_old; //! DMDA containing fields
    DM                     da_sc_old; //! DMDA containing scalars

    //! Interapolation matrices
    Mat                    interp_mat, interp_sc_mat;

    // Spatial information
    //! Left, right etc... limits for the domain
    Mesh                   mesh_size; //!< Mesh size (x,y,z)
    //!
    Mesh                   mesh_ranks; //!< Mesh of processors, i.e. proc in each direction
    PetscReal              l_x; //!< Length x-direction
    PetscReal              l_y; //!< Length y-direction
    PetscReal              l_z; //!< Length z-direction
    //! Local porion of the domain OWNED by the processor (for for cylces)
    PetscInt               xs, ys, zs, xm, ym, zm;
    //! Mesh resolution in each direction
    PetscReal              dxi, dyi, dzi;
    //! Mesh minimum in each direction
    PetscReal              h; // = min(dx, dy, dz)
    //! The domain boundaries location
    PetscReal              domain[2][MESH_MAX];
    //   PetscReal                  area;

    Vec                    D, Df, Db; // As struct volumedata (differences D = central Df forward, Db backward)

    //! Keep old DMDA info and refinement interp upon refinement?
    bool                   keep_old;
    //! Info about old dmda has been generated
    bool                   has_old;

    std::string            mesh_str;

    // TODO Remove
    Attila * const         p_cm_;
    //!
    Options * const        p_opt_;
private:
    //!
    PetscErrorCode generate_dmdas(void);
    //!
    PetscErrorCode after_dmda_changed(void);

    //! The stencil width for the scheme (we only need one for now).
    PetscInt               stencil;
};

//!
//! Allow to refine any vector to the new vector computed by halving the coordinates
//! TODO: this currently only works with refined domain aobtained by doubling of sizes. In future we might want to
//! implement a new thing where
//! Holy grail would be to switch to some cleaner templating (wmaybe?), in fact all of this is done because of PETSc's
//! C-style casting to void of the array, we need to big the data structure according to the array type but have to do
//! this manually
//! @param V vector belonging to the old mesh that gets projected to the new one
//! @param ref_V vector belonging to the new mesh to which data is copied
//! @tparam scalar templated flag used to check if you want the "scalar" of "field" version
//!
template <bool scalar>
PetscErrorCode Domain::refine_to_domain(Vec V, Vec ref_V)
{

    if(scalar) {
        if( p_opt_->enable_3d ) {
            PetscScalar         ***V_array, ***ref_V_array;
            ierr = DMDAVecGetArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
            for (int k=zs; k<zs+zm; ++k) {
                for (int j=ys; j<ys+ym; ++j) {
                    for (int i=xs; i<xs+xm; ++i) {
                        ref_V_array[k][j][i] = V_array[k / 2][j / 2][i / 2];
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
        } else {
            PetscScalar         **V_array, **ref_V_array;
            ierr = DMDAVecGetArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
            for (int j=ys; j<ys+ym; ++j) {
                for (int i=xs; i<xs+xm; ++i) {
                    ref_V_array[j][i] = V_array[j / 2][i / 2];
                }
            }
            ierr = DMDAVecRestoreArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
        }
    } else {
        if( p_opt_->enable_3d ) {
            Field3D         ***V_array, ***ref_V_array;
            ierr = DMDAVecGetArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
            for (int k=zs; k<zs+zm; ++k) {
                for (int j=ys; j<ys+ym; ++j) {
                    for (int i=xs; i<xs+xm; ++i) {
                        ref_V_array[k][j][i] = V_array[k / 2][j / 2][i / 2];
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
        } else {
            Field2D         **V_array, **ref_V_array;
            ierr = DMDAVecGetArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
            for (int j=ys; j<ys+ym; ++j) {
                for (int i=xs; i<xs+xm; ++i) {
                    ref_V_array[j][i] = V_array[j / 2][i / 2];
                }
            }
            ierr = DMDAVecRestoreArray(da_sc_old, V, &V_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, ref_V, &ref_V_array); CHKERRQ(ierr);
        }
    }

    PetscFunctionReturn(ierr);
}

//! Average vorticity (DEBUG?)
//! offset -1 or 1 (depending if you average forward or backwards)
template <bool scalar, int offset>
PetscErrorCode Domain::avg(Vec stag_w, Vec w)
{
    FUNCTIONBEGIN(p_cm_);

    Vec     stag_w_local;
    if( scalar ) { // Scalar
        ierr = DMGetLocalVector(da_sc, &stag_w_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalBegin(da_sc, stag_w, INSERT_VALUES, stag_w_local); CHKERRQ(ierr);
        ierr = DMGlobalToLocalEnd(da_sc, stag_w, INSERT_VALUES, stag_w_local); CHKERRQ(ierr);

        if( !p_opt_->enable_3d ) { // 2D version
            PetscReal        **stag_w_array, **w_array;
            ierr = DMDAVecGetArray(da_sc, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, w, &w_array); CHKERRQ(ierr);
            for (int j=ys; j<ys+ym; ++j) {
                for (int i=xs; i<xs+xm; ++i) {
                    w_array[j][i] = 1.0/4.0 * ( stag_w_array[j][i] + stag_w_array[j+offset][i]
                            + stag_w_array[j][i+offset] + stag_w_array[j+offset][i+offset] );
                }
            }
            ierr = DMDAVecRestoreArray(da_sc, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, w, &w_array); CHKERRQ(ierr);
        } else { // 3D
            PetscReal        ***stag_w_array, ***w_array;
            ierr = DMDAVecGetArray(da_sc, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da_sc, w, &w_array); CHKERRQ(ierr);
            for (int k=zs; k<zs+zm; ++k) {
                for (int j=ys; j<ys+ym; ++j) {
                    for (int i=xs; i<xs+xm; ++i) {
                        w_array[k][j][i] = 1.0/8.0 * ( stag_w_array[k][j][i] + stag_w_array[k][j+offset][i]
                                + stag_w_array[k][j][i+offset] + stag_w_array[k][j+offset][i+offset]
                                +stag_w_array[k+offset][j][i] + stag_w_array[k+offset][j+offset][i]
                                + stag_w_array[k+offset][j][i+offset] + stag_w_array[k+offset][j+offset][i+offset] );
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da_sc, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da_sc, w, &w_array); CHKERRQ(ierr);
        }

        ierr = DMRestoreLocalVector(da_sc, &stag_w_local); CHKERRQ(ierr);
    } else { // FIELD
        ierr = DMGetLocalVector(da, &stag_w_local); CHKERRQ(ierr);

        ierr = DMGlobalToLocalBegin(da, stag_w, INSERT_VALUES, stag_w_local); CHKERRQ(ierr);
        ierr = DMGlobalToLocalEnd(da, stag_w, INSERT_VALUES, stag_w_local); CHKERRQ(ierr);

        if( !p_opt_->enable_3d ) { // 2D version
            Field2D        **stag_w_array, **w_array;
            ierr = DMDAVecGetArray(da, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, w, &w_array); CHKERRQ(ierr);
            for (int j=ys; j<ys+ym; ++j) {
                for (int i=xs; i<xs+xm; ++i) {
                    w_array[j][i] =  ( stag_w_array[j][i] + stag_w_array[j+offset][i]
                            + stag_w_array[j][i+offset] + stag_w_array[j+offset][i+offset] ) * 1.0/4.0;
                }
            }
            ierr = DMDAVecRestoreArray(da, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, w, &w_array); CHKERRQ(ierr);
        } else { // 3D
            Field3D        ***stag_w_array, ***w_array;
            ierr = DMDAVecGetArray(da, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecGetArray(da, w, &w_array); CHKERRQ(ierr);
            for (int k=zs; k<zs+zm; ++k) {
                for (int j=ys; j<ys+ym; ++j) {
                    for (int i=xs; i<xs+xm; ++i) {
                        w_array[k][j][i] = ( stag_w_array[k][j][i] + stag_w_array[k][j+offset][i]
                                + stag_w_array[k][j][i+offset] + stag_w_array[k][j+offset][i+offset]
                                + stag_w_array[k+offset][j][i] + stag_w_array[k+offset][j+offset][i]
                                + stag_w_array[k+offset][j][i+offset] + stag_w_array[k+offset][j+offset][i+offset] ) * 1.0/8.0;
                    }
                }
            }
            ierr = DMDAVecRestoreArray(da, stag_w_local, &stag_w_array); CHKERRQ(ierr);
            ierr = DMDAVecRestoreArray(da, w, &w_array); CHKERRQ(ierr);
        }

        ierr = DMRestoreLocalVector(da, &stag_w_local); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

