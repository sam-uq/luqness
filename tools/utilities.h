/*
 * Copyright 2015 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <iterator>

#include <sys/stat.h>

#include "structures.h"

//! Check if a file exists
//!Use sys/stat.h to access file
inline bool exists(const std::string& name)
{
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

//!
//! Maximum number of DOFs a mesh should have until you do a LU decomp
//!
#define       MAX_DOF_PER_LU     512*512

//!
//! Check whether this mesh is better dealt with with LU decomp or not.
//!
inline bool auto_lu(Mesh mesh)
{
    return mesh[0] * mesh[1] * mesh[2] < MAX_DOF_PER_LU ? true : false;
}

//!
//! Stringize a vector
//!
std::string to_str(std::vector<int> vec);

//!
//! Number of levels, given mesh,multigrid type, etc...
//!
int auto_mg_levels(Mesh mesh, Mesh ranks_mesh,
                   int dim, int min_pow = 1,
                   MgType::MgType mg_type = MgType::PC_MG);
