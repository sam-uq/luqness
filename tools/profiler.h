/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

//! @file profiler.h
//! File contain class definition/declaration of Profiler class, which consists on timing/profiling routines

#include "tools.h"

#include <petscsys.h>
#include <petsctime.h>
#include <ctime>

#include <string>
#include <sstream>
#include <map>
#include <vector>

class Attila;

//!
//! \brief The Profiler class is a custom profiler class that
//! tracks most used/expensive calls and summarizes the information.
//!
class Profiler {
public:
    //! Store time and take first snapshot called "Constructor"
    Profiler(Attila * p_cm) : p_cm_(p_cm) {
#if DO_PROFILING
        PetscLogDefaultBegin();
        ierr = Timer(&time_since_epoch); CHKERRV(ierr);
        tic("profiler_constructor");
#endif // DO_PROFILING
    }
    //! Calls snapshot toc(Destructor) and print profiling information (print())
    ~Profiler();

    //! Push a new PETSc stage, contrary to PETSc own routine, automatically check for stage reuse
    PetscErrorCode push_stage(std::string stage) {
        #if DO_PROFILING
        PetscLogStage s;
        if( stages.count(stage) == 0 ) {
            ierr = PetscLogStageRegister(stage.c_str(), &s); CHKERRQ(ierr);
            stages.insert( std::pair<std::string, PetscLogStage>(stage, s) );
        }

        ierr = PetscLogStagePush(stages.at(stage)); CHKERRQ(ierr);

        #endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }

    //! Pop last stage (no sanity check performed)
    PetscErrorCode pop_stage(void) {
        #if DO_PROFILING
        ierr = PetscLogStagePop(); CHKERRQ(ierr);

        #endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }

    //! Push a new PETSc event, contrary to PETSc own routine, automatically check for stage reuse
    PetscErrorCode begin_event(std::string event) {
#if DO_PROFILING
        PetscLogEvent e;
        if( events.count(event) == 0 ) {
            ierr = PetscLogEventRegister(event.c_str(), 0, &e); CHKERRQ(ierr);
            events.insert( std::pair<std::string, PetscLogEvent>(event, e) );
        }

        ierr = PetscLogEventBegin(events.at(event),0,0,0,0); CHKERRQ(ierr);

#endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }

    //! Pop last event (no sanity check performed)
    PetscErrorCode end_event(std::string event, int flops = 0) {
#if DO_PROFILING
        ierr = PetscLogFlops(flops); CHKERRQ(ierr);
        ierr = PetscLogEventEnd(events.at(event),0,0,0,0); CHKERRQ(ierr);

#endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }

    //! Push a snapshot with elapsed time (no current time saving), i.e. time since epoch
    PetscErrorCode tic(std::string name = "tic", bool map_last_tic= false) {
#if DO_PROFILING
        ierr = Timer(&last_tic_time); CHKERRQ(ierr);
        timings.push_back(std::pair<std::string, PetscLogDouble>(name, last_tic_time - time_since_epoch));
        if( map_last_tic ) {
            try {
                tic_map.at(name) = last_tic_time;
            }
            catch (const std::out_of_range& oor) {
                // pass
                tic_map.insert( std::pair<std::string, PetscLogDouble>(name, last_tic_time) );
            }
        }

#endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }


    //! Return time elapsed (since last tic)
    PetscLogDouble toc(std::string name = "") const {
#if DO_PROFILING
        PetscLogDouble time;
        ierr = Timer(&time); CHKERRQ(ierr);
        if( name == "" ) {
            return time - last_tic_time;
        } else {
            try {
                return time - tic_map.at(name);
            }
            catch (const std::out_of_range& oor) {
                return time - last_tic_time;
            }
        }
#endif // DO_PROFILING
        return 0;
    }

    //! Implements a counter, each string is pushed the first time with value 0, then is incremented
    PetscErrorCode count(std::string name) {
#if DO_PROFILING
        if( counters.count(name) == 0 ) {
            counters.insert( std::pair<std::string,PetscLogStage>(name, 1) );
        } else {
            ++counters.at(name);
        }

#endif // DO_PROFILING
        PetscFunctionReturn(ierr);
    }

    //! Print out all the information gathered and also outputs necessary files
    PetscErrorCode print(void);

    //! Set the name ptr for prepend
    void set_name(char * name) { name_ = name; }

private:
    //! Communication routines accessor (needed for communicators)
    Attila *const p_cm_;

    //! Time since constructor, and last time since tic()
    PetscLogDouble time_since_epoch, last_tic_time;
    //! For each string (a "snapshot", taken wit tic or toc) we store the time
    std::vector< std::pair<std::string, PetscLogDouble> > timings;
    //! For each string store a counter counted with count(std::string)
    std::map<std::string, uint> counters;
    //! Store PETSc stages indexed by strings
    std::map<std::string, PetscLogStage> stages;
    //! Store PETSc eventss indexed by strings
    std::map<std::string, PetscLogEvent> events;
    //! Store PETSc eventss indexed by strings
    std::map<std::string, PetscLogDouble> tic_map;

    //! Name to prepend to profiler
    char * name_;
};

//! Will be defined in main.cpp, created and destructed there
extern Profiler                * p_profiler;

