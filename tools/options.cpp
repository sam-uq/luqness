/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "options.h"

//! Here we provide all default values and setup all the memory we may need later.
//!
Options::Options()
    : pretend(PETSC_FALSE)
    , enable_3d(PETSC_FALSE)
    , use_euler(PETSC_TRUE)
    , enable_forcing(PETSC_TRUE)
    , lambda(0.5)
    , adaptive(PETSC_TRUE)
    , T(1.)
    , max_itr(-1)
    //    , anisotropic(PETSC_FALSE)
    , nu(0.)
    , re(0.)
    , transport_viscosity(0.0005)
    , transport_porosity(1.0)
    , theta(1.0)
    , do_initial_cleansing(PETSC_FALSE)
    , do_initial_iterates(PETSC_FALSE)
    , use_explicit(PETSC_FALSE)
    , force_initial_data_as_velocity(PETSC_FALSE)
    , force_initial_data_as_vorticity(PETSC_FALSE)
    , num_refinements(1)
    , snapshot_fps(0.1)
    , num_snapshot(MAX_SNAPSHOTS)
    , save_times(PETSC_FALSE)
    , save_fps(PETSC_FALSE)
    , save_initial(PETSC_FALSE)
    , save_final(PETSC_FALSE)
    , save_velocity(PETSC_FALSE)
    , save_vorticity(PETSC_FALSE)
    , save_pressure(PETSC_FALSE)
    , save_ps(PETSC_FALSE)
    , save_gamma2(PETSC_FALSE)
    , save_mean(PETSC_FALSE)
    , save_m2(PETSC_FALSE)
    , save_int(PETSC_FALSE)
    , save_sim(PETSC_FALSE)
    , save_levels(PETSC_FALSE)
    , save_diff(PETSC_FALSE)
    , save_hist(PETSC_FALSE)
    , save_energy(PETSC_FALSE)
    , save_nothing(PETSC_FALSE)
    , push_snapshot_to_mav(1)
    , master_seed(-1)
    , hist_size(MAX_HIST_DATA)
    , mlmc_levels(1)
    , num_visc(0.1)
    , conv_type(ConvectionType::BCG)
    , proj_type(ProjectionType::Laplacian)
    , projupd_type(ProjectionUpdateType::VelocityProjection)
    , pressupd_type(PressureUpdateType::IncrementalPressure)
    , ts_type(TimeSteppingType::Forward)
    , alloc_type(AllocatorType::Dynamic)
    , diff_type(DiffType::None)
    , nr_points(MAX_DIM)
    , base_work(1)
    , max_dof_per_rank(100*100*100)
    , det_parallel_portion(0.97)
    , alloc_only(PETSC_FALSE)
    //    , not_transfer_U(PETSC_TRUE)
    //    , not_transfer_w(PETSC_TRUE)
    //    , not_transfer_Gp(PETSC_TRUE)
    //    , not_transfer_ps(PETSC_TRUE)
    //    , not_transfer_gamma2(PETSC_TRUE)
    , auto_mg_levels(PETSC_FALSE)
    , auto_lu(PETSC_FALSE)
    , help(PETSC_FALSE) {
    FUNCTIONBEGIN_MACRO("", WORLD);

    mesh_opt[0] = 32;
    mesh_opt[1] = 32;
    mesh_opt[2] = 32;

    domain_array[0] = 0.;
    domain_array[1] = 1.;
    domain_array[2] = 0.;
    domain_array[3] = 1.;
    domain_array[4] = 0.;
    domain_array[5] = 1.;

    for(int i = 0; i < BDRIES_MAX; i++) {
        boundaries_array[i] = new char[2];
        strcpy(boundaries_array[i], "P");
    }

    strcpy(solver_name, "projection");

    const_sim_number = PETSC_FALSE;
    force_snapshot_times = PETSC_TRUE;

    time_t        timer;
    time(&timer);
    sprintf(simulation_name, "unnamed-%ld", timer);
}

//! Destructor: deallocate following arrays
//! and objects: boundaries_array
//!
Options::~Options() {
}

//! read options from command line
//!
PetscErrorCode Options::read_options() {
    // Temporary data for option setting
    PetscBool             flag, eps_flag;
    PetscInt              bdries_max = BDRIES_MAX, domain_max = DOMAIN_MAX, mesh_max = MESH_MAX;
    PetscInt              simulations_max = MAX_SIMULATIONS;
    paramc = MAX_PARAMS;
    samp_paramc = MAX_PARAMS;
    char                  temp_str[PETSC_MAX_PATH_LEN];

#if PETSC_VERSION_MINOR <= 6
    // Configuration files will override the option database
    ierr = PetscOptionsGetString("", "-i", temp_str, PETSC_MAX_PATH_LEN, &flag);
    if(flag) {
        ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD, temp_str , PETSC_FALSE);
    } else {
        ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD, "conf.cfg" , PETSC_FALSE);
    }
#else
    // Configuration files will override the option database
    ierr = PetscOptionsGetString(NULL, "", "-i", temp_str, PETSC_MAX_PATH_LEN, &flag);
    if(flag) {
        ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD, NULL, temp_str , PETSC_FALSE);
    } else {
        ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD, NULL, "conf.cfg" , PETSC_FALSE);
    }
#endif

    /////////////////////////////////////////////////
    /// Help mode
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, PETSC_NULL, "General options", "general"); CHKERRQ(ierr);
    {
        ierr = PetscOptionsName("-help", "Print help and quit.", "", &help); CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// General options for the simulation like 3d, adaptivity, etc....
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, PETSC_NULL, "Simulation options", "simulation"); CHKERRQ(ierr);
    {
        // Setup name
        PetscOptionsString("-name", "Name to give to the simulation.", "",
                           simulation_name, simulation_name, PETSC_MAX_PATH_LEN, &flag);
        PetscOptionsName("-pretend", "Pretend to run the simulation but actually don't.", "", &pretend);
        if( pretend ) {
            WARNING("", WORLD, ""
                    RED("The flag \"-pretend\" has ben detected, this will supress any actual computation. "
                        "This is only for debug!!!!! Disable it you want a production run.")
                    "\n", "");
        }
        PetscOptionsName("-enable_3d", "Enable 3D simulation [experimental].", "", &enable_3d);
        if( enable_3d ) {
            WARNING("", WORLD, "3D is enabled for this run. This feature is still experimental.\n", "");
        }
        // Get adaptive option
        adaptive = PETSC_TRUE;
        PetscOptionsName("-adaptive", "Use adaptive CFL number", "", &flag);
        if( !help && !flag ) {
            adaptive = PETSC_FALSE;
            WARNING("", WORLD, "\"-adaptive\" is not set!\n", "");
        }
        // FIXME:?
        PetscOptionsName("-no_force_snapshots", "Step size is *NOT* forced to accommodate selected snapshots", "",
                         &flag);
        if( !help && flag ) {
            WARNING("", WORLD, "\"-no_force_snapshots\" is set! This may cause problems for convergence"
                    " plots...\n", "");
            force_snapshot_times = PETSC_FALSE;
        } else if( !help &&!flag ) {
            WARNING("", WORLD, "\"-no_force_snapshots\" is *NOT* set! This may cause problems (eventually)...\n", "");
        }

        PetscOptionsName("-forcing", "Enable computation of forcing.", "", &enable_forcing);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the estimator, i.e. type (Mc, MlMc, ...) and samples
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, PETSC_NULL, "Estimator options", "estimator"); CHKERRQ(ierr);
    {
        // Sampler configuration
        PetscOptionsStringArray("-smp", "Choose the type of sampler"
                                        "(Well, File, Gauss (det.), Boost, stdc++11 (not yet) ", "",
                                samp_paramv, &samp_paramc, &flag);
        if( help ) {
            // No need to worry
        } else if( !flag || !strcmp(samp_name(), "well") ) {
            DEBUG("", WORLD, "The random number generetor Well512a will be used.\n", "");
            sampler_type = SamplerType::Well;
        } else if ( !strcmp(samp_name(), "load") ) {
            DEBUG("", WORLD, "Random numbers will be read from a file.\n", "");
            WARNING("", WORLD, "This feature is not yet implemented.\n", "");
            sampler_type = SamplerType::Load;
        } else if ( !strcmp(samp_name(), "gauss") ) {
            DEBUG("", WORLD, "Quadrature points will be generated as Gauss-Legendre.\n", "");
            WARNING("", WORLD, "Seems like you are using a Gauss deterministic sampler, this is experimental.\n", "");
            sampler_type = SamplerType::Gauss;
        } else if ( !strcmp(samp_name(), "boost") ) {
            DEBUG("", WORLD, "The Boost random number generetor will be used.\n", "");
            WARNING("", WORLD, "Not yet implemented! This requires the Boost Package\n", "");
#if !BOOST_ENABLE
            WARNING("", WORLD, "Boost not enabled! This requires the Boost Package\n", "");
#endif
            sampler_type = SamplerType::Boost;
        } else if ( !strcmp(samp_name(), "c11") ) {
            DEBUG("", WORLD, "The C11 random number generetor will be used.\n", "");
            WARNING("", WORLD, "Not yet implemented! This requires the std C11\n", "");
            // TODO
            sampler_type = SamplerType::C11;
#if !CXX11_ENABLE
            WARNING("", WORLD, "CXX11 not enabled! This requires the C++11 standard.\n", "");
#endif
        }

        switch(sampler_type) {
        case SamplerType::Gauss:
            quad_type = QuadratureType::Deterministic;
            WARNING("", WORLD, "Seems like you are using a quadrature, this is experimental.\n", "");
            if( sampler_type != SamplerType::Gauss) {
                WARNING("", WORLD, "Seems like you want to use a quadrature rule without gauss "
                                   "sampler, this is only currently working with that.\n", "");
            }
            break;
        default:
            quad_type = QuadratureType::Stochastic;
            break;
        }

        // Get number of refinements
        PetscOptionsInt("-ref", "Number of refinements.", "", num_refinements, &num_refinements, &flag);
        if( !help && !flag ) {
            OUTPUT("", WORLD, "\"-refinements\" option not found, falling back to default, refinements=%d.\n",
                   num_refinements);
        }
        // Get master seed
        PetscOptionsInt("-master-seed", "Master sid for simulations, default = -1 = srand(TIME)*PID", "",
                        master_seed, &master_seed, &flag);
        // Get number of simulations
        PetscOptionsIntArray("-sim", "Number of simulations", "",
                             simulations_array, &simulations_max, &flag);
        if( !help && !flag ) {
            simulations_max = 1;
            const_sim_number = PETSC_TRUE;
            simulations_array[0] = 1;
            OUTPUT("", WORLD, "\"-sim\" option not found, falling back to default.\n", "");
        } else if( !help && simulations_max == 1 ) {
            const_sim_number = PETSC_TRUE;
        } else if( !help && simulations_max != num_refinements ) {
            WARNING("", WORLD, "invalid \"-sim\" option (must be a number or an array of"
                               "length '-refinements'')\n", "");
        }
        PetscOptionsString("-int", "Choose the type of integrator ", "", temp_str, temp_str,
                           PETSC_MAX_PATH_LEN, &flag);
        if( !flag || !strcmp(temp_str, "serial") ) {
            integrator_type = IntegratorType::Serial;
            WARNING("", WORLD, "This feature is currently unavailable!\n", "");
        } else if ( !strcmp(temp_str, "parallel") ) {
            integrator_type = IntegratorType::Parallel;
        } else {
            integrator_type = IntegratorType::Serial;
            WARNING("", WORLD, "This feature is currently unavailable!\n", "");
        }

        // Get number of nodes per grid (for Gauss)
        //        PetscOptionsIntArray("-grid", "Number of nodes per dimension (in quadratures)", "",
        //                             points_array, &no_points, &flag);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// MLMC Options
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "mlmc_", "MlMc options", "mlmc"); CHKERRQ(ierr);
    {
        PetscOptionsInt("-levels", "Number of levels for MlMc.", "", mlmc_levels, &mlmc_levels, &flag);
        if( QuadratureType::Deterministic == quad_type && mlmc_levels > 1) {
            WARNING("", WORLD, "Seems you want to use multiple levels with deterministic quadrature, "
                    "this is currently unavailable!\n", "");
        }
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Solution parameters, like final time, solver type, mesh and model
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, PETSC_NULL, "Parameters", "parameters"); CHKERRQ(ierr);
    {
        // Get lambda option (CFL)
        PetscOptionsReal("-lambda", "Set CFL number.", "", lambda, &lambda, &flag);
        if( !help && !flag ) {
            WARNING("", WORLD, "\"-lambda\" option not found, falling back to default, lambda = %f\n", lambda);
        }
        // Get final time options
        PetscOptionsReal("-T", "Final time for the simulation.", "", T, &T, &flag);
        if( !help && !flag ) {
            WARNING("", WORLD, "\"-final-time\" option not found, falling back to default, T = %f\n", T);
        }
        // Get max num itr (if any)
        PetscOptionsInt("-itr", "Maximal number of iterations.", "", max_itr, &max_itr, &flag);
        if( !help && !flag ) {

        }
        // Get domain info
        PetscOptionsRealArray("-dom", "Select domain for the simulation.", "",
                              domain_array, &domain_max, &dom_provided);
        if( !help && !dom_provided ) {
            WARNING("", WORLD, "\"-domain\" option not found, falling back to default, dom = [0,1]^d\n", "");
        } else if( !help
                   && ( ( enable_3d && DOMAIN_MAX != domain_max )
                        || ( !enable_3d && DOMAIN_MAX - 2 != domain_max ) ) ) {
            WARNING("", WORLD, "invalid \"-domain\" option (must be of the form -mesh L,R,B,T), "
                               "falling back to default, domain = [0,1]^3\n", "");
        }
        // Get mesh sizes
        PetscOptionsIntArray("-mesh", "Specify dimension of coarsest mesh.", "", mesh_opt.data, &mesh_max, &flag);
        if( !help && !flag ) {
            WARNING("", WORLD, "\"-mesh\" option not found, falling back to default, mesh = 32x32\n", "");
        } else if( !help
                   && ( ( enable_3d && MESH_MAX != mesh_max )
                        ||  ( !enable_3d && MESH_MAX - 1 != mesh_max ) ) ) {
            WARNING("", WORLD, "invalid \"-mesh\" option (must be of the form -mesh Nx,Ny), falling back to"
                               "default, mesh = 32x32x32\n", "");
        }
        // Coordinates type
        PetscOptionsString("-coords", "Select coordinate type", "",
                           temp_str, temp_str, PETSC_MAX_PATH_LEN, &flag);
        if( flag ) {
            coord_type = CoordType::Uniform;
            if( !strcmp(temp_str, "non_uniform") ) {
                coord_type = CoordType::NonUniform;
            }
        }
        // Choose model
        // TODO: print all available initial data
        PetscOptionsStringArray("-model", "Select initial data", "", paramv, &paramc, &flag);
        if( !help && !flag ) {
            WARNING("", WORLD, "You must (should) specify a model! Nevermind, I'll come up with something...\n", "");
        }
        // Choose BC
        PetscOptionsStringArray("-bd", "Specify boundary conditions", "",
                                boundaries_array, &bdries_max, &boundaries_provided);
        if( !help
                && ( !flag or ( enable_3d && bdries_max != BDRIES_MAX )
                     or ( !enable_3d && bdries_max != BDRIES_MAX - 2 ) ) )  {
            WARNING("", WORLD, "No boundary conditions specified or wrong number "
                               "(must be '-bd L,R,B,T[,D,U]'), falling back to periodic...\n", "");
        }

    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// General solver types
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "", "Choose general solvers", "solver"); CHKERRQ(ierr);
    {
        // Choose solver
        PetscOptionsString("-solver", "Solver type", "", solver_name, solver_name, PETSC_MAX_PATH_LEN, &flag);
        if( !help && !flag ) {
            DEBUG("", WORLD, "No solver selected, falling back to default 'projection BCG'\n", "");
        }
        if( !strcmp(solver_name, "vortex") ) {
            solver = SolverType::Vort;
        } else {
            solver = SolverType::Proj;
        }
        PetscOptionsString("-use_stab", "Enable stable scheme (+ backward to use backward 1st order).",
                           "", temp_str, temp_str, PETSC_MAX_PATH_LEN, &flag);
        if( flag ) {
            if( !strcmp(temp_str, "backward") ) {
                ts_type = TimeSteppingType::BackwardStable;
            } else {
                ts_type = TimeSteppingType::CrankNicolson;
            }
        }
        PetscOptionsString("-ts_type", "Choose time stepping type for Solver.",
                           "", temp_str, temp_str, PETSC_MAX_PATH_LEN, &flag);
        if( flag && !strcmp(temp_str, "lf") ) {
            ts_type = TimeSteppingType::LeapFrog;
            if( solver == SolverType::Vort) {
                ERROR("", WORLD, "No LeapFrog integration with vorticity formulation!\n", "");
            }
            if( adaptive || force_snapshot_times ) {
                ERROR("", WORLD, "No adaptive/forced steps with LeapFrog integration!\n", "");
            }
        }

        PetscOptionsString("-use_diff", "Enable numerical diffusion.", "",
                           temp_str, temp_str, PETSC_MAX_PATH_LEN, &enable_numerical_diff);
        if( enable_numerical_diff ) {
            diff_type = DiffType::Eddy;
            if( !strcmp(temp_str, "roe") ) {
                diff_type = DiffType::Roe;
            } else if( !strcmp(temp_str, "rusanov") ) {
                diff_type = DiffType::Rusanov;
            }
        }

        PetscOptionsReal("-num_visc", "Numerical viscosity parameter.", "", num_visc, &num_visc, &flag);
        // Initial data custom target
        PetscOptionsName("-force_velocity", "Force a vorticity model to use initial velocity instead.", "",
                         &force_initial_data_as_velocity);
        PetscOptionsName("-force_vorticity", "Force a velocity model to use initial vorticity instead.", "",
                         &force_initial_data_as_vorticity);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Model options
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "model", "Model options", "model"); CHKERRQ(ierr);
    {
        // Use model defaults
        PetscOptionsName("-use_defaults", "Use model defaults.", "", &model_use_defaults);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Snapshot options: how many, when, what etc...
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "save_", "Snapshots", "snapshots"); CHKERRQ(ierr);
    {
        //! Setup snapshot information
        PetscOptionsRealArray("-times", "Take snapshots of the simulation (else takes only final snapshot).", "",
                              snapshot_times, &num_snapshot, &save_times);
        for(int i = 1; i < num_snapshot; i++) {
            if( !help && i > 0 && snapshot_times[i] <= snapshot_times[i-1] ) {
                WARNING("", WORLD, "Invalid snapshots at %f (s < s+1)...\n", snapshot_times[i]);
            }
            if( !help && ( snapshot_times[i] <= 0. || snapshot_times[i]>= T ) ) {
                WARNING("", WORLD, "Invalid snapshots at %f (0 < s < T)...\n", snapshot_times[i]);
            }
        }
        //! Save fraes per second and interval
        PetscOptionsReal("-fps", "Frames per second for snapshots (overrides other snapshot options).", "",
                         snapshot_fps, &snapshot_fps, &save_fps);

        // Save initial data snapshot
        //        PetscOptionsString("-format", "Format to use to save files.", "", temp_str, temp_str,
        //                           PETSC_MAX_PATH_LEN, &flag);

        // Save initial data snapshot
        PetscOptionsName("-initial", "Snapshot initial data.", "", &save_initial);
        // Enable final snapshot
        PetscOptionsName("-final", "Snapshot final data.", "", &save_final);
        // Enable final snapshot
        PetscOptionsName("-sim", "Save individual simulations data files", "", &save_sim);
        // Enable final snapshot
        PetscOptionsName("-levels", "Save individual levels (mean/var) data files", "", &save_levels);

        //! Save (hence compute) vorticity
        PetscOptionsName("-velocity", "Save quantity velocity.", "", &save_velocity);
        //! Save (hence compute) vorticity
        PetscOptionsName("-vorticity", "Save quantity vorticity.", "", &save_vorticity);
        //! Save pressure
        PetscOptionsName("-pressure", "Save quantity vorticity.", "", &save_pressure);
        //! Save pressure
        PetscOptionsName("-ps", "Save quantity passive scalar.", "", &save_ps);
        //! Save gamma2 quandity
        PetscOptionsName("-gamma2", "Save quantity gamma2 for 3D simulations (a bit expensive if the case).", "",
                         &save_gamma2);

        //! Save (hence compute) mean
        PetscOptionsName("-mean", "Save statistics of the solution (Mean).", "", &save_mean);
        //! Save (hence compute) variance
        PetscOptionsName("-m2", "Save statistics of the solution (Var (M2)).", "", &save_m2);
        //! Save (hence compute) MlMc diff
        PetscOptionsName("-diff", "Save statistics of the solution (Level difference).", "", &save_diff);
        //! Save (hence compute) MlMc diff
        //! TODO NOT WORKING YET
        PetscOptionsName("-int", "Save statistics of the solution (integral).", "", &save_int);
        //! Interval of snapshot to push to mav
        PetscOptionsInt("-push-to-mav", "Interval of snapshots to push to mav.",
                         "", push_snapshot_to_mav, &push_snapshot_to_mav, &flag);
        if(push_snapshot_to_mav != 0 && !save_mean and !save_m2 and !save_diff and !save_int) {
            WARNING("", WORLD, "Interval for push to mav was %d, but changed to 0, because "
                               "no statistic would've been computed.\n",
                    push_snapshot_to_mav);
            push_snapshot_to_mav = 0;
        }
        if(simulations_array[0] == 1 and (save_mean or save_m2 or save_diff or save_int)) {
            WARNING("", WORLD, "Save mean of a single deterministic simulation? Whoot?\n",
                    "");
        }

        //! Save
        PetscOptionsRealArray("-hist", "Save histograms.", "", hist_array, &hist_size, &save_hist);
        int dim = enable_3d ? 3 : 2;
        if( save_hist && hist_size % dim != 0 ) {
            WARNING("", WORLD, "Seems you provided an bad number (%d)"
                               "of hisogram data, ignoring last one!\n",
                    hist_size);
        }
        if( save_hist ) {
            if(hist_size < dim) {
                save_hist = PETSC_FALSE;
                WARNING("", WORLD, "Insufficient number (%d) of hisogram data. \n",
                        hist_size);
            }
            hist_size /= dim;
            hist_data.reserve(hist_size);
            for(int i = 0; i < hist_size; ++i) {
                if( hist_array[dim*i] < domain_array[0] || hist_array[dim*i] > domain_array[1]
                || hist_array[dim*i+1] < domain_array[2] || hist_array[dim*i+1] > domain_array[3]
                || (enable_3d && (hist_array[dim*i+2] < domain_array[4] || hist_array[dim*i+2] > domain_array[5]) ) ) {
                    WARNING("", WORLD, "Coordinate external to domain... Skipping.\n", "");

                continue;
                    }
                    Field3D field;
                    field[0] = hist_array[dim*i];
                    field[1] = hist_array[dim*i+1];
                    field[2] = enable_3d ? hist_array[dim*i+2] : 0;
                    hist_data.push_back(field);
            }
        }

        //! Save (hence compute) energy
        PetscOptionsName("-energy", "Save energy (L_2 norm) (a bit expensive if the case).", "", &save_energy);

        //! Do not save anything on the disk
        PetscOptionsName("-nothing", "Save nothing.", "", &save_nothing);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    ///Navier stokes options
    /////////////////////////////////////////////////
    // TODO: same dt, etc in the code
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "ns_", "Naviers-Stokes (viscosity) opt.", "navierstokes"); CHKERRQ(ierr);
    {
        // Reynolds number
        // TODO: implement for proj
        PetscOptionsReal("-re","Specify Reynold number." ,"", re , &re, &flag);
        PetscOptionsReal("-eps","Specify viscosity." ,"", nu, &nu, &eps_flag);
        if( !help && flag ) {
            nu = 1 / re;
            WARNING("", WORLD, "\"-re\" is set! Using Navier-Stokes... This is experimental.\n", "");
            use_euler = PETSC_FALSE;
            if( eps_flag ) {
                WARNING("", WORLD, "\"-eps\" and  \"-re\" are both set!"
                                   "This is bad. Ignoring \"-eps\"...\n", "");
            }
        } else {
            if( !eps_flag ) {
                nu = 0.;
                use_euler = PETSC_TRUE;
            } else {
                WARNING("", WORLD, "\"-eps\" is set! Using Navier-Stokes... This is experimental.\n", "");
                use_euler = PETSC_FALSE;
            }
        }
        if( !help && use_euler ) {
            WARNING("", WORLD, "\"-re\" is *NOT* set! Using Euler...\n", "");
        }
        PetscOptionsName("-explicit","Use explicit diffusion." ,"", &use_explicit);
        if(use_explicit and not use_euler) {
            WARNING("", WORLD,
                    "Using explicit viscosity treatment for nu = %f: make sure that the CFL condition is enforced! (lambda = %f.)\n",
                    nu, lambda);
            WARNING("", WORLD,
                    "Explicit treatment of Navier-Stokes may not be implemented/enforce for all types of evolution.\n",
                    nu, lambda);
        }
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the projection
    /////////////////////////////////////////////////
    // use MG instead of PREONLY LU
    // use FFTW instead of laplacian etc...
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "proj_", "Projection options", "projection"); CHKERRQ(ierr);
    {
        // Setup solver options
        PetscOptionsName("-use_fft", "Enable FFT for solution of poisson equation.", "", &enable_fft);
        PetscOptionsName("-oldksp", "Disable MG (or other set compute operator) as solver for poisson equation.", "",
                         &old_ksp_handling_);
        old_ksp_handling_ = (PetscBool) !old_ksp_handling_;
        PetscOptionsName("-exact", "use an exact, decoupled Laplacian projection.", "", &flag);
        if( flag ) {
            proj_type = ProjectionType::ExactLaplacian;
        }
        PetscOptionsName("-initial_cleansing", "Do a data divergence cleansing for the initial data.",
                         "WARNING: as of 3.5.0, with mg and divergence free ID, this should not be used.",
                         &do_initial_cleansing);
        if( !help && old_ksp_handling_ && do_initial_cleansing ) {
            WARNING("", WORLD, "Seems you want to use an iterative solver and a initial div cleansing.\n",
                    "");
            WARNING("", WORLD, "As of 3.5.0, due to a bug in Petsc, if you intend to use -pc_type mg, "
                               "make sure that your initial data is not divergence-free.\n", "");
            WARNING("", WORLD, "Othw. you will be using a zero rhs which causes incorrect behaviour!\n", "");
        }
        if( !help && !do_initial_cleansing ) {
            WARNING("", WORLD, "Initial cleansing is off, make sure that's what you want "
                               "(may cause problems with non solenoidal ID!\n", "");
        }
        // TODO also set number of iterates
        PetscOptionsName("-initial_iterates", "Compute some iterates for the pressure in the projection method.", "",
                         &do_initial_iterates);
        if( old_ksp_handling_ && do_initial_cleansing ) {
            WARNING("", WORLD, "Using initial iterates is not yet implemented.\n", "");
        }
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the projection solver
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "vp_", "Projection solver options", "velocitypressure"); CHKERRQ(ierr);
    {
        // Setup solver options
        PetscOptionsName("-use_almgren", "Enable FFT for solution of poisson equation.", "", &flag);
        if( flag ) {
            if( enable_3d ) {
                WARNING("", WORLD, "Incompatible options Almgren + 3D...\n", "");
            }
            conv_type = ConvectionType::Almgren;
        }

        PetscOptionsName("-direct_pressure", "Use an direct pressure as update.", "", &flag);
        if( flag ) { // Non increment pressure
            pressupd_type = PressureUpdateType::DirectPressure;
        } else { // Default, increment pressure
            pressupd_type = PressureUpdateType::IncrementalPressure;
        }
        PetscOptionsName("-proj_velocity", "Project velocity instead of acceleration.", "", &flag);
        if( flag ) { // Project velocity
            projupd_type =  ProjectionUpdateType::VelocityProjection;
        } else { // Project acceleration
            projupd_type =  ProjectionUpdateType::AccelerationProjection;
        }
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the vortex solver
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "vort_",
                             "Vortex solver options", "vortex");
            CHKERRQ(ierr);
    {
        // Setup solver options
        PetscOptionsName("-ftcs", "Enable FTCS for vortex solver.", "", &enable_ftcs);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the tracer advection
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "ps_", "Transport", "transport"); CHKERRQ(ierr);
    {
        // Turn on/off transport
        PetscOptionsName("-tracer", "Compute passive scalar (turn off for efficiency).", "", &do_transport); CHKERRQ(ierr);
        // Other parameters (transport)
        PetscOptionsReal("-viscosity", "Viscosity for transport of passive scalar.", "", transport_viscosity, &transport_viscosity, &flag);
        PetscOptionsReal("-porosity", "Porosity for transport of passive scalar.", "", transport_porosity, &transport_porosity, &flag);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// Options for the tracer advection
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "alloc_", "Allocator", "allocator"); CHKERRQ(ierr);
    {
        PetscOptionsString("-type", "Select type of allocator.",
                           "", temp_str, temp_str, PETSC_MAX_PATH_LEN, &flag);
        if( flag &&  !strcmp(temp_str, "static") )  alloc_type = AllocatorType::Static;

        PetscInt count = MAX_MLMC_LEVELS;
        PetscOptionsIntArray("-rank_per_lvl", "Number of ranks of a single simulation (not of a whole level simulation)"
                             "for given levels.", "",
                             ranks_per_level,  &count, &flag);
        if( alloc_type == AllocatorType::Static && ( !flag || count < mlmc_levels ) ) {
            ERROR("", WORLD, "Insufficient length of ranks per level given (%d), since you want %d "
                             "levels.\n",
                  count, mlmc_levels);
        }
        PetscOptionsIntArray("-parallel_sims", "Number of parallel simulations for given levels (rks of a lvl = "
                             "parallel sims * ranks per lvl).", "",
                             parallel_sims, &count, &flag);
        if( alloc_type == AllocatorType::Static && ( !flag || count < mlmc_levels ) ) {
            ERROR("", WORLD, "Insufficient length of parallel sims given (%d), since you want %d levels.\n",
                  count, mlmc_levels);
        }
        PetscOptionsIntArray("-sim_per_level", "Number of simulations for given levels (in total).", "",
                             sim_per_level, &count, &flag);
        if( alloc_type == AllocatorType::Static && ( !flag || count < mlmc_levels ) ) {
            ERROR("", WORLD, "Insufficient length of sim per level given (%d), since you want %d levels.\n",
                  count, mlmc_levels);
        }
        PetscOptionsInt("-base_work", "Cost of one deterministic sim at coarsest mesh.", ""
                        , base_work, &base_work, &flag);
        PetscOptionsInt("-max_dof_per_rank", "Max number of dof coexisting in one rank.", ""
                        , max_dof_per_rank, &max_dof_per_rank, &flag);
        PetscOptionsReal("-parallel_portion", "Parallel portion of a deterministic simulation.", ""
                         , det_parallel_portion, &det_parallel_portion, &flag);
        alloc_size = 1;
        PetscOptionsInt("-only", "Just go as far as computing allocation and stuff.", "", alloc_size, &alloc_size,
                        &alloc_only); CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    /////////////////////////////////////////////////
    /// General solver types
    /////////////////////////////////////////////////
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "", "Choose/Set general flags/settings", "flags"); CHKERRQ(ierr);
    {
        PetscOptionsName("-nested_mg", "Automatically does MG+GAMG when possible.", "", &nested_mg); CHKERRQ(ierr);
        PetscOptionsName("-auto_lu", "Automatically does LU when possible.", "", &auto_lu); CHKERRQ(ierr);
        PetscOptionsName("-auto_mg_levels",
                         "Automatically find MG levels when enabled.", "", &auto_mg_levels); CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    if( help ) {
        PetscFinalize();
        exit(0);
        return 0;
    }

    // Do some mangling
    if( !enable_3d ) mesh_opt[2] = 1;

    PetscFunctionReturn(ierr);
}
