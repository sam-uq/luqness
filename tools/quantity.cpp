/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "../tools/quantity.h"

//! Construct and allocate memory for vector as needed.
//! - Vector for U (velocity) is allocated if using projection solver *or* if velocity is requested
//! - Vector for Gp (pressure) is allocated if using projection *and* it's explicitly requested
//! - Vector for omega (vorticity) is allocate if using vortex solver *or* if vorticity is requested
//! - Vector for transport is allocated if transport is enabled
//! - Vector for gamma2 is allocated if requested
//! @param opt_ Option preallocated instance used to grab data to decide what needs to be allocated.
//! @param dom_ Domain instance toi get access to DAs and get size informations.
//! @param name_ Name to be given to the quantity, used to save the data accordingly.
//! @param preallocate_ actually allcoates memory for the data (and initialize to 0).
Quantity::Quantity(Options *opt_, Domain *dom_, Attila *p_cm, ModelBase *p_mod, std::string name_, bool preallocate_)
    : num_adds(0), sum_weights(0), p_opt_(opt_), p_dom_(dom_), p_cm_(p_cm), p_mod_(p_mod)
    , name(name_), preallocate(preallocate_) {
    FUNCTIONBEGIN(p_cm_);

    phase_dim = p_opt_->enable_3d ? 3 : 2;

    dof_ = 0;
    components_ = CompType::None;

    if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
        components_ |= CompType::U;
        dof_ += phase_dim;
    }

    if( p_opt_->solver == SolverType::Proj ) {
        components_ |= CompType::Gp;
        dof_ += phase_dim;
    }

    if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
        components_ |= CompType::omega;
        ++dof_;
    }

    if( p_opt_->do_transport ) {
        components_ |= CompType::ps;
        ++dof_;
    }

    if( p_opt_->save_gamma2 ) {
        components_ |= CompType::gamma2;
        ++dof_;
    }


    OUTPUT(p_cm_, INTRADM, "Created quantity with name \"%s\" and with component signature %d"
                           " (dof=%d)\n", name.c_str(), components_, dof_);
    DEBUG(p_cm_, INTRADM, "Created quantity with name \"%s\" and with component signature %d"
                          " (dof=%d)\n", name.c_str(), components_, dof_);

    if(preallocate) {
        if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
            ierr = DMCreateGlobalVector(p_dom_->da, &U); CHKERRV(ierr);
            ierr = VecSet(U, 0); CHKERRV(ierr);
        }
        if( p_opt_->solver == SolverType::Proj ) {
            ierr = DMCreateGlobalVector(p_dom_->da, &Gp); CHKERRV(ierr);
            ierr = VecSet(Gp, 0); CHKERRV(ierr);
        }
        if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
            if( p_opt_->enable_3d ) {
                ierr = DMCreateGlobalVector(p_dom_->da, &omega); CHKERRV(ierr);
                ierr = VecSet(omega, 0); CHKERRV(ierr);
            } else {
                ierr = DMCreateGlobalVector(p_dom_->da_sc, &omega); CHKERRV(ierr);
                ierr = VecSet(omega, 0); CHKERRV(ierr);
            }
        }
        if( p_opt_->do_transport ) {
            ierr = DMCreateGlobalVector(p_dom_->da_sc, &ps); CHKERRV(ierr);
            ierr = VecSet(ps, 0); CHKERRV(ierr);
        }
        if( p_opt_->save_gamma2 ) {
            ierr = DMCreateGlobalVector(p_dom_->da_sc, &gamma2); CHKERRV(ierr);
            ierr = VecSet(gamma2, 0); CHKERRV(ierr);
        }
    }
}

//! Destroys vectors compatibly with the allocation made in the constructor.
//!
Quantity::~Quantity() {
    if(preallocate) {
        if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
            ierr = VecDestroy(&U); CHKERRV(ierr);
        }
        if( p_opt_->solver == SolverType::Proj ) {
            ierr = VecDestroy(&Gp); CHKERRV(ierr);
        }
        if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
            ierr = VecDestroy(&omega); CHKERRV(ierr);
        }
        if( p_opt_->do_transport ) {
            ierr = VecDestroy(&ps); CHKERRV(ierr);
        }
        if( p_opt_->save_gamma2 ) {
            ierr = VecDestroy(&gamma2); CHKERRV(ierr);
        }
    }
}

#if META == 1

//!
//!
PetscErrorCode Quantity::update_meta(void) {

    //? TODO for now unavailable
    //     time = dom.t;
    //? TODO for now unavailable
    //     frame = ;

    mesh = p_dom_->mesh_size;
    time = 0;
    frame = 0;

    PetscFunctionReturn(ierr);

}

#endif // META

//! Save all the data that has been alloocated to (separate) files for each component.  The option "save_nothing"
//! overrides everything in this function.
PetscErrorCode Quantity::save_all(std::string name_) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->save_nothing ) {
        PetscFunctionReturn(ierr);
    }

    p_profiler->begin_event("save_all");

#if META == 1
    ierr = update_meta(); CHKERRQ(ierr);
#endif // META

    if( name_ != "" ) {
        name = name_;
    }

    if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
        ierr = save_vec(U, "U", p_opt_->enable_3d ? 3 : 2); CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
        ierr = save_vec(Gp, "Gp", p_opt_->enable_3d ? 3 : 2); CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
        ierr = save_vec(omega, "w", p_opt_->enable_3d ? 3 : 1); CHKERRQ(ierr);
    }

    if( p_opt_->do_transport && p_opt_->save_ps ) {
        ierr = save_vec(ps, "ps", 1); CHKERRQ(ierr);
    }

    if( p_opt_->save_gamma2 ) {
        ierr = save_vec(gamma2, "gamma2", 1); CHKERRQ(ierr);
    }

    p_profiler->end_event("save_all");

    PetscFunctionReturn(ierr);
}

//! Call's VecView for the appropriate viewer and for the requested vector giving the correct component name.
//! File format is made in the following way:
//! [name]_[mesh]_[component]_[quantity].[ext]
//! - [name]: name given to the simulation;
//! - [mesh]: x-compoennt of the mesh;
//! - [component]: compoenent name, e.g. "U" for velocity or "gamma2" for gamma_2;
//! - [quantity]:name given to this quantity (in the constructor), e.g. "mean" or "sim0_f5" (f* stand for "frame to be
//!   used with "paraview");
//! - [ext]: extension, currently ".vts" for VtK output or ".bin" for binary.
//! TODO: use "runtime" choosing of file format.
//! @param[in] V vector with data to be saved.
//! @param[in] compname name of the component that is hanbled in \param V
PetscErrorCode Quantity::save_vec(Vec V, std::string compname, uint dof) {
    PetscViewer   viewer;
    char complete_file_name[PETSC_MAX_PATH_LEN];
    char file_name[PETSC_MAX_PATH_LEN];
    sprintf(file_name, "%s_%s_%s_%s", p_opt_->simulation_name, p_dom_->mesh_str.c_str(), compname.c_str(), name.c_str());
#ifdef VTS_OUT
    // NEW NAMING CONVENTION
    sprintf(complete_file_name, "%s.vts", file_name);
    OUTPUT(p_cm_, INTRADM, "Exporting to \"%s\"...\n", complete_file_name);
    ierr = PetscViewerVTKOpen(p_cm_->intra_domain_comm, complete_file_name, FILE_MODE_WRITE, &viewer); CHKERRQ(ierr);
    ierr = VecView(V, viewer); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
    ierr = p_profiler->count("save_vec_vts"); CHKERRQ(ierr);
    #endif // VTS_OUT
#ifdef HDF_OUT
    // NEW NAMING CONVENTION
    sprintf(complete_file_name, "%s.hdf5", file_name);
    OUTPUT(p_cm_, INTRADM, "Exporting to \"%s\"...\n", complete_file_name);
    ierr = PetscViewerHDF5Open(p_cm_->intra_domain_comm, complete_file_name, FILE_MODE_WRITE, &viewer); CHKERRQ(ierr);
    ierr = VecView(V, viewer); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
    ierr = p_profiler->count("save_vec_vts"); CHKERRQ(ierr);
#endif // HDF_OUT
#ifdef BIN_OUT
    sprintf(complete_file_name, "%s.bin", file_name);
    OUTPUT(p_cm_, INTRADM, "Exporting to \"%s\"...\n", complete_file_name);
    ierr = PetscViewerBinaryOpen(p_cm_->intra_domain_comm, complete_file_name, FILE_MODE_WRITE, &viewer); CHKERRQ(ierr);
    ierr = VecView(V, viewer); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
    ierr = p_profiler->count("save_vec_bin"); CHKERRQ(ierr);
#endif // BIN_OUT
#ifdef DRAW_OUT
    ierr = PetscViewerDrawOpen(p_cm_->intra_domain_comm, NULL,NULL,0,0,300,300,&viewer); CHKERRQ(ierr);
    OUTPUT(p_cm_, INTRADM, "Drawing vector\n", "");
    ierr = VecView(V, viewer); CHKERRQ(ierr); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);
    ierr = p_profiler->count("save_vec_draw"); CHKERRQ(ierr);
#endif // DRAW_OUT
#if META == 1
    DEBUG(p_cm_, INTRADM, "Metadata vector.\n", "");

    time = p_mod_->t;

    std::stringstream metadata_sstream;
    metadata_sstream << "name=\"" << p_opt_->simulation_name << "\"" << std::endl;
    metadata_sstream << "dof" << META_SEPARATOR << dof << std::endl;
    metadata_sstream << "size=\"" << p_dom_->mesh_str.c_str() << "\"" << std::endl;
    metadata_sstream << "frame" << META_SEPARATOR << frame << std::endl;
    metadata_sstream << "time" << META_SEPARATOR << time << std::endl;
    metadata_sstream << "component=\"" << compname << "\"" << std::endl;
    metadata_sstream << "type=\"" << name << "\"" << std::endl;
    metadata_sstream << "additions" << META_SEPARATOR << num_adds << std::endl;
    metadata_sstream << "sum_weights" << META_SEPARATOR << sum_weights << std::endl;
    metadata_sstream << "is3d" << META_SEPARATOR << p_opt_->enable_3d << std::endl;
    metadata_sstream << "domain" << META_SEPARATOR
    << "((" << p_opt_->domain_array[0] << "," << p_opt_->domain_array[1] << "),"
    << " (" << p_opt_->domain_array[2] << "," << p_opt_->domain_array[3] << "),"
    << " (" << p_opt_->domain_array[4] << "," << p_opt_->domain_array[5] << "))" << std::endl;


    if(p_cm_->intra_domain_rank == 0) {
        sprintf(complete_file_name, "%s.py", file_name);
        FILE *f = fopen(complete_file_name, "w");
        if(f == NULL) {
            ERROR(p_cm_, INTRADM,
                  "Failure while opening MUFFALO file for 'w'.\n", "");
        }
        ierr = PetscFPrintf(p_cm_->intra_domain_comm, f,
                            metadata_sstream.str().c_str() );
                            CHKERRQ(ierr);
        int err = fclose(f);
        if(err != 0) {
            ERROR(p_cm_, INTRADM,
                  "Failure while writing MUFFALO to file.\n", "");
        }
    }
#endif

    PetscFunctionReturn(ierr);
}

//! Global online variacne algorithm implementation, does two thing:
//!  - increments the number of additions performed so far
//!  - callsatt_to_mav on each component as needed
//! Contrary to add_as_quad this class adds *this* to mean and variance
PetscErrorCode Quantity::add_to_mav(Quantity * mean, Quantity * variance) {
    FUNCTIONBEGIN(p_cm_);

    // Add num num_adds
    ++mean->num_adds;
    ++variance->num_adds;

    if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
        ierr = add_to_mav<false>(p_dom_->da, U, mean->U, variance->U, mean->num_adds);  CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
        ierr = add_to_mav<false>(p_dom_->da, Gp, mean->Gp, variance->Gp, mean->num_adds);  CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
        if( p_opt_->enable_3d ) {
            ierr = add_to_mav<false>(p_dom_->da, omega, mean->omega, variance->omega, mean->num_adds);  CHKERRQ(ierr);
        } else {
            ierr = add_to_mav<true>(p_dom_->da_sc, omega, mean->omega, variance->omega, mean->num_adds);  CHKERRQ(ierr);
        }
    }

    if( p_opt_->do_transport && p_opt_->save_ps ) {
        ierr = add_to_mav<true>(p_dom_->da_sc, ps, mean->ps, variance->ps, mean->num_adds);  CHKERRQ(ierr);
    }

    if( p_opt_->save_gamma2 ) {
        ierr = add_to_mav<true>(p_dom_->da_sc, gamma2, mean->gamma2, variance->gamma2, mean->num_adds);  CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

//! Normalize the quantity as if it is "m2" (in online variance algorithm add_to_mav) dividing by the number of
//! additions performed, this results in the variance.
//!
PetscErrorCode Quantity::normalize_as_var(int ddof) {
    FUNCTIONBEGIN(p_cm_);

    if( num_adds > ddof ) {
        if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
            ierr = VecScale(U, 1. / (PetscReal) (num_adds - ddof)); CHKERRQ(ierr);
        }

        if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
            ierr = VecScale(Gp, 1. / (PetscReal) (num_adds - ddof)); CHKERRQ(ierr);
        }

        if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
            ierr = VecScale(omega, 1. / (PetscReal) (num_adds - ddof)); CHKERRQ(ierr);
        }

        if( p_opt_->do_transport && p_opt_->save_ps ) {
            ierr = VecScale(ps, 1. / (PetscReal) (num_adds - ddof)); CHKERRQ(ierr);
        }

        if( p_opt_->save_gamma2 ) {
            ierr = VecScale(gamma2, 1. / (PetscReal) (num_adds - ddof)); CHKERRQ(ierr);
        }
    } else {
        if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
            ierr = VecSet(U, 0); CHKERRQ(ierr);
        }

        if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
            ierr = VecSet(Gp, 0); CHKERRQ(ierr);
        }

        if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
            ierr = VecSet(omega, 0); CHKERRQ(ierr);
        }

        if( p_opt_->do_transport && p_opt_->save_ps ) {
            ierr = VecSet(ps, 0); CHKERRQ(ierr);
        }

        if( p_opt_->save_gamma2 ) {
            ierr = VecSet(gamma2, 0); CHKERRQ(ierr);
        }
    }

    PetscFunctionReturn(ierr);
}

//! Add the value of  *this* to ing (not viceversa) as a (weighted) quadrature rule; also increments
//! the number of additions done by this quantity (and not from qt).
PetscErrorCode Quantity::add_as_quad(double weight, Quantity * intg) {
    FUNCTIONBEGIN(p_cm_);

    // Increment number of additions
    ++intg->num_adds;

    intg->sum_weights += weight;

    if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
        ierr = VecAXPY(intg->U, weight, U); CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
        ierr = VecAXPY(intg->Gp, weight, Gp); CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
        ierr = VecAXPY(intg->omega, weight, omega); CHKERRQ(ierr);
    }

    if( p_opt_->do_transport && p_opt_->save_ps ) {
        ierr = VecAXPY(intg->ps, weight, ps); CHKERRQ(ierr);
    }

    if( p_opt_->save_gamma2 ) {
        ierr = VecAXPY(intg->gamma2, weight, gamma2); CHKERRQ(ierr);
    }

    PetscFunctionReturn(ierr);
}

//! Add the value of  *this* to int (not viceversa) as a (weighted) quadrature rule; also increments
//! the number of additions done by this quantity (and not from qt).
PetscErrorCode Quantity::add_as_quad_and_var(double weight, Quantity * intg, Quantity * intg_m2) {
    FUNCTIONBEGIN(p_cm_);

    if( p_opt_->solver == SolverType::Proj || p_opt_->save_velocity ) {
        ierr = add_as_quad_and_var<false>(p_dom_->da, weight, U, intg->U, intg_m2->U,
                                          intg->num_adds, intg->sum_weights); CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Proj && p_opt_->save_pressure) {
        ierr = add_as_quad_and_var<false>(p_dom_->da, weight, Gp, intg->Gp, intg_m2->Gp,
                                          intg->num_adds, intg->sum_weights);  CHKERRQ(ierr);
    }

    if( p_opt_->solver == SolverType::Vort || p_opt_->save_vorticity ) {
        if( p_opt_->enable_3d ) {
            ierr = add_as_quad_and_var<false>(p_dom_->da, weight,
                                              omega, intg->omega, intg_m2->omega,
                                              intg->num_adds, intg->sum_weights); CHKERRQ(ierr);
        } else {
            ierr = add_as_quad_and_var<true>(p_dom_->da_sc, weight,
                                             omega, intg->omega, intg_m2->omega,
                                             intg->num_adds, intg->sum_weights); CHKERRQ(ierr);
        }
    }

    if( p_opt_->do_transport && p_opt_->save_ps ) {
        ierr = add_as_quad_and_var<true>(p_dom_->da_sc, weight,
                                         ps, intg->ps, intg_m2->ps, intg->num_adds, intg->sum_weights);  CHKERRQ(ierr);
    }

    if( p_opt_->save_gamma2 ) {
        ierr = add_as_quad_and_var<true>(p_dom_->da_sc, weight, gamma2,
                                         intg->gamma2, intg_m2->gamma2,
                                         intg->num_adds, intg->sum_weights); CHKERRQ(ierr);
    }

    // How the algorithm is done is important that this is done *after*

    // Add num num_adds
    ++intg->num_adds;
    ++intg_m2->num_adds;

    intg->sum_weights += weight;
    intg_m2->sum_weights += weight;

    PetscFunctionReturn(ierr);
}
