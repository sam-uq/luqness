include_directories(${LUQNESS_SOURCE_DIR}/tools)
link_directories(${LUQNESS_BINARY_DIR}/tools)

file(GLOB PROGRAMS *.cpp *.h)

foreach(program ${PROGRAMS})
  list(APPEND luqness_SOURCES ${program})
endforeach(program)

set(luqness_SOURCES ${luqness_SOURCES} PARENT_SCOPE)
