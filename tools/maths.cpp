/*
 * Copyright 2014 Filippo Leonardi <filippo.leonardi@sam.math.ethz.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "maths.h"

//! This routine return the second largest eigenvalue
//! of a symmetric matrix (fails completely for a NON symmetric
//! matrix).
//! This is used for the gamma2 vistualization
//! of the symmetric part of the gradient tesndo
PetscScalar eig3(PetscScalar a00, PetscScalar a11, PetscScalar a22, PetscScalar a01, PetscScalar a02, PetscScalar a12)
{
    PetscScalar inv3 = 1. / 3.;
    PetscScalar root3 = sqrt(3);

    PetscScalar c0 = a00*a11*a22 + 2.0*a01*a02*a12 - a00*a12*a12 - a11*a02*a02 - a22*a01*a01;
    PetscScalar c1 = a00*a11 - a01*a01 + a00*a22 - a02*a02 + a11*a22 - a12*a12;
    PetscScalar c2 = a00 + a11 + a22;

    PetscScalar c2Div3 = c2*inv3;
    PetscScalar aDiv3 = (c1 - c2*c2Div3)*inv3;

    if (aDiv3 > 0.0) { aDiv3 = 0.0; }

    PetscScalar mbDiv2 = 0.5*(c0 + c2Div3*(2.0*c2Div3*c2Div3 - c1));
    PetscScalar q = mbDiv2*mbDiv2 + aDiv3*aDiv3*aDiv3;

    if (q > 0.0) { q = 0.0; }

    PetscScalar magnitude = sqrt(-aDiv3);
    PetscScalar angle = atan2(sqrt(-q),mbDiv2)*inv3;

    PetscScalar cs = cos(angle);
    PetscScalar sn = sin(angle);

    PetscScalar root0 = c2Div3 + 2.0*magnitude*cs;
    PetscScalar root1 = c2Div3 - magnitude*(cs + root3*sn);
    PetscScalar root2 = c2Div3 - magnitude*(cs - root3*sn);

    if( root0 > root1 ) {
        if( root1 > root2 ) return root1;
        else {
            if( root0 > root2 ) return root2;
            else return root0;
        }
    } else {
        if( root0 > root2 ) return root0;
        else {
            if( root1 > root2 ) return root2;
            else return root1;
        }
    }
}

//! (c) XKCD
//!
int getRandomNumber()
{
    return 4; // Chosen by fair dice roll. Guaranteed to be random.
}
